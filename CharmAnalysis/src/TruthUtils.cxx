/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Greg Ottino
/// @author Miha Muskinja

#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthVertex.h>

#include <CharmAnalysis/TruthUtils.h>

namespace TruthUtils {

int chargeDecayMode(std::vector<const xAOD::TruthParticle *> children) {

  int nPi = 0;
  int nK = 0;
  int nPi0 = 0;
  int np = 0;

  for (const xAOD::TruthParticle *child : children) {
    // Don't include Kshort and Lambda decay products (which are created in the
    // simulation)
    if (child->barcode() > 20000)
      continue;
    if (abs(child->pdgId()) == 211)
      nPi++;
    else if (abs(child->pdgId()) == 321)
      nK++;
    else if (abs(child->pdgId()) == 2212)
      np++;
    else if (abs(child->pdgId()) == 111)
      nPi0++;
  }
  if (nPi == 1 && nK == 1 && np == 0)
    return 0;
  else if (nPi == 2 && nK == 1 && nPi0 == 1 && np == 0)
    return 4;
  else if (nPi == 2 && nK == 1 && np == 0)
    return 1;
  else if (nPi == 1 && nK == 2 && np == 0)
    return 2;
  else if (nPi == 4 && nK == 1 && np == 0)
    return 3;
  else if (nPi == 1 && nK == 0 && np == 0) {
    return 5;
  } else if (nPi == 0 && nK == 1 && np == 0 && nPi0 == 0) {
    return 6;
  } else if (nPi == 0 && nK == 0 && np == 1 && nPi0 == 0) {
    return 7;
  } else if (nPi == 1 && nK == 1 && np == 1 && nPi0 == 0) {
    return 8;
  } else
    return 0;
}

bool isCharmedHadron(const xAOD::TruthParticle *particle) {
  if ((abs(particle->pdgId()) == 411 ||   // D+
       abs(particle->pdgId()) == 413 ||   // D*+
       abs(particle->pdgId()) == 431 ||   // Ds
       abs(particle->pdgId()) == 421 ||   // D0
       abs(particle->pdgId()) == 4122 ||  // Lambda_C
       abs(particle->pdgId()) == 4132 ||  // Xi_C^0
       abs(particle->pdgId()) == 4232 ||  // Xi_C^+
       abs(particle->pdgId()) == 4212 ||  // Xi_C^0
       abs(particle->pdgId()) == 4322 ||  // Xi'_C+  This is in fact EM not weak
       abs(particle->pdgId()) == 4332) && // Omega_C
      particle ==
          findFinalParticle(particle)) // confirms not intermediate state
    return true;
  else
    return false;
}

bool isBottomHadron(const xAOD::TruthParticle *particle) {
  if (abs(particle->pdgId()) == 511 or  // B0
      abs(particle->pdgId()) == 521 or  // B+
      abs(particle->pdgId()) == 531 or  // B_s0
      abs(particle->pdgId()) == 541 or  // B_c+
      abs(particle->pdgId()) == 5122 or // Lambda_b0
      abs(particle->pdgId()) == 5132 or // Xi_b-
      abs(particle->pdgId()) == 5232 or // Xi_b0
      abs(particle->pdgId()) == 5332)   // Omega_b-
    return true;
  else
    return false;
}

bool fromDStar(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *p = findInitialParticle(particle);
  for (unsigned int parIter = 0; parIter < p->nParents(); parIter++) {
    const xAOD::TruthParticle *parent = p->parent(parIter);
    if (abs(parent->pdgId()) == 413) {
      return true;
    }
  }
  return false;
}

bool phiInDecayChain(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *p = findFinalParticle(particle);
  for (unsigned int i = 0; i < p->nChildren(); i++) {
    if (p->child(i)->pdgId() == 333)
      return true;
  }
  return false;
}

bool lambdaToPipInDecayChain(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *p = findFinalParticle(particle);
  std::vector<const xAOD::TruthParticle *> resultsVec =
      findStatusOneChildren(p);
  ;
  for (const xAOD::TruthParticle *child : resultsVec) {
    // Lambda or LambdaBar found
    if (abs(child->pdgId()) == 3122) {
      // Check the decay mode
      if (child->nChildren() == 2 && child->child(0) != NULL &&
          child->child(1) != NULL) {
        if (abs(child->child(0)->pdgId()) == 211 &&
            abs(child->child(1)->pdgId()) == 2212)
          return true;
        if (abs(child->child(1)->pdgId()) == 211 &&
            abs(child->child(0)->pdgId()) == 2212)
          return true;
      }
    }
  }
  return false;
}

bool lambdaInDecayChain(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *p = findFinalParticle(particle);
  std::vector<const xAOD::TruthParticle *> resultsVec =
      findStatusOneChildren(p);
  ;
  for (const xAOD::TruthParticle *child : resultsVec) {
    // Lambda or LambdaBar found
    if (abs(child->pdgId()) == 3122)
      return true;
  }
  return false;
}

bool kshortInDecayChain(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *p = findFinalParticle(particle);
  std::vector<const xAOD::TruthParticle *> resultsVec =
      findStatusOneChildren(p);
  ;
  for (const xAOD::TruthParticle *child : resultsVec) {
    if (abs(child->pdgId()) == 310)
      return true;
  }
  return false;
}

bool kshortToPiPiInDecayChain(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *p = findFinalParticle(particle);
  std::vector<const xAOD::TruthParticle *> resultsVec =
      findStatusOneChildren(p);
  ;
  for (const xAOD::TruthParticle *child : resultsVec) {
    if (abs(child->pdgId()) == 310) {
      // Check the decay mode
      if (child->nChildren() == 2 && child->child(0) != NULL &&
          child->child(1) != NULL) {
        if (abs(child->child(0)->pdgId()) == 211 &&
            abs(child->child(1)->pdgId()) == 211)
          return true;
      }
    }
  }
  return false;
}

bool fromBdecay(const xAOD::TruthParticle *particle) {
  for (unsigned int i = 0; i < particle->nParents(); i++) {
    if (particle->parent(i)->isHeavyHadron()) {
      if (particle->parent(i)->isBottomHadron() ||
          abs(particle->parent(i)->pdgId()) == 5)
        return true;
      else
        return fromBdecay(particle->parent(i));
    }
  }
  return false;
}

const xAOD::TruthParticle *getSecondaryD0(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *p = findFinalParticle(particle);
  const xAOD::TruthParticle *p_out = NULL;
  for (unsigned int i = 0; i < p->nChildren(); i++) {
    const xAOD::TruthParticle *child = p->child(i);
    if (abs(child->pdgId()) == 421) {
      p_out = child;
      if (p_out != NULL)
        return p_out;
    }
  }
  return p_out;
}

const xAOD::TruthParticle *
getSecondaryDplus(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *p = findFinalParticle(particle);
  const xAOD::TruthParticle *p_out = NULL;
  for (unsigned int i = 0; i < p->nChildren(); i++) {
    const xAOD::TruthParticle *child = p->child(i);
    if (abs(child->pdgId()) == 411) {
      p_out = child;
      if (p_out != NULL)
        return p_out;
    }
  }
  return p_out;
}

const xAOD::TruthParticle *
findFinalParticle(const xAOD::TruthParticle *particle) {
  // Check if there are daughters
  if (particle->nChildren() < 1)
    return particle;

  const xAOD::TruthParticle *finalPar = particle;
  int pdgId = particle->pdgId();
  unsigned int nullCounter = 0;
  bool isFinal = false;

  while (!isFinal) {
    isFinal = true;
    if (finalPar->nChildren() < 1)
      return finalPar;
    nullCounter = 0;
    for (unsigned int i = 0; i < finalPar->nChildren(); i++) {
      if (finalPar->child(i) == NULL && nullCounter < finalPar->nChildren()) {
        nullCounter++;
        continue;
      } else if (finalPar->child(i) == NULL &&
                 nullCounter == finalPar->nChildren()) {
        isFinal = true;
        continue;
      }
      if (abs(finalPar->child(i)->pdgId()) == abs(pdgId)) {
        finalPar = finalPar->child(i);
        isFinal = false;
      }
    }
  }

  return finalPar;
}

const xAOD::TruthParticle *
findInitialParticle(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *initialPar = particle;
  int pdgId = particle->pdgId();
  unsigned int nullCounter = 0;
  bool isInitial = false;

  while (!isInitial) {
    isInitial = true;
    if (initialPar->nParents() < 1) {
      return initialPar;
    }
    nullCounter = 0;
    for (unsigned int i = 0; i < initialPar->nParents(); i++) {
      if (initialPar->parent(i) == NULL &&
          nullCounter < initialPar->nParents()) {
        nullCounter++;
        continue;
      } else if (initialPar->parent(i) == NULL &&
                 nullCounter == initialPar->nParents()) {
        isInitial = true;
        continue;
      }
      if (initialPar->parent(i)->pdgId() == pdgId) {
        initialPar = initialPar->parent(i);
        isInitial = false;
      }
    }
  } // End while

  return initialPar;
}

std::vector<const xAOD::TruthParticle *>
findStatusOneChildren(const xAOD::TruthParticle *particle) {
  std::vector<const xAOD::TruthParticle *> resultsVec;

  if (particle == NULL) {
    return resultsVec;
  } // protect against NULL pointers

  if (particle->status() == 1) {
    resultsVec.push_back(particle);
    return resultsVec;
  } // recursive end

  for (unsigned int i = 0; i < particle->nChildren(); i++) {
    const xAOD::TruthParticle *daughter = findFinalParticle(particle->child(i));
    if (daughter == NULL) {
      continue;
    }

    if (daughter->status() == 1 && daughter->nChildren() > 0) {
      //                if(abs(daughter->pdgId())==3122)
      //                    resultsVec.push_back(daughter);
      std::vector<const xAOD::TruthParticle *> resultsVecTmp;
      resultsVecTmp = findStatusOneChildren(daughter);
      resultsVec.insert(resultsVec.end(), resultsVecTmp.begin(),
                        resultsVecTmp.end());
    } else {
      std::vector<const xAOD::TruthParticle *> resultsVecTmp;
      resultsVecTmp = findStatusOneChildren(daughter);
      resultsVec.insert(resultsVec.end(), resultsVecTmp.begin(),
                        resultsVecTmp.end());
    }
  }
  return resultsVec;
}

float cosThetaStar(const xAOD::TruthParticle *kPart,
                   const xAOD::TruthParticle *dPart) {
  float cts = 0.;
  TLorentzVector kVec, dVec;
  kVec.SetPtEtaPhiM(kPart->pt(), kPart->eta(), kPart->phi(), kPart->m());
  dVec.SetPtEtaPhiM(dPart->pt(), dPart->eta(), dPart->phi(), dPart->m());
  TVector3 boost_v0 = dVec.BoostVector();
  boost_v0 *= -1.0;
  kVec.Boost(boost_v0);
  cts = cos(dVec.Angle(kVec.Vect()));
  return cts;
}

float Lxy(const xAOD::TruthParticle *particle, const xAOD::TruthVertex *prodVtx) {
  if (!particle || !prodVtx){
    return -999.;
  }
  const xAOD::TruthParticle *pFinal = TruthUtils::findFinalParticle(particle);
  if (!pFinal || !pFinal->hasDecayVtx()){
      return -999.;
  }
  
  float dx = pFinal->decayVtx()->x() - prodVtx->x();
  float dy = pFinal->decayVtx()->y() - prodVtx->y();

  return (pFinal->px() * dx + pFinal->py() * dy) / pFinal->pt();
}

float ImpactParam(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *pInitial =
      TruthUtils::findInitialParticle(particle);
  const xAOD::TruthParticle *pFinal = TruthUtils::findFinalParticle(particle);
  if (pInitial->hasProdVtx() == false || pFinal->hasDecayVtx() == false)
    return -999.;

  float dx = pFinal->decayVtx()->x() - pInitial->prodVtx()->x();
  float dy = pFinal->decayVtx()->y() - pInitial->prodVtx()->y();

  return fabs((pFinal->px() * -1. * dy + pFinal->py() * dx) / pFinal->pt());
}

// returns D parent barcode, or if there is no D parent returns -1
int findDParentBarcode(const xAOD::TruthParticle *particle, int D0code) {
  int D0codeInternal = 0;
  const xAOD::TruthParticle *p = findInitialParticle(particle);
  if (p->nParents() != 1 && D0code < 1) {
    return 0;
  } else if (p->nParents() != 1 && D0code > 0) {
    return D0code;
  }

  const xAOD::TruthParticle *parent = p->parent(0);
  if (parent == NULL) {
    return 0;
  }

  if (abs(parent->pdgId()) == 421) {
    D0codeInternal = findFinalParticle(parent)->barcode();
    return findDParentBarcode(parent, D0codeInternal);
  } else if (isCharmedHadron(parent)) {
    return findFinalParticle(parent)->barcode();
  } else if (D0code > 1) {
    return D0code;
  } else if (abs(parent->pdgId()) < 10) {
    return 0;
  } else {
    return findDParentBarcode(parent, 0);
  }
}

const xAOD::TruthParticle *
findGenericParent(const xAOD::TruthParticle *particle) {
  const xAOD::TruthParticle *p = findInitialParticle(particle);
  if (p->nParents() != 1) {
    return 0;
  }

  const xAOD::TruthParticle *parent = p->parent(0);
  if (parent == NULL) {
    return 0;
  }

  return parent;
}

int GetTruthBarcode(std::vector<const xAOD::TrackParticle *> &daughterTracks,
                    std::vector<int> &daughterDCodes,
                    std::vector<int> &daughterParentTruthPdgId,
                    std::vector<int> &daughterCodes,
                    std::vector<int> &daughterTruthPdgId) {
  daughterDCodes.clear();
  daughterCodes.clear();
  daughterParentTruthPdgId.clear();
  daughterTruthPdgId.clear();
  for (unsigned int i = 0; i < daughterTracks.size(); i++) {
    ElementLink<xAOD::TruthParticleContainer> truthLink =
        daughterTracks[i]->auxdata<ElementLink<xAOD::TruthParticleContainer>>(
            "truthParticleLink");
    if (truthLink.isValid()) {
      int tmp = findDParentBarcode((*truthLink), 0);
      if (tmp > 1) {
        daughterDCodes.push_back(tmp);
        daughterParentTruthPdgId.push_back(0);
      } else {
        const xAOD::TruthParticle *particle_tmp =
            findGenericParent((*truthLink));
        if (particle_tmp) {
          daughterDCodes.push_back(particle_tmp->barcode());
          daughterParentTruthPdgId.push_back(particle_tmp->pdgId());
        } else {
          daughterDCodes.push_back(-1);
          daughterParentTruthPdgId.push_back(-1);
        }
      }
      daughterCodes.push_back(findFinalParticle(*truthLink)->barcode());
      daughterTruthPdgId.push_back(findFinalParticle(*truthLink)->pdgId());
    } else {

      daughterDCodes.push_back(-1);
      daughterCodes.push_back(-1);
      daughterTruthPdgId.push_back(-1);
      daughterParentTruthPdgId.push_back(-1);
    }
  }
  // check if all particles have a truth link and they are all the same
  if (std::adjacent_find(daughterDCodes.begin(), daughterDCodes.end(),
                         std::not_equal_to<>()) == daughterDCodes.end() &&
      daughterDCodes.size() == daughterTracks.size()) {
    return daughterDCodes[0];
  } else {
    return 0;
  }
}

} // namespace TruthUtils
