/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <CharmAnalysis/TrackJetDecoratorAlg.h>
#include <InDetTrackSystematicsTools/InDetTrackSmearingTool.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetConstituentVector.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <xAODTracking/VertexContainer.h>

//
// method implementations
//

TrackJetDecoratorAlg ::TrackJetDecoratorAlg(const std::string &name,
                                            ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      m_trackToVertexIPEstimator(
          "Trk::TrackToVertexIPEstimator/TrackToVertexIPEstimator"),
      m_InDetTrackSelectionTool(
          "InDet::InDetTrackSelectionTool/TrackSelectionTool", this) {
  // Track selection tool
  declareProperty("TrackSelectionTool", m_InDetTrackSelectionTool);

  // Jet type
  declareProperty("JetType", m_jetType);

  // Syst flag
  declareProperty("DoSyst", m_doSyst = true);
}

StatusCode TrackJetDecoratorAlg ::initialize() {

  ANA_MSG_INFO("Initializing TrackJetDecoratorAlg");
  ANA_CHECK(m_trackToVertexIPEstimator.retrieve());
  ATH_CHECK(m_InDetTrackSelectionTool.retrieve());

  // Hard-code systematics
  const CP::SystematicSet RecommendedSystematics = {
      InDet::TrackSystematicMap[InDet::TrackSystematic::TRK_RES_D0_MEAS],
      InDet::TrackSystematicMap[InDet::TrackSystematic::TRK_RES_Z0_MEAS],
      InDet::TrackSystematicMap[InDet::TrackSystematic::TRK_RES_D0_DEAD],
      InDet::TrackSystematicMap[InDet::TrackSystematic::TRK_RES_Z0_DEAD],
      InDet::TrackSystematicMap[InDet::TrackSystematic::TRK_BIAS_D0_WM],
      InDet::TrackSystematicMap[InDet::TrackSystematic::TRK_BIAS_Z0_WM],
      InDet::TrackSystematicMap
          [InDet::TrackSystematic::TRK_BIAS_QOVERP_SAGITTA_WM],
      InDet::TrackSystematicMap[InDet::TrackSystematic::TRK_FAKE_RATE_LOOSE],
      InDet::TrackSystematicMap[InDet::TrackSystematic::TRK_EFF_LOOSE_GLOBAL],
      InDet::TrackSystematicMap[InDet::TrackSystematic::TRK_EFF_LOOSE_IBL],
      InDet::TrackSystematicMap[InDet::TrackSystematic::TRK_EFF_LOOSE_PP0],
      InDet::TrackSystematicMap
          [InDet::TrackSystematic::TRK_EFF_LOOSE_PHYSMODEL],
  };

  ANA_CHECK(m_systematics.retrieve());

  if (m_doSyst)
    ANA_CHECK(m_systematics->setObjectSystematics(m_jetType + "Jets_%SYS%",
                                                  RecommendedSystematics));

  // Setup stuff
  ANA_CHECK(m_jetsHandle.initialize(m_systematicsList));
  ANA_CHECK(m_tracksHandle.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());
  ANA_CHECK(m_preselection.initialize());

  // decorators
  m_numConstituents =
      std::make_unique<SG::AuxElement::Decorator<long unsigned int>>(
          "numConstituents");
  m_daughter_pt =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughter__pt");
  m_daughter_eta =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughter__eta");
  m_daughter_phi =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughter__phi");
  m_daughter_z0sinTheta =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughter__z0sinTheta");
  m_daughter_d0 =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughter__d0");
  m_daughter_d0PV =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughter__d0PV");
  m_daughter_passLoose =
      std::make_unique<SG::AuxElement::Decorator<std::vector<bool>>>(
          "daughter__passLoose");
  m_daughter_trackId =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "daughter__trackId");

  return StatusCode::SUCCESS;
}

StatusCode TrackJetDecoratorAlg ::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    // Primary vertex
    const xAOD::Vertex *primaryVertex = nullptr;
    const xAOD::VertexContainer *primaryVertices = nullptr;
    CHECK(evtStore()->retrieve(primaryVertices, "PrimaryVertices"));
    for (const xAOD::Vertex *vertex : *primaryVertices) {
      if (vertex->vertexType() == xAOD::VxType::PriVtx) {
        // The default "PrimaryVertex" container is ordered in
        // sum-pt, and the tracking group recommends to pick the one
        // with the maximum sum-pt, so this will do the right thing.
        // If the user needs a different primary vertex, he needs to
        // provide a reordered primary vertex container and point
        // this algorithm to it. Currently there is no central
        // algorithm to do that, so users will have to write their
        // own (15 Aug 18).
        if (primaryVertex == nullptr) {
          primaryVertex = vertex;
          break;
        }
      }
    }
    if (!primaryVertex) {
      ATH_MSG_WARNING("No primary vertex found");
      return StatusCode::SUCCESS;
    }
    // Retrieve jet container
    const xAOD::JetContainer *jets = nullptr;
    ANA_CHECK(m_jetsHandle.retrieve(jets, sys));

    // Retrieve track container
    const xAOD::TrackParticleContainer *tracks = nullptr;
    ANA_CHECK(m_tracksHandle.retrieve(tracks, sys));
    for (const xAOD::Jet *jet : *jets) {
      // Initialize track vectors
      (*m_numConstituents)(*jet) = jet->numConstituents();
      (*m_daughter_pt)(*jet) = {};
      (*m_daughter_eta)(*jet) = {};
      (*m_daughter_phi)(*jet) = {};
      (*m_daughter_z0sinTheta)(*jet) = {};
      (*m_daughter_d0)(*jet) = {};
      (*m_daughter_d0PV)(*jet) = {};
      (*m_daughter_passLoose)(*jet) = {};
      (*m_daughter_trackId)(*jet) = {};

      // Loop over jet constituents (tracks)
      xAOD::JetConstituentVector constituents = jet->getConstituents();
      for (auto *constituent : constituents) {
        for (auto *track : *tracks) {
          if (track->pt() == constituent->rawConstituent()->pt()) {
            // Save track
            (*m_daughter_pt)(*jet).push_back(track->pt());
            (*m_daughter_eta)(*jet).push_back(track->eta());
            (*m_daughter_phi)(*jet).push_back(track->phi());
            (*m_daughter_passLoose)(*jet).push_back(
                m_InDetTrackSelectionTool->accept(*track, primaryVertex));
            (*m_daughter_trackId)(*jet).push_back(
                std::find(tracks->begin(), tracks->end(), track) -
                tracks->begin());

            // // z0sinTheta
            // const double vertex_z = primaryVertex ? primaryVertex->z() : 0;
            // const float deltaZ0SinTheta = (track->z0() + track->vz() -
            // vertex_z) * sin(track->p4().Theta());
            // (*m_daughter_z0sinTheta)(*jet).push_back(deltaZ0SinTheta);

            // d0
            (*m_daughter_d0)(*jet).push_back(track->d0());
            const Trk::ImpactParametersAndSigma *ipAndSigma =
                m_trackToVertexIPEstimator->estimate(track, primaryVertex);
            if (ipAndSigma != NULL) {
              (*m_daughter_z0sinTheta)(*jet).push_back(
                  ipAndSigma->IPz0SinTheta);
              (*m_daughter_d0PV)(*jet).push_back(ipAndSigma->IPd0);
              delete ipAndSigma;
            } else {
              (*m_daughter_z0sinTheta)(*jet).push_back(999);
              (*m_daughter_d0PV)(*jet).push_back(999);
            }
          }
        }
      }

      // Check that we got all constituents
      if ((*m_daughter_pt)(*jet).size() != jet->numConstituents()) {
        ANA_MSG_FATAL("Missing some jet constituents!");
        return StatusCode::FAILURE;
      }
    } // loop over jets
  }
  return StatusCode::SUCCESS;
}
