/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Greg Ottino
/// @author Miha Muskinja

//
// includes
//

#include <CharmAnalysis/TruthSelectionAlg.h>
#include <CharmAnalysis/TruthUtils.h>

#include "MCTruthClassifier/MCTruthClassifier.h"
#include <MCTruthClassifier/MCTruthClassifierDefs.h>
#include <xAODTruth/TruthVertexContainer.h>
#include <xAODTruth/TruthEventContainer.h>

//
// method implementations
//

TruthSelectionAlg ::TruthSelectionAlg(const std::string &name,
                                      ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator) {
  declareProperty("tool", m_classifier, "the MCTC tool we apply");
  declareProperty("selectionDecoration", m_selectionDecoration,
                  "the decoration for the truth selection");
  declareProperty("TruthElectrons", m_truthElectrons,
                  "name of the truth electron container");
  declareProperty("TruthMuons", m_truthMuons,
                  "name of the truth muon container");
  declareProperty(
      "DressLeptons", m_dressLeptons,
      "get dressed lepton quantities from the specialized containers");
  declareProperty("DoTruthSelection", m_do_truth_selection);
  declareProperty("DoTruthCharm", m_do_truth_charm = true);
  declareProperty("DoTruthBottom", m_do_truth_bottom = false);
  declareProperty("DoTruthLeptons", m_do_truth_leptons = true);
  declareProperty("MaxDEta", m_trueD_MaxEta = -1.0);
  declareProperty("MinDPt", m_trueD_MinPt = 0.0);
  declareProperty("MaxLepEta", m_trueLep_MaxEta = -1.0);
  declareProperty("MinLepPt", m_trueLep_MinPt = 0.0);
}

StatusCode TruthSelectionAlg ::initialize() {

  // container handles
  ANA_CHECK(m_truthHandle.initialize(m_systematicsList));
  ANA_CHECK(m_turthLeptonHandle.initialize(m_systematicsList));

  // list of systematics
  ANA_CHECK(m_systematicsList.initialize());

  // selection decoration
  if (m_selectionDecoration.empty()) {
    ANA_MSG_ERROR("no selection decoration name set");
    return StatusCode::FAILURE;
  }

  // truth selection decorator
  m_truthSelectionDecorator =
      std::make_unique<SG::AuxElement::Decorator<bool>>("truth_selection");

  // aux element decorators
  ANA_CHECK(
      makeSelectionWriteAccessor(m_selectionDecoration, m_selectionAccessor));
  initializeAuxElements();

  if (!m_classifier.empty()) {
    ANA_CHECK(m_classifier.retrieve());
  }

  return StatusCode::SUCCESS;
}

StatusCode TruthSelectionAlg ::execute()
{
    for (const auto &sys : m_systematicsList.systematicsVector())
    {
        // Event Info
        const xAOD::EventInfo *eventInfo = nullptr;
        ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
        const xAOD::TruthVertexContainer *truthVertices = nullptr;
        ANA_CHECK(evtStore()->retrieve(truthVertices, "TruthVertices"));

        // Get the beam interaction point as PV
        const xAOD::TruthEventContainer *truthEvents = nullptr;
        ANA_CHECK(evtStore()->retrieve(truthEvents, "TruthEvents"));
        truthPV = (*(truthEvents->begin()))->signalProcessVertex();
    
        // Truth Particles Container
        const xAOD::TruthParticleContainer *truth = nullptr;
        ANA_CHECK(m_truthHandle.retrieve(truth, sys));

    // Check for available decorations
    checkDecorations(truth);

    // Truth Electrons/Muons Container
    const xAOD::TruthParticleContainer *truthElectrons = nullptr;
    const xAOD::TruthParticleContainer *truthMuons = nullptr;
    if (!m_truthElectrons.empty() && !m_truthMuons.empty()) {
      ANA_CHECK(evtStore()->retrieve(truthElectrons, m_truthElectrons));
      ANA_CHECK(evtStore()->retrieve(truthMuons, m_truthMuons));
    }

    auto truthLeptons = std::make_unique<xAOD::TruthParticleContainer>();
    auto truthLeptons_aux = std::make_unique<xAOD::TruthParticleAuxContainer>();
    truthLeptons->setStore(truthLeptons_aux.get());

    m_N_trueLep_fiducial = 0;
    m_N_trueD_fiducial = 0;
    for (const xAOD::TruthParticle *particle : *truth) {
      const int pdgId = particle->pdgId();
      if (pdgId == 21 or pdgId == 22)
        continue; // skip gluon (21) and photon (22) first

      // test for zero barcode
      if (particle->barcode() == 0) {
        ANA_MSG_ERROR("particle " << particle->pdgId()
                                  << " has zero barcode in event number: "
                                  << eventInfo->eventNumber());
        return StatusCode::FAILURE;
      }

      // fail by default
      m_selectionAccessor->setBool(*particle, false);

      // prompt leptons
      if (m_do_truth_leptons && particle->isLepton() &&
          (particle->status() == 1)) {
        // classify leptons
        unsigned int result{};
        // if (m_classificationAcc->isAvailable(*particle))
        // {
        //     result = (*m_classificationAcc)(*particle);
        // }
        // else
        // {
        //     result = m_classifier->classify(particle);
        // }
        result = m_classifier->classify(particle);

        if ((MCTruthClassifier::isPrompt(result, false) == 1) &&
            !MCTruthClassifier::isGeant(result)) {
          // copy the particle pointer
          xAOD::TruthParticle *truthLepton = new xAOD::TruthParticle();
          truthLeptons->push_back(truthLepton);
          truthLepton->setPdgId(particle->pdgId());
          truthLepton->setBarcode(particle->barcode());
          truthLepton->setStatus(particle->status());
          truthLepton->setM(particle->m());
          truthLepton->setPx(particle->px());
          truthLepton->setPy(particle->py());
          truthLepton->setPz(particle->pz());
          truthLepton->setE(particle->e());
          (*m_outcomeAcc)(*truthLepton) = (*m_outcomeAcc)(*particle);
          (*m_originAcc)(*truthLepton) = (*m_originAcc)(*particle);
          (*m_typeAcc)(*truthLepton) = (*m_typeAcc)(*particle);
          (*m_pt)(*truthLepton) = particle->pt();
          (*m_eta)(*truthLepton) = particle->eta();
          (*m_phi)(*truthLepton) = particle->phi();
          if (m_dressLeptons) {
            if (m_e_dressed->isAvailable(*particle)) {
              (*m_e_dressed)(*truthLepton) = (*m_e_dressed)(*particle);
              (*m_pt_dressed)(*truthLepton) = (*m_pt_dressed)(*particle);
              (*m_eta_dressed)(*truthLepton) = (*m_eta_dressed)(*particle);
              (*m_phi_dressed)(*truthLepton) = (*m_phi_dressed)(*particle);
            } else {
              (*m_e_dressed)(*truthLepton) = -999;
              (*m_pt_dressed)(*truthLepton) = -999;
              (*m_eta_dressed)(*truthLepton) = -999;
              (*m_phi_dressed)(*truthLepton) = -999;
            }
          }
          m_selectionAccessor->setBool(*truthLepton, false);
          if ((particle->charge() != 0) && (particle->pt() > m_trueLep_MinPt) &&
              ((m_trueLep_MaxEta > 0 &&
                fabs(particle->eta()) < m_trueLep_MaxEta) ||
               m_trueLep_MaxEta <= 0)) {
            m_N_trueLep_fiducial++;
          }

          // find dressed quantities
          if (truthElectrons && truthMuons) {
            if (fabs(truthLepton->pdgId()) == 11) {
              for (const xAOD::TruthParticle *truthElectron : *truthElectrons) {
                if (truthElectron->barcode() == particle->barcode()) {
                  (*m_e_dressed)(*truthLepton) = (*m_e_dressed)(*truthElectron);
                  (*m_pt_dressed)(*truthLepton) =
                      (*m_pt_dressed)(*truthElectron);
                  (*m_eta_dressed)(*truthLepton) =
                      (*m_eta_dressed)(*truthElectron);
                  (*m_phi_dressed)(*truthLepton) =
                      (*m_phi_dressed)(*truthElectron);
                }
              }
            } else if (fabs(truthLepton->pdgId()) == 13) {
              for (const xAOD::TruthParticle *truthMuon : *truthMuons) {
                if (truthMuon->barcode() == particle->barcode()) {
                  (*m_e_dressed)(*truthLepton) = (*m_e_dressed)(*truthMuon);
                  (*m_pt_dressed)(*truthLepton) = (*m_pt_dressed)(*truthMuon);
                  (*m_eta_dressed)(*truthLepton) = (*m_eta_dressed)(*truthMuon);
                  (*m_phi_dressed)(*truthLepton) = (*m_phi_dressed)(*truthMuon);
                }
              }
            } else {
              (*m_e_dressed)(*truthLepton) = -999;
              (*m_pt_dressed)(*truthLepton) = -999;
              (*m_eta_dressed)(*truthLepton) = -999;
              (*m_phi_dressed)(*truthLepton) = -999;
            }
          }
        }
        // next truth particle
        continue;
      }

      // Charm hadrons
      if (m_do_truth_charm and TruthUtils::isCharmedHadron(particle)) {
        // from here on record only particles at the end of decay chain
        if (TruthUtils::findFinalParticle(particle) == particle) {
          bool accept = GetCharmDaughters(particle);
          m_selectionAccessor->setBool(*particle, accept);
          if ((particle->pt() > m_trueD_MinPt) &&
              ((m_trueD_MaxEta > 0 && fabs(particle->eta()) < m_trueD_MaxEta) ||
               m_trueD_MaxEta <= 0)) {
            m_N_trueD_fiducial++;
          }
        }
      }

      // Bottom hadrons
      if (m_do_truth_bottom and TruthUtils::isBottomHadron(particle)) {
        ANA_MSG_VERBOSE("B PDGID: " << particle->pdgId());
        if (TruthUtils::findFinalParticle(particle) == particle) {
          const auto &children = TruthUtils::findStatusOneChildren(particle);
          decorateTruthParticle(particle, children);
          decorateDAux(particle, children);
          m_selectionAccessor->setBool(*particle, true);
        }
      }
    }

    // truth event decision
    if ((m_N_trueLep_fiducial > 0) && (m_N_trueD_fiducial > 0)) {
      (*m_truthSelectionDecorator)(*eventInfo) = true;
    } else {
      (*m_truthSelectionDecorator)(*eventInfo) = false;
    }

    // record the truth lepton container
    ANA_CHECK(m_turthLeptonHandle.record(std::move(truthLeptons),
                                         std::move(truthLeptons_aux), sys));
  }
  return StatusCode::SUCCESS;
}

bool TruthSelectionAlg ::GetCharmDaughters(
    const xAOD::TruthParticle *particle) {

  const xAOD::TruthParticle *p = TruthUtils::findFinalParticle(particle);
  if (p == NULL) {
    return false;
  }
  if (p->nChildren() < 1) {
    return false;
  }

  std::vector<const xAOD::TruthParticle *> truth =
      TruthUtils::findStatusOneChildren(p);

  // determine D meson decay mode
  ANA_MSG_VERBOSE("D PDGID: " << p->pdgId());

  // D+
  if (abs(p->pdgId()) == 411 && TruthUtils::chargeDecayMode(truth) == 1) {
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = 4;
    return true;
  } else if (abs(p->pdgId()) == 411 &&
             TruthUtils::chargeDecayMode(truth) == 2 &&
             TruthUtils::phiInDecayChain(particle)) {
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = 51;
    return true;
  }
  // D*+ kpi
  else if (abs(particle->pdgId()) == 413) {
    const xAOD::TruthParticle *Dzero = TruthUtils::getSecondaryD0(particle);
    const xAOD::TruthParticle *Dplus = TruthUtils::getSecondaryDplus(particle);
    int decayMode_tmp = 0;
    if (TruthUtils::chargeDecayMode(truth) == 1 && Dzero != NULL) {
      truth.push_back(TruthUtils::getSecondaryD0(particle));
      decayMode_tmp = 1;
    }
    // D*+ kpipi0
    else if (TruthUtils::chargeDecayMode(truth) == 4 && Dzero != NULL) {
      truth.push_back(TruthUtils::getSecondaryD0(particle));
      decayMode_tmp = 2;
    }
    // D*+ k3pi
    else if (TruthUtils::chargeDecayMode(truth) == 3 && Dzero != NULL) {
      truth.push_back(TruthUtils::getSecondaryD0(particle));
      decayMode_tmp = 3;
    } else if (TruthUtils::chargeDecayMode(truth) == 4 && Dplus != NULL) {
      truth.push_back(TruthUtils::getSecondaryDplus(particle));
      decayMode_tmp = 41;
    } else if (Dzero != NULL) {
      truth.push_back(TruthUtils::getSecondaryD0(particle));
      decayMode_tmp = 0;
    } else if (Dplus != NULL) {
      truth.push_back(TruthUtils::getSecondaryDplus(particle));
      decayMode_tmp = 40;
    }
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = decayMode_tmp;
    return true;
    // D_S+
  } else if (abs(particle->pdgId()) == 431 &&
             TruthUtils::chargeDecayMode(truth) == 2 &&
             TruthUtils::phiInDecayChain(particle)) {
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = 5;
    return true;
  } else if (abs(particle->pdgId()) == 421) {
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = 0;
    return true;
  } else if (abs(particle->pdgId()) == 4122 &&
             TruthUtils::chargeDecayMode(truth) == 5 &&
             TruthUtils::lambdaInDecayChain(particle)) {
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = 6;
    if (TruthUtils::lambdaToPipInDecayChain(particle))
      (*m_decayMode)(*p) = 106;
    return true;
  } else if (abs(particle->pdgId()) == 4122 &&
             TruthUtils::chargeDecayMode(truth) == 7 &&
             TruthUtils::kshortInDecayChain(particle)) {
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = 7; // Kshortp
    if (TruthUtils::kshortToPiPiInDecayChain(particle))
      (*m_decayMode)(*p) = 107;
    return true;
  } else if (abs(particle->pdgId()) == 4122 &&
             TruthUtils::chargeDecayMode(truth) == 8) {
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = 11; // pKpi
    return true;
  } else if ((abs(particle->pdgId()) == 411 || abs(particle->pdgId()) == 431) &&
             TruthUtils::chargeDecayMode(truth) == 6 &&
             TruthUtils::kshortInDecayChain(particle)) {
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = 8; // KshortK
    if (TruthUtils::kshortToPiPiInDecayChain(particle))
      (*m_decayMode)(*p) = 108;
    return true;
  } else if ((abs(particle->pdgId()) == 411 || abs(particle->pdgId()) == 431) &&
             TruthUtils::chargeDecayMode(truth) == 5 &&
             TruthUtils::kshortInDecayChain(particle)) {
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = 9; // KshortPi
    if (TruthUtils::kshortToPiPiInDecayChain(particle))
      (*m_decayMode)(*p) = 109;
    return true;
  } else {
    decorateTruthParticle(p, truth);
    decorateDAux(p, truth);
    (*m_decayMode)(*p) = 0;
    return true;
  }
  return false;
}

void TruthSelectionAlg ::decorateDAux(
    const xAOD::TruthParticle *particle,
    std::vector<const xAOD::TruthParticle *> children) {
  // TODO: what if there are more kaons?
  const xAOD::TruthParticle *kaon = NULL;
  for (const xAOD::TruthParticle *child : children) {
    if (abs(child->pdgId()) == 321) {
      kaon = child;
    }
  }
  if (kaon != NULL) {
    (*m_costhetastarT)(*particle) = TruthUtils::cosThetaStar(kaon, particle);
  } else {
    (*m_costhetastarT)(*particle) = 0;
  }
  (*m_LxyT)(*particle) = TruthUtils::Lxy(particle, truthPV);
  (*m_ImpactT)(*particle) = TruthUtils::ImpactParam(particle);
  (*m_fromBdecay)(*particle) = TruthUtils::fromBdecay(particle);
}

void TruthSelectionAlg ::initializeAuxElements() {

  // Accessors
  m_outcomeAcc = std::make_unique<const SG::AuxElement::Accessor<unsigned int>>(
      "classifierParticleOutCome");
  m_originAcc = std::make_unique<const SG::AuxElement::Accessor<unsigned int>>(
      "classifierParticleOrigin");
  m_typeAcc = std::make_unique<const SG::AuxElement::Accessor<unsigned int>>(
      "classifierParticleType");
  m_classificationAcc =
      std::make_unique<const SG::AuxElement::Accessor<unsigned int>>(
          "Classification");
  m_e_dressed =
      std::make_unique<const SG::AuxElement::Accessor<float>>("e_dressed");
  m_pt_dressed =
      std::make_unique<const SG::AuxElement::Accessor<float>>("pt_dressed");
  m_eta_dressed =
      std::make_unique<const SG::AuxElement::Accessor<float>>("eta_dressed");
  m_phi_dressed =
      std::make_unique<const SG::AuxElement::Accessor<float>>("phi_dressed");

  // Decorators
  m_pt = std::make_unique<SG::AuxElement::Decorator<float>>("pt");
  m_eta = std::make_unique<SG::AuxElement::Decorator<float>>("eta");
  m_phi = std::make_unique<SG::AuxElement::Decorator<float>>("phi");
  m_costhetastarT =
      std::make_unique<SG::AuxElement::Decorator<float>>("cosThetaStarT");
  m_LxyT = std::make_unique<SG::AuxElement::Decorator<float>>("LxyT");
  m_ImpactT = std::make_unique<SG::AuxElement::Decorator<float>>("ImpactT");
  m_dRDLepT = std::make_unique<SG::AuxElement::Decorator<float>>("DeltaRT");
  m_fromBdecay =
      std::make_unique<SG::AuxElement::Decorator<bool>>("fromBdecay");
  m_decayMode = std::make_unique<SG::AuxElement::Decorator<int>>("decayMode");
  m_VertexPositionT =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "vertexPosition");
  m_daughterT_barcode =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "daughterInfoT__barcode");
  m_daughterT_pt =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfoT__pt");
  m_daughterT_eta =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfoT__eta");
  m_daughterT_phi =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfoT__phi");
  m_daughterT_pdgId =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "daughterInfoT__pdgId");
  // m_daughterT_fromDstar =
  // std::make_unique<SG::AuxElement::Decorator<std::vector<bool>>>("daughterInfoT__fromDstar");
}

void TruthSelectionAlg ::decorateTruthParticle(
    const xAOD::TruthParticle *p,
    std::vector<const xAOD::TruthParticle *> truthVec) {
  // truth leptons only have px, py, pz by default
  if (!m_has_pt_decoration) {
    (*m_pt)(*p) = p->pt();
  }
  if (!m_has_eta_decoration) {
    (*m_eta)(*p) = p->eta();
  }
  if (!m_has_phi_decoration) {
    (*m_phi)(*p) = p->phi();
  }

  (*m_VertexPositionT)(*p) = {};
  (*m_costhetastarT)(*p) = -999;
  (*m_LxyT)(*p) = -999;
  (*m_ImpactT)(*p) = -999;
  (*m_fromBdecay)(*p) = false;
  (*m_decayMode)(*p) = -1;
  (*m_daughterT_barcode)(*p) = {};
  (*m_daughterT_pt)(*p) = {};
  (*m_daughterT_eta)(*p) = {};
  (*m_daughterT_phi)(*p) = {};
  (*m_daughterT_pdgId)(*p) = {};

  if (p->prodVtx() != NULL) {
    (*m_VertexPositionT)(*p) = {p->prodVtx()->x(), p->prodVtx()->y(),
                                p->prodVtx()->z()};
  }

  if (truthVec.size() > 0) {
    for (unsigned int i = 0; i < truthVec.size(); i++) {
      (*m_daughterT_pdgId)(*p).push_back(truthVec[i]->pdgId());
      (*m_daughterT_barcode)(*p).push_back(truthVec[i]->barcode());
      (*m_daughterT_pt)(*p).push_back(truthVec[i]->pt());
      (*m_daughterT_eta)(*p).push_back(truthVec[i]->eta());
      (*m_daughterT_phi)(*p).push_back(truthVec[i]->phi());
    }
  }

  return;
}

void TruthSelectionAlg ::checkDecorations(
    const xAOD::TruthParticleContainer *truth) {
  for (auto *p : *truth) {
    if (m_pt->isAvailable(*p)) {
      m_has_pt_decoration = true;
    }
    if (m_eta->isAvailable(*p)) {
      m_has_eta_decoration = true;
    }
    if (m_phi->isAvailable(*p)) {
      m_has_phi_decoration = true;
    }
    break;
  }
}
