/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <CharmAnalysis/METDecoratorAlg.h>

//
// method implementations
//

METDecoratorAlg ::METDecoratorAlg(const std::string &name,
                                  ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator) {
  declareProperty("METComponent", m_MetComponent);
  declareProperty("METDecoration", m_MET);
  declareProperty("METPhiDecoration", m_METPhi);
}

StatusCode METDecoratorAlg ::initialize() {

  if (!m_MET.empty()) {
    m_METAccessor = std::make_unique<SG::AuxElement::Accessor<float>>(m_MET);
  }
  if (!m_METPhi.empty()) {
    m_METPhiAccessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_METPhi);
  }

  // Sys handles
  ANA_CHECK(m_eventInfoHandle.initialize(m_systematicsList));
  ANA_CHECK(m_metHandle.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());

  return StatusCode::SUCCESS;
}

StatusCode METDecoratorAlg ::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    // charm event info
    auto METInfo = std::make_unique<xAOD::EventInfo>();
    auto METInfo_store = std::make_unique<xAOD::EventAuxInfo>();
    METInfo->setStore(METInfo_store.get());

    // MET container
    const xAOD::MissingETContainer *MET;
    ANA_CHECK(m_metHandle.retrieve(MET, sys));
    ANA_CHECK((*MET)[m_MetComponent] != nullptr);

    // MET
    const xAOD::MissingET *missingEt = (*MET)[m_MetComponent];
    (*m_METAccessor)(*METInfo) = missingEt->met();
    (*m_METPhiAccessor)(*METInfo) = missingEt->phi();

    // record the new EventInfo object
    ANA_CHECK(m_eventInfoHandle.record(std::move(METInfo),
                                       std::move(METInfo_store), sys));
  }
  return StatusCode::SUCCESS;
}
