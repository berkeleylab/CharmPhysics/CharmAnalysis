/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

///
/// Kindly stolen from Tadej Novak
///

#include <CharmAnalysis/TriggerMatchingAlg.h>

#include <xAODEventInfo/EventInfo.h>

TriggerMatchingAlg::TriggerMatchingAlg(const std::string &name,
                                       ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator), m_trigMatchingTool("", this) {
  declareProperty("tool", m_trigMatchingTool, "trigger matching tool");
  declareProperty("matchingDecoration", m_matchingDecoration,
                  "the decoration the trigger pass status");
  declareProperty("matchingPairDecoration", m_matchingPairDecoration,
                  "the decoration for matched pairs");

  declareProperty("triggersSingleElectron", m_trigListSingleElectron,
                  "single electron triggers");
  declareProperty("triggersSingleMuon", m_trigListSingleMuon,
                  "single muon triggers");
  declareProperty("triggersSingleTau", m_trigListSingleTau,
                  "single tau triggers");
  declareProperty("triggersDiElectron", m_trigListDiElectron,
                  "di-electron triggers");
  declareProperty("triggersDiMuon", m_trigListDiMuon, "di-muon triggers");
  declareProperty("triggersDiTau", m_trigListDiTau, "di-tau triggers");
  declareProperty("triggersElectronMuon", m_trigListElectronMuon,
                  "electron-muon triggers");
  declareProperty("triggersElectronTau", m_trigListElectronTau,
                  "electron-tau triggers");
  declareProperty("triggersMuonTau", m_trigListMuonTau, "muon-tau triggers");
}

StatusCode TriggerMatchingAlg::initialize() {
  if (m_matchingDecoration.empty()) {
    ATH_MSG_ERROR("The decoration name needs to be defined");
    return StatusCode::FAILURE;
  }

  if (m_trigListSingleElectron.empty() && m_trigListSingleMuon.empty() &&
      m_trigListSingleTau.empty() && m_trigListDiElectron.empty() &&
      m_trigListDiMuon.empty() && m_trigListDiTau.empty() &&
      m_trigListElectronMuon.empty() && m_trigListElectronTau.empty() &&
      m_trigListMuonTau.empty()) {
    ATH_MSG_ERROR("At least one list of triggers needs to be provided");
    return StatusCode::FAILURE;
  }

  ANA_CHECK(m_trigMatchingTool.retrieve());

  for (const std::string &chain : m_trigListSingleElectron) {
    m_matchingAccessors.emplace(chain, m_matchingDecoration + "_" + chain);
  }
  for (const std::string &chain : m_trigListSingleMuon) {
    m_matchingAccessors.emplace(chain, m_matchingDecoration + "_" + chain);
  }
  for (const std::string &chain : m_trigListSingleTau) {
    m_matchingAccessors.emplace(chain, m_matchingDecoration + "_" + chain);
  }

  if (!m_matchingPairDecoration.empty()) {
    for (const std::string &chain : m_trigListDiElectron) {
      m_matchingPairAccessors.emplace(chain,
                                      m_matchingPairDecoration + "_" + chain);
    }
    for (const std::string &chain : m_trigListDiMuon) {
      m_matchingPairAccessors.emplace(chain,
                                      m_matchingPairDecoration + "_" + chain);
    }
    for (const std::string &chain : m_trigListDiTau) {
      m_matchingPairAccessors.emplace(chain,
                                      m_matchingPairDecoration + "_" + chain);
    }
    for (const std::string &chain : m_trigListElectronMuon) {
      m_matchingPairAccessors.emplace(chain,
                                      m_matchingPairDecoration + "_" + chain);
    }
    for (const std::string &chain : m_trigListElectronTau) {
      m_matchingPairAccessors.emplace(chain,
                                      m_matchingPairDecoration + "_" + chain);
    }
    for (const std::string &chain : m_trigListMuonTau) {
      m_matchingPairAccessors.emplace(chain,
                                      m_matchingPairDecoration + "_" + chain);
    }
  }

  if (m_electronsHandle)
    ANA_CHECK(m_electronsHandle.initialize(m_systematicsList));

  if (m_muonsHandle)
    ANA_CHECK(m_muonsHandle.initialize(m_systematicsList));

  if (m_tausHandle)
    ANA_CHECK(m_tausHandle.initialize(m_systematicsList));

  ANA_CHECK(m_systematicsList.initialize());

  return StatusCode::SUCCESS;
}

StatusCode TriggerMatchingAlg::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    const xAOD::EventInfo *evtInfo{};
    ANA_CHECK(evtStore()->retrieve(evtInfo, "EventInfo"));

    // init pairs
    for (const std::string &chain : m_trigListDiElectron) {
      (m_matchingPairAccessors.at(chain))(*evtInfo) =
          std::vector<std::pair<uint8_t, uint8_t>>();
    }
    for (const std::string &chain : m_trigListDiMuon) {
      (m_matchingPairAccessors.at(chain))(*evtInfo) =
          std::vector<std::pair<uint8_t, uint8_t>>();
    }
    for (const std::string &chain : m_trigListDiTau) {
      (m_matchingPairAccessors.at(chain))(*evtInfo) =
          std::vector<std::pair<uint8_t, uint8_t>>();
    }
    for (const std::string &chain : m_trigListElectronMuon) {
      (m_matchingPairAccessors.at(chain))(*evtInfo) =
          std::vector<std::pair<uint8_t, uint8_t>>();
    }
    for (const std::string &chain : m_trigListElectronTau) {
      (m_matchingPairAccessors.at(chain))(*evtInfo) =
          std::vector<std::pair<uint8_t, uint8_t>>();
    }
    for (const std::string &chain : m_trigListMuonTau) {
      (m_matchingPairAccessors.at(chain))(*evtInfo) =
          std::vector<std::pair<uint8_t, uint8_t>>();
    }

    const xAOD::ElectronContainer *electrons{};
    if (m_electronsHandle)
      ANA_CHECK(m_electronsHandle.retrieve(electrons, sys));

    const xAOD::MuonContainer *muons{};
    if (m_muonsHandle)
      ANA_CHECK(m_muonsHandle.retrieve(muons, sys));

    const xAOD::TauJetContainer *taus{};
    if (m_tausHandle)
      ANA_CHECK(m_tausHandle.retrieve(taus, sys));

    if (electrons != nullptr) {
      if (!m_trigListSingleElectron.empty()) {
        ANA_MSG_VERBOSE("Doing single electron trigger matching...");

        for (const xAOD::Electron *electron : *electrons) {
          for (const std::string &chain : m_trigListSingleElectron) {
            (m_matchingAccessors.at(chain))(*electron) =
                m_trigMatchingTool->match(*electron, chain, 0.07, false);
          }
        }
      }
    }

    if (muons != nullptr) {
      if (!m_trigListSingleMuon.empty()) {
        ANA_MSG_VERBOSE("Doing single muon trigger matching...");

        for (const xAOD::Muon *muon : *muons) {
          for (const std::string &chain : m_trigListSingleMuon) {
            (m_matchingAccessors.at(chain))(*muon) =
                m_trigMatchingTool->match(*muon, chain, 0.1, false);
          }
        }
      }
    }

    if (taus != nullptr) {
      if (!m_trigListSingleTau.empty()) {
        ANA_MSG_VERBOSE("Doing single tau trigger matching...");

        for (const xAOD::TauJet *tau : *taus) {
          for (const std::string &chain : m_trigListSingleTau) {
            (m_matchingAccessors.at(chain))(*tau) =
                m_trigMatchingTool->match(*tau, chain, 0.1, false);
          }
        }
      }
    }

    if (electrons != nullptr) {
      if (!m_trigListDiElectron.empty()) {
        ANA_MSG_DEBUG("Doing di-electron trigger matching...");

        if (electrons->size() >= 2) {
          std::vector<const xAOD::IParticle *> particles;

          for (size_t i = 0; i < electrons->size() - 1; ++i) {
            for (size_t j = i + 1; j < electrons->size(); ++j) {
              // test a new pair
              particles.clear();
              particles.push_back(electrons->at(i));
              particles.push_back(electrons->at(j));

              for (const std::string &chain : m_trigListDiElectron) {
                // store the pair if matched
                if (m_trigMatchingTool->match(particles, chain, 0.1, false)) {
                  (m_matchingPairAccessors.at(chain))(*evtInfo).emplace_back(i,
                                                                             j);
                }
              }
            }
          }
        }
      }
    }

    if (muons != nullptr) {
      if (!m_trigListDiMuon.empty()) {
        ANA_MSG_DEBUG("Doing di-muon trigger matching...");

        if (muons->size() >= 2) {
          std::vector<const xAOD::IParticle *> particles;

          for (size_t i = 0; i < muons->size() - 1; ++i) {
            for (size_t j = i + 1; j < muons->size(); ++j) {
              // test a new pair
              particles.clear();
              particles.push_back(muons->at(i));
              particles.push_back(muons->at(j));

              for (const std::string &chain : m_trigListDiMuon) {
                // store the pair if matched
                if (m_trigMatchingTool->match(particles, chain, 0.1, false)) {
                  (m_matchingPairAccessors.at(chain))(*evtInfo).emplace_back(i,
                                                                             j);
                }
              }
            }
          }
        }
      }
    }

    if (taus != nullptr) {
      if (!m_trigListDiTau.empty()) {
        ANA_MSG_DEBUG("Doing di-tau trigger matching...");

        if (taus->size() >= 2) {
          std::vector<const xAOD::IParticle *> particles;

          for (size_t i = 0; i < taus->size() - 1; ++i) {
            for (size_t j = i + 1; j < taus->size(); ++j) {
              // test a new pair
              particles.clear();
              particles.push_back(taus->at(i));
              particles.push_back(taus->at(j));

              for (const std::string &chain : m_trigListDiTau) {
                // store the pair if matched
                if (m_trigMatchingTool->match(particles, chain, 0.1, false)) {
                  (m_matchingPairAccessors.at(chain))(*evtInfo).emplace_back(i,
                                                                             j);
                }
              }
            }
          }
        }
      }
    }

    if (electrons != nullptr && muons != nullptr) {
      if (!m_trigListElectronMuon.empty()) {
        ANA_MSG_DEBUG("Doing electron-muon trigger matching...");

        std::vector<const xAOD::IParticle *> particles;

        for (size_t i = 0; i < electrons->size(); ++i) {
          for (size_t j = 0; j < muons->size(); ++j) {
            // test a new pair
            particles.clear();
            particles.push_back(electrons->at(i));
            particles.push_back(muons->at(j));

            for (const std::string &chain : m_trigListElectronMuon) {
              // store the pair if matched
              if (m_trigMatchingTool->match(particles, chain, 0.1, false)) {
                (m_matchingPairAccessors.at(chain))(*evtInfo).emplace_back(i,
                                                                           j);
              }
            }
          }
        }
      }
    }

    if (electrons != nullptr && taus != nullptr) {
      if (!m_trigListElectronTau.empty()) {
        ANA_MSG_DEBUG("Doing electron-tau trigger matching...");

        std::vector<const xAOD::IParticle *> particles;

        for (size_t i = 0; i < electrons->size(); ++i) {
          for (size_t j = 0; j < taus->size(); ++j) {
            // test a new pair
            particles.clear();
            particles.push_back(electrons->at(i));
            particles.push_back(taus->at(j));

            for (const std::string &chain : m_trigListElectronTau) {
              // store the pair if matched
              if (m_trigMatchingTool->match(particles, chain, 0.1, false)) {
                (m_matchingPairAccessors.at(chain))(*evtInfo).emplace_back(i,
                                                                           j);
              }
            }
          }
        }
      }
    }

    if (muons != nullptr && taus != nullptr) {
      if (!m_trigListMuonTau.empty()) {
        ANA_MSG_DEBUG("Doing muon-tau trigger matching...");

        std::vector<const xAOD::IParticle *> particles;

        for (size_t i = 0; i < muons->size(); ++i) {
          for (size_t j = 0; j < taus->size(); ++j) {
            // test a new pair
            particles.clear();
            particles.push_back(muons->at(i));
            particles.push_back(taus->at(j));

            for (const std::string &chain : m_trigListMuonTau) {
              // store the pair if matched
              if (m_trigMatchingTool->match(particles, chain, 0.1, false)) {
                (m_matchingPairAccessors.at(chain))(*evtInfo).emplace_back(i,
                                                                           j);
              }
            }
          }
        }
      }
    }
  }

  return StatusCode::SUCCESS;
}
