/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <PATCore/TAccept.h>

#include <SelectionHelpers/SelectionHelpers.h>

#include <xAODEgamma/Electron.h>
#include <xAODEgamma/ElectronxAODHelpers.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODMuon/Muon.h>
#include <xAODTracking/Vertex.h>
#include <xAODTracking/VertexContainer.h>

#include <CharmAnalysis/TrackSelectionAroundMesonAlg.h>

//
// method implementations
//

TrackSelectionAroundMesonAlg::TrackSelectionAroundMesonAlg(
    const std::string &name, ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator), m_max_dR(1.2)

{
  // various keys
  declareProperty("selectionDecoration", m_selectionDecoration,
                  "the decoration for the track selection");
  declareProperty("maxDeltaR", m_max_dR, "the dR cut value");
}

StatusCode TrackSelectionAroundMesonAlg::initialize() {
  ANA_CHECK(m_tracksHandle.initialize(m_systematicsList));
  ANA_CHECK(m_mesonHandle.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());

  if (m_selectionDecoration.empty()) {
    ANA_MSG_ERROR("no selection decoration name set");
    return StatusCode::FAILURE;
  }
  ANA_CHECK(
      makeSelectionWriteAccessor(m_selectionDecoration, m_selectionAccessor));

  // decorators
  m_trackId = std::make_unique<SG::AuxElement::Decorator<int>>("trackId");

  return StatusCode::SUCCESS;
}

StatusCode TrackSelectionAroundMesonAlg::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    // boson container
    const xAOD::CompositeParticleContainer *mesons = nullptr;
    ANA_CHECK(m_mesonHandle.retrieve(mesons, sys));

    // tracks handle
    xAOD::TrackParticleContainer *tracks = nullptr;
    ANA_CHECK(m_tracksHandle.getCopy(tracks, sys));

    // loop over tracks
    for (xAOD::TrackParticle *track : *tracks) {
      // decorate track
      (*m_trackId)(*track) =
          std::find(tracks->begin(), tracks->end(), track) - tracks->begin();
      m_selectionAccessor->setBool(*track, false);

      // loop over mesons
      for (auto *meson : *mesons) {
        if (track->p4().DeltaR(meson->p4()) < m_max_dR) {
          // Set bits for cutflow
          m_selectionAccessor->setBool(*track, true);
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}
