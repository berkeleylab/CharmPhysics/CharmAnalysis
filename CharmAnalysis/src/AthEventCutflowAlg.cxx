/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include "GaudiKernel/IAlgorithm.h"

#include <CharmAnalysis/AthEventCutflowAlg.h>

#include <regex>

//
// method implementations
//

namespace CP {

AthEventCutflowAlg ::AthEventCutflowAlg(const std::string &name,
                                        ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      m_algManager("ApplicationMgr", this->name()) {
  declareProperty("algorithmList", m_algs, "List of algorithms for cutflow.");
  declareProperty("histPattern", m_histo_name, "Cutflow histogram pattern.");
}

StatusCode AthEventCutflowAlg ::initialize() {
  ANA_CHECK(m_systematicsList.initialize());
  ATH_CHECK(m_algManager.retrieve());

  // book cutflow histogram
  ANA_CHECK(book(TH1F(m_histo_name.c_str(), m_histo_name.c_str(), m_algs.size(),
                      0.5, m_algs.size() + 0.5)));
  int bin = 0;
  for (auto &alg_name : m_algs) {
    bin++;
    hist(m_histo_name.c_str())->GetXaxis()->SetBinLabel(bin, alg_name.c_str());
  }
  return StatusCode::SUCCESS;
}

StatusCode AthEventCutflowAlg ::execute() {

  // fill algorithm decision
  int bin = 0;
  for (auto &alg_name : m_algs) {
    bin++;
    IAlgorithm *alg = nullptr;
    ATH_MSG_DEBUG("Attempting to retrive " << alg_name);
    ATH_CHECK(m_algManager->getAlgorithm(alg_name, alg));
    bool algPassed = alg->isExecuted() && alg->filterPassed();
    if (!algPassed)
      break;
    hist(m_histo_name.c_str())->Fill(bin, 1);
  }

  return StatusCode::SUCCESS;
}

StatusCode AthEventCutflowAlg ::finalize() { return StatusCode::SUCCESS; }

} // namespace CP
