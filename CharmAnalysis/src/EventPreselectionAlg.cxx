/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include <CharmAnalysis/EventPreselectionAlg.h>

EventPreselectionAlg::EventPreselectionAlg(const std::string &name,
                                           ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator) {
  declareProperty("minLeptons", m_min_leptons, "");
  declareProperty("maxLeptons", m_max_leptons, "");
  declareProperty("minLeptonsLight", m_min_leptons_light, "");
  declareProperty("maxLeptonsLight", m_max_leptons_light, "");

  declareProperty("minElectrons", m_min_electrons, "");
  declareProperty("maxElectrons", m_max_electrons, "");
  declareProperty("minMuons", m_min_muons, "");
  declareProperty("maxMuons", m_max_muons, "");
  declareProperty("minTaus", m_min_taus, "");
  declareProperty("maxTaus", m_max_taus, "");

  declareProperty("minJets", m_min_jets, "");
  declareProperty("maxJets", m_max_jets, "");
  declareProperty("minBJets", m_min_bjets, "");
  declareProperty("maxBJets", m_max_bjets, "");
}

StatusCode EventPreselectionAlg::initialize() {
  ANA_CHECK(m_electronsHandle.initialize(m_systematicsList, SG::AllowEmpty));
  ANA_CHECK(m_electronSelectionDecoration.initialize(
      m_systematicsList, m_electronsHandle, SG::AllowEmpty));
  ANA_CHECK(m_muonsHandle.initialize(m_systematicsList, SG::AllowEmpty));
  ANA_CHECK(m_muonSelectionDecoration.initialize(
      m_systematicsList, m_muonsHandle, SG::AllowEmpty));
  ANA_CHECK(m_jetsHandle.initialize(m_systematicsList, SG::AllowEmpty));
  ANA_CHECK(m_jetSelectionDecoration.initialize(m_systematicsList, m_jetsHandle,
                                                SG::AllowEmpty));
  ANA_CHECK(m_bjetSelectionDecoration.initialize(m_systematicsList,
                                                 m_jetsHandle, SG::AllowEmpty));
  ANA_CHECK(m_tausHandle.initialize(m_systematicsList, SG::AllowEmpty));
  ANA_CHECK(m_tauSelectionDecoration.initialize(m_systematicsList, m_tausHandle,
                                                SG::AllowEmpty));

  ANA_CHECK(m_systematicsList.initialize());

  return StatusCode::SUCCESS;
}

StatusCode EventPreselectionAlg::execute() {
  m_total++;

  setFilterPassed(false);

  bool validEvent{};
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    const xAOD::JetContainer *jets{};
    if (m_jetsHandle)
      ANA_CHECK(m_jetsHandle.retrieve(jets, sys));

    const xAOD::ElectronContainer *electrons{};
    if (m_electronsHandle)
      ANA_CHECK(m_electronsHandle.retrieve(electrons, sys));

    const xAOD::MuonContainer *muons{};
    if (m_muonsHandle)
      ANA_CHECK(m_muonsHandle.retrieve(muons, sys));

    const xAOD::TauJetContainer *taus{};
    if (m_tausHandle)
      ANA_CHECK(m_tausHandle.retrieve(taus, sys));

    // Update event flag
    validEvent =
        validEvent || executePreselection(sys, electrons, muons, taus, jets);
  }

  if (validEvent) {
    m_passed++;
    setFilterPassed(true);
  }

  return StatusCode::SUCCESS;
}

StatusCode EventPreselectionAlg::finalize() {
  ATH_MSG_INFO("Events passing preselection: " << m_passed << " / " << m_total);

  return StatusCode::SUCCESS;
}

bool EventPreselectionAlg::executePreselection(
    const CP::SystematicSet &sys, const xAOD::ElectronContainer *electrons,
    const xAOD::MuonContainer *muons, const xAOD::TauJetContainer *taus,
    const xAOD::JetContainer *jets) const {
  int elCount{};
  if (electrons != nullptr) {
    for (const xAOD::Electron *electron : *electrons) {
      if (m_electronSelectionDecoration.get(*electron, sys) == 1) {
        elCount++;
      }
    }
  }
  if (m_min_electrons >= 0 && elCount < m_min_electrons) {
    ANA_MSG_DEBUG("Rejecting: min electrons " << m_min_electrons << ", actual "
                                              << elCount);
    return false;
  }
  if (m_max_electrons >= 0 && elCount > m_max_electrons) {
    ANA_MSG_DEBUG("Rejecting: max electrons " << m_max_electrons << ", actual "
                                              << elCount);
    return false;
  }

  int muCount{};
  if (muons != nullptr) {
    for (const xAOD::Muon *muon : *muons) {
      if (m_muonSelectionDecoration.get(*muon, sys) == 1) {
        muCount++;
      }
    }
  }
  if (m_min_muons >= 0 && muCount < m_min_muons) {
    ANA_MSG_DEBUG("Rejecting: min muons " << m_min_muons << ", actual "
                                          << muCount);
    return false;
  }
  if (m_max_muons >= 0 && muCount > m_max_muons) {
    ANA_MSG_DEBUG("Rejecting: max muons " << m_max_muons << ", actual "
                                          << muCount);
    return false;
  }

  int tauCount{};
  if (taus != nullptr) {
    for (const xAOD::TauJet *tau : *taus) {
      if (m_tauSelectionDecoration.get(*tau, sys) == 1) {
        tauCount++;
      }
    }
  }
  if (m_min_taus >= 0 && tauCount < m_min_taus) {
    ANA_MSG_DEBUG("Rejecting: min taus " << m_min_taus << ", actual "
                                         << tauCount);
    return false;
  }
  if (m_max_taus >= 0 && tauCount > m_max_taus) {
    ANA_MSG_DEBUG("Rejecting: max taus " << m_max_taus << ", actual "
                                         << tauCount);
    return false;
  }

  int leptonCount = 0;
  leptonCount += elCount;
  leptonCount += muCount;
  leptonCount += tauCount;

  int leptonCountLight = 0;
  leptonCountLight += elCount;
  leptonCountLight += muCount;

  if (m_min_leptons >= 0 && leptonCount < m_min_leptons) {
    ANA_MSG_DEBUG("Rejecting: min leptons " << m_min_leptons << ", actual "
                                            << leptonCount);
    return false;
  }
  if (m_max_leptons >= 0 && leptonCount > m_max_leptons) {
    ANA_MSG_DEBUG("Rejecting: max leptons " << m_max_leptons << ", actual "
                                            << leptonCount);
    return false;
  }

  if (m_min_leptons_light >= 0 && leptonCountLight < m_min_leptons_light) {
    ANA_MSG_DEBUG("Rejecting: min light leptons "
                  << m_min_leptons_light << ", actual " << leptonCountLight);
    return false;
  }
  if (m_max_leptons_light >= 0 && leptonCountLight > m_max_leptons_light) {
    ANA_MSG_DEBUG("Rejecting: max light leptons "
                  << m_max_leptons_light << ", actual " << leptonCountLight);
    return false;
  }

  if (m_min_jets >= 0 || m_max_jets >= 0) {
    int jetCount{};
    if (jets != nullptr) {
      for (const xAOD::Jet *jet : *jets) {
        if (m_jetSelectionDecoration.get(*jet, sys) == 1) {
          jetCount++;
        }
      }
    }
    if (m_min_jets >= 0 && jetCount < m_min_jets) {
      ANA_MSG_DEBUG("Rejecting: min jets " << m_min_jets << ", actual "
                                           << jetCount);
      return false;
    }
    if (m_max_jets >= 0 && jetCount > m_max_jets) {
      ANA_MSG_DEBUG("Rejecting: max jets " << m_max_jets << ", actual "
                                           << jetCount);
      return false;
    }
  }

  if (m_min_bjets >= 0 || m_max_bjets >= 0) {
    int bjetCount{};
    if (jets != nullptr) {
      for (const xAOD::Jet *jet : *jets) {
        if (m_jetSelectionDecoration.get(*jet, sys) == 1 &&
            m_bjetSelectionDecoration.get(*jet, sys) == 1) {
          bjetCount++;
        }
      }
    }
    if (m_min_bjets >= 0 && bjetCount < m_min_bjets) {
      ANA_MSG_DEBUG("Rejecting: min bjets " << m_min_bjets << ", actual "
                                            << bjetCount);
      return false;
    }
    if (m_max_bjets >= 0 && bjetCount > m_max_bjets) {
      ANA_MSG_DEBUG("Rejecting: max bjets " << m_max_bjets << ", actual "
                                            << bjetCount);
      return false;
    }
  }

  return true;
}
