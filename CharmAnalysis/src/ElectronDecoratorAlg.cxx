/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <xAODEgamma/EgammaxAODHelpers.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <xAODTracking/Vertex.h>
#include <xAODTracking/VertexContainer.h>

#include <CharmAnalysis/ElectronDecoratorAlg.h>

//
// method implementations
//

ElectronDecoratorAlg ::ElectronDecoratorAlg(const std::string &name,
                                            ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      m_trackToVertexIPEstimator(
          "Trk::TrackToVertexIPEstimator/TrackToVertexIPEstimator") {
  declareProperty("extraVars", m_extra_vars = false);
  declareProperty("primaryVertexKey", m_primaryVertexKey = "PrimaryVertices");
  declareProperty("caloClusterEtaDecoration", m_caloClusterEtaDecoration,
                  "the decoration for the calo cluster eta");
  declareProperty("EOverPDecoration", m_EOverPDecoration,
                  "the decoration for the EOverP variable");
  declareProperty("POverPDecoration", m_POverPDecoration,
                  "the decoration for the POverP variable");
  declareProperty("DeltaPhi2Decoration", m_deltaPhi2Decoration,
                  "the decoration for the deltaPhi2 variable");
}

StatusCode ElectronDecoratorAlg ::initialize() {
  if (m_caloClusterEtaDecoration.empty()) {
    ANA_MSG_ERROR("Calo cluster eta decoration should not be empty.");
    return StatusCode::FAILURE;
  }
  if (m_EOverPDecoration.empty()) {
    ANA_MSG_ERROR("EOverP decoration should not be empty.");
    return StatusCode::FAILURE;
  }
  if (m_POverPDecoration.empty()) {
    ANA_MSG_ERROR("POverP decoration should not be empty.");
    return StatusCode::FAILURE;
  }
  if (m_deltaPhi2Decoration.empty()) {
    ANA_MSG_ERROR("deltaPhi2 decoration should not be empty.");
    return StatusCode::FAILURE;
  }

  ANA_CHECK(m_trackToVertexIPEstimator.retrieve());

  m_caloClusterEtaDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(
          m_caloClusterEtaDecoration);
  m_EOverPDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_EOverPDecoration);
  m_POverPDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_POverPDecoration);
  m_deltaPhi2Decorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_deltaPhi2Decoration);
  m_z0sinThetaDecorator = std::make_unique<SG::AuxElement::Decorator<float>>(
      m_z0sinThetaDecoration);
  m_d0sigDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_d0sigDecoration);
  m_d0Decorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_d0Decoration);
  m_d0sigPVDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_d0sigPVDecoration);
  m_d0PVDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_d0PVDecoration);

  ANA_CHECK(m_electronsHandle.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());
  ANA_CHECK(m_preselection.initialize());
  return StatusCode::SUCCESS;
}

StatusCode ElectronDecoratorAlg ::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    // Primary vertex
    const xAOD::Vertex *primaryVertex = nullptr;
    const xAOD::VertexContainer *primaryVertices = nullptr;
    CHECK(evtStore()->retrieve(primaryVertices, m_primaryVertexKey));
    for (const xAOD::Vertex *vertex : *primaryVertices) {
      if (vertex->vertexType() == xAOD::VxType::PriVtx) {
        // The default "PrimaryVertex" container is ordered in
        // sum-pt, and the tracking group recommends to pick the one
        // with the maximum sum-pt, so this will do the right thing.
        // If the user needs a different primary vertex, he needs to
        // provide a reordered primary vertex container and point
        // this algorithm to it. Currently there is no central
        // algorithm to do that, so users will have to write their
        // own (15 Aug 18).
        if (primaryVertex == nullptr) {
          primaryVertex = vertex;
          break;
        }
      }
    }
    if (!primaryVertex) {
      ATH_MSG_WARNING("No primary vertex found");
    }

    const xAOD::ElectronContainer *electrons = nullptr;
    ANA_CHECK(m_electronsHandle.retrieve(electrons, sys));
    for (const xAOD::Electron *electron : *electrons) {

      // calo cluster eta
      (*m_caloClusterEtaDecorator)(*electron) =
          electron->caloCluster() != nullptr ? electron->caloCluster()->etaBE(2)
                                             : -999.0;

      // EOverP
      (*m_EOverPDecorator)(*electron) =
          (electron->caloCluster() != nullptr &&
           electron->trackParticle() != nullptr)
              ? electron->caloCluster()->e() *
                    fabs(electron->trackParticle()->qOverP())
              : -999.0;

      // POverP
      // Momentum lost by the track between the perigee and the last
      // measurement point divided by the original momentum
      unsigned int index = -1;
      if (electron->trackParticle() != nullptr &&
          electron->trackParticle()->indexOfParameterAtPosition(
              index, xAOD::LastMeasurement)) {
        double LMPX = electron->trackParticle()->parameterPX(index);
        double LMPY = electron->trackParticle()->parameterPY(index);
        double LMPZ = electron->trackParticle()->parameterPZ(index);
        double refittedTrack_LMP =
            sqrt(LMPX * LMPX + LMPY * LMPY + LMPZ * LMPZ);
        double refittedTrack_LMqOverP =
            electron->trackParticle()->charge() / refittedTrack_LMP;
        double unrefittedTrack_qOverP = electron->trackParticle()->qOverP();
        (*m_POverPDecorator)(*electron) =
            1 - (unrefittedTrack_qOverP / refittedTrack_LMqOverP);
      } else {
        (*m_POverPDecorator)(*electron) = -999.0;
      }

      // deltaPhi2
      float deltaPhi2;
      if (electron->trackCaloMatchValue(
              deltaPhi2, xAOD::EgammaParameters::deltaPhiRescaled2)) {
        (*m_deltaPhi2Decorator)(*electron) = deltaPhi2;
      } else {
        (*m_deltaPhi2Decorator)(*electron) = -999.0;
      }

      // get track
      const xAOD::TrackParticle *track = electron->trackParticle();

      // z0sinTheta
      const double vertex_z = primaryVertex ? primaryVertex->z() : 0;
      const float deltaZ0SinTheta =
          (track->z0() + track->vz() - vertex_z) * sin(electron->p4().Theta());
      (*m_z0sinThetaDecorator)(*electron) = deltaZ0SinTheta;

      // d0sig
      const float d0sig = xAOD::TrackingHelpers::d0significance(
          track, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(),
          eventInfo->beamPosSigmaXY());
      (*m_d0sigDecorator)(*electron) = d0sig;
      (*m_d0Decorator)(*electron) = track->d0();

      if (m_extra_vars) {
        // pre-GSF track
        const xAOD::TrackParticle *preGSF_track =
            xAOD::EgammaHelpers::getOriginalTrackParticle(electron);

        // d0sig vs PV
        const Trk::ImpactParametersAndSigma *ipAndSigma =
            m_trackToVertexIPEstimator->estimate(preGSF_track, primaryVertex);
        if (ipAndSigma != NULL) {
          (*m_d0sigPVDecorator)(*electron) =
              ipAndSigma->IPd0 / ipAndSigma->sigmad0;
          (*m_d0PVDecorator)(*electron) = ipAndSigma->IPd0;
          delete ipAndSigma;
        } else {
          (*m_d0sigPVDecorator)(*electron) = 999;
          (*m_d0PVDecorator)(*electron) = 999;
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}
