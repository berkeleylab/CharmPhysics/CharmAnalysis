/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <xAODTruth/TruthParticle.h>

#include <CharmAnalysis/TruthJetDecoratorAlg.h>

//
// method implementations
//

TruthJetDecoratorAlg ::TruthJetDecoratorAlg(const std::string &name,
                                            ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator) {}

StatusCode TruthJetDecoratorAlg ::initialize() {
  ANA_CHECK(m_truthJetsHandle.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());
  ANA_CHECK(m_preselection.initialize());

  // Decorators
  m_ghostB_pt = std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
      "ghostB_pt");
  m_ghostB_eta =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "ghostB_eta");
  m_ghostB_phi =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "ghostB_phi");
  m_ghostB_m = std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
      "ghostB_m");
  m_ghostB_pdgId =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "ghostB_pdgId");
  m_ghostC_pt = std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
      "ghostC_pt");
  m_ghostC_eta =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "ghostC_eta");
  m_ghostC_phi =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "ghostC_phi");
  m_ghostC_m = std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
      "ghostC_m");
  m_ghostC_pdgId =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "ghostC_pdgId");

  return StatusCode::SUCCESS;
}

StatusCode TruthJetDecoratorAlg ::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    const xAOD::JetContainer *truthJets = nullptr;
    ANA_CHECK(m_truthJetsHandle.retrieve(truthJets, sys));
    for (const xAOD::Jet *truthJet : *truthJets) {
      // fetch truth C and B hadrons
      const std::string labelB = "GhostBHadronsFinal";
      const std::string labelC = "GhostCHadronsFinal";
      std::vector<const xAOD::TruthParticle *> ghostB;
      std::vector<const xAOD::TruthParticle *> ghostC;
      truthJet->getAssociatedObjects<xAOD::TruthParticle>(labelB, ghostB);
      truthJet->getAssociatedObjects<xAOD::TruthParticle>(labelC, ghostC);

      // initialize
      (*m_ghostB_pt)(*truthJet) = {};
      (*m_ghostB_eta)(*truthJet) = {};
      (*m_ghostB_phi)(*truthJet) = {};
      (*m_ghostB_m)(*truthJet) = {};
      (*m_ghostB_pdgId)(*truthJet) = {};
      (*m_ghostC_pt)(*truthJet) = {};
      (*m_ghostC_eta)(*truthJet) = {};
      (*m_ghostC_phi)(*truthJet) = {};
      (*m_ghostC_m)(*truthJet) = {};
      (*m_ghostC_pdgId)(*truthJet) = {};

      // fill
      for (auto *tp : ghostB) {
        (*m_ghostB_pt)(*truthJet).push_back(tp->pt());
        (*m_ghostB_eta)(*truthJet).push_back(tp->eta());
        (*m_ghostB_phi)(*truthJet).push_back(tp->phi());
        (*m_ghostB_m)(*truthJet).push_back(tp->m());
        (*m_ghostB_pdgId)(*truthJet).push_back(tp->pdgId());
      }
      for (auto *tp : ghostC) {
        (*m_ghostC_pt)(*truthJet).push_back(tp->pt());
        (*m_ghostC_eta)(*truthJet).push_back(tp->eta());
        (*m_ghostC_phi)(*truthJet).push_back(tp->phi());
        (*m_ghostC_m)(*truthJet).push_back(tp->m());
        (*m_ghostC_pdgId)(*truthJet).push_back(tp->pdgId());
      }
    }
  }
  return StatusCode::SUCCESS;
}
