/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <GaudiKernel/IAlgorithm.h>

#include <xAODEventInfo/EventInfo.h>

#include <CharmAnalysis/AthEventFilterAlg.h>

#include <regex>

//
// method implementations
//

namespace CP {

AthEventFilterAlg ::AthEventFilterAlg(const std::string &name,
                                      ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      m_algManager("ApplicationMgr", this->name()) {
  declareProperty("algorithmList", m_algs, "List of algorithms for cutflow.");
}

StatusCode AthEventFilterAlg ::initialize() {
  ATH_CHECK(m_algManager.retrieve());

  // book cutflow histogram
  ANA_CHECK(book(
      TH1F("cutflow", "cutflow", m_algs.size(), 0.5, m_algs.size() + 0.5)));
  for (unsigned int i = 0; i < m_algs.size(); i++) {
    std::string alg_name = m_algs.at(i);
    hist("cutflow")->GetXaxis()->SetBinLabel(i + 1, alg_name.c_str());
  }

  return StatusCode::SUCCESS;
}

StatusCode AthEventFilterAlg ::execute() {
  // Event info
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
  m_is_mc = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);

  bool passed = true;
  for (unsigned int i = 0; i < m_algs.size(); i++) {
    std::string alg_name = m_algs.at(i);
    IAlgorithm *alg = nullptr;
    ATH_MSG_DEBUG("Attempting to retrive " << alg_name);
    ATH_CHECK(m_algManager->getAlgorithm(alg_name, alg));
    bool algPassed = alg->isExecuted() && alg->filterPassed();
    if (!algPassed) {
      ATH_MSG_DEBUG("Alg " << alg_name << " rejected event.");
      passed = false;
      break;
    } else {
      ATH_MSG_DEBUG("Alg " << alg_name << " passed event.");
      hist("cutflow")->Fill(i + 1, 1);
    }
  }
  if (!passed) {
    if (!m_is_mc) {
      ATH_MSG_DEBUG("Event rejected.");
      setFilterPassed(false);
    } else {
      ATH_MSG_DEBUG("Event rejected.");
      setFilterPassed(false);
    }
  } else {
    ATH_MSG_DEBUG("Event passed.");
  }

  return StatusCode::SUCCESS;
}

} // namespace CP
