/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <xAODTracking/Vertex.h>
#include <xAODTracking/VertexContainer.h>

#include <CharmAnalysis/TrackDecoratorAlg.h>

//
// method implementations
//

TrackDecoratorAlg ::TrackDecoratorAlg(const std::string &name,
                                      ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      m_trackToVertexIPEstimator(
          "Trk::TrackToVertexIPEstimator/TrackToVertexIPEstimator"),
      m_trackOriginTool("InDet::InDetTrackTruthOriginTool") {
  declareProperty("primaryVertexKey", m_primaryVertexKey = "PrimaryVertices");
  declareProperty("trackOriginTool", m_trackOriginTool);
}

StatusCode TrackDecoratorAlg ::initialize() {
  ANA_CHECK(m_trackToVertexIPEstimator.retrieve());
  ATH_CHECK(m_trackOriginTool.retrieve());

  ANA_CHECK(m_tracksHandle.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());
  ANA_CHECK(m_preselection.initialize());

  // Decorators
  m_ptDecorator = std::make_unique<SG::AuxElement::Decorator<float>>("pt");
  m_etaDecorator = std::make_unique<SG::AuxElement::Decorator<float>>("eta");
  m_z0sinThetaDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>("z0sinTheta");
  m_d0sigDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>("d0sig");
  m_d0Decorator = std::make_unique<SG::AuxElement::Decorator<float>>("d0");
  m_d0sigPVDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>("d0sigPV");
  m_d0PVDecorator = std::make_unique<SG::AuxElement::Decorator<float>>("d0PV");
  m_passLooseDecorator =
      std::make_unique<SG::AuxElement::Decorator<bool>>("passLoose");
  m_passTightDecorator =
      std::make_unique<SG::AuxElement::Decorator<bool>>("passTight");
  m_trackOrigin =
      std::make_unique<SG::AuxElement::Decorator<int>>("trackOrigin");

  // Selection Decorators
  ANA_CHECK(makeSelectionReadAccessor(m_selectionDecorationLoose,
                                      m_selectionAccessorLoose));
  ANA_CHECK(makeSelectionReadAccessor(m_selectionDecorationTight,
                                      m_selectionAccessorTight));

  return StatusCode::SUCCESS;
}

StatusCode TrackDecoratorAlg ::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    // Primary vertex
    const xAOD::Vertex *primaryVertex = nullptr;
    const xAOD::VertexContainer *primaryVertices = nullptr;
    CHECK(evtStore()->retrieve(primaryVertices, m_primaryVertexKey));
    for (const xAOD::Vertex *vertex : *primaryVertices) {
      if (vertex->vertexType() == xAOD::VxType::PriVtx) {
        // The default "PrimaryVertex" container is ordered in
        // sum-pt, and the tracking group recommends to pick the one
        // with the maximum sum-pt, so this will do the right thing.
        // If the user needs a different primary vertex, he needs to
        // provide a reordered primary vertex container and point
        // this algorithm to it. Currently there is no central
        // algorithm to do that, so users will have to write their
        // own (15 Aug 18).
        if (primaryVertex == nullptr) {
          primaryVertex = vertex;
          break;
        }
      }
    }
    if (!primaryVertex) {
      ATH_MSG_WARNING("No primary vertex found");
      return StatusCode::SUCCESS;
    }

    const xAOD::TrackParticleContainer *tracks = nullptr;
    ANA_CHECK(m_tracksHandle.retrieve(tracks, sys));
    for (const xAOD::TrackParticle *track : *tracks) {
      (*m_ptDecorator)(*track) = track->pt();
      (*m_etaDecorator)(*track) = track->eta();
      (*m_passLooseDecorator)(*track) =
          m_selectionAccessorLoose->getBool(*track);
      (*m_passTightDecorator)(*track) =
          m_selectionAccessorTight->getBool(*track);

      // z0sinTheta
      const double vertex_z = primaryVertex ? primaryVertex->z() : 0;
      const float deltaZ0SinTheta =
          (track->z0() + track->vz() - vertex_z) * sin(track->p4().Theta());
      (*m_z0sinThetaDecorator)(*track) = deltaZ0SinTheta;

      // d0sig
      const float d0sig = xAOD::TrackingHelpers::d0significance(
          track, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(),
          eventInfo->beamPosSigmaXY());
      (*m_d0sigDecorator)(*track) = d0sig;
      (*m_d0Decorator)(*track) = track->d0();

      // d0sig vs PV
      const Trk::ImpactParametersAndSigma *ipAndSigma =
          m_trackToVertexIPEstimator->estimate(track, primaryVertex);
      if (ipAndSigma != NULL) {
        (*m_d0sigPVDecorator)(*track) = ipAndSigma->IPd0 / ipAndSigma->sigmad0;
        (*m_d0PVDecorator)(*track) = ipAndSigma->IPd0;
        delete ipAndSigma;
      } else {
        (*m_d0sigPVDecorator)(*track) = 999;
        (*m_d0PVDecorator)(*track) = 999;
      }

      // track origin
      int origin = -1;
      if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
        origin = m_trackOriginTool->getTrackOrigin(track);
      }
      (*m_trackOrigin)(*track) = origin;
    }
  }
  return StatusCode::SUCCESS;
}
