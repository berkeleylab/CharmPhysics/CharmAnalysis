/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <xAODTracking/Vertex.h>
#include <xAODTracking/VertexContainer.h>

#include <CharmAnalysis/MuonDecoratorAlg.h>

//
// method implementations
//

MuonDecoratorAlg ::MuonDecoratorAlg(const std::string &name,
                                    ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      m_trackToVertexIPEstimator(
          "Trk::TrackToVertexIPEstimator/TrackToVertexIPEstimator") {
  declareProperty("extraVars", m_extra_vars = false);
  declareProperty("primaryVertexKey", m_primaryVertexKey = "PrimaryVertices");
}

StatusCode MuonDecoratorAlg ::initialize() {
  m_z0sinThetaDecorator = std::make_unique<SG::AuxElement::Decorator<float>>(
      m_z0sinThetaDecoration);
  m_d0sigDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_d0sigDecoration);
  m_d0Decorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_d0Decoration);
  m_d0sigPVDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_d0sigPVDecoration);
  m_d0PVDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_d0PVDecoration);

  ANA_CHECK(m_muonsHandle.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());
  ANA_CHECK(m_preselection.initialize());
  return StatusCode::SUCCESS;
}

StatusCode MuonDecoratorAlg ::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    // Primary vertex
    const xAOD::Vertex *primaryVertex = nullptr;
    const xAOD::VertexContainer *primaryVertices = nullptr;
    CHECK(evtStore()->retrieve(primaryVertices, m_primaryVertexKey));
    for (const xAOD::Vertex *vertex : *primaryVertices) {
      if (vertex->vertexType() == xAOD::VxType::PriVtx) {
        // The default "PrimaryVertex" container is ordered in
        // sum-pt, and the tracking group recommends to pick the one
        // with the maximum sum-pt, so this will do the right thing.
        // If the user needs a different primary vertex, he needs to
        // provide a reordered primary vertex container and point
        // this algorithm to it. Currently there is no central
        // algorithm to do that, so users will have to write their
        // own (15 Aug 18).
        if (primaryVertex == nullptr) {
          primaryVertex = vertex;
          break;
        }
      }
    }
    if (!primaryVertex) {
      ATH_MSG_WARNING("No primary vertex found");
    }

    const xAOD::MuonContainer *muons = nullptr;
    ANA_CHECK(m_muonsHandle.retrieve(muons, sys));
    for (const xAOD::Muon *muon : *muons) {
      // get track
      const xAOD::TrackParticle *track = muon->primaryTrackParticle();

      // z0sinTheta
      const double vertex_z = primaryVertex ? primaryVertex->z() : 0;
      const float deltaZ0SinTheta =
          (track->z0() + track->vz() - vertex_z) * sin(muon->p4().Theta());
      (*m_z0sinThetaDecorator)(*muon) = deltaZ0SinTheta;

      // d0sig
      const float d0sig = xAOD::TrackingHelpers::d0significance(
          track, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(),
          eventInfo->beamPosSigmaXY());
      (*m_d0sigDecorator)(*muon) = d0sig;
      (*m_d0Decorator)(*muon) = track->d0();

      if (m_extra_vars) {
        // d0sig vs PV
        const Trk::ImpactParametersAndSigma *ipAndSigma =
            m_trackToVertexIPEstimator->estimate(track, primaryVertex);
        if (ipAndSigma != NULL) {
          (*m_d0sigPVDecorator)(*muon) = ipAndSigma->IPd0 / ipAndSigma->sigmad0;
          (*m_d0PVDecorator)(*muon) = ipAndSigma->IPd0;
          delete ipAndSigma;
        } else {
          (*m_d0sigPVDecorator)(*muon) = 999;
          (*m_d0PVDecorator)(*muon) = 999;
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}
