/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

/// @author Cesar Gonzalez Renteria, based on Run 1 code from Lea Caminada
/// and WeWmSelectionAlg.cxx written by Marjorie Shapiro

//
// includes
//

#include <CharmAnalysis/Definitions.h>
#include <CharmAnalysis/ZSelectionAlg.h>

#include <TrkTrack/Track.h>

//
// method implementations
//

ZSelectionAlg ::ZSelectionAlg(const std::string &name, ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator), m_electrons(0), m_muons(0),
      m_requireTightElectron(false), m_minPt(15.0 * CLHEP::GeV),
      m_minZMass(66.0 * CLHEP::GeV), m_maxZMass(116.0 * CLHEP::GeV),
      m_doZmumu(true), m_doZee(true), m_doLeptonVeto(false),
      m_enableFilter(true), m_eventsSeen(0), m_eventsPass(0), m_ZmumuPass(0),
      m_ZmumuCand(0), m_ZeePass(0), m_ZeeCand(0) {
  declareProperty("requireTightElectron", m_requireTightElectron);
  declareProperty("minPt", m_minPt);
  declareProperty("minZMass", m_minZMass);
  declareProperty("maxZMass", m_maxZMass);
  declareProperty("doZmumu", m_doZmumu);
  declareProperty("doZee", m_doZee);
  declareProperty("doLeptonVeto", m_doLeptonVeto);
  declareProperty("enableFilter", m_enableFilter);
}

StatusCode ZSelectionAlg ::initialize() {

  ANA_CHECK(m_electronsHandle.initialize(m_systematicsList));
  ANA_CHECK(m_muonsHandle.initialize(m_systematicsList));
  ANA_CHECK(m_compositesHandle.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());

  ANA_CHECK(initCutflowHistograms());
  // initialize decorators
  m_decayType = std::make_unique<SG::AuxElement::Decorator<int>>("decayType");

  ATH_MSG_INFO("Initialize ZSelectionAlg");
  return StatusCode::SUCCESS;
}

StatusCode ZSelectionAlg ::execute() {
  ++m_eventsSeen;
  bool OKOK = false;

  for (const auto &sys : m_systematicsList.systematicsVector()) {
    // Event info
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    // Electrons
    ANA_CHECK(m_electronsHandle.retrieve(m_electrons, sys));

    // Muons
    ANA_CHECK(m_muonsHandle.retrieve(m_muons, sys));

    // output composite particle container
    auto composites = std::make_unique<xAOD::CompositeParticleContainer>();
    auto composites_aux =
        std::make_unique<xAOD::CompositeParticleAuxContainer>();
    composites->setStore(composites_aux.get());

    // Reconstruct Z-->mumu candidates
    if (m_doZmumu && findZmumu(composites.get()))
      OKOK = true;

    // Reconstruct Z-->ee candidates
    if (m_doZee && findZee(composites.get()))
      OKOK = true;

    // decorate composite particles
    for (const xAOD::CompositeParticle *cp : *composites.get()) {
      decorateCompositeParticle(cp);
    }

    // record composite particle container
    ANA_CHECK(m_compositesHandle.record(std::move(composites),
                                        std::move(composites_aux), sys));

    if (OKOK)
      m_eventsPass++;
    if (m_enableFilter) {
      setFilterPassed(OKOK);
    } else {
      setFilterPassed(true);
    }
  }
  return StatusCode::SUCCESS;
}

bool ZSelectionAlg::findZee(xAOD::CompositeParticleContainer *composites) {

  std::vector<xAOD::Electron> electrons;

  int CutState = 0;
  m_hcutsZee->Fill(CutState);
  ++CutState;
  if (m_electrons->empty())
    return false;

  // Apply veto on leptons != 2 if requested
  if (m_doLeptonVeto && m_electrons->size() != 2)
    return false;
  m_hcutsZee->Fill(CutState);
  ++CutState;

  int nPassLepton = 0;

  for (unsigned int i = 0; i < m_electrons->size(); i++) {
    // Require minimum pT for the first electron
    if (m_electrons->at(i)->pt() < m_minPt)
      continue;

    const TLorentzVector e4vect1 = m_electrons->at(i)->p4();

    for (unsigned int j = i + 1; j < m_electrons->size(); j++) {

      // Require minimum pT for the second electron
      if (m_electrons->at(j)->pt() < m_minPt)
        continue;

      // ANA_MSG_INFO("FCTight Bool = " <<
      // m_electrons->at(i)->auxdecor<bool>("isIsolated_FCTight"));

      // ANA_MSG_INFO("e1 = " << m_electrons->at(i)->charge());
      // ANA_MSG_INFO("e2 = " << m_electrons->at(j)->charge());
      // ANA_MSG_INFO("e1 * e2 = " << m_electrons->at(i)->charge() *
      // m_electrons->at(j)->charge());

      // Require both leptons to be opposite sign
      if (m_electrons->at(i)->charge() * m_electrons->at(j)->charge() > 0.)
        continue;
      m_hcutsZee->Fill(CutState);
      ++CutState;
      nPassLepton++;

      // We have a Z candidate.  Save it
      xAOD::CompositeParticle *Zee = new xAOD::CompositeParticle;

      const TLorentzVector e4vect2 = m_electrons->at(j)->p4();

      const TLorentzVector Z4vect = e4vect1 + e4vect2;

      if (Z4vect.M() < m_minZMass || Z4vect.M() > m_maxZMass)
        continue;
      m_hcutsZee->Fill(CutState);
      ++CutState;

      // Z Boson passes all cuts. Add to Container
      composites->push_back(Zee);

      Zee->addPart(m_electrons->at(i));
      Zee->addPart(m_electrons->at(j));
      Zee->setPdgId(23);
      Zee->setP4(Z4vect);
      (*m_decayType)(*Zee) = 11;

    } // end of second lepton loop
  }   // end of first lepton loop

  // iterate through Leptons
  if (!nPassLepton)
    return false;
  m_hcutsZee->Fill(CutState);
  ++CutState;
  m_ZeePass++;
  return true;
}

bool ZSelectionAlg::findZmumu(xAOD::CompositeParticleContainer *composites) {

  int CutState = 0;
  m_hcutsZmumu->Fill(CutState);
  ++CutState;
  if (m_muons->empty())
    return false;

  // Apply veto on leptons != 2 if requested
  if (m_doLeptonVeto && m_muons->size() != 2)
    return false;
  m_hcutsZmumu->Fill(CutState);
  ++CutState;

  int nPassLepton = 0;

  for (unsigned int i = 0; i < m_muons->size(); i++) {
    // Require minimum pT for the first muon
    if (m_muons->at(i)->pt() < m_minPt)
      continue;

    const TLorentzVector mu4vect1 = m_muons->at(i)->p4();

    for (unsigned int j = i + 1; j < m_muons->size(); j++) {

      // Require minimum pT for the second muon
      if (m_muons->at(j)->pt() < m_minPt)
        continue;

      // Require both leptons to be opposite sign
      if (m_muons->at(i)->charge() * m_muons->at(j)->charge() > 0.)
        continue;
      m_hcutsZmumu->Fill(CutState);
      ++CutState;
      nPassLepton++;

      // We have a Z candidate.  Save it
      xAOD::CompositeParticle *Zmumu = new xAOD::CompositeParticle;

      TLorentzVector mu4vect2 = m_muons->at(j)->p4();

      TLorentzVector Z4vect = mu4vect1 + mu4vect2;

      if (Z4vect.M() < m_minZMass || Z4vect.M() > m_maxZMass)
        continue;
      m_hcutsZmumu->Fill(CutState);
      ++CutState;

      // Z Boson has passed all cuts. Add to Container
      composites->push_back(Zmumu);

      Zmumu->addPart(m_muons->at(i));
      Zmumu->addPart(m_muons->at(j));
      Zmumu->setPdgId(23);
      Zmumu->setP4(Z4vect);
      (*m_decayType)(*Zmumu) = 13;

    } // end of second lepton loop
  }   // end of first lepton loop

  // iterate through Leptons
  if (!nPassLepton)
    return false;
  m_hcutsZmumu->Fill(CutState);
  ++CutState;
  m_ZmumuPass++;
  return true;
}

StatusCode ZSelectionAlg::initCutflowHistograms() {

  int ncuts = 5;

  std::string theName = name();
  std::string name = theName + "_Ztomumu";
  ANA_CHECK(book(
      TH1I(name.c_str(), "Z#rightarrow #mu#mu Selection", ncuts, 0, ncuts)));
  m_hcutsZmumu = (TH1I *)hist(name);
  m_hcutsZmumu->GetYaxis()->SetTitle("entries");
  m_hcutsZmumu->GetXaxis()->SetBinLabel(1, "all");
  m_hcutsZmumu->GetXaxis()->SetBinLabel(2, "nLeptons");
  m_hcutsZmumu->GetXaxis()->SetBinLabel(3, "OppositeCharge");
  m_hcutsZmumu->GetXaxis()->SetBinLabel(4, "InZMassWindow");
  m_hcutsZmumu->GetXaxis()->SetBinLabel(5, "GoodLepton");

  name = theName + "_Ztoee";
  ANA_CHECK(
      book(TH1I(name.c_str(), "Z#rightarrow ee Selection", ncuts, 0, ncuts)));
  m_hcutsZee = (TH1I *)hist(name);
  m_hcutsZee->GetYaxis()->SetTitle("entries");
  m_hcutsZee->GetXaxis()->SetBinLabel(1, "all");
  m_hcutsZee->GetXaxis()->SetBinLabel(2, "nLeptons");
  m_hcutsZee->GetXaxis()->SetBinLabel(3, "OppositeCharge");
  m_hcutsZee->GetXaxis()->SetBinLabel(4, "InZMassWindow");
  m_hcutsZee->GetXaxis()->SetBinLabel(5, "GoodLepton");

  return StatusCode::SUCCESS;
}

StatusCode ZSelectionAlg::finalize() {
  ATH_MSG_INFO("Events Processed: "
               << m_eventsSeen << "\n Events Passed: " << m_eventsPass
               << "\n Z to mumu Events Passed: " << m_ZmumuPass << ","
               << "\n Z to ee  Events Passed : " << m_ZeePass << ","
               << "\n Z to mumu Candidates: " << m_ZmumuCand << ","
               << "\n Z to ee  Candidates: " << m_ZeeCand);
  return StatusCode::SUCCESS;
}

void ZSelectionAlg ::decorateCompositeParticle(
    const xAOD::CompositeParticle *cp) {
  /* Not entirely sure why this is needed..
     Should be there by default but it is not.
  */
  cp->auxdecor<float>("eta") = cp->eta();
  cp->auxdecor<float>("phi") = cp->phi();
  cp->auxdecor<float>("pt") = cp->pt();
  cp->auxdecor<float>("m") = cp->m();
  cp->auxdecor<int>("pdgId") = cp->pdgId();
}
