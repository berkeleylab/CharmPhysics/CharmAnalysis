/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include "xAODBTagging/BTagging.h"

#include <CharmAnalysis/JetDecoratorAlg.h>

//
// method implementations
//

JetDecoratorAlg ::JetDecoratorAlg(const std::string &name,
                                  ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator), m_btagSelTool("", this) {
  declareProperty("extraVars", m_extraVars = false);
  declareProperty("btagSelTool", m_btagSelTool);
  declareProperty("TaggerName", m_taggerName);
  declareProperty("quantileDecoration", m_quantileDecoration);
  declareProperty("DL1puDecoration", m_DL1puDecoration);
  declareProperty("DL1pbDecoration", m_DL1pbDecoration);
  declareProperty("DL1pcDecoration", m_DL1pcDecoration);
  declareProperty("JetFitter_nVTX", m_JetFitterNVTXDecoration);
  declareProperty("JetFitter_mass", m_JetFitterMassDecoration);
  declareProperty("JetFitter_nTracksAtVtx", m_JetFitterNTracksAtVtxDecoration);
  declareProperty("SV1_NGTinSvx", m_SV1NGTinSvxDecoration);
  declareProperty("SV1_mass", m_SV1massvtxDecoration);
  declareProperty("MV2c100Decoration", m_MV2c100Decoration = "MV2c100");
  declareProperty("MV2cl100Decoration", m_MV2cl100Decoration = "MV2cl100");
}

StatusCode JetDecoratorAlg ::initialize() {

  ANA_MSG_INFO("Initializing JetDecoratorAlg");

  // Setup the container handling
  ANA_CHECK(m_jetsHandle.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());
  ANA_CHECK(m_preselection.initialize());

  // Retreive the tool
  if (m_taggerName.size()) {
    ANA_CHECK(m_btagSelTool.retrieve());
    // Decoration name
    if (m_quantileDecoration.empty()) {
      ANA_MSG_ERROR("quantileDecoration decoration should not be empty.");
      return StatusCode::FAILURE;
    }
  }

  // Decorators
  m_quantileDecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<int>>(m_quantileDecoration);
  m_DL1puDecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_DL1puDecoration);
  m_DL1pbDecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_DL1pbDecoration);
  m_DL1pcDecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_DL1pcDecoration);
  m_JetFitterNVTXDecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<int>>(
          m_JetFitterNVTXDecoration);
  m_JetFitterNTracksAtVtxDecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<int>>(
          m_JetFitterNTracksAtVtxDecoration);
  m_JetFitterMassDecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(
          m_JetFitterMassDecoration);
  m_SV1NGTinSvxDecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<int>>(m_SV1NGTinSvxDecoration);
  m_SV1massvtxDecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(
          m_SV1massvtxDecoration);
  m_MV2c100DecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_MV2c100Decoration);
  m_MV2cl100DecorationDecorator =
      std::make_unique<SG::AuxElement::Decorator<float>>(m_MV2cl100Decoration);

  return StatusCode::SUCCESS;
}

StatusCode JetDecoratorAlg ::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {

    // Retrieve jet container
    const xAOD::JetContainer *jets = nullptr;
    ANA_MSG_DEBUG("Retrieving jet container: " << m_jetsHandle.getName(sys));

    ANA_CHECK(m_jetsHandle.retrieve(jets, sys));
    for (const xAOD::Jet *jet : *jets) {

      // b-tagging link
      const xAOD::BTagging *btag = jet->btagging();

      if (m_taggerName.size()) {
        // Decorate jet
        int quantile = m_btagSelTool->getQuantile(*jet);
        (*m_quantileDecorationDecorator)(*jet) = quantile;

        // DL1 discriminants
        if (m_taggerName.find("DL1") != std::string::npos) {
          // get DL1 variables
          double dl1_pu(-999.);
          double dl1_pb(-999.);
          double dl1_pc(-999.);
          if ((!btag->pu(m_taggerName, dl1_pu)) ||
              (!btag->pb(m_taggerName, dl1_pb)) ||
              (!btag->pc(m_taggerName, dl1_pc))) {
          }
          (*m_DL1puDecorationDecorator)(*jet) = dl1_pu;
          (*m_DL1pbDecorationDecorator)(*jet) = dl1_pb;
          (*m_DL1pcDecorationDecorator)(*jet) = dl1_pc;
        }
      }

      if (m_extraVars) {
        // MV2c discriminants
        double weight_mv2cl100(-999.);
        double weight_mv2c100(-999.);
        int nTracksJF = -1;
        int nVTXJF = -1;
        float massJF = -1.;
        int nTracksSV1 = -1;
        float massSV1 = -1.;
        if ((!btag->MVx_discriminant("MV2c100", weight_mv2c100)) ||
            (!btag->MVx_discriminant("MV2cl100", weight_mv2cl100))) {
        }
        (*m_MV2c100DecorationDecorator)(*jet) = weight_mv2c100;
        (*m_MV2cl100DecorationDecorator)(*jet) = weight_mv2cl100;

        if (btag) {
          btag->taggerInfo(nVTXJF, xAOD::JetFitter_nVTX);
          btag->taggerInfo(nTracksJF, xAOD::JetFitter_nTracksAtVtx);
          btag->taggerInfo(massJF, xAOD::JetFitter_mass);
          btag->taggerInfo(nTracksSV1, xAOD::SV1_NGTinSvx);
          btag->taggerInfo(massSV1, xAOD::SV1_masssvx);
        }
        (*m_JetFitterNVTXDecorationDecorator)(*jet) = nVTXJF;
        (*m_JetFitterNTracksAtVtxDecorationDecorator)(*jet) = nTracksJF;
        (*m_JetFitterMassDecorationDecorator)(*jet) = massJF;
        (*m_SV1NGTinSvxDecorationDecorator)(*jet) = nTracksSV1;
        (*m_SV1massvtxDecorationDecorator)(*jet) = massSV1;
      }

    } // loop over jets
  }
  return StatusCode::SUCCESS;
}
