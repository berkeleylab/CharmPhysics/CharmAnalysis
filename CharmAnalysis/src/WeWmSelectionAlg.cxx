/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

/// @author Marjorie Shapiro, based on Run 1 code from Lea Caminada

//
// includes
//

#include <CharmAnalysis/Definitions.h>
#include <CharmAnalysis/WeWmSelectionAlg.h>

#include <TrkTrack/Track.h>

//
// method implementations
//

WeWmSelectionAlg ::WeWmSelectionAlg(const std::string &name,
                                    ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      //      m_tracks(0),
      m_electrons(0), m_muons(0), m_MET(0), m_invertCuts(false),
      m_requireTightElectron(false), m_minPt(25.0 * CLHEP::GeV),
      m_minEtMiss(25.0 * CLHEP::GeV), m_minMt(50.0 * CLHEP::GeV), m_doWm(true),
      m_doWe(true), m_doLeptonVeto(false), m_enableFilter(true),
      m_eventsSeen(0), m_eventsPass(0), m_wmuPass(0), m_wmuCand(0), m_wePass(0),
      m_weCand(0) {
  declareProperty("invertCuts", m_invertCuts);
  declareProperty("requireTightElectron", m_requireTightElectron);
  declareProperty("minPt", m_minPt);
  declareProperty("minEtmiss", m_minEtMiss);
  declareProperty("minMt", m_minMt);
  declareProperty("doWm", m_doWm);
  declareProperty("doWe", m_doWe);
  declareProperty("doLeptonVeto", m_doLeptonVeto);
  declareProperty("enableFilter", m_enableFilter);
  declareProperty("MetComponent", m_MetComponent = "Final");
}

StatusCode WeWmSelectionAlg ::initialize() {

  ANA_CHECK(m_electronsHandle.initialize(m_systematicsList));
  ANA_CHECK(m_muonsHandle.initialize(m_systematicsList));
  ANA_CHECK(m_compositesHandle.initialize(m_systematicsList));
  ANA_CHECK(m_metHandle.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());

  ANA_CHECK(initCutflowHistograms());
  // initialize decorators
  m_decayType = std::make_unique<SG::AuxElement::Decorator<int>>("decayType");

  ATH_MSG_INFO("Initialize WeWmSelectionAlg");
  return StatusCode::SUCCESS;
}

StatusCode WeWmSelectionAlg ::execute() {
  ++m_eventsSeen;
  bool OKOK = false;

  for (const auto &sys : m_systematicsList.systematicsVector()) {
    // Event info
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    // Tracks
    //            ANA_CHECK(m_tracksHandle.retrieve(m_tracks, sys));

    // Electrons
    ANA_CHECK(m_electronsHandle.retrieve(m_electrons, sys));

    // Muons
    ANA_CHECK(m_muonsHandle.retrieve(m_muons, sys));

    // Missing Et
    //            CHECK(evtStore()->retrieve(m_MET,
    //            "MET_Reference_AntiKt4EMPFlow"));
    ANA_CHECK(m_metHandle.retrieve(m_MET, sys));

    // output composite particle container
    auto composites = std::make_unique<xAOD::CompositeParticleContainer>();
    auto composites_aux =
        std::make_unique<xAOD::CompositeParticleAuxContainer>();
    composites->setStore(composites_aux.get());

    // Reconstruct W-->munu candidates
    if (m_doWm && findWmunu(composites.get()))
      OKOK = true;

    // Reconstruct W-->enu candidates
    if (m_doWe && findWenu(composites.get()))
      OKOK = true;

    // decorate composite particles
    for (const xAOD::CompositeParticle *cp : *composites.get()) {
      decorateCompositeParticle(cp);
    }

    // record composite particle container
    ANA_CHECK(m_compositesHandle.record(std::move(composites),
                                        std::move(composites_aux), sys));

    if (OKOK)
      m_eventsPass++;
    if (m_enableFilter) {
      setFilterPassed(OKOK);
    } else {
      setFilterPassed(true);
    }
  }
  return StatusCode::SUCCESS;
}

bool WeWmSelectionAlg::findWenu(xAOD::CompositeParticleContainer *composites) {

  int CutState = 0;
  m_hcutsWe->Fill(CutState);
  ++CutState;
  if (m_electrons->empty())
    return false;

  // Apply veto on multiple leptons if requested
  if (m_doLeptonVeto && m_electrons->size() != 1)
    return false;
  m_hcutsWe->Fill(CutState);
  ++CutState;

  int nPassLepton = 0;
  int nPassMT = 0;

  // Impose missing Et requirements.  Inverted cuts on MET used for background
  // studies
  /*
  for (xAOD::MissingETContainer::const_iterator it=m_MET->begin();
  it!=m_MET->end(); it++) { std::cout << "I found MET nane " << (*it)->name() <<
  std::endl;
  }
  */
  ANA_CHECK((*m_MET)[m_MetComponent] != nullptr);

  const xAOD::MissingET *missingEt = (*m_MET)[m_MetComponent];

  if (!m_invertCuts) {
    if (missingEt->met() < m_minEtMiss)
      return false;
  } else if (missingEt->met() > m_minEtMiss)
    return false;
  m_hcutsWe->Fill(CutState);
  ++CutState;

  for (const auto *electron : *m_electrons) {

    // Require minimum pT for the muon
    if (electron->pt() < m_minPt)
      continue;
    nPassLepton++;

    double dphi = missingEt->phi() - electron->phi();
    while (dphi >= TMath::Pi()) {
      dphi -= 2 * TMath::Pi();
    }
    while (dphi < -TMath::Pi()) {
      dphi += 2 * TMath::Pi();
    }
    double mt = sqrt(2 * electron->pt() * (missingEt->met()) * (1 - cos(dphi)));

    if (!m_invertCuts) {
      if (!(m_minMt < mt))
        continue;
    } else {
      if (!(mt <= m_minMt))
        continue;
    }

    ++nPassMT;

    // We have a W candidate.  Save it
    xAOD::CompositeParticle *We = new xAOD::CompositeParticle;
    composites->push_back(We);

    TLorentzVector e4vect;
    // Set pz=0 so we get the transverse mass.
    // Should we use muon mass here?  Make it zero to agree with defn above
    e4vect.SetPtEtaPhiM(electron->pt(), 0.0, electron->phi(), 0.0);

    TLorentzVector nu4vect;
    nu4vect.SetPtEtaPhiM(missingEt->met(), 0.0, missingEt->phi(), 0.0);

    TLorentzVector W4vect = e4vect + nu4vect;

    We->addPart(electron);
    We->setCharge(electron->charge());
    We->setPdgId((electron->charge()) ? 24 : -24);
    We->setP4(W4vect);
    (*m_decayType)(*We) = 11;

  } // iterate through muons
  if (!nPassLepton)
    return false;
  m_hcutsWe->Fill(CutState);
  ++CutState;
  if (!nPassMT)
    return false;
  m_hcutsWe->Fill(CutState);
  ++CutState;

  m_wePass++;
  return true;
}

bool WeWmSelectionAlg::findWmunu(xAOD::CompositeParticleContainer *composites) {

  int CutState = 0;
  m_hcutsWmu->Fill(CutState);
  ++CutState;
  if (m_muons->empty())
    return false;

  // Apply veto on multiple leptons if requested
  if (m_doLeptonVeto && m_muons->size() != 1)
    return false;
  m_hcutsWmu->Fill(CutState);
  ++CutState;

  int nPassLepton = 0;
  int nPassMT = 0;

  // Impose missing Et requirements.  Inverted cuts on MET used for background
  // studies
  /*
  for (xAOD::MissingETContainer::const_iterator it=m_MET->begin();
  it!=m_MET->end(); it++) { std::cout << "I found MET nane " << (*it)->name() <<
  std::endl;
  }
  */
  ANA_CHECK((*m_MET)[m_MetComponent] != nullptr);

  const xAOD::MissingET *missingEt = (*m_MET)[m_MetComponent];

  if (!m_invertCuts) {
    if (missingEt->met() < m_minEtMiss)
      return false;
  } else if (missingEt->met() > m_minEtMiss)
    return false;
  m_hcutsWmu->Fill(CutState);
  ++CutState;

  for (const auto *muon : *m_muons) {

    // Require minimum pT for the muon
    if (muon->pt() < m_minPt)
      continue;
    nPassLepton++;

    double dphi = missingEt->phi() - muon->phi();
    while (dphi >= TMath::Pi()) {
      dphi -= 2 * TMath::Pi();
    }
    while (dphi < -TMath::Pi()) {
      dphi += 2 * TMath::Pi();
    }
    double mt = sqrt(2 * muon->pt() * (missingEt->met()) * (1 - cos(dphi)));

    if (!m_invertCuts) {
      if (!(m_minMt < mt))
        continue;
    } else {
      if (!(mt <= m_minMt))
        continue;
    }

    ++nPassMT;

    // We have a W candidate.  Save it
    xAOD::CompositeParticle *Wm = new xAOD::CompositeParticle;
    composites->push_back(Wm);

    TLorentzVector mu4vect;
    // Set pz=0 so we get the transverse mass.
    // Should we use muon mass here?  Make it zero to agree with defn above
    mu4vect.SetPtEtaPhiM(muon->pt(), 0.0, muon->phi(), 0.0);

    TLorentzVector nu4vect;
    nu4vect.SetPtEtaPhiM(missingEt->met(), 0.0, missingEt->phi(), 0.0);

    TLorentzVector W4vect = mu4vect + nu4vect;

    Wm->addPart(muon);
    Wm->setCharge(muon->charge());
    Wm->setPdgId((muon->charge()) ? 24 : -24);
    Wm->setP4(W4vect);
    (*m_decayType)(*Wm) = 13;

  } // iterate through muons
  if (!nPassLepton)
    return false;
  m_hcutsWmu->Fill(CutState);
  ++CutState;
  if (!nPassMT)
    return false;
  m_hcutsWmu->Fill(CutState);
  ++CutState;

  m_wmuPass++;
  return true;
}

StatusCode WeWmSelectionAlg::initCutflowHistograms() {

  int ncuts = 5;

  std::string theName = name();
  std::string name = theName + "_WtoMuon";
  ANA_CHECK(book(
      TH1I(name.c_str(), "W#rightarrow #mu#nu Selection", ncuts, 0, ncuts)));
  m_hcutsWmu = (TH1I *)hist(name);
  m_hcutsWmu->GetYaxis()->SetTitle("entries");
  m_hcutsWmu->GetXaxis()->SetBinLabel(1, "all");
  m_hcutsWmu->GetXaxis()->SetBinLabel(2, "nLepton");
  m_hcutsWmu->GetXaxis()->SetBinLabel(3, "MET");
  m_hcutsWmu->GetXaxis()->SetBinLabel(4, "GoodLepton");
  m_hcutsWmu->GetXaxis()->SetBinLabel(5, "M_{T}");

  name = theName + "_WtoElectron";
  ANA_CHECK(book(
      TH1I(name.c_str(), "W#rightarrow #mu#nu Selection", ncuts, 0, ncuts)));
  m_hcutsWe = (TH1I *)hist(name);
  m_hcutsWe->GetYaxis()->SetTitle("entries");
  m_hcutsWe->GetXaxis()->SetBinLabel(1, "all");
  m_hcutsWe->GetXaxis()->SetBinLabel(2, "nLepton");
  m_hcutsWe->GetXaxis()->SetBinLabel(3, "MET");
  m_hcutsWe->GetXaxis()->SetBinLabel(4, "GoodLepton");
  m_hcutsWe->GetXaxis()->SetBinLabel(5, "M_{T}");

  return StatusCode::SUCCESS;
}

StatusCode WeWmSelectionAlg::finalize() {
  ATH_MSG_INFO("Events Processed: "
               << m_eventsSeen << "\n Events Passed: " << m_eventsPass
               << "\n W to mu Events Passed: " << m_wmuPass << ","
               << "\n W to e  Event sPassed : " << m_wePass << ","
               << "\n W to mu Candidates: " << m_wmuCand << ","
               << "\n W to e  Candidates: " << m_weCand);
  return StatusCode::SUCCESS;
}

void WeWmSelectionAlg ::decorateCompositeParticle(
    const xAOD::CompositeParticle *cp) {
  /* Not entirely sure why this is needed..
     Should be there by default but it is not.
  */
  cp->auxdecor<float>("eta") = cp->eta();
  cp->auxdecor<float>("phi") = cp->phi();
  cp->auxdecor<float>("pt") = cp->pt();
  cp->auxdecor<float>("m") = cp->m();
  cp->auxdecor<int>("pdgId") = cp->pdgId();
}
