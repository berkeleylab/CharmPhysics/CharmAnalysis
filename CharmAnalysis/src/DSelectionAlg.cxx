/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
 */

/// @author Miha Muskinja
///
/// decayType coding:
///       10: D0 -> K pi
///       20: D0 -> K pi pi0
///       30: D0 -> K pi pi pi
///        1: D* -> D0 (K pi) pi
///        2: D* -> D0 (K pi pi0) pi
///        3: D* -> D0 (K pi pi pi) pi
///        4: D+ -> K pi pi
///        5: Ds/D+ -> Phi pi, Phi-->KK
///        6: LambdaC --> Lambda pi
///        7: LambdaC --> Kshort  p
///        8: Ds --> Kshort  K
///        9: D+ --> Kshort  pi
///       11: 11: LambdaC --> pKpi
///

//
// includes
//

#include <CharmAnalysis/DSelectionAlg.h>
#include <CharmAnalysis/Definitions.h>
#include <CharmAnalysis/TruthUtils.h>
#include <TrkTrack/Track.h>
#include <TrkVKalVrtCore/TrkVKalVrtCore.h>
#include <VxVertex/PrimaryVertexSelector.h>
#include <xAODTracking/Vertex.h>
#include <xAODTracking/VertexContainer.h>
#include <xAODTruth/TruthParticleContainer.h>

//
// method implementations
//

DSelectionAlg ::DSelectionAlg(const std::string &name, ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      m_trackToVertexIPEstimator(
          "Trk::TrackToVertexIPEstimator/TrackToVertexIPEstimator"),
      m_VKVrtFitter(""), m_doApproximateFit(true),
      m_CascadeTools("DerivationFramework::CascadeTools"), m_primaryVertex(0),
      m_LambdaVertex(0), m_LambdaBarVertex(0), m_enableFilter(true),
      m_maxd0D0_Kpi(1 * CLHEP::mm), m_maxd0D0_Kpipi0(10 * CLHEP::mm),
      m_maxd0PiSlow(1 * CLHEP::mm), m_maxd0LambdaCToLambdaPi(1 * CLHEP::mm),
      m_maxd0LambdaCToKsP(1 * CLHEP::mm), m_maxd0DToKsK(1 * CLHEP::mm),
      m_maxd0DtoKsPi(1 * CLHEP::mm), m_maxdM(200 * CLHEP::MeV),
      m_maxDPdRKPi(1.0), m_maxdRD0PiSlow(0.3), m_maxdRKPi(1), m_maxfitChi2D0(5),
      m_maxfitChi2LambdaC(50), m_maxMassD0_Kpi(MASS_D0 + 0.04 * CLHEP::GeV),
      m_maxMassD0_Kpipi0(1.7 * CLHEP::GeV), m_maxMassD0in_Kpi(2.2 * CLHEP::GeV),
      m_maxMassD0in_Kpipi0(2.2 * CLHEP::GeV), m_maxMassDP(2.2 * CLHEP::GeV),
      m_maxMassDPin(2.4 * CLHEP::GeV), m_maxMassLambdaC(2.5 * CLHEP::GeV),
      m_maxMassLambdaCin(2.7 * CLHEP::GeV), m_maxd0LambdaC(1 * CLHEP::mm),
      m_mincosthetastarDP(-0.8), m_mincosthetastarDs(-1.0),
      m_mincosthetastarLambdaC(-1.0), m_maxcosthetastarDP(1.0),
      m_maxcosthetastarDs(0.8), m_maxcosthetastarLambdaC(1.0),
      m_mindM(100 * CLHEP::MeV), m_mindMDP(180 * CLHEP::MeV),
      m_mindMLC(180 * CLHEP::MeV), m_minLxyD0(0),
      m_minMassD0_Kpi(MASS_D0 - 0.04 * CLHEP::GeV),
      m_minMassD0_Kpipi0(1.5 * CLHEP::GeV), m_minMassD0in_Kpi(1.0 * CLHEP::GeV),
      m_minMassD0in_Kpipi0(1.0 * CLHEP::GeV), m_minMassDP(1.6 * CLHEP::GeV),
      m_minMassDPin(1.4 * CLHEP::GeV), m_minMassLambdaC(2.0 * CLHEP::GeV),
      m_minMassLambdaCin(1.9 * CLHEP::GeV), m_minPtD0(5 * CLHEP::GeV),
      m_minTrackPtD0(500 * CLHEP::MeV), m_minTrackPtDP(800 * CLHEP::MeV),
      m_minTrackPtLambdaC(500 * CLHEP::MeV), m_minLxyLambdaC(0.0),
      m_phiwindowDP(8 * CLHEP::MeV), m_phiwindowLC(8 * CLHEP::MeV),
      m_phiwindowDs(8 * CLHEP::MeV), m_minPtDP(5 * CLHEP::GeV),
      m_minPtLambdaC(5 * CLHEP::GeV), m_minLxyDP(0.5 * CLHEP::mm),
      m_minLxyDs(0.5 * CLHEP::mm), m_minLxyLambdaCToLambdaPi(0.0),
      m_minLxyLambdaCToKsP(0.0), m_minLxyDToKsK(0.0), m_minLxyDToKsPi(0.0),
      m_minTrackPtD3(500 * CLHEP::MeV), m_constrV0(true), m_V0_Index(-1) {
  declareProperty("VKVrtFitter", m_VKVrtFitter);
  declareProperty("primaryVertexKey", m_primaryVertexKey);
  declareProperty("LambdaVertexKey", m_LambdaVertexKey = "V0LambdaVertices");
  declareProperty("LambdaBarVertexKey",
                  m_LambdaBarVertexKey = "V0LambdabarVertices");
  declareProperty("KshortVertexKey", m_KSVertexKey = "V0KshortVertices");
  declareProperty("constraintType", m_constraintType);
  declareProperty("doApproximateFit", m_doApproximateFit);
  declareProperty("enableFilter", m_enableFilter);

  // bools
  declareProperty("doDplus", m_doDplus = true);
  declareProperty("doDsubs", m_doDsubs = true);
  declareProperty("doD0Kpi", m_doD0Kpi = true);
  declareProperty("doD0K2pi", m_doD0K2pi = true);
  declareProperty("doD0K3pi", m_doD0K3pi = true);
  declareProperty("doD0K3pi_only", m_doD0K3pi_only = false);
  declareProperty("doLambdaCLambdaPi", m_doLambdaCLambdaPi = false);
  declareProperty("doLambdaCKshortPi", m_doLambdaCKshortP = false);
  declareProperty("doKshortK", m_doKshortK = false);
  declareProperty("doKshortPi", m_doKshortpi = false);
  declareProperty("doTruthMatching", m_doTruthMathing = true);
  declareProperty("doLambdaC", m_doLambdaC = true);
  // constraints
  declareProperty("Maxd0D0_Kpi", m_maxd0D0_Kpi);
  declareProperty("Maxd0D0_Kpipi0", m_maxd0D0_Kpipi0);
  declareProperty("Maxd0PiSlow", m_maxd0PiSlow);
  declareProperty("Maxd0LambdaCtoLambdaPi", m_maxd0LambdaCToLambdaPi);
  declareProperty("Maxd0LambaCtoKsP", m_maxd0LambdaCToKsP);
  declareProperty("Maxd0DToKsK", m_maxd0DToKsK);
  declareProperty("Maxd0DToKsPi", m_maxd0DtoKsPi);
  declareProperty("MaxdM", m_maxdM);
  declareProperty("MaxDPdRKPi", m_maxDPdRKPi);
  declareProperty("MaxdRD0PiSlow", m_maxdRD0PiSlow);
  declareProperty("MaxDRKPi", m_maxdRKPi);
  declareProperty("MaxfitChi2D0", m_maxfitChi2D0);
  declareProperty("MaxfitChi2LambdaC", m_maxfitChi2LambdaC);
  declareProperty("MaxMassD0_Kpi", m_maxMassD0_Kpi);
  declareProperty("MaxMassD0_Kpipi0", m_maxMassD0_Kpipi0);
  declareProperty("MaxMassD0in_Kpi", m_maxMassD0in_Kpi);
  declareProperty("MaxMassD0in_Kpipi0", m_maxMassD0in_Kpipi0);
  declareProperty("MaxMassDP", m_maxMassDP);
  declareProperty("MaxMassDPin", m_maxMassDPin);
  declareProperty("MaxMassLambdaC", m_maxMassLambdaC);
  declareProperty("MaxMassLambdaCin", m_maxMassLambdaCin);
  declareProperty("MincosthetastarDP", m_mincosthetastarDP);
  declareProperty("MincosthetastarDs", m_mincosthetastarDs);
  declareProperty("MincosthetastarLambdaC", m_mincosthetastarLambdaC);
  declareProperty("MaxcosthetastarDP", m_maxcosthetastarDP);
  declareProperty("MaxcosthetastarDs", m_maxcosthetastarDs);
  declareProperty("MaxcosthetastarLambdaC", m_maxcosthetastarLambdaC);
  declareProperty("MindM", m_mindM);
  declareProperty("MindMDP", m_mindMDP);
  declareProperty("MindMLC", m_mindMLC);
  declareProperty("MinLxyD0", m_minLxyD0);
  declareProperty("MinMassD0_Kpi", m_minMassD0_Kpi);
  declareProperty("MinMassD0_Kpipi0", m_minMassD0_Kpipi0);
  declareProperty("MinMassD0in_Kpi", m_minMassD0in_Kpi);
  declareProperty("MinMassD0in_Kpipi0", m_minMassD0in_Kpipi0);
  declareProperty("MinMassDP", m_minMassDP);
  declareProperty("MinMassDPin", m_minMassDPin);
  declareProperty("MinMassLambdaC", m_minMassLambdaC);
  declareProperty("MinMassLambdaCin", m_minMassLambdaCin);
  declareProperty("MinPtD0", m_minPtD0);
  declareProperty("MinTrackPtD0", m_minTrackPtD0);
  declareProperty("MinTrackPtDP", m_minTrackPtDP);
  declareProperty("MinTrackPtLambdaC", m_minTrackPtLambdaC);
  declareProperty("PhiwindowDP", m_phiwindowDP);
  declareProperty("PhiwindowLC", m_phiwindowLC);
  declareProperty("PhiwindowDs", m_phiwindowDs);
  declareProperty("MinPtDP", m_minPtDP);
  declareProperty("MinPtLambaC", m_minPtLambdaC);
  declareProperty("MinLxyDP", m_minLxyDP);
  declareProperty("MinLxyDs", m_minLxyDs);
  declareProperty("MinLxyLambaCToLambdaPi", m_minLxyLambdaCToLambdaPi);
  declareProperty("MinLxyLambdaCToKsP", m_minLxyLambdaCToKsP);
  declareProperty("MinLxyDToKsK", m_minLxyDToKsK);
  declareProperty("MinLxyDToKsPi", m_minLxyDToKsPi);
  declareProperty("MinTrackPtD3", m_minTrackPtD3);
  declareProperty("constrainV0", m_constrV0);
  declareProperty("Maxd0LambdaC", m_maxd0LambdaC);
  declareProperty("MinLxyLambdaC", m_minLxyLambdaC);
}

StatusCode DSelectionAlg ::initialize() {
  ANA_CHECK(m_tracksHandle.initialize(m_systematicsList));
  ANA_CHECK(m_compositesHandle.initialize(m_systematicsList));
  ANA_CHECK(m_compositesHandle_k3pi.initialize(m_systematicsList));
  ANA_CHECK(m_compositesHandle_vee.initialize(m_systematicsList));
  ANA_CHECK(m_systematicsList.initialize());

  ANA_CHECK(m_trackToVertexIPEstimator.retrieve());
  ANA_CHECK(m_VKVrtFitter.retrieve());
  ANA_CHECK(m_CascadeTools.retrieve());

  // initialize decorators
  initializeDecorators();

  // selection accessor for tight tracks
  if (m_selectionDecorationLoose.empty() ||
      m_selectionDecorationTight.empty()) {
    ANA_MSG_ERROR("no selection decoration name set");
    return StatusCode::FAILURE;
  }
  ANA_CHECK(makeSelectionReadAccessor(m_selectionDecorationLoose,
                                      m_selectionAccessorLoose));
  ANA_CHECK(makeSelectionReadAccessor(m_selectionDecorationTight,
                                      m_selectionAccessorTight));

  return StatusCode::SUCCESS;
}

StatusCode DSelectionAlg ::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    // Event info
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
    m_is_mc = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);

    // Tracks
    const xAOD::TrackParticleContainer *tracks = nullptr;
    ANA_CHECK(m_tracksHandle.retrieve(tracks, sys));
    ANA_MSG_DEBUG("Number of tracks: " << tracks->size());

    // Primary vertex
    m_primaryVertex = nullptr;
    const xAOD::VertexContainer *primaryVertices = nullptr;
    CHECK(evtStore()->retrieve(primaryVertices, m_primaryVertexKey));
    for (const xAOD::Vertex *vertex : *primaryVertices) {
      if (vertex->vertexType() == xAOD::VxType::PriVtx) {
        // The default "PrimaryVertex" container is ordered in
        // sum-pt, and the tracking group recommends to pick the one
        // with the maximum sum-pt, so this will do the right thing.
        // If the user needs a different primary vertex, he needs to
        // provide a reordered primary vertex container and point
        // this algorithm to it. Currently there is no central
        // algorithm to do that, so users will have to write their
        // own (15 Aug 18).
        if (m_primaryVertex == nullptr) {
          m_primaryVertex = vertex;
          break;
        }
      }
    }
    if (!m_primaryVertex) {
      ATH_MSG_WARNING("No primary vertex found");
      return StatusCode::SUCCESS;
    }

    // output composite particle container
    auto composites = std::make_unique<xAOD::CompositeParticleContainer>();
    auto composites_aux =
        std::make_unique<xAOD::CompositeParticleAuxContainer>();
    auto composites_k3pi = std::make_unique<xAOD::CompositeParticleContainer>();
    auto composites_k3pi_aux =
        std::make_unique<xAOD::CompositeParticleAuxContainer>();
    auto composites_vee = std::make_unique<xAOD::CompositeParticleContainer>();
    auto composites_vee_aux =
        std::make_unique<xAOD::CompositeParticleAuxContainer>();
    composites->setStore(composites_aux.get());
    composites_k3pi->setStore(composites_k3pi_aux.get());
    composites_vee->setStore(composites_vee_aux.get());

    // global counters
    m_NDplus = 0;
    m_NDsubs = 0;
    m_NDstar = 0;
    m_NDstar3pi = 0;
    m_ND0K3pi = 0;
    m_NLambdaC = 0;

    // D+ -> K pi pi candidates
    if (m_doDplus) {
      for (unsigned int tid1 = 0; tid1 < tracks->size(); tid1++) {
        // track1
        const xAOD::TrackParticle *trk1 = tracks->at(tid1);
        if (!(trk1->pt() > m_minTrackPtDP))
          continue;

        for (unsigned int tid2 = tid1 + 1; tid2 < tracks->size(); tid2++) {
          // track2
          const xAOD::TrackParticle *trk2 = tracks->at(tid2);
          if (!(trk2->pt() > m_minTrackPtDP))
            continue;

          // same-charge
          if (!((trk1->charge() * trk2->charge()) > 0))
            continue;

          TVector3 t1;
          t1.SetPtEtaPhi(trk1->pt(), trk1->eta(), trk1->phi());
          TVector3 t2;
          t2.SetPtEtaPhi(trk2->pt(), trk2->eta(), trk2->phi());

          if (!(t1.DeltaR(t2) < m_maxDPdRKPi))
            continue;

          for (unsigned int tid3 = 0; tid3 < tracks->size(); tid3++) {
            // track3
            const xAOD::TrackParticle *trk3 = tracks->at(tid3);
            if (!(trk3->pt() > m_minTrackPtDP))
              continue;

            if (trk3 == trk1)
              continue;
            if (trk3 == trk2)
              continue;

            // opposite-charge
            if (!((trk1->charge() * trk3->charge()) < 0))
              continue;

            TVector3 t3;
            t3.SetPtEtaPhi(trk3->pt(), trk3->eta(), trk3->phi());
            if (!(t1.DeltaR(t3) < m_maxDPdRKPi))
              continue;
            if (!(t2.DeltaR(t3) < m_maxDPdRKPi))
              continue;

            // combinatios
            TLorentzVector k;
            k.SetPtEtaPhiM(t3.Pt(), t3.Eta(), t3.Phi(), MASS_KAON);
            TLorentzVector p1;
            p1.SetPtEtaPhiM(t1.Pt(), t1.Eta(), t1.Phi(), MASS_PION);
            TLorentzVector p2;
            p2.SetPtEtaPhiM(t2.Pt(), t2.Eta(), t2.Phi(), MASS_PION);

            if ((k + p1 + p2).M() > m_minMassDPin &&
                (k + p1 + p2).M() < m_maxMassDPin &&
                (k + p1 + p2).Pt() > 3.5 * CLHEP::GeV) {
              bool found = FindDplusCandidate(m_primaryVertex, trk3, trk1, trk2,
                                              composites.get());
              if (found) {
                m_NDplus++;
                ATH_MSG_VERBOSE("Found a D+ -> K pi pi candidate!");
              }
            }

          } // end tid3

        } // end tid2

      } // end tid1
    }

    // LambdaC ->p K pi candidates
    if (m_doLambdaC) {
      for (unsigned int tid1 = 0; tid1 < tracks->size(); tid1++) {
        // track1
        const xAOD::TrackParticle *trk1 = tracks->at(tid1);
        if (!(trk1->pt() > m_minTrackPtLambdaC))
          continue;

        for (unsigned int tid2 = 0; tid2 < tracks->size(); tid2++) {
          if (tid2 == tid1)
            continue;
          // track2
          const xAOD::TrackParticle *trk2 = tracks->at(tid2);
          if (!(trk2->pt() > m_minTrackPtLambdaC))
            continue;

          // opposte-charge
          if (!((trk1->charge() * trk2->charge()) < 0))
            continue;

          TVector3 t1;
          t1.SetPtEtaPhi(trk1->pt(), trk1->eta(), trk1->phi());
          TVector3 t2;
          t2.SetPtEtaPhi(trk2->pt(), trk2->eta(), trk2->phi());

          if (!(t1.DeltaR(t2) < m_maxDPdRKPi))
            continue;

          for (unsigned int tid3 = 0; tid3 < tracks->size(); tid3++) {
            // track3
            const xAOD::TrackParticle *trk3 = tracks->at(tid3);
            if (!(trk3->pt() > m_minTrackPtLambdaC))
              continue;

            if (trk3 == trk1)
              continue;
            if (trk3 == trk2)
              continue;

            // same charge
            if (!((trk1->charge() * trk3->charge()) > 0))
              continue;

            TVector3 t3;
            t3.SetPtEtaPhi(trk3->pt(), trk3->eta(), trk3->phi());
            if (!(t1.DeltaR(t3) < m_maxDPdRKPi))
              continue;
            if (!(t2.DeltaR(t3) < m_maxDPdRKPi))
              continue;

            // combinatios
            TLorentzVector pr;
            pr.SetPtEtaPhiM(t1.Pt(), t1.Eta(), t1.Phi(), MASS_PROTON);
            TLorentzVector K;
            K.SetPtEtaPhiM(t2.Pt(), t2.Eta(), t2.Phi(), MASS_KAON);
            TLorentzVector pion;
            pion.SetPtEtaPhiM(t3.Pt(), t3.Eta(), t3.Phi(), MASS_PION);
            if ((pr + K + pion).M() > m_minMassLambdaCin &&
                (pr + K + pion).M() < m_maxMassLambdaCin &&
                (pr + K + pion).Pt() > 3.5 * CLHEP::GeV) {
              bool found = FindLambdacCandidate(m_primaryVertex, trk1, trk2,
                                                trk3, composites.get());
              if (found) {
                m_NLambdaC++;
                ATH_MSG_VERBOSE("Found a LambdaC-->p K pi  candidate!");
              }
            }

          } // end tid3

        } // end tid2

      } // end tid1
    }

    // Ds+ -> K K pi candidates
    if (m_doDsubs) {
      for (unsigned int tid1 = 0; tid1 < tracks->size(); tid1++) {
        // track1
        const xAOD::TrackParticle *trk1 = tracks->at(tid1);
        if (!(trk1->pt() > m_minTrackPtDP))
          continue;

        for (unsigned int tid2 = tid1 + 1; tid2 < tracks->size(); tid2++) {
          // track2
          const xAOD::TrackParticle *trk2 = tracks->at(tid2);
          if (!(trk2->pt() > m_minTrackPtDP))
            continue;

          // opposite-charge
          if (!((trk1->charge() * trk2->charge()) < 0))
            continue;

          TLorentzVector k1;
          k1.SetPtEtaPhiM(trk1->pt(), trk1->eta(), trk1->phi(), MASS_KAON);
          TLorentzVector k2;
          k2.SetPtEtaPhiM(trk2->pt(), trk2->eta(), trk2->phi(), MASS_KAON);
          if (!(k1.DeltaR(k2) < m_maxDPdRKPi))
            continue;

          // require phi mass window
          if (!(std::abs((k1 + k2).M() - MASS_PHI) < m_phiwindowDs))
            continue;

          for (unsigned int tid3 = 0; tid3 < tracks->size(); tid3++) {
            // track3
            const xAOD::TrackParticle *trk3 = tracks->at(tid3);
            if (!(trk3->pt() > m_minTrackPtDP))
              continue;

            if (trk3 == trk1)
              continue;
            if (trk3 == trk2)
              continue;

            TLorentzVector pi;
            pi.SetPtEtaPhiM(trk3->pt(), trk3->eta(), trk3->phi(), MASS_PION);
            if (!(k1.DeltaR(pi) < m_maxDPdRKPi))
              continue;
            if (!(k2.DeltaR(pi) < m_maxDPdRKPi))
              continue;

            if ((k1 + k2 + pi).M() > m_minMassDPin &&
                (k1 + k2 + pi).M() < m_maxMassDPin &&
                (k1 + k2 + pi).Pt() > 3.5 * CLHEP::GeV) {
              bool found = FindDsCandidate(m_primaryVertex, trk1, trk2, trk3,
                                           composites.get());
              if (found) {
                m_NDsubs++;
                ATH_MSG_VERBOSE("Found a Ds+ -> K K pi candidate!");
              }
            }

          } // end tid3

        } // end tid2

      } // end tid1
    }

    // D* -> D0 pi -> K pi (pi0) pi candidates
    if (m_doD0Kpi || m_doD0K2pi) {
      for (unsigned int tid1 = 0; tid1 < tracks->size(); tid1++) {
        // track1
        const xAOD::TrackParticle *trk1 = tracks->at(tid1);
        if (!(trk1->pt() > m_minTrackPtD0))
          continue;

        for (unsigned int tid2 = tid1 + 1; tid2 < tracks->size(); tid2++) {
          // track2
          const xAOD::TrackParticle *trk2 = tracks->at(tid2);
          if (!(trk2->pt() > m_minTrackPtD0))
            continue;

          // opposite-charge
          if (!((trk1->charge() * trk2->charge()) < 0))
            continue;

          TVector3 t1;
          t1.SetPtEtaPhi(trk1->pt(), trk1->eta(), trk1->phi());
          TVector3 t2;
          t2.SetPtEtaPhi(trk2->pt(), trk2->eta(), trk2->phi());

          if (!(t1.DeltaR(t2) < m_maxdRKPi))
            continue;

          // all combinations
          TLorentzVector k1;
          k1.SetPtEtaPhiM(t1.Pt(), t1.Eta(), t1.Phi(), MASS_KAON);
          TLorentzVector p1;
          p1.SetPtEtaPhiM(t1.Pt(), t1.Eta(), t1.Phi(), MASS_PION);
          TLorentzVector k2;
          k2.SetPtEtaPhiM(t2.Pt(), t2.Eta(), t2.Phi(), MASS_KAON);
          TLorentzVector p2;
          p2.SetPtEtaPhiM(t2.Pt(), t2.Eta(), t2.Phi(), MASS_PION);

          // D0 -> K pi
          if (m_doD0Kpi && (k1 + p2).M() > m_minMassD0in_Kpi &&
              (k1 + p2).M() < m_maxMassD0in_Kpi &&
              (k1 + p2).Pt() > 3.5 * CLHEP::GeV) {
            bool found = FindDstarCandidate(1, m_primaryVertex, trk1, trk2,
                                            composites.get(), tracks);
            if (found) {
              m_NDstar++;
              ATH_MSG_VERBOSE("Found a D0 -> K pi candidate!");
            }
          }
          // D0 -> K pi pi0
          if (m_doD0K2pi && (k1 + p2).M() > m_minMassD0in_Kpipi0 &&
              (k1 + p2).M() < m_maxMassD0in_Kpipi0 &&
              (k1 + p2).Pt() > 3.5 * CLHEP::GeV) {
            bool found = FindDstarCandidate(0, m_primaryVertex, trk1, trk2,
                                            composites.get(), tracks);
            if (found) {
              m_NDstar++;
              ATH_MSG_VERBOSE("Found a D0 -> K pi pi0 candidate!");
            }
          }
          // D0 -> K pi
          if (m_doD0Kpi && (k2 + p1).M() > m_minMassD0in_Kpi &&
              (k2 + p1).M() < m_maxMassD0in_Kpi &&
              (k2 + p1).Pt() > 3.5 * CLHEP::GeV) {
            bool found = FindDstarCandidate(1, m_primaryVertex, trk2, trk1,
                                            composites.get(), tracks);
            if (found) {
              m_NDstar++;
              ATH_MSG_VERBOSE("Found a D0 -> K pi candidate!");
            }
          }
          // D0 -> K pi pi0
          if (m_doD0K2pi && (k2 + p1).M() > m_minMassD0in_Kpipi0 &&
              (k2 + p1).M() < m_maxMassD0in_Kpipi0 &&
              (k2 + p1).Pt() > 3.5 * CLHEP::GeV) {
            bool found = FindDstarCandidate(0, m_primaryVertex, trk2, trk1,
                                            composites.get(), tracks);
            if (found) {
              m_NDstar++;
              ATH_MSG_VERBOSE("Found a D0 -> K pi pi0 candidate!");
            }
          }

        } // end tid2

      } // end tid1
    }

    // D* -> D0 pi -> K pi pi pi pi candidates
    if (m_doD0K3pi || m_doD0K3pi_only) {
      for (unsigned int tid1 = 0; tid1 < tracks->size(); tid1++) {
        // track1
        const xAOD::TrackParticle *trk1 = tracks->at(tid1);
        if (!(trk1->pt() > m_minTrackPtD3))
          continue;

        for (unsigned int tid2 = tid1 + 1; tid2 < tracks->size(); tid2++) {
          // track2
          const xAOD::TrackParticle *trk2 = tracks->at(tid2);
          if (!(trk2->pt() > m_minTrackPtD3))
            continue;

          // same-charge
          if (!((trk1->charge() * trk2->charge()) > 0))
            continue;

          TVector3 t1;
          t1.SetPtEtaPhi(trk1->pt(), trk1->eta(), trk1->phi());
          TVector3 t2;
          t2.SetPtEtaPhi(trk2->pt(), trk2->eta(), trk2->phi());
          if (!(t1.DeltaR(t2) < m_maxdRKPi))
            continue;

          for (unsigned int tid3 = 0; tid3 < tracks->size(); tid3++) {
            // track3
            const xAOD::TrackParticle *trk3 = tracks->at(tid3);
            if (!(trk3->pt() > m_minTrackPtD3))
              continue;

            if (trk3 == trk1)
              continue;
            if (trk3 == trk2)
              continue;

            // opposite-charge
            if (!((trk1->charge() * trk3->charge()) < 0))
              continue;

            TVector3 t3;
            t3.SetPtEtaPhi(trk3->pt(), trk3->eta(), trk3->phi());
            if (!(t1.DeltaR(t3) < m_maxdRKPi))
              continue;
            if (!(t2.DeltaR(t3) < m_maxdRKPi))
              continue;

            for (unsigned int tid4 = tid3 + 1; tid4 < tracks->size(); tid4++) {
              // track4
              const xAOD::TrackParticle *trk4 = tracks->at(tid4);
              if (!(trk4->pt() > m_minTrackPtD3))
                continue;

              if (trk4 == trk1)
                continue;
              if (trk4 == trk2)
                continue;

              // opposite-charge
              if (!((trk1->charge() * trk4->charge()) < 0))
                continue;

              TVector3 t4;
              t4.SetPtEtaPhi(trk4->pt(), trk4->eta(), trk4->phi());
              if (!(t1.DeltaR(t4) < m_maxdRKPi))
                continue;
              if (!(t2.DeltaR(t4) < m_maxdRKPi))
                continue;
              if (!(t3.DeltaR(t4) < m_maxdRKPi))
                continue;

              // combinatios
              TLorentzVector k_1;
              k_1.SetPtEtaPhiM(t1.Pt(), t1.Eta(), t1.Phi(), MASS_KAON);
              TLorentzVector p1_1;
              p1_1.SetPtEtaPhiM(t2.Pt(), t2.Eta(), t2.Phi(), MASS_PION);
              TLorentzVector p2_1;
              p2_1.SetPtEtaPhiM(t3.Pt(), t3.Eta(), t3.Phi(), MASS_PION);
              TLorentzVector p3_1;
              p3_1.SetPtEtaPhiM(t4.Pt(), t4.Eta(), t4.Phi(), MASS_PION);

              TLorentzVector k_2;
              k_2.SetPtEtaPhiM(t2.Pt(), t2.Eta(), t2.Phi(), MASS_KAON);
              TLorentzVector p1_2;
              p1_2.SetPtEtaPhiM(t1.Pt(), t1.Eta(), t1.Phi(), MASS_PION);
              TLorentzVector p2_2;
              p2_2.SetPtEtaPhiM(t3.Pt(), t3.Eta(), t3.Phi(), MASS_PION);
              TLorentzVector p3_2;
              p3_2.SetPtEtaPhiM(t4.Pt(), t4.Eta(), t4.Phi(), MASS_PION);

              // D0 -> K pi pi pi
              if ((k_1 + p1_1 + p2_1 + p3_1).M() > m_minMassD0in_Kpi &&
                  (k_1 + p1_1 + p2_1 + p3_1).M() < m_maxMassD0in_Kpi &&
                  (k_1 + p1_1 + p2_1 + p3_1).Pt() > 3.5 * CLHEP::GeV) {
                // D0 from D*
                if (m_doD0K3pi) {
                  bool found =
                      FindDK3PiCandidate(m_primaryVertex, trk1, trk2, trk3,
                                         trk4, composites_k3pi.get(), tracks);
                  if (found) {
                    m_NDstar3pi++;
                    ATH_MSG_VERBOSE("D* -> D0 pi -> K pi pi pi pi candidate!");
                  }
                }
                // D0 without the D*
                if (m_doD0K3pi_only) {
                  bool found =
                      FindD0K3PiCandidate(m_primaryVertex, trk1, trk2, trk3,
                                          trk4, composites.get());
                  if (found) {
                    m_ND0K3pi++;
                    ATH_MSG_VERBOSE("D0 -> K pi pi pi candidate!");
                  }
                }
              }
              // D0 -> K pi pi pi
              if ((k_2 + p1_2 + p2_2 + p3_2).M() > m_minMassD0in_Kpi &&
                  (k_2 + p1_2 + p2_2 + p3_2).M() < m_maxMassD0in_Kpi &&
                  (k_2 + p1_2 + p2_2 + p3_2).Pt() > 3.5 * CLHEP::GeV) {
                // D0 from D*
                if (m_doD0K3pi) {
                  bool found =
                      FindDK3PiCandidate(m_primaryVertex, trk2, trk1, trk3,
                                         trk4, composites_k3pi.get(), tracks);
                  if (found) {
                    m_NDstar3pi++;
                    ATH_MSG_VERBOSE("D* -> D0 pi -> K pi pi pi pi candidate!");
                  }
                }
                // D0 without the D*
                if (m_doD0K3pi_only) {
                  bool found =
                      FindD0K3PiCandidate(m_primaryVertex, trk2, trk1, trk3,
                                          trk4, composites.get());
                  if (found) {
                    m_ND0K3pi++;
                    ATH_MSG_VERBOSE("D0 -> K pi pi pi candidate!");
                  }
                }
              }

            } // end tid4

          } // end tid3

        } // end tid2

      } // end tid1
    }

    if (m_doLambdaCLambdaPi) {
      const xAOD::VertexContainer *lambdaVertices = nullptr;
      CHECK(evtStore()->retrieve(lambdaVertices, m_LambdaVertexKey));
      int nfound = selectV0TrackCandidates(m_primaryVertex, lambdaVertices,
                                           composites_vee.get(), tracks, 6, +1);
      m_NLambdaC += nfound;
      if (nfound > 0)
        ATH_MSG_VERBOSE(nfound << " LambdaC candidates found!");
      const xAOD::VertexContainer *lambdaBarVertices = nullptr;
      CHECK(evtStore()->retrieve(lambdaBarVertices, m_LambdaBarVertexKey));
      nfound = selectV0TrackCandidates(m_primaryVertex, lambdaBarVertices,
                                       composites_vee.get(), tracks, 6, -1);
      m_NLambdaC += nfound;
      if (nfound > 0)
        ATH_MSG_VERBOSE(nfound << " LambdaCBar candidates found!");
    }

    if (m_doLambdaCKshortP) {
      const xAOD::VertexContainer *KSVertices = nullptr;
      CHECK(evtStore()->retrieve(KSVertices, m_KSVertexKey));
      int nfound = selectV0TrackCandidates(m_primaryVertex, KSVertices,
                                           composites_vee.get(), tracks, 7, 0);
      m_NLambdaC += nfound;
      if (nfound > 0)
        ATH_MSG_VERBOSE(nfound << " LambdaC candidates found!");
    }

    if (m_doKshortK) {
      const xAOD::VertexContainer *KSVertices = nullptr;
      CHECK(evtStore()->retrieve(KSVertices, m_KSVertexKey));
      int nfound = selectV0TrackCandidates(m_primaryVertex, KSVertices,
                                           composites_vee.get(), tracks, 8, 0);
      m_NDsubs += nfound;
      if (nfound > 0)
        ATH_MSG_VERBOSE(nfound << " KshortK candidates found!");
    }

    if (m_doKshortpi) {
      const xAOD::VertexContainer *KSVertices = nullptr;
      CHECK(evtStore()->retrieve(KSVertices, m_KSVertexKey));
      int nfound = selectV0TrackCandidates(m_primaryVertex, KSVertices,
                                           composites_vee.get(), tracks, 9, 0);
      m_NDplus += nfound;
      if (nfound > 0)
        ATH_MSG_VERBOSE(nfound << " KshortK candidates found!");
    }

    // Search over all decay types; decorate composite particles
    for (const xAOD::CompositeParticle *cp : *composites.get()) {
      DecorateCompositeParticle(cp, tracks);
    }

    for (const xAOD::CompositeParticle *cp : *composites_k3pi.get()) {
      DecorateCompositeParticle(cp, tracks);
    }

    for (const xAOD::CompositeParticle *cp : *composites_vee.get()) {
      DecorateCompositeParticle(cp, tracks);
    }

    // record composite particle container
    ANA_CHECK(m_compositesHandle.record(std::move(composites),
                                        std::move(composites_aux), sys));
    ANA_CHECK(m_compositesHandle_k3pi.record(
        std::move(composites_k3pi), std::move(composites_k3pi_aux), sys));
    ANA_CHECK(m_compositesHandle_vee.record(
        std::move(composites_vee), std::move(composites_vee_aux), sys));

    if (m_enableFilter && (m_NDplus + m_NDsubs + m_NDstar + m_NDstar3pi +
                               m_ND0K3pi + m_NLambdaC ==
                           0)) {
      ATH_MSG_VERBOSE("No candidates found. Not passing the event.");
      setFilterPassed(false);
      return StatusCode::SUCCESS;
    }
  }
  return StatusCode::SUCCESS;
}

bool DSelectionAlg ::FindDstarCandidate(
    bool vertexConstraint, const xAOD::Vertex *primaryVertex,
    const xAOD::TrackParticle *Ktrack, const xAOD::TrackParticle *Pitrack,
    xAOD::CompositeParticleContainer *composites,
    const xAOD::TrackParticleContainer *tracks) {
  //
  // make a D0 -> K pi (pi0)
  //
  std::vector<const xAOD::TrackParticle *> Tracks;
  std::vector<double> Masses;

  Tracks.clear();
  Tracks.push_back(Ktrack);
  Tracks.push_back(Pitrack);

  Masses.clear();
  Masses.push_back(MASS_KAON);
  Masses.push_back(MASS_PION);

  FitOutput fitOutput = {};

  FitInput fitInput{vertexConstraint, *primaryVertex, Tracks, Masses};

  FitSelection fitSelection{
      vertexConstraint ? m_maxd0D0_Kpi : m_maxd0D0_Kpipi0,
      m_minLxyD0,
      vertexConstraint ? m_maxMassD0_Kpi : m_maxMassD0_Kpipi0,
      vertexConstraint ? m_minMassD0_Kpi : m_minMassD0_Kpipi0,
      m_maxfitChi2D0,
      m_minPtD0};

  bool success = VertexFit(fitInput, fitOutput, fitSelection);
  if (!success)
    return false;

  //
  // now add slow pion
  //
  for (unsigned int tid3 = 0; tid3 < tracks->size(); tid3++) {
    // track3
    const xAOD::TrackParticle *trk3 = tracks->at(tid3);

    if (trk3 == Ktrack || trk3 == Pitrack)
      continue;

    // opposite charge w.r.t. kaon
    if (trk3->charge() * Ktrack->charge() > 0)
      continue;

    const Trk::ImpactParametersAndSigma *ipAndSigma3 =
        m_trackToVertexIPEstimator->estimate(trk3, primaryVertex);
    if (!(std::abs(ipAndSigma3->IPd0) < m_maxd0PiSlow)) {
      delete ipAndSigma3;
      continue;
    }

    //
    // create pion vector
    //
    TLorentzVector pion;
    pion.SetPtEtaPhiM(trk3->pt(), trk3->eta(), trk3->phi(), MASS_PION);

    if (!(pion.DeltaR(fitOutput.Momentum) < m_maxdRD0PiSlow)) {
      delete ipAndSigma3;
      continue;
    }

    TLorentzVector vDstar = fitOutput.Momentum + pion;
    double deltaM = vDstar.M() - fitOutput.Momentum.M();

    if (!(deltaM > m_mindM && deltaM < m_maxdM)) {
      delete ipAndSigma3;
      continue;
    }

    //
    // record the D0 particle
    //
    xAOD::CompositeParticle *Dzero = new xAOD::CompositeParticle;
    composites->push_back(Dzero);
    Dzero->setCharge(0);
    Dzero->setPdgId(-421 * Ktrack->charge());
    Dzero->setP4(fitOutput.Momentum);
    Dzero->addPart(Ktrack, false);
    Dzero->addPart(Pitrack, false);
    if (vertexConstraint)
      (*m_decayType)(*Dzero) = 10;
    else
      (*m_decayType)(*Dzero) = 20;

    // save fit output as a decoration
    (*m_fitOutput)(*Dzero) = fitOutput;

    // D0 index
    unsigned int D0_index = composites->size() - 1;
    (*m_D0Index)(*Dzero) = D0_index;

    // Daughter info
    std::vector<int> daughterBarcodes_Dzero;
    std::vector<int> daughterDBarcodes_Dzero;
    std::vector<int> daughterTruthPdgId_Dzero;
    std::vector<int> daughterParentTruthPdgId_Dzero;
    if (m_is_mc && m_doTruthMathing) {
      std::vector<const xAOD::TrackParticle *> tracks = {Ktrack, Pitrack};
      (*m_truthBarcode)(*Dzero) = TruthUtils::GetTruthBarcode(
          tracks, daughterDBarcodes_Dzero, daughterParentTruthPdgId_Dzero,
          daughterBarcodes_Dzero, daughterTruthPdgId_Dzero);
    } else {
      (*m_truthBarcode)(*Dzero) = -1;
      daughterBarcodes_Dzero = {0, 0};
      daughterDBarcodes_Dzero = {0, 0};
      daughterTruthPdgId_Dzero = {0, 0};
      daughterParentTruthPdgId_Dzero = {0, 0};
    }

    DaughterInfo daughterInfo{
        {Ktrack, Pitrack},        {PDGID_KAON, PDGID_PION},
        daughterDBarcodes_Dzero,  daughterBarcodes_Dzero,
        daughterTruthPdgId_Dzero, daughterParentTruthPdgId_Dzero};
    (*m_daughter_info)(*Dzero) = daughterInfo;

    //
    // record the D* particle
    //
    xAOD::CompositeParticle *Dstar = new xAOD::CompositeParticle;
    composites->push_back(Dstar);
    Dstar->setCharge(-Ktrack->charge());
    Dstar->setPdgId(413 * Dstar->charge());
    Dstar->setP4(vDstar);
    Dstar->addPart(Ktrack, false);
    Dstar->addPart(Pitrack, false);
    Dstar->addPart(trk3, false);
    if (vertexConstraint)
      (*m_decayType)(*Dstar) = 1;
    else
      (*m_decayType)(*Dstar) = 2;

    // save fit output as a decoration
    (*m_fitOutput)(*Dstar) = fitOutput;

    // D0 index
    (*m_D0Index)(*Dstar) = D0_index;

    // Delta Mass
    (*m_DeltaMass)(*Dstar) = Dstar->m() - Dzero->m();

    // Slow pion d0
    (*m_SlowPionD0)(*Dstar) = ipAndSigma3->IPd0;
    (*m_SlowPionZ0SinTheta)(*Dstar) = ipAndSigma3->IPz0SinTheta;

    // Daughter info
    std::vector<int> daughterBarcodes_Dstar;
    std::vector<int> daughterDBarcodes_Dstar;
    std::vector<int> daughterTruthPdgId_Dstar;
    std::vector<int> daughterParentTruthPdgId_Dstar;

    if (m_is_mc && m_doTruthMathing) {
      std::vector<const xAOD::TrackParticle *> tracks = {Ktrack, Pitrack, trk3};
      (*m_truthBarcode)(*Dstar) = TruthUtils::GetTruthBarcode(
          tracks, daughterDBarcodes_Dstar, daughterParentTruthPdgId_Dstar,
          daughterBarcodes_Dstar, daughterTruthPdgId_Dstar);
    } else {
      (*m_truthBarcode)(*Dstar) = -1;
      daughterBarcodes_Dstar = {0, 0, 0};
      daughterDBarcodes_Dstar = {0, 0, 0};
      daughterTruthPdgId_Dstar = {0, 0, 0};
      daughterParentTruthPdgId_Dstar = {0, 0, 0};
    }

    DaughterInfo daughterInfo_Dstar{
        {Ktrack, Pitrack, trk3},  {PDGID_KAON, PDGID_PION, PDGID_PION},
        daughterDBarcodes_Dstar,  daughterBarcodes_Dstar,
        daughterTruthPdgId_Dstar, daughterParentTruthPdgId_Dstar};
    (*m_daughter_info)(*Dstar) = daughterInfo_Dstar;

    delete ipAndSigma3;
    return true;

  } // trk3 loop

  return false;
}

bool DSelectionAlg ::FindDplusCandidate(
    const xAOD::Vertex *primaryVertex, const xAOD::TrackParticle *Ktrack,
    const xAOD::TrackParticle *Pitrack1, const xAOD::TrackParticle *Pitrack2,
    xAOD::CompositeParticleContainer *composites) {
  //
  // make a D+ -> K pi pi
  //
  std::vector<const xAOD::TrackParticle *> Tracks;
  std::vector<double> Masses;

  Tracks.clear();
  Tracks.push_back(Ktrack);
  Tracks.push_back(Pitrack1);
  Tracks.push_back(Pitrack2);

  Masses.clear();
  Masses.push_back(MASS_KAON);
  Masses.push_back(MASS_PION);
  Masses.push_back(MASS_PION);

  FitOutput fitOutput = {};

  FitInput fitInput{true, *primaryVertex, Tracks, Masses};

  FitSelection fitSelection{m_maxd0D0_Kpi, m_minLxyDP,     m_maxMassDP,
                            m_minMassDP,   m_maxfitChi2D0, m_minPtDP};

  bool success = VertexFit(fitInput, fitOutput, fitSelection);
  if (!success)
    return false;

  TLorentzVector tk;
  tk.SetPtEtaPhiM(Ktrack->pt(), Ktrack->eta(), Ktrack->phi(), MASS_KAON);
  TLorentzVector tpi1;
  tpi1.SetPtEtaPhiM(Pitrack1->pt(), Pitrack1->eta(), Pitrack1->phi(),
                    MASS_PION);
  TLorentzVector tpi2;
  tpi2.SetPtEtaPhiM(Pitrack2->pt(), Pitrack2->eta(), Pitrack2->phi(),
                    MASS_PION);

  // cos thetastar
  double thetastar = 0.;
  TLorentzVector tkboost = tk;
  TVector3 boost_v0 = (tk + tpi1 + tpi2).BoostVector();
  boost_v0 *= -1.0;
  tkboost.Boost(boost_v0);
  thetastar = (tk + tpi1 + tpi2).Angle(tkboost.Vect());

  if (!(cos(thetastar) > m_mincosthetastarDP))
    return false;
  if (!(cos(thetastar) <= m_maxcosthetastarDP))
    return false;

  // reject D*
  double mKpi1 = (tk + tpi1).M();
  double mKpi2 = (tk + tpi2).M();
  if (fitOutput.Momentum.M() - mKpi1 < m_mindMDP)
    return false;
  if (fitOutput.Momentum.M() - mKpi2 < m_mindMDP)
    return false;

  // reject Ds
  TLorentzVector tk1;
  tk1.SetPtEtaPhiM(Pitrack1->pt(), Pitrack1->eta(), Pitrack1->phi(), MASS_KAON);
  TLorentzVector tk2;
  tk2.SetPtEtaPhiM(Pitrack2->pt(), Pitrack2->eta(), Pitrack2->phi(), MASS_KAON);
  double mPhi1 = fabs((tk + tk1).M() - MASS_PHI);
  double mPhi2 = fabs((tk + tk2).M() - MASS_PHI);
  if (mPhi1 < m_phiwindowDP)
    return false;
  if (mPhi2 < m_phiwindowDP)
    return false;

  //
  // record the D+ particle
  //
  xAOD::CompositeParticle *Dplus = new xAOD::CompositeParticle;
  composites->push_back(Dplus);
  Dplus->setCharge(-Ktrack->charge());
  Dplus->setPdgId(411 * Dplus->charge());
  Dplus->setP4(fitOutput.Momentum);
  Dplus->addPart(Ktrack, false);
  Dplus->addPart(Pitrack1, false);
  Dplus->addPart(Pitrack2, false);
  (*m_decayType)(*Dplus) = 4;

  // save fit output as a decoration
  (*m_fitOutput)(*Dplus) = fitOutput;

  // D+ specific cuts: costhetastar, mKpi, mPhi
  (*m_costhetastar)(*Dplus) = cos(thetastar);
  (*m_mKpi1)(*Dplus) = mKpi1;
  (*m_mKpi2)(*Dplus) = mKpi2;
  (*m_mPhi1)(*Dplus) = mPhi1;
  (*m_mPhi2)(*Dplus) = mPhi2;
  (*m_mKpipi)(*Dplus) = -999;

  // Daughter info
  std::vector<int> daughterBarcodes;
  std::vector<int> daughterDBarcodes;
  std::vector<int> daughterTruthPdgId;
  std::vector<int> daughterParentTruthPdgId;
  if (m_is_mc && m_doTruthMathing) {
    std::vector<const xAOD::TrackParticle *> tracks = {Ktrack, Pitrack1,
                                                       Pitrack2};
    (*m_truthBarcode)(*Dplus) = TruthUtils::GetTruthBarcode(
        tracks, daughterDBarcodes, daughterParentTruthPdgId, daughterBarcodes,
        daughterTruthPdgId);
  } else {
    (*m_truthBarcode)(*Dplus) = -1;
    daughterBarcodes = {0, 0, 0};
    daughterDBarcodes = {0, 0, 0};
    daughterTruthPdgId = {0, 0, 0};
    daughterParentTruthPdgId = {0, 0, 0};
  }

  DaughterInfo daughterInfo{{Ktrack, Pitrack1, Pitrack2},
                            {PDGID_KAON, PDGID_PION, PDGID_PION},
                            daughterDBarcodes,
                            daughterBarcodes,
                            daughterTruthPdgId,
                            daughterTruthPdgId};
  (*m_daughter_info)(*Dplus) = daughterInfo;

  return true;
}

bool DSelectionAlg ::FindDsCandidate(
    const xAOD::Vertex *primaryVertex, const xAOD::TrackParticle *Ktrack1,
    const xAOD::TrackParticle *Ktrack2, const xAOD::TrackParticle *Pitrack,
    xAOD::CompositeParticleContainer *composites) {
  //
  // make a Ds -> K K pi
  //
  std::vector<const xAOD::TrackParticle *> Tracks;
  std::vector<double> Masses;

  Tracks.clear();
  Tracks.push_back(Ktrack1);
  Tracks.push_back(Ktrack2);
  Tracks.push_back(Pitrack);

  Masses.clear();
  Masses.push_back(MASS_KAON);
  Masses.push_back(MASS_KAON);
  Masses.push_back(MASS_PION);

  FitOutput fitOutput = {};

  FitInput fitInput{true, *primaryVertex, Tracks, Masses};

  FitSelection fitSelection{m_maxd0D0_Kpi, m_minLxyDs,     m_maxMassDP,
                            m_minMassDP,   m_maxfitChi2D0, m_minPtDP};

  bool success = VertexFit(fitInput, fitOutput, fitSelection);
  if (!success)
    return false;

  TLorentzVector tk1;
  tk1.SetPtEtaPhiM(Ktrack1->pt(), Ktrack1->eta(), Ktrack1->phi(), MASS_KAON);
  TLorentzVector tk2;
  tk2.SetPtEtaPhiM(Ktrack2->pt(), Ktrack2->eta(), Ktrack2->phi(), MASS_KAON);
  TLorentzVector tpi;
  tpi.SetPtEtaPhiM(Pitrack->pt(), Pitrack->eta(), Pitrack->phi(), MASS_PION);

  // cos thetastar
  TLorentzVector tpiboost = tpi;
  TVector3 boost_v0 = (tk1 + tk2 + tpi).BoostVector();
  boost_v0 *= -1.0;
  tpiboost.Boost(boost_v0);
  double thetastar = (tk1 + tk2 + tpi).Angle(tpiboost.Vect());

  if (!(cos(thetastar) > m_mincosthetastarDs))
    return false;
  if (!(cos(thetastar) <= m_maxcosthetastarDs))
    return false;

  // cos theta prime
  TLorentzVector tpiboost1 = tpi;
  TVector3 boost_v1 = (tk1 + tk2).BoostVector();
  boost_v1 *= -1.0;
  tpiboost1.Boost(boost_v1);

  TLorentzVector tk1boost1 = tk1;
  tk1boost1.Boost(boost_v1);

  TLorentzVector tk2boost1 = tk2;
  tk2boost1.Boost(boost_v1);

  double thetaprime1 = (tk1boost1.Vect()).Angle(tpiboost1.Vect());
  double thetaprime2 = (tk2boost1.Vect()).Angle(tpiboost1.Vect());
  double fitPhiMass = (tk1 + tk2).M();
  //
  // record the Ds particle
  //
  xAOD::CompositeParticle *Ds = new xAOD::CompositeParticle;
  composites->push_back(Ds);
  Ds->setCharge(Pitrack->charge());
  Ds->setPdgId(431 * Ds->charge());
  Ds->setP4(fitOutput.Momentum);
  Ds->addPart(Ktrack1, false);
  Ds->addPart(Ktrack2, false);
  Ds->addPart(Pitrack, false);
  (*m_decayType)(*Ds) = 5;
  Ds->auxdecor<float>("thetaprime1") = thetaprime1;
  Ds->auxdecor<float>("thetaprime2") = thetaprime2;

  // save fit output as a decoration
  (*m_fitOutput)(*Ds) = fitOutput;

  // Ds specific cuts: costhetastar, mKpi, mPhi
  (*m_costhetastar)(*Ds) = cos(thetastar);
  (*m_mKpi1)(*Ds) = -999;
  (*m_mKpi2)(*Ds) = -999;
  (*m_mPhi1)(*Ds) = fitPhiMass;
  (*m_mPhi2)(*Ds) = -999;
  (*m_mKpipi)(*Ds) = -999;

  // Daughter info
  std::vector<int> daughterBarcodes_Ds;
  std::vector<int> daughterDBarcodes_Ds;
  std::vector<int> daughterTruthPdgId_Ds;
  std::vector<int> daughterParentTruthPdgId_Ds;
  if (m_is_mc && m_doTruthMathing) {
    std::vector<const xAOD::TrackParticle *> tracks = {Ktrack1, Ktrack2,
                                                       Pitrack};
    (*m_truthBarcode)(*Ds) = TruthUtils::GetTruthBarcode(
        tracks, daughterDBarcodes_Ds, daughterParentTruthPdgId_Ds,
        daughterBarcodes_Ds, daughterTruthPdgId_Ds);
  } else {
    (*m_truthBarcode)(*Ds) = -1;
    daughterDBarcodes_Ds = {0, 0, 0};
    daughterBarcodes_Ds = {0, 0, 0};
    daughterTruthPdgId_Ds = {0, 0, 0};
    daughterParentTruthPdgId_Ds = {0, 0, 0};
  }

  DaughterInfo daughterInfo{
      {Ktrack1, Ktrack2, Pitrack}, {PDGID_KAON, PDGID_KAON, PDGID_PION},
      daughterDBarcodes_Ds,        daughterBarcodes_Ds,
      daughterTruthPdgId_Ds,       daughterParentTruthPdgId_Ds};

  (*m_daughter_info)(*Ds) = daughterInfo;

  return true;
}

bool DSelectionAlg ::FindD0K3PiCandidate(
    const xAOD::Vertex *primaryVertex, const xAOD::TrackParticle *Ktrack,
    const xAOD::TrackParticle *Pitrack1, const xAOD::TrackParticle *Pitrack2,
    const xAOD::TrackParticle *Pitrack3,
    xAOD::CompositeParticleContainer *composites) {
  //
  // make a D0 -> K pi pi pi
  //
  std::vector<const xAOD::TrackParticle *> Tracks;
  std::vector<double> Masses;

  Tracks.clear();
  Tracks.push_back(Ktrack);
  Tracks.push_back(Pitrack1);
  Tracks.push_back(Pitrack2);
  Tracks.push_back(Pitrack3);

  Masses.clear();
  Masses.push_back(MASS_KAON);
  Masses.push_back(MASS_PION);
  Masses.push_back(MASS_PION);
  Masses.push_back(MASS_PION);

  FitOutput fitOutput = {};

  FitInput fitInput{true, *primaryVertex, Tracks, Masses};

  FitSelection fitSelection{m_maxd0D0_Kpi,   m_minLxyD0,     m_maxMassD0_Kpi,
                            m_minMassD0_Kpi, m_maxfitChi2D0, m_minPtD0};

  bool success = VertexFit(fitInput, fitOutput, fitSelection);
  if (!success)
    return false;

  //
  // record the D0 particle
  //
  xAOD::CompositeParticle *Dzero = new xAOD::CompositeParticle;
  composites->push_back(Dzero);
  Dzero->setCharge(0);
  Dzero->setPdgId(-421 * Ktrack->charge());
  Dzero->setP4(fitOutput.Momentum);
  Dzero->addPart(Ktrack, false);
  Dzero->addPart(Pitrack1, false);
  Dzero->addPart(Pitrack2, false);
  Dzero->addPart(Pitrack3, false);
  (*m_decayType)(*Dzero) = 30;

  // save fit output as a decoration
  (*m_fitOutput)(*Dzero) = fitOutput;

  // D0 index
  unsigned int D0_index = composites->size() - 1;
  (*m_D0Index)(*Dzero) = D0_index;

  // Daughter info
  std::vector<int> daughterBarcodes_Dzero;
  std::vector<int> daughterDBarcodes_Dzero;
  std::vector<int> daughterTruthPdgId_Dzero;
  std::vector<int> daughterParentTruthPdgId_Dzero;
  if (m_is_mc && m_doTruthMathing) {
    std::vector<const xAOD::TrackParticle *> tracks = {Ktrack, Pitrack1,
                                                       Pitrack2, Pitrack3};
    (*m_truthBarcode)(*Dzero) = TruthUtils::GetTruthBarcode(
        tracks, daughterDBarcodes_Dzero, daughterParentTruthPdgId_Dzero,
        daughterBarcodes_Dzero, daughterTruthPdgId_Dzero);
  } else {
    (*m_truthBarcode)(*Dzero) = -1;
    daughterBarcodes_Dzero = {0, 0, 0, 0};
    daughterDBarcodes_Dzero = {0, 0, 0, 0};
    daughterTruthPdgId_Dzero = {0, 0, 0, 0};
    daughterParentTruthPdgId_Dzero = {0, 0, 0, 0};
  }

  DaughterInfo daughterInfo{{Ktrack, Pitrack1, Pitrack2, Pitrack3},
                            {PDGID_KAON, PDGID_PION, PDGID_PION, PDGID_PION},
                            daughterDBarcodes_Dzero,
                            daughterBarcodes_Dzero,
                            daughterTruthPdgId_Dzero,
                            daughterParentTruthPdgId_Dzero};

  (*m_daughter_info)(*Dzero) = daughterInfo;

  return false;
}

bool DSelectionAlg ::FindDK3PiCandidate(
    const xAOD::Vertex *primaryVertex, const xAOD::TrackParticle *Ktrack,
    const xAOD::TrackParticle *Pitrack1, const xAOD::TrackParticle *Pitrack2,
    const xAOD::TrackParticle *Pitrack3,
    xAOD::CompositeParticleContainer *composites,
    const xAOD::TrackParticleContainer *tracks) {
  //
  // make a D0 -> K pi pi pi
  //
  std::vector<const xAOD::TrackParticle *> Tracks;
  std::vector<double> Masses;

  Tracks.clear();
  Tracks.push_back(Ktrack);
  Tracks.push_back(Pitrack1);
  Tracks.push_back(Pitrack2);
  Tracks.push_back(Pitrack3);

  Masses.clear();
  Masses.push_back(MASS_KAON);
  Masses.push_back(MASS_PION);
  Masses.push_back(MASS_PION);
  Masses.push_back(MASS_PION);

  FitOutput fitOutput = {};

  FitInput fitInput{true, *primaryVertex, Tracks, Masses};

  FitSelection fitSelection{m_maxd0D0_Kpi,   m_minLxyD0,     m_maxMassD0_Kpi,
                            m_minMassD0_Kpi, m_maxfitChi2D0, m_minPtD0};

  bool success = VertexFit(fitInput, fitOutput, fitSelection);
  if (!success)
    return false;

  //
  // now add slow pion
  //
  for (unsigned int tid5 = 0; tid5 < tracks->size(); tid5++) {
    // track5
    const xAOD::TrackParticle *trk5 = tracks->at(tid5);

    if (trk5 == Ktrack || trk5 == Pitrack1 || trk5 == Pitrack2 ||
        trk5 == Pitrack3)
      continue;

    const Trk::ImpactParametersAndSigma *ipAndSigma5 =
        m_trackToVertexIPEstimator->estimate(trk5, primaryVertex);
    if (!(std::abs(ipAndSigma5->IPd0) < m_maxd0PiSlow)) {
      delete ipAndSigma5;
      continue;
    }

    //
    // create pion vector
    //
    TLorentzVector pion;
    pion.SetPtEtaPhiM(trk5->pt(), trk5->eta(), trk5->phi(), MASS_PION);

    if (!(pion.DeltaR(fitOutput.Momentum) < m_maxdRD0PiSlow)) {
      delete ipAndSigma5;
      continue;
    }

    TLorentzVector vDstar = fitOutput.Momentum + pion;
    double deltaM = vDstar.M() - fitOutput.Momentum.M();

    if (!(deltaM > m_mindM && deltaM < m_maxdM)) {
      delete ipAndSigma5;
      continue;
    }

    //
    // record the D0 particle
    //
    xAOD::CompositeParticle *Dzero = new xAOD::CompositeParticle;
    composites->push_back(Dzero);
    Dzero->setCharge(0);
    Dzero->setPdgId(-421 * Ktrack->charge());
    Dzero->setP4(fitOutput.Momentum);
    Dzero->addPart(Ktrack, false);
    Dzero->addPart(Pitrack1, false);
    Dzero->addPart(Pitrack2, false);
    Dzero->addPart(Pitrack3, false);
    (*m_decayType)(*Dzero) = 30;

    // save fit output as a decoration
    (*m_fitOutput)(*Dzero) = fitOutput;

    // D0 index
    unsigned int D0_index = composites->size() - 1;
    (*m_D0Index)(*Dzero) = D0_index;

    // Daughter info
    std::vector<int> daughterBarcodes_Dzero;
    std::vector<int> daughterDBarcodes_Dzero;
    std::vector<int> daughterTruthPdgId_Dzero;
    std::vector<int> daughterParentTruthPdgId_Dzero;
    if (m_is_mc && m_doTruthMathing) {
      std::vector<const xAOD::TrackParticle *> tracks = {Ktrack, Pitrack1,
                                                         Pitrack2, Pitrack3};
      (*m_truthBarcode)(*Dzero) = TruthUtils::GetTruthBarcode(
          tracks, daughterDBarcodes_Dzero, daughterParentTruthPdgId_Dzero,
          daughterBarcodes_Dzero, daughterTruthPdgId_Dzero);
    } else {
      (*m_truthBarcode)(*Dzero) = -1;
      daughterBarcodes_Dzero = {0, 0, 0, 0};
      daughterDBarcodes_Dzero = {0, 0, 0, 0};
      daughterTruthPdgId_Dzero = {0, 0, 0, 0};
      daughterParentTruthPdgId_Dzero = {0, 0, 0, 0};
    }

    DaughterInfo daughterInfo{{Ktrack, Pitrack1, Pitrack2, Pitrack3},
                              {PDGID_KAON, PDGID_PION, PDGID_PION, PDGID_PION},
                              daughterDBarcodes_Dzero,
                              daughterBarcodes_Dzero,
                              daughterTruthPdgId_Dzero,
                              daughterParentTruthPdgId_Dzero};

    (*m_daughter_info)(*Dzero) = daughterInfo;

    //
    // record the D*0 particle
    //
    xAOD::CompositeParticle *Dstar = new xAOD::CompositeParticle;
    composites->push_back(Dstar);
    Dstar->setCharge(-Ktrack->charge());
    Dstar->setPdgId(413 * Dstar->charge());
    Dstar->setP4(vDstar);
    Dstar->addPart(Ktrack, false);
    Dstar->addPart(Pitrack1, false);
    Dstar->addPart(Pitrack2, false);
    Dstar->addPart(Pitrack3, false);
    Dstar->addPart(trk5, false);
    (*m_decayType)(*Dstar) = 3;

    // save fit output as a decoration
    (*m_fitOutput)(*Dstar) = fitOutput;

    // D0 index
    (*m_D0Index)(*Dstar) = D0_index;

    // Delta Mass
    (*m_DeltaMass)(*Dstar) = Dstar->m() - Dzero->m();

    // Slow pion d0
    (*m_SlowPionD0)(*Dstar) = ipAndSigma5->IPd0;
    (*m_SlowPionZ0SinTheta)(*Dstar) = ipAndSigma5->IPz0SinTheta;

    // Daughter info
    std::vector<int> daughterBarcodes_Dstar;
    std::vector<int> daughterDBarcodes_Dstar;
    std::vector<int> daughterTruthPdgId_Dstar;
    std::vector<int> daughterParentTruthPdgId_Dstar;
    if (m_is_mc && m_doTruthMathing) {
      std::vector<const xAOD::TrackParticle *> tracks = {
          Ktrack, Pitrack1, Pitrack2, Pitrack3, trk5};
      (*m_truthBarcode)(*Dstar) = TruthUtils::GetTruthBarcode(
          tracks, daughterDBarcodes_Dstar, daughterParentTruthPdgId_Dstar,
          daughterBarcodes_Dstar, daughterTruthPdgId_Dstar);
    } else {
      (*m_truthBarcode)(*Dstar) = -1;
      daughterBarcodes_Dstar = {0, 0, 0, 0, 0};
      daughterDBarcodes_Dstar = {0, 0, 0, 0, 0};
      daughterTruthPdgId_Dstar = {0, 0, 0, 0, 0};
      daughterParentTruthPdgId_Dstar = {0, 0, 0, 0, 0};
    }

    DaughterInfo daughterInfo_Dstar{
        {Ktrack, Pitrack1, Pitrack2, Pitrack3, trk5},
        {PDGID_KAON, PDGID_PION, PDGID_PION, PDGID_PION, PDGID_PION},
        daughterDBarcodes_Dstar,
        daughterBarcodes_Dstar,
        daughterTruthPdgId_Dstar,
        daughterParentTruthPdgId_Dstar};
    (*m_daughter_info)(*Dstar) = daughterInfo_Dstar;

    delete ipAndSigma5;
    return true;

  } // trk5 loop

  return false;
}

int DSelectionAlg ::selectV0TrackCandidates(
    const xAOD::Vertex *primaryVertex, const xAOD::VertexContainer *vertices,
    xAOD::CompositeParticleContainer *composites,
    const xAOD::TrackParticleContainer *tracks, int decayType,
    int reqTrackCharge) {
  // Note: reqTrackCharge==0 means keep both charges

  int npassed = 0;
  if (!(decayType == 6 || decayType == 7 || decayType == 8 || decayType == 9)) {
    ATH_MSG_WARNING("selectV0TrackCandidates called with unknown decayType "
                    << decayType);
    return false;
  }

  //  for (const xAOD::Vertex *vertex : *vertices) {
  for (unsigned int itv = 0; itv < vertices->size(); itv++) {
    const xAOD::Vertex *vertex = vertices->at(itv);
    if (vertex->trackParticleLinks().size() != 2) {
      ATH_MSG_WARNING("V0 vertex wih  " << vertex->trackParticleLinks().size()
                                        << " tracks");
      continue;
    }
    // Get the trackParticleLinks for the V0 candidate
    ElementLink<DataVector<xAOD::TrackParticle>> link1 =
        vertex->trackParticleLinks()[0];
    ElementLink<DataVector<xAOD::TrackParticle>> link2 =
        vertex->trackParticleLinks()[1];
    TLorentzVector vtxFourMom;
    vtxFourMom.SetXYZM(
        vertex->auxdata<float>("px"), vertex->auxdata<float>("py"),
        vertex->auxdata<float>("pz"), vertex->auxdata<float>("mass"));
    // Iterate throught selected tracks
    for (unsigned int tid1 = 0; tid1 < tracks->size(); tid1++) {
      const xAOD::TrackParticle *trk1 = tracks->at(tid1);
      // Remove tracks associated with the V0
      // Comparing links doesn't work, so check four-momenta
      //        if(*link1 == trk1) continue;
      //        if(*link2 == trk1) continue;
      if ((*link1)->p4() == trk1->p4())
        continue;
      if ((*link2)->p4() == trk1->p4())
        continue;
      // Min pT cut for tracks
      if (!(trk1->pt() > m_minTrackPtLambdaC))
        continue;
      if ((reqTrackCharge * trk1->charge() < 0))
        continue;
      TLorentzVector trkFourMom;
      if (decayType == 6) {
        trkFourMom.SetPtEtaPhiM(trk1->pt(), trk1->eta(), trk1->phi(),
                                MASS_PION);
      } else if (decayType == 7) {
        trkFourMom.SetPtEtaPhiM(trk1->pt(), trk1->eta(), trk1->phi(),
                                MASS_PROTON);
      } else if (decayType == 8) {
        trkFourMom.SetPtEtaPhiM(trk1->pt(), trk1->eta(), trk1->phi(),
                                MASS_KAON);
      } else if (decayType == 9) {
        trkFourMom.SetPtEtaPhiM(trk1->pt(), trk1->eta(), trk1->phi(),
                                MASS_PION);
      }
      if (!(trkFourMom.DeltaR(vtxFourMom) < m_maxDPdRKPi))
        continue;
      // Calculate mass
      double candMass = (vtxFourMom + trkFourMom).M();
      double minMass = m_minMassDPin;
      double maxMass = m_maxMassDPin;
      if (decayType == 6 || decayType == 7) {
        minMass = m_minMassLambdaCin;
        maxMass = m_maxMassLambdaCin;
      }
      if (candMass > minMass && candMass < maxMass &&
          (vtxFourMom + trkFourMom).Pt() > m_minPtLambdaC) {
        m_V0_Index = itv;
        bool found = FindV0TrackCandidate(primaryVertex, vertex, trk1,
                                          composites, decayType);
        if (found)
          npassed++;
      }
    }
  }
  return npassed;
}

bool DSelectionAlg ::FindV0TrackCandidate(
    const xAOD::Vertex *primaryVertex, const xAOD::Vertex *vertex,
    const xAOD::TrackParticle *theTrack,
    xAOD::CompositeParticleContainer *composites, int decayType) {
  if (!(decayType == 6 || decayType == 7 || decayType == 8 || decayType == 9)) {
    ATH_MSG_WARNING("FindV0TrackCandidate called with unknown decay type "
                    << decayType);
    return false;
  }

  // Define and fill  the std::vectors that will be passed to the cascade vertex
  // finder
  // ====================================================================================
  std::vector<const xAOD::TrackParticle *> tracksCvertex;
  std::vector<const xAOD::TrackParticle *> tracksV0;
  std::vector<float> pdgIDV0;
  std::vector<double> massesCvertex;
  std::vector<double> massesV0;
  std::vector<double> Masses;
  int fitPDGid = 0;
  int trkPDGid = 0;
  double massVee = 0;

  ElementLink<DataVector<xAOD::TrackParticle>> link1 =
      vertex->trackParticleLinks()[0];
  ElementLink<DataVector<xAOD::TrackParticle>> link2 =
      vertex->trackParticleLinks()[1];
  const xAOD::TrackParticle *v0tk1 = *link1;
  const xAOD::TrackParticle *v0tk2 = *link2;
  tracksV0.push_back(v0tk1);
  tracksV0.push_back(v0tk2);
  double d0Cut = 0.0;
  double lxyCut = 0.0;

  if (decayType == 6) {
    massesCvertex.push_back(MASS_PION);
    massVee = MASS_LAMBDA0;
    if (theTrack->charge() * ((*link1)->charge()) > 0) {
      // Lambda
      massesV0.push_back(MASS_PROTON);
      massesV0.push_back(MASS_PION);
      pdgIDV0.push_back(PDGID_PROTON);
      pdgIDV0.push_back(-PDGID_PION);
    } else {
      // LambdaBar
      massesV0.push_back(MASS_PION);
      massesV0.push_back(MASS_PROTON);
      pdgIDV0.push_back(PDGID_PION);
      pdgIDV0.push_back(-PDGID_PROTON);
    }
    fitPDGid = 4122;
    trkPDGid = PDGID_PION;
    d0Cut = m_maxd0LambdaCToLambdaPi;
    lxyCut = m_minLxyLambdaCToLambdaPi;
  } else if (decayType == 7) {
    massesCvertex.push_back(MASS_PROTON);
    massVee = MASS_KSHORT;
    massesV0.push_back(MASS_PION);
    massesV0.push_back(MASS_PION);
    pdgIDV0.push_back(PDGID_PION);
    pdgIDV0.push_back(-PDGID_PION);
    fitPDGid = 4122;
    trkPDGid = -PDGID_PROTON;
    d0Cut = m_maxd0LambdaCToKsP;
    lxyCut = m_minLxyLambdaCToKsP;
  } else if (decayType == 8) {
    massesCvertex.push_back(MASS_KAON);
    massVee = MASS_KSHORT;
    massesV0.push_back(MASS_PION);
    massesV0.push_back(MASS_PION);
    pdgIDV0.push_back(PDGID_PION);
    pdgIDV0.push_back(-PDGID_PION);
    fitPDGid = 431;
    trkPDGid = PDGID_KAON;
    d0Cut = m_maxd0DToKsK;
    lxyCut = m_minLxyDToKsK;
  } else if (decayType == 9) {
    massesCvertex.push_back(MASS_PION);
    massVee = MASS_KSHORT;
    massesV0.push_back(MASS_PION);
    massesV0.push_back(MASS_PION);
    pdgIDV0.push_back(PDGID_PION);
    pdgIDV0.push_back(-PDGID_PION);
    fitPDGid = 411;
    trkPDGid = PDGID_PION;
    d0Cut = m_maxd0DtoKsPi;
    lxyCut = m_minLxyDToKsPi;
  }

  tracksCvertex.push_back(theTrack);

  // Setup the cascade vertex finder
  m_VKVrtFitter->setDefault();
  int robustness = 0;
  m_VKVrtFitter->setRobustness(robustness);
  // Build up the topology
  std::vector<Trk::VertexID> vrtList;

  // V0 vertex
  Trk::VertexID vID;
  if (m_constrV0) {
    vID = m_VKVrtFitter->startVertex(tracksV0, massesV0, massVee);
  } else {
    vID = m_VKVrtFitter->startVertex(tracksV0, massesV0);
  }
  vrtList.push_back(vID);

  // LambdaC vertex
  Trk::VertexID vID2 =
      m_VKVrtFitter->nextVertex(tracksCvertex, massesCvertex, vrtList);
  vrtList.push_back(vID2);
  // Do the fit
  Trk::VxCascadeInfo *result(m_VKVrtFitter->fitCascade());
  if (result == NULL) {
    return false;
  } else {
    result->getSVOwnership(
        true); // to deleted the vertices of cascade together with VxCascadeInfo
  }
  // Make required cuts on fitted results

  const std::vector<std::vector<TLorentzVector>> &moms =
      result->getParticleMoms();
  TLorentzVector vtxFourMom = moms[1][0] + moms[1][1];
  double fit_mass = (vtxFourMom).M();
  // cos thetastar
  double thetastar = 0.;
  TLorentzVector tkboost = moms[1][0];
  TVector3 boost_v0 = vtxFourMom.BoostVector();
  boost_v0 *= -1.0;
  tkboost.Boost(boost_v0);
  thetastar = (vtxFourMom).Angle(tkboost.Vect());
  if (decayType == 6 || decayType == 7) {
    if (!(cos(thetastar) > m_mincosthetastarLambdaC)) {
      delete result;
      return false;
    }
    if (!(cos(thetastar) <= m_maxcosthetastarLambdaC)) {
      delete result;
      return false;
    }
    if (!(fit_mass > m_minMassLambdaC && fit_mass < m_maxMassLambdaC &&
          (vtxFourMom).Pt() > m_minPtLambdaC)) {
      delete result;
      return false;
    }
    if (result->fitChi2() > m_maxfitChi2LambdaC) {
      delete result;
      return false;
    }
  } else if (decayType == 8 || decayType == 9) {
    if (!(cos(thetastar) > m_mincosthetastarDP)) {
      delete result;
      return false;
    }
    if (!(cos(thetastar) <= m_maxcosthetastarDP)) {
      delete result;
      return false;
    }
    if (!(fit_mass > m_minMassDP && fit_mass < m_maxMassDP &&
          (vtxFourMom).Pt() > m_minPtDP)) {
      delete result;
      return false;
    }
    if (result->fitChi2() > m_maxfitChi2D0) {
      delete result;
      return false;
    }
  }
  //
  // cuts passed: record the candidate
  //
  FitOutput fitOutput = {};

  // save fit output as a decoration
  fitOutput.Momentum = vtxFourMom;
  fitOutput.Chi2 = result->fitChi2();
  fitOutput.Charge = theTrack->charge();
  //    fitOutput.TrkAtVrt = xx;
  fitOutput.VertexPosition = ((result->vertices())[1])->position();

  Amg::Vector3D primVtxPos(primaryVertex->position().x(),
                           primaryVertex->position().y(),
                           primaryVertex->position().z());

  Amg::Vector3D vDist = fitOutput.VertexPosition - primVtxPos;
  fitOutput.Lxy = (vDist.x() * fitOutput.Momentum.Px() +
                   vDist.y() * fitOutput.Momentum.Py()) /
                  fitOutput.Momentum.Perp();

  // ADD LXY HERE
  fitOutput.LxyErr =
      m_CascadeTools->lxyError(moms[1], result->getCovariance()[1],
                               ((result->vertices())[1]), primaryVertex);

  if (!(fabs(fitOutput.Lxy) > lxyCut)) {
    delete result;
    return false;
  }

  std::vector<double> Impact;
  double vs = sin(atan2(fitOutput.Momentum.Py(), fitOutput.Momentum.Px()) -
                  atan2(vDist.y(), vDist.x()));
  vs = vs >= 0. ? 1. : -1.;
  // CascadeTools returns an unsigned impact paramter.  Sign it here
  Impact.push_back(vs * m_CascadeTools->a0xy(moms[1], ((result->vertices())[1]),
                                             primaryVertex));
  Impact.push_back(
      m_CascadeTools->a0z(moms[1], ((result->vertices())[1]), primaryVertex));
  Impact.push_back(acos(m_CascadeTools->cosTheta(
      moms[1], ((result->vertices())[1]), primaryVertex)));
  std::vector<double> ImpactError;
  ImpactError.push_back(
      m_CascadeTools->a0xyError(moms[1], result->getCovariance()[1],
                                ((result->vertices())[1]), primaryVertex));
  ImpactError.push_back(
      m_CascadeTools->a0zError(moms[1], result->getCovariance()[1],
                               ((result->vertices())[1]), primaryVertex));
  // Not sure how to calculate the error on Theta
  ImpactError.push_back(0.0);

  fitOutput.Impact = Impact;
  fitOutput.ImpactError = ImpactError;
  // Not sure this is correct
  double a0err =
      m_CascadeTools->a0Error(moms[1], result->getCovariance()[1],
                              (result->vertices())[1], primaryVertex);
  double a0 =
      m_CascadeTools->a0(moms[1], (result->vertices())[1], primaryVertex);
  if (a0err != 0) {
    fitOutput.ImpactSignificance = a0 / a0err;
  } else {
    fitOutput.ImpactSignificance = 999;
  }
  fitOutput.ErrorMatrix = {0};
  fitOutput.Chi2PerTrack = {0};

  if (!(fabs(fitOutput.Impact[0]) < d0Cut)) {
    delete result;
    return false;
  }

  // Daughter info
  std::vector<int> daughterBarcodes;
  std::vector<int> daughterDBarcodes;
  std::vector<int> daughterTruthPdgId;
  std::vector<int> daughterParentTruthPdgId;

  xAOD::CompositeParticle *theCharmCandidate = new xAOD::CompositeParticle;
  composites->push_back(theCharmCandidate);
  theCharmCandidate->setCharge(theTrack->charge());
  theCharmCandidate->setPdgId(fitPDGid * theTrack->charge());
  theCharmCandidate->setP4(vtxFourMom);

  theCharmCandidate->addPart(theTrack, false);
  theCharmCandidate->addPart(*link1, false);
  theCharmCandidate->addPart(*link2, false);
  (*m_decayType)(*theCharmCandidate) = decayType;
  (*m_V0Index)(*theCharmCandidate) = m_V0_Index;
  (*m_costhetastar)(*theCharmCandidate) = cos(thetastar);
  if (m_is_mc && m_doTruthMathing) {
    std::vector<const xAOD::TrackParticle *> tracks = {theTrack, *link1,
                                                       *link2};
    (*m_truthBarcode)(*theCharmCandidate) = TruthUtils::GetTruthBarcode(
        tracks, daughterDBarcodes, daughterParentTruthPdgId, daughterBarcodes,
        daughterTruthPdgId);
  } else {
    (*m_truthBarcode)(*theCharmCandidate) = -1;
    daughterBarcodes = {0};
    daughterDBarcodes = {0};
    daughterTruthPdgId = {0};
    daughterParentTruthPdgId = {0};
  }

  DaughterInfo daughterInfo{//      {theTrack,tracksV0[0],tracksV0[2]},
                            //       {PDGID_PION,pdgIDV0[0],pdgIDV0[1]},
                            {theTrack},         {trkPDGid * theTrack->charge()},
                            daughterDBarcodes,  daughterBarcodes,
                            daughterTruthPdgId, daughterParentTruthPdgId};

  (*m_daughter_info)(*theCharmCandidate) = daughterInfo;
  (*m_fitOutput)(*theCharmCandidate) = fitOutput;

  //  std::cout <<  "theCharmCandidate- fit candidate mass: " << fit_mass  << "
  //  chisq " << fitOutput.Chi2 << " decayType " << decayType<< std::endl;
  delete result;
  return true;
}
bool DSelectionAlg ::FindLambdacCandidate(
    const xAOD::Vertex *primaryVertex, const xAOD::TrackParticle *ptrack,
    const xAOD::TrackParticle *Ktrack, const xAOD::TrackParticle *Pitrack,
    xAOD::CompositeParticleContainer *composites) {
  //
  // make a LambdaC -> p K pi candidate
  //
  std::vector<const xAOD::TrackParticle *> Tracks;
  std::vector<double> Masses;

  Tracks.clear();
  Tracks.push_back(ptrack);
  Tracks.push_back(Ktrack);
  Tracks.push_back(Pitrack);

  Masses.clear();
  Masses.push_back(MASS_PROTON);
  Masses.push_back(MASS_KAON);
  Masses.push_back(MASS_PION);

  FitOutput fitOutput = {};

  FitInput fitInput{true, *primaryVertex, Tracks, Masses};

  FitSelection fitSelection{m_maxd0LambdaC,      m_minLxyLambdaC,
                            m_maxMassLambdaC,    m_minMassLambdaC,
                            m_maxfitChi2LambdaC, m_minPtLambdaC};

  bool success = VertexFit(fitInput, fitOutput, fitSelection);
  if (!success)
    return false;

  TLorentzVector tp;
  tp.SetPtEtaPhiM(ptrack->pt(), ptrack->eta(), ptrack->phi(), MASS_PROTON);
  TLorentzVector tk;
  tk.SetPtEtaPhiM(Ktrack->pt(), Ktrack->eta(), Ktrack->phi(), MASS_KAON);
  TLorentzVector tpi;
  tpi.SetPtEtaPhiM(Pitrack->pt(), Pitrack->eta(), Pitrack->phi(), MASS_PION);

  // cos thetastar
  double thetastar = 0.;
  TLorentzVector tkboost = tk;
  TVector3 boost_v0 = (tp + tk + tpi).BoostVector();
  boost_v0 *= -1.0;
  tkboost.Boost(boost_v0);
  thetastar = (tp + tk + tpi).Angle(tkboost.Vect());

  if (!(cos(thetastar) > m_mincosthetastarDP))
    return false;
  if (!(cos(thetastar) <= m_maxcosthetastarDP))
    return false;

  //
  // Assume D+->Kpipi and record mass
  //
  TLorentzVector tpi1;
  tpi1.SetPtEtaPhiM(ptrack->pt(), ptrack->eta(), ptrack->phi(), MASS_PION);
  double mKpipi = (tpi1 + tpi + tk).M();

  // reject D*
  double mKpi1 = (tk + tpi).M();
  double mKpi2 = (tk + tpi1).M();
  if (mKpi1 < m_mindMLC)
    return false;
  if (mKpi2 < m_mindMLC)
    return false;

  // reject Ds
  TLorentzVector tk1;
  tk1.SetPtEtaPhiM(ptrack->pt(), ptrack->eta(), ptrack->phi(), MASS_KAON);
  TLorentzVector tk2;
  tk2.SetPtEtaPhiM(Pitrack->pt(), Pitrack->eta(), Pitrack->phi(), MASS_KAON);
  double mPhi1 = fabs((tk + tk1).M() - MASS_PHI);
  double mPhi2 = fabs((tk + tk2).M() - MASS_PHI);
  if (mPhi1 < m_phiwindowLC)
    return false;
  if (mPhi2 < m_phiwindowLC)
    return false;

  //
  // record the LambdaC particle
  //
  xAOD::CompositeParticle *LambdaC = new xAOD::CompositeParticle;
  composites->push_back(LambdaC);
  LambdaC->setCharge(ptrack->charge());
  LambdaC->setPdgId(4122 * LambdaC->charge());
  LambdaC->setP4(fitOutput.Momentum);
  LambdaC->addPart(ptrack, false);
  LambdaC->addPart(Ktrack, false);
  LambdaC->addPart(Pitrack, false);
  (*m_decayType)(*LambdaC) = 11;

  // save fit output as a decoration
  (*m_fitOutput)(*LambdaC) = fitOutput;

  // D+ specific cuts: costhetastar, mKpi, mPhi
  (*m_costhetastar)(*LambdaC) = cos(thetastar);
  (*m_mKpi1)(*LambdaC) = mKpi1;
  (*m_mKpi2)(*LambdaC) = mKpi2;
  (*m_mPhi1)(*LambdaC) = mPhi1;
  (*m_mPhi2)(*LambdaC) = mPhi2;
  (*m_mKpipi)(*LambdaC) = mKpipi;

  // Daughter info
  std::vector<int> daughterBarcodes;
  std::vector<int> daughterDBarcodes;
  std::vector<int> daughterTruthPdgId;
  std::vector<int> daughterParentTruthPdgId;
  if (m_is_mc && m_doTruthMathing) {
    std::vector<const xAOD::TrackParticle *> tracks = {ptrack, Ktrack, Pitrack};
    (*m_truthBarcode)(*LambdaC) = TruthUtils::GetTruthBarcode(
        tracks, daughterDBarcodes, daughterParentTruthPdgId, daughterBarcodes,
        daughterTruthPdgId);
  } else {
    (*m_truthBarcode)(*LambdaC) = -1;
    daughterBarcodes = {0, 0, 0};
    daughterDBarcodes = {0, 0, 0};
    daughterTruthPdgId = {0, 0, 0};
    daughterParentTruthPdgId = {0, 0, 0};
  }

  DaughterInfo daughterInfo{
      {ptrack, Ktrack, Pitrack}, {PDGID_PROTON, PDGID_KAON, PDGID_PION},
      daughterDBarcodes,         daughterBarcodes,
      daughterTruthPdgId,        daughterParentTruthPdgId};
  (*m_daughter_info)(*LambdaC) = daughterInfo;

  return true;
}

bool DSelectionAlg ::VertexFit(FitInput &fitInput, FitOutput &fitOutput,
                               FitSelection &fitSelection) {
  m_VKVrtFitter->setDefault();

  // fit constraints
  if (fitInput.PointedFit) {
    if (m_constraintType >= 0) {
      m_VKVrtFitter->setCnstType(m_constraintType);
    }
    m_VKVrtFitter->setVertexForConstraint(
        fitInput.PrimaryVertex.position().x(),
        fitInput.PrimaryVertex.position().y(),
        fitInput.PrimaryVertex.position().z());
    m_VKVrtFitter->setCovVrtForConstraint(
        fitInput.PrimaryVertex.covariancePosition()(Trk::x, Trk::x),
        fitInput.PrimaryVertex.covariancePosition()(Trk::y, Trk::x),
        fitInput.PrimaryVertex.covariancePosition()(Trk::y, Trk::y),
        fitInput.PrimaryVertex.covariancePosition()(Trk::z, Trk::x),
        fitInput.PrimaryVertex.covariancePosition()(Trk::z, Trk::y),
        fitInput.PrimaryVertex.covariancePosition()(Trk::z, Trk::z));
  }
  m_VKVrtFitter->setMassInputParticles(fitInput.Masses);

  // Calculate the full covariance matrix (off by default)
  m_VKVrtFitter->setMomCovCalc(1);

  //
  // get approximate position
  //
  if (m_doApproximateFit) {
    StatusCode sc = m_VKVrtFitter->VKalVrtFitFast(fitInput.Tracks,
                                                  fitOutput.VertexPosition);
    if (!sc.isSuccess()) {
      m_VKVrtFitter->setApproximateVertex(fitInput.PrimaryVertex.x(),
                                          fitInput.PrimaryVertex.y(),
                                          fitInput.PrimaryVertex.z());
    } else {
      m_VKVrtFitter->setApproximateVertex(fitOutput.VertexPosition.x(),
                                          fitOutput.VertexPosition.y(),
                                          fitOutput.VertexPosition.z());
    }
  }

  //
  // run the vertex fit
  //
  StatusCode sc = m_VKVrtFitter->VKalVrtFit(
      fitInput.Tracks, {}, fitOutput.VertexPosition, fitOutput.Momentum,
      fitOutput.Charge, fitOutput.ErrorMatrix, fitOutput.Chi2PerTrack,
      fitOutput.TrkAtVrt, fitOutput.Chi2);
  if (!sc.isSuccess())
    return false;

  //
  // calculate the distance
  //
  Amg::Vector3D primVtxPos(fitInput.PrimaryVertex.x(),
                           fitInput.PrimaryVertex.y(),
                           fitInput.PrimaryVertex.z());

  Amg::Vector3D vDist = fitOutput.VertexPosition - primVtxPos;
  fitOutput.Lxy = (vDist.x() * fitOutput.Momentum.Px() +
                   vDist.y() * fitOutput.Momentum.Py()) /
                  fitOutput.Momentum.Perp();
  fitOutput.LxyErr = 999;

  //
  // Get Impact
  //
  sc = m_VKVrtFitter->VKalVrtCvtTool(
      fitOutput.VertexPosition, fitOutput.Momentum, fitOutput.ErrorMatrix, 0,
      fitOutput.Perigee, fitOutput.PerigeeCovariance);
  if (!sc.isSuccess())
    return false;

  const Trk::Track *track = m_VKVrtFitter->CreateTrkTrack(
      fitOutput.Perigee, fitOutput.PerigeeCovariance);
  if (!track)
    return false;

  fitOutput.ImpactSignificance = m_VKVrtFitter->VKalGetImpact(
      track, primVtxPos, 0, fitOutput.Impact, fitOutput.ImpactError);
  delete track;

  if (BasicKinematicCuts(fitOutput, fitSelection)) {
    return true;
  } else {
    return false;
  }
}

bool DSelectionAlg ::BasicKinematicCuts(FitOutput &fitOutput,
                                        FitSelection &fitSelection) {
  if (!(fitOutput.Momentum.M() > fitSelection.massMin &&
        fitOutput.Momentum.M() < fitSelection.massMax))
    return false;

  if (!(fitOutput.Momentum.Perp() > fitSelection.pTMin))
    return false;

  if (!(fitOutput.Lxy > fitSelection.LxyMin))
    return false;

  if (!(fitOutput.Chi2 < fitSelection.maxChi2))
    return false;

  if (!(fabs(fitOutput.Impact[0]) < fitSelection.d0cut))
    return false;

  return true;
}

void DSelectionAlg ::DecorateCompositeParticle(
    const xAOD::CompositeParticle *cp,
    const xAOD::TrackParticleContainer *tracks) {
  ANA_MSG_DEBUG("Decorating composite particle " << cp->pdgId());

  cp->auxdecor<float>("eta") = cp->eta();
  cp->auxdecor<float>("phi") = cp->phi();
  cp->auxdecor<float>("pt") = cp->pt();
  cp->auxdecor<float>("m") = cp->m();
  cp->auxdecor<int>("pdgId") = cp->pdgId();

  // isolation
  double ptcone20 = 0;
  double ptcone30 = 0;
  double ptcone40 = 0;
  for (unsigned int tid = 0; tid < tracks->size(); tid++) {
    const xAOD::TrackParticle *trk = tracks->at(tid);
    if (!cp->contains(trk)) {
      double deltaR = trk->p4().DeltaR(cp->p4());
      if (deltaR < 0.2) {
        ptcone20 += trk->pt();
      }
      if (deltaR < 0.3) {
        ptcone30 += trk->pt();
      }
      if (deltaR < 0.4) {
        ptcone40 += trk->pt();
      }
    }
  }
  (*m_ptcone20)(*cp) = ptcone20;
  (*m_ptcone30)(*cp) = ptcone30;
  (*m_ptcone40)(*cp) = ptcone40;
  //

  // fitOutput
  FitOutput fitOutput = cp->auxdecor<FitOutput>("fitOutput");
  (*m_fitOutput__PerigeeCovariance)(*cp) = fitOutput.PerigeeCovariance;
  (*m_fitOutput__Perigee)(*cp) = fitOutput.Perigee;
  (*m_fitOutput__ImpactError)(*cp) = fitOutput.ImpactError[0];
  (*m_fitOutput__Impact)(*cp) = fitOutput.Impact[0];
  (*m_fitOutput__ImpactZ0)(*cp) = fitOutput.Impact[1];
  (*m_fitOutput__ImpactZ0Error)(*cp) = fitOutput.ImpactError[1];
  (*m_fitOutput__ImpactTheta)(*cp) = fitOutput.Impact[2];
  (*m_fitOutput__ImpactZ0SinTheta)(*cp) =
      fitOutput.Impact[1] * sin(fitOutput.Impact[2]);
  (*m_fitOutput__ImpactSignificance)(*cp) = fitOutput.ImpactSignificance;
  (*m_fitOutput__ErrorMatrix)(*cp) = fitOutput.ErrorMatrix;
  (*m_fitOutput__Chi2PerTrack)(*cp) = fitOutput.Chi2PerTrack;
  (*m_fitOutput__Charge)(*cp) = fitOutput.Charge;
  (*m_fitOutput__Lxy)(*cp) = fitOutput.Lxy;
  (*m_fitOutput__LxyErr)(*cp) = fitOutput.LxyErr;
  (*m_fitOutput__Chi2)(*cp) = fitOutput.Chi2;
  (*m_fitOutput__VertexPosition)(*cp) = {(float)fitOutput.VertexPosition.x(),
                                         (float)fitOutput.VertexPosition.y(),
                                         (float)fitOutput.VertexPosition.z()};

  // daughterInfo
  DaughterInfo daughterInfo = cp->auxdecor<DaughterInfo>("daughterInfo");
  (*m_daughter_passLoose)(*cp) = {};
  (*m_daughter_passTight)(*cp) = {};
  (*m_daughter_truthMatchProbability)(*cp) = {};
  (*m_daughter_trackId)(*cp) = {};
  (*m_daughter_pdgId)(*cp) = {};
  (*m_daughter_pt)(*cp) = {};
  (*m_daughter_eta)(*cp) = {};
  (*m_daughter_phi)(*cp) = {};
  (*m_daughter_chi2)(*cp) = {};
  (*m_daughter_truthBarcode)(*cp) = {};
  (*m_daughter_truthDBarcode)(*cp) = {};
  (*m_daughter_truthPdgId)(*cp) = {};
  (*m_daughter_truthParentPdgId)(*cp) = {};
  (*m_daughter_z0SinTheta)(*cp) = {};
  (*m_daughter_z0SinThetaPV)(*cp) = {};
  (*m_daughter_d0)(*cp) = {};
  (*m_daughter_d0PV)(*cp) = {};
  std::vector<float> daughter_pdgIds = daughterInfo.daughterPdgIds;
  // skip last track for D* with soft pion in the decay since it is not vertexed
  int skipSoftPi = 0;
  if (abs(cp->pdgId()) == 413)
    skipSoftPi = 1;
  for (unsigned int i = 0; i < daughter_pdgIds.size(); i++) {
    // tmp track to have post fit kinematic values, skip for D* soft pion
    double pt_tmp = daughterInfo.daughterTracks[i]->pt();
    double eta_tmp = daughterInfo.daughterTracks[i]->eta();
    double phi_tmp = daughterInfo.daughterTracks[i]->phi();
    if (i < daughter_pdgIds.size() - skipSoftPi) {
      pt_tmp = 1. / fabs(fitOutput.TrkAtVrt.at(i).at(2)) *
               sin(fitOutput.TrkAtVrt.at(i).at(1));
      eta_tmp = -log(tan(fitOutput.TrkAtVrt.at(i).at(1) / 2.));
      phi_tmp = fitOutput.TrkAtVrt.at(i).at(0);
    }
    // Filling output branches
    (*m_daughter_passLoose)(*cp).push_back(
        m_selectionAccessorLoose->getBool(*daughterInfo.daughterTracks[i]));
    (*m_daughter_passTight)(*cp).push_back(
        m_selectionAccessorTight->getBool(*daughterInfo.daughterTracks[i]));
    if (m_is_mc)
      (*m_daughter_truthMatchProbability)(*cp).push_back(
          (*daughterInfo.daughterTracks[i])
              .auxdata<float>("truthMatchProbability"));
    (*m_daughter_trackId)(*cp).push_back(
        std::find(tracks->begin(), tracks->end(),
                  daughterInfo.daughterTracks[i]) -
        tracks->begin());
    (*m_daughter_pdgId)(*cp).push_back(
        daughter_pdgIds[i] * daughterInfo.daughterTracks[i]->charge());
    (*m_daughter_pt)(*cp).push_back(pt_tmp);
    (*m_daughter_eta)(*cp).push_back(eta_tmp);
    (*m_daughter_phi)(*cp).push_back(phi_tmp);
    (*m_daughter_chi2)(*cp).push_back(
        daughterInfo.daughterTracks[i]->chiSquared());
    (*m_daughter_truthBarcode)(*cp).push_back(
        daughterInfo.daughterTruthBarcode[i]);
    (*m_daughter_truthDBarcode)(*cp).push_back(
        daughterInfo.daughterTruthDBarcode[i]);
    (*m_daughter_truthPdgId)(*cp).push_back(daughterInfo.daughterTruthPdgId[i]);
    (*m_daughter_truthParentPdgId)(*cp).push_back(
        daughterInfo.daughterParentTruthPdgId[i]);
    (*m_daughter_z0SinTheta)(*cp).push_back(
        daughterInfo.daughterTracks[i]->z0() *
        sin(daughterInfo.daughterTracks[i]->theta()));
    (*m_daughter_d0)(*cp).push_back(daughterInfo.daughterTracks[i]->d0());

    // impact parameter w.r.t. the PV
    const Trk::ImpactParametersAndSigma *ipAndSigma =
        m_trackToVertexIPEstimator->estimate(daughterInfo.daughterTracks[i],
                                             m_primaryVertex);
    (*m_daughter_z0SinThetaPV)(*cp).push_back(ipAndSigma->IPz0SinTheta);
    (*m_daughter_d0PV)(*cp).push_back(ipAndSigma->IPd0);
    delete ipAndSigma;
  }

  if (fabs(cp->pdgId()) != 413) {
    (*m_DeltaMass)(*cp) = -999;
    (*m_SlowPionD0)(*cp) = -999;
    (*m_SlowPionZ0SinTheta)(*cp) = -999;
  }

  if (fabs(cp->pdgId()) != 413 && fabs(cp->pdgId()) != 421) {
    (*m_D0Index)(*cp) = -999;
  }

  if ((fabs(cp->pdgId()) != 411) && (fabs(cp->pdgId()) != 431) &&
      (fabs(cp->pdgId()) != 4122)) {
    (*m_costhetastar)(*cp) = -999;
    (*m_mKpi1)(*cp) = -999;
    (*m_mKpi2)(*cp) = -999;
    (*m_mPhi1)(*cp) = -999;
    (*m_mPhi2)(*cp) = -999;
    (*m_mKpipi)(*cp) = -999;
  }
  // Modes with Vees
  if (cp->auxdecor<int>("decayType") == 6 ||
      cp->auxdecor<int>("decayType") == 7 ||
      cp->auxdecor<int>("decayType") == 8 ||
      cp->auxdecor<int>("decayType") == 9) {
    (*m_mKpi1)(*cp) = -999;
    (*m_mKpi2)(*cp) = -999;
    (*m_mPhi1)(*cp) = -999;
    (*m_mPhi2)(*cp) = -999;
    (*m_mKpipi)(*cp) = -999;
  } else {
    (*m_V0Index)(*cp) = -999;
  }
}

void DSelectionAlg ::initializeDecorators() {
  m_D0Index = std::make_unique<SG::AuxElement::Decorator<int>>("D0Index");
  m_V0Index = std::make_unique<SG::AuxElement::Decorator<int>>("V0Index");
  m_DeltaMass = std::make_unique<SG::AuxElement::Decorator<float>>("DeltaMass");
  m_SlowPionD0 =
      std::make_unique<SG::AuxElement::Decorator<float>>("SlowPionD0");
  m_SlowPionZ0SinTheta =
      std::make_unique<SG::AuxElement::Decorator<float>>("SlowPionZ0SinTheta");

  m_ptcone20 = std::make_unique<SG::AuxElement::Decorator<float>>("ptcone20");
  m_ptcone30 = std::make_unique<SG::AuxElement::Decorator<float>>("ptcone30");
  m_ptcone40 = std::make_unique<SG::AuxElement::Decorator<float>>("ptcone40");

  m_truthBarcode =
      std::make_unique<SG::AuxElement::Decorator<int>>("truthBarcode");
  m_decayType = std::make_unique<SG::AuxElement::Decorator<int>>("decayType");
  m_fitOutput__ImpactError = std::make_unique<SG::AuxElement::Decorator<float>>(
      "fitOutput__ImpactError");
  m_fitOutput__Impact =
      std::make_unique<SG::AuxElement::Decorator<float>>("fitOutput__Impact");
  m_fitOutput__ImpactZ0 =
      std::make_unique<SG::AuxElement::Decorator<float>>("fitOutput__ImpactZ0");
  m_fitOutput__ImpactZ0Error =
      std::make_unique<SG::AuxElement::Decorator<float>>(
          "fitOutput__ImpactZ0Error");
  m_fitOutput__ImpactTheta = std::make_unique<SG::AuxElement::Decorator<float>>(
      "fitOutput__ImpactTheta");
  m_fitOutput__ImpactZ0SinTheta =
      std::make_unique<SG::AuxElement::Decorator<float>>(
          "fitOutput__ImpactZ0SinTheta");
  m_fitOutput__ImpactSignificance =
      std::make_unique<SG::AuxElement::Decorator<float>>(
          "fitOutput__ImpactSignificance");
  m_fitOutput__Charge =
      std::make_unique<SG::AuxElement::Decorator<int>>("fitOutput__Charge");
  m_fitOutput__Lxy =
      std::make_unique<SG::AuxElement::Decorator<float>>("fitOutput__Lxy");
  m_fitOutput__LxyErr =
      std::make_unique<SG::AuxElement::Decorator<float>>("fitOutput__LxyErr");
  m_fitOutput__Chi2 =
      std::make_unique<SG::AuxElement::Decorator<float>>("fitOutput__Chi2");
  m_fitOutput =
      std::make_unique<SG::AuxElement::Decorator<FitOutput>>("fitOutput");
  m_fitOutput__VertexPosition =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "fitOutput__VertexPosition");
  m_fitOutput__PerigeeCovariance =
      std::make_unique<SG::AuxElement::Decorator<std::vector<double>>>(
          "fitOutput__PerigeeCovariance");
  m_fitOutput__Perigee =
      std::make_unique<SG::AuxElement::Decorator<std::vector<double>>>(
          "fitOutput__Perigee");
  m_fitOutput__TrkAtVrt = std::make_unique<
      SG::AuxElement::Decorator<std::vector<std::vector<double>>>>(
      "fitOutput__TrkAtVrt");
  m_fitOutput__ErrorMatrix =
      std::make_unique<SG::AuxElement::Decorator<std::vector<double>>>(
          "fitOutput__ErrorMatrix");
  m_fitOutput__Chi2PerTrack =
      std::make_unique<SG::AuxElement::Decorator<std::vector<double>>>(
          "fitOutput__Chi2PerTrack");

  m_costhetastar =
      std::make_unique<SG::AuxElement::Decorator<float>>("costhetastar");
  m_mKpi1 = std::make_unique<SG::AuxElement::Decorator<float>>("mKpi1");
  m_mKpi2 = std::make_unique<SG::AuxElement::Decorator<float>>("mKpi2");
  m_mPhi1 = std::make_unique<SG::AuxElement::Decorator<float>>("mPhi1");
  m_mPhi2 = std::make_unique<SG::AuxElement::Decorator<float>>("mPhi2");
  m_mKpipi = std::make_unique<SG::AuxElement::Decorator<float>>("mKpipi");

  m_daughter_info =
      std::make_unique<SG::AuxElement::Decorator<DaughterInfo>>("daughterInfo");
  m_daughter_pt =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfo__pt");
  m_daughter_eta =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfo__eta");
  m_daughter_phi =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfo__phi");
  m_daughter_chi2 =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfo__chi2");
  m_daughter_z0SinTheta =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfo__z0SinTheta");
  m_daughter_z0SinThetaPV =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfo__z0SinThetaPV");
  m_daughter_d0 =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfo__d0");
  m_daughter_d0PV =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfo__d0PV");
  m_daughter_truthBarcode =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "daughterInfo__truthBarcode");
  m_daughter_truthDBarcode =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "daughterInfo__truthDBarcode");
  m_daughter_truthPdgId =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "daughterInfo__truthPdgId");
  m_daughter_truthParentPdgId =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "daughterInfo__truthParentPdgId");
  m_daughter_pdgId =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "daughterInfo__pdgId");
  m_daughter_trackId =
      std::make_unique<SG::AuxElement::Decorator<std::vector<int>>>(
          "daughterInfo__trackId");
  m_daughter_passLoose =
      std::make_unique<SG::AuxElement::Decorator<std::vector<bool>>>(
          "daughterInfo__passLoose");
  m_daughter_passTight =
      std::make_unique<SG::AuxElement::Decorator<std::vector<bool>>>(
          "daughterInfo__passTight");
  m_daughter_truthMatchProbability =
      std::make_unique<SG::AuxElement::Decorator<std::vector<float>>>(
          "daughterInfo__truthMatchProbability");
}
