/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

// local
#include <CharmAnalysis/TruthConverterAlg.h>

// ROOT
#include <TString.h>

// CLHEP
#include <CLHEP/Matrix/SymMatrix.h>
#include <CLHEP/Matrix/Vector.h>
#include <CLHEP/RandomObjects/RandMultiGauss.h>

// athena
#include <PathResolver/PathResolver.h>
#include <TrkParameters/TrackParameters.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticleAuxContainer.h>
#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthVertex.h>

//
// method implementations
//

TruthConverterAlg::TruthConverterAlg(const std::string &name,
                                     ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      m_extrapolator("Trk::Extrapolator/AtlasExtrapolator"),
      m_rand("AtRndmGenSvc", name) {
  declareProperty("ParameterizationFile", m_parameterizationFile);
  declareProperty("TruthParticles", m_truthParticles);
  declareProperty("TrackParticles", m_trackParticles);
}

StatusCode TruthConverterAlg::initialize() {
  // load parameterization file
  std::string resolvedFileName = PathResolver::find_file(
      m_parameterizationFile, "DATAPATH", PathResolver::RecursiveSearch);
  if (resolvedFileName != "") {
    ATH_MSG_INFO("Parameterisation file found: " << resolvedFileName);
  } else {
    ATH_MSG_ERROR(
        "Parameterisation file not found: " << m_parameterizationFile);
    return StatusCode::FAILURE;
  }

  // open ROOT file
  m_file_para = TFile::Open((resolvedFileName).c_str(), "READ");
  if (m_file_para) {
    ATH_MSG_INFO("Parameterisation file opened succsefully");
  } else {
    ATH_MSG_ERROR("Parameterisation file could not be opened");
    return StatusCode::FAILURE;
  }

  // retreive extrapolator
  ATH_CHECK(m_extrapolator.retrieve());

  return StatusCode::SUCCESS;
}

StatusCode TruthConverterAlg::execute() {
  // random engine
  CLHEP::HepRandomEngine *rndm = m_rand->GetEngine("CharmAnalysisRng");

  // common eventInfo object
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

  // truth particle container
  const xAOD::TruthParticleContainer *truthParticles = nullptr;
  ANA_CHECK(evtStore()->retrieve(truthParticles, m_truthParticles));

  // track particle container
  auto trackParticles = std::make_unique<xAOD::TrackParticleContainer>();
  auto trackParticles_aux = std::make_unique<xAOD::TrackParticleAuxContainer>();
  trackParticles->setStore(trackParticles_aux.get());

  // beamspot
  Amg::Vector3D beamspot(eventInfo->beamPosX(), eventInfo->beamPosY(),
                         eventInfo->beamPosZ());

  // perigee surface
  Trk::PerigeeSurface persf(beamspot);

  for (const xAOD::TruthParticle *p : *truthParticles) {
    // only 'generator stable' particles
    // potentially neglects Lambda -> p pi decays
    if (!(p->status() == 1 && p->barcode() < 200000.)) {
      continue;
    }

    // only for charged particles
    if (p->charge() == 0) {
      continue;
    }

    // start from the production vertex
    if (p->hasProdVtx() && p->prodVtx()) {
      // truth vertex and momentum
      const xAOD::TruthVertex *prodVtx = p->prodVtx();
      const Amg::Vector3D position(prodVtx->x(), prodVtx->y(), prodVtx->z());
      const Amg::Vector3D momentum(p->px(), p->py(), p->pz());
      double charge = p->charge();
      const Trk::CurvilinearParameters cParameters(position, momentum, charge);
      std::unique_ptr<const Trk::TrackParameters> tP(
          m_extrapolator->extrapolate(cParameters, persf, Trk::anyDirection,
                                      false));

      // rarely fails to extrapolate
      if (!tP) {
        ATH_MSG_WARNING("Could not extrapolate truth particle to beamspot");
        continue;
      }

      // create a new xAOD::TrackParticle and push it into the container
      xAOD::TrackParticle *trackParticle = new xAOD::TrackParticle;
      trackParticles->push_back(trackParticle);

      // set origin
      trackParticle->setParametersOrigin(beamspot[0], beamspot[1], beamspot[2]);

      // covariance matrix
      TMatrixDSym *cov = get_cov(p->pt(), p->eta());
      double covMatrix[15] = {
          (*cov)(0, 0), (*cov)(1, 0), (*cov)(1, 1), (*cov)(2, 0), (*cov)(2, 1),
          (*cov)(2, 2), (*cov)(3, 0), (*cov)(3, 1), (*cov)(3, 2), (*cov)(3, 3),
          (*cov)(4, 0), (*cov)(4, 1), (*cov)(4, 2), (*cov)(4, 3), (*cov)(4, 4)};
      const std::vector<float> covMatrixVec(covMatrix, covMatrix + 15);
      trackParticle->setDefiningParametersCovMatrixVec(covMatrixVec);

      // mu vector
      TVectorD *mu = get_mu(p->pt(), p->eta());

      // flip sign of mean qoverp, because mean was computed using absolute
      // values of |qoverp|: |qoverp_reco| - |qoverp_truth| this is not needed
      // for qoverp elements of covariance matrix, because the matrix was
      // computed using no absolute values
      (*mu)[4] *= charge;

      // Hep Vector of means
      CLHEP::HepVector muHepVector(5);
      // Hep Covariance matrix
      CLHEP::HepSymMatrix covHepMatrix(5);
      // Fill Hep Vector and Covariance matrix
      for (unsigned int i = 0; i < 5; i++) {
        muHepVector[i] = (*mu)[i];
        for (unsigned int j = 0; j <= i; j++) {
          covHepMatrix[i][j] = (*cov)(i, j);
        }
      }

      // Make a new random gauss generator with engine, muVec, and covMatrix
      auto randMultiGauss = std::unique_ptr<CLHEP::RandMultiGauss>(
          // pass randomEngine as a reference so CLHEP doesn't delete it
          new CLHEP::RandMultiGauss(*rndm, muHepVector, covHepMatrix));

      // Generate correlated random gaussian numbers
      CLHEP::HepVector muHepVectorSmeared = randMultiGauss->fire();
      const double d0corr = muHepVectorSmeared[0];
      const double z0corr = muHepVectorSmeared[1];
      const double phicorr = muHepVectorSmeared[2];
      const double thetacorr = muHepVectorSmeared[3];
      const double qoverpcorr = muHepVectorSmeared[4];

      // set parameters
      trackParticle->setDefiningParameters(
          tP->parameters()[Trk::d0] + d0corr,
          tP->parameters()[Trk::z0] + z0corr,
          tP->parameters()[Trk::phi] + phicorr,
          tP->parameters()[Trk::theta] + thetacorr,
          tP->parameters()[Trk::qOverP] + qoverpcorr);

      // cleanup
      delete cov;
      delete mu;
    }
  }

  // record track particles
  ANA_CHECK(evtStore()->record(std::move(trackParticles_aux),
                               m_trackParticles + "Aux."));
  ANA_CHECK(evtStore()->record(std::move(trackParticles), m_trackParticles));

  return StatusCode::SUCCESS;
}

TMatrixDSym *TruthConverterAlg::get_cov(double pt, double eta) {
  unsigned int pt_bin = 0;
  for (unsigned int i = 0; i < m_pt_bins.size() - 1; i++) {
    if (pt > m_pt_bins[i] && pt <= m_pt_bins[i + 1]) {
      pt_bin = i;
      break;
    }
  }
  if (pt > m_pt_bins.back()) {
    pt_bin = m_pt_bins.size() - 2;
  }

  unsigned int eta_bin = 0;
  for (unsigned int i = 0; i < m_eta_bins.size() - 1; i++) {
    if (eta > m_eta_bins[i] && eta <= m_eta_bins[i + 1]) {
      eta_bin = i;
      break;
    }
  }
  if (eta > m_eta_bins.back()) {
    eta_bin = m_eta_bins.size() - 2;
  }

  TString name;
  name.Form("covmat_ptbin%.2i_etabin%.2i", pt_bin, eta_bin);
  TMatrixDSym *cov = dynamic_cast<TMatrixDSym *>(m_file_para->Get(name));
  return cov;
}

TVectorD *TruthConverterAlg::get_mu(double pt, double eta) {
  unsigned int pt_bin = 0;
  for (unsigned int i = 0; i < m_pt_bins.size() - 1; i++) {
    if (pt > m_pt_bins[i] && pt <= m_pt_bins[i + 1]) {
      pt_bin = i;
      break;
    }
  }
  if (pt > m_pt_bins.back()) {
    pt_bin = m_pt_bins.size() - 2;
  }

  unsigned int eta_bin = 0;
  for (unsigned int i = 0; i < m_eta_bins.size() - 1; i++) {
    if (eta > m_eta_bins[i] && eta <= m_eta_bins[i + 1]) {
      eta_bin = i;
      break;
    }
  }
  if (eta > m_eta_bins.back()) {
    eta_bin = m_eta_bins.size() - 2;
  }

  TString name;
  name.Form("meanvec_ptbin%.2i_etabin%.2i", pt_bin, eta_bin);
  TVectorD *mu = dynamic_cast<TVectorD *>(m_file_para->Get(name));
  return mu;
}
