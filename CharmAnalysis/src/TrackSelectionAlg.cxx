/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <PATCore/TAccept.h>

#include <SelectionHelpers/SelectionHelpers.h>

#include <xAODEgamma/Electron.h>
#include <xAODEgamma/ElectronxAODHelpers.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODMuon/Muon.h>
#include <xAODTracking/Vertex.h>
#include <xAODTracking/VertexContainer.h>

#include <CharmAnalysis/TrackSelectionAlg.h>

//
// method implementations
//

// Apply the d0 shift in 2015-2016 data
// the numbers are from https://cds.cern.ch/record/2667199
double rescaled_d0(double d0, UInt_t run) {
  double shift = 0.;
  if (run < 296939) {
    shift = 0.00422 * CLHEP::mm;
  } else if (run <= 300908) {
    shift = 0.00353 * CLHEP::mm;
  } else if (run <= 311481) {
    shift = -0.00335 * CLHEP::mm;
  }
  return d0 + shift;
}

TrackSelectionAlg ::TrackSelectionAlg(const std::string &name,
                                      ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      m_InDetTrackSelectionTool(
          "InDet::InDetTrackSelectionTool/TrackSelectionTool", this),
      m_InDetTrackSmearingTool(
          "InDet::InDetTrackSmearingTool/TrackSmearingTool", this),
      m_InDetTrackBiasingTool("InDet::InDetTrackBiasingTool/TrackBiasingTool",
                              this),
      m_InDetTrackTruthFilterTool(
          "InDet::InDetTrackTruthFilterTool/TrackTruthFilterTool", this)

{
  // various keys
  declareProperty("selectionDecoration", m_selectionDecoration,
                  "the decoration for the track selection");

  // remove tracks from bosons
  declareProperty("removeTracksFromBosons", m_removeTracksFromBosons = true);

  // user primary vertex
  // (If one wishes to check the track's z0 relative
  // to a vertex rather than to the beamspot, one can provide a vertex)
  declareProperty("usePrimaryVertex", m_usePrimaryVertex = false);

  // fix d0 mis-alignment in 2015 and 2016 data
  declareProperty("fixd0", m_fixd0 = true);

  // primary vertex key
  declareProperty("primaryVertexKey", m_primaryVertexKey = "PrimaryVertices");

  // track selection tool
  declareProperty("TrackSelectionTool", m_InDetTrackSelectionTool);

  // apply d0 smearing
  declareProperty("ApplySysSmearing", m_applySysSmearing = false);

  // apply d0 biasing
  declareProperty("ApplySysBiasing", m_applySysBiasing = false);

  // apply efficiency sys
  declareProperty("ApplySysEfficiency", m_applySysEfficiency = false);

  // track smearing tool
  declareProperty("TrackSmearingTool", m_InDetTrackSmearingTool);

  // track smearing tool
  declareProperty("TrackBiasingTool", m_InDetTrackBiasingTool);

  // track truth filter tool
  declareProperty("TrackTruthFilterTool", m_InDetTrackTruthFilterTool);
}

StatusCode TrackSelectionAlg::initialize() {
  // Retrieve the track selection tool
  ANA_CHECK(m_InDetTrackSelectionTool.retrieve());

  // Retrieve the track smearing tool if systematics should be applied
  if (m_applySysSmearing) {
    // Retrieve the track smearing tool
    ANA_CHECK(m_InDetTrackSmearingTool.retrieve());
    ANA_CHECK(m_systematicsList.addSystematics(*m_InDetTrackSmearingTool));
  }

  // Retrieve the track biasing tool if systematics should be applied
  if (m_applySysBiasing) {
    // Retrieve the track biasing tool
    ANA_CHECK(m_InDetTrackBiasingTool.retrieve());
    ANA_CHECK(m_systematicsList.addSystematics(*m_InDetTrackBiasingTool));
  }

  // Retrieve the track truth filter tool if systematics should be applied
  if (m_applySysEfficiency) {
    // Retrieve the track truth filter tool
    ANA_CHECK(m_InDetTrackTruthFilterTool.retrieve());
    ANA_CHECK(m_systematicsList.addSystematics(*m_InDetTrackTruthFilterTool));
  }

  // Initialize the tracks handle with the systematics list
  ANA_CHECK(m_tracksHandle.initialize(m_systematicsList));

  // If tracks should be removed from bosons, initialize the boson handle with
  // the systematics list
  if (m_removeTracksFromBosons) {
    ANA_CHECK(m_bosonHandle.initialize(m_systematicsList));
  }

  // If no selection decoration name is set, return failure
  if (m_selectionDecoration.empty()) {
    ANA_MSG_ERROR("No selection decoration name set");
    return StatusCode::FAILURE;
  }

  // Create a write accessor for the selection decoration
  ANA_CHECK(
      makeSelectionWriteAccessor(m_selectionDecoration, m_selectionAccessor));

  // Initialize the systematics list
  ANA_CHECK(m_systematicsList.initialize());

  return StatusCode::SUCCESS;
}

StatusCode TrackSelectionAlg::execute() {
  for (const auto &sys : m_systematicsList.systematicsVector()) {
    // event info
    const xAOD::EventInfo *eventInfo;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    // setup Systematics
    if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
      if (m_applySysSmearing) {
        ANA_CHECK(m_InDetTrackSmearingTool->applySystematicVariation(sys));
      }
      if (m_applySysBiasing) {
        ANA_CHECK(m_InDetTrackBiasingTool->applySystematicVariation(sys));
      }
      if (m_applySysEfficiency) {
        ANA_CHECK(m_InDetTrackTruthFilterTool->applySystematicVariation(sys));
      }
    }

    // boson container
    const xAOD::CompositeParticleContainer *bosons = nullptr;
    std::vector<const xAOD::TrackParticle *> bosonTracks;

    // bosons handle
    if (m_removeTracksFromBosons) {
      ANA_CHECK(m_bosonHandle.retrieve(bosons, sys));

      // collection of track particles associated with bosons
      for (const auto *boson : *bosons) {
        for (std::size_t i = 0; i < boson->nParts(); i++) {
          const xAOD::IParticle *part = boson->part(i);
          // electrons
          if (part->type() == xAOD::Type::Electron) {
            const xAOD::Electron *electron =
                static_cast<const xAOD::Electron *>(part);
            // get the pre-GSF track for electrons
            bosonTracks.push_back(
                xAOD::EgammaHelpers::getOriginalTrackParticle(electron));
          }
          // muons
          else if (part->type() == xAOD::Type::Muon) {
            const xAOD::Muon *muon = static_cast<const xAOD::Muon *>(part);
            // get the ID track
            bosonTracks.push_back(*muon->inDetTrackParticleLink());
          }
        }
      }
    }

    // tracks handle
    xAOD::TrackParticleContainer *tracks = nullptr;
    ANA_CHECK(m_tracksHandle.getCopy(tracks, sys));

    // Primary vertex
    const xAOD::Vertex *primaryVertex = nullptr;
    if (m_usePrimaryVertex) {
      const xAOD::VertexContainer *primaryVertices = nullptr;
      CHECK(evtStore()->retrieve(primaryVertices, m_primaryVertexKey));
      for (const xAOD::Vertex *vertex : *primaryVertices) {
        if (vertex->vertexType() == xAOD::VxType::PriVtx) {
          // The default "PrimaryVertex" container is ordered in
          // sum-pt, and the tracking group recommends to pick the one
          // with the maximum sum-pt, so this will do the right thing.
          // If the user needs a different primary vertex, he needs to
          // provide a reordered primary vertex container and point
          // this algorithm to it. Currently there is no central
          // algorithm to do that, so users will have to write their
          // own (15 Aug 18).
          if (primaryVertex == nullptr) {
            primaryVertex = vertex;
            break;
          }
        }
      }
      if (!primaryVertex) {
        ATH_MSG_WARNING("No primary vertex found");
        for (xAOD::TrackParticle *track : *tracks) {
          m_selectionAccessor->setBool(*track, false);
        }
        return StatusCode::SUCCESS;
      }
    }

    // loop over tracks
    for (xAOD::TrackParticle *track : *tracks) {

      // fix d0 for 2015/2016 data
      if (m_fixd0) {
        if (!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
          UInt_t runNumber = eventInfo->runNumber();
          const float d0 = track->d0();
          track->setDefiningParameters(rescaled_d0(d0, runNumber), track->z0(),
                                       track->phi0(), track->theta(),
                                       track->qOverP());
        }
      }

      // Apply d0/z0 smearing systematic if required
      if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
        if (m_applySysSmearing && m_InDetTrackSmearingTool->applyCorrection(
                                      *track) == CP::CorrectionCode::Error) {
          ATH_MSG_ERROR("Could not apply smearing corrections.");
        }

        // Apply d0/z0 biasing systematic if required
        if (m_applySysBiasing && m_InDetTrackBiasingTool->applyCorrection(
                                     *track) == CP::CorrectionCode::Error) {
          ATH_MSG_ERROR("Could not apply biasing corrections.");
        }
      }

      // The workhorse accept() function does not return a simple boolean,
      // though it can be treated as such. The tool stores its own TAccept
      // object which the user can check by reference (it is expensive to
      // copy).
      if (m_usePrimaryVertex) {
        // Don't ask me why the tool does not take a track
        // pointer together with the vertex...
        m_InDetTrackSelectionTool->accept(*track, primaryVertex);
      } else {
        m_InDetTrackSelectionTool->accept(track);
      }

      // retreive the TAccept object
      Root::TAccept accept = m_InDetTrackSelectionTool->getTAccept();

      if (m_removeTracksFromBosons) {
        // add new cut
        accept.addCut("bosonTrackVeto",
                      "track is not matched to any reconstructed boson");

        // boson track veto
        bool pass = true;
        TLorentzVector trackP4 = track->p4();
        for (const auto *bosonTrack : bosonTracks) {
          if (trackP4 == bosonTrack->p4()) {
            pass = false;
            break;
          }
        }
        // set bit
        accept.setCutResult("bosonTrackVeto", pass);
      }

      // Apply efficiency systematic if required
      if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) &&
          m_applySysEfficiency) {
        accept.addCut("efficiencySys", "track passes efficiency cut");
        accept.setCutResult("efficiencySys", true);
        if (!m_InDetTrackTruthFilterTool->accept(track)) {
          accept.setCutResult("efficiencySys", false);
        }
      }

      // Set bits for cutflow
      m_selectionAccessor->setBits(*track, CP::selectionFromAccept(accept));
    }
  }
  return StatusCode::SUCCESS;
}
