/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include <vector>

#include "../CharmAnalysis/DSelectionAlg.h"

#ifdef __CINT__

#pragma link C++ class std::vector < std::vector < double>> + ;

#endif
