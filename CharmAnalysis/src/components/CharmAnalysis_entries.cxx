#include <GaudiKernel/DeclareFactoryEntries.h>

#include <CharmAnalysis/AthEventCutflowAlg.h>
#include <CharmAnalysis/AthEventFilterAlg.h>
#include <CharmAnalysis/DSelectionAlg.h>
#include <CharmAnalysis/ElectronDecoratorAlg.h>
#include <CharmAnalysis/EventInfoCreatorAlg.h>
#include <CharmAnalysis/EventPreselectionAlg.h>
#include <CharmAnalysis/FakePriVtxAlg.h>
#include <CharmAnalysis/JetDecoratorAlg.h>
#include <CharmAnalysis/METDecoratorAlg.h>
#include <CharmAnalysis/MuonDecoratorAlg.h>
#include <CharmAnalysis/TrackDecoratorAlg.h>
#include <CharmAnalysis/TrackJetDecoratorAlg.h>
#include <CharmAnalysis/TrackSelectionAlg.h>
#include <CharmAnalysis/TrackSelectionAroundMesonAlg.h>
#include <CharmAnalysis/TriggerMatchingAlg.h>
#include <CharmAnalysis/TruthConverterAlg.h>
#include <CharmAnalysis/TruthJetDecoratorAlg.h>
#include <CharmAnalysis/TruthSelectionAlg.h>
#include <CharmAnalysis/WeWmSelectionAlg.h>
#include <CharmAnalysis/ZSelectionAlg.h>

DECLARE_ALGORITHM_FACTORY(CP::AthEventCutflowAlg)
DECLARE_ALGORITHM_FACTORY(CP::AthEventFilterAlg)
DECLARE_ALGORITHM_FACTORY(DSelectionAlg)
DECLARE_ALGORITHM_FACTORY(ElectronDecoratorAlg)
DECLARE_ALGORITHM_FACTORY(EventInfoCreatorAlg)
DECLARE_ALGORITHM_FACTORY(EventPreselectionAlg)
DECLARE_ALGORITHM_FACTORY(FakePriVtxAlg)
DECLARE_ALGORITHM_FACTORY(JetDecoratorAlg)
DECLARE_ALGORITHM_FACTORY(METDecoratorAlg)
DECLARE_ALGORITHM_FACTORY(MuonDecoratorAlg)
DECLARE_ALGORITHM_FACTORY(TrackDecoratorAlg)
DECLARE_ALGORITHM_FACTORY(TrackJetDecoratorAlg)
DECLARE_ALGORITHM_FACTORY(TrackSelectionAlg)
DECLARE_ALGORITHM_FACTORY(TrackSelectionAroundMesonAlg)
DECLARE_ALGORITHM_FACTORY(TriggerMatchingAlg)
DECLARE_ALGORITHM_FACTORY(TruthConverterAlg)
DECLARE_ALGORITHM_FACTORY(TruthJetDecoratorAlg)
DECLARE_ALGORITHM_FACTORY(TruthSelectionAlg)
DECLARE_ALGORITHM_FACTORY(WeWmSelectionAlg)
DECLARE_ALGORITHM_FACTORY(ZSelectionAlg)
