/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/***************************************************************************
                         FakePriVtxAlg.cxx  -  Description
                             -------------------
 ***************************************************************************/
#include <CLHEP/Random/Ranlux64Engine.h>
#include <CharmAnalysis/FakePriVtxAlg.h>

#include <vector>
#include <xAODTracking/Vertex.h>
#include <xAODTracking/VertexAuxContainer.h>
#include <xAODTracking/VertexContainer.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODTruth/TruthVertexContainer.h>

FakePriVtxAlg::FakePriVtxAlg(const std::string &name, ISvcLocator *pSvcLoc)
    : AnaAlgorithm(name, pSvcLoc), m_rndmGenSvc("AtRndmGenSvc", name),
      m_vxCandidatesOutputName("FakeVxPrimaryVertex"),
      m_truthVertexKey("TruthVertices"), m_pRandomEngine(0), m_GaussGenerator(0)

{
  declareProperty("VxCandidatesOutputName", m_vxCandidatesOutputName);
  declareProperty("TruthVertexKey", m_truthVertexKey);
  declareProperty("RandomSeed", m_randomSeed = 1234567);
  declareProperty("AtRndmGenSvc", m_rndmGenSvc);
  declareProperty("vtxSigmax", m_vtxSigx = 0.0076);
  declareProperty("vtxSigmay", m_vtxSigy = 0.0076);
  declareProperty("vtxSigmaz", m_vtxSigz = 0.059);
  declareProperty("doMinbias", m_do_minbias = false);
  declareProperty("minbiasMux", m_minbias_mux = -0.5);
  declareProperty("minbiasMuy", m_minbias_muy = -0.5);
  declareProperty("minbiasMuz", m_minbias_muz = -0.1);
  declareProperty("minbiasVtxSigx", m_minbias_vtxSigx = 0.066);
  declareProperty("minbiasVtxSigy", m_minbias_vtxSigy = 0.066);
  declareProperty("minbiasVtxSigz", m_minbias_vtxSigz = 42.2);
}

FakePriVtxAlg::~FakePriVtxAlg() {}

StatusCode FakePriVtxAlg::initialize() {
  // Get the random number service
  if (m_rndmGenSvc.retrieve().isFailure()) {
    ATH_MSG_ERROR("Could not initialize ATLAS Random Generator Service");
    return StatusCode::FAILURE;
  }

  if (m_pRandomEngine == 0) {
    m_pRandomEngine = new CLHEP::Ranlux64Engine(m_randomSeed);
  }
  m_GaussGenerator = new CLHEP::RandGauss(m_pRandomEngine);
  return StatusCode::SUCCESS;
}

StatusCode FakePriVtxAlg::execute() {
  auto theVxContainer = std::make_unique<xAOD::VertexContainer>();
  auto aux = std::make_unique<xAOD::VertexAuxContainer>();
  theVxContainer->setStore(aux.get());

  xAOD::Vertex *dummyVertex = new xAOD::Vertex();
  theVxContainer.get()->push_back(dummyVertex);

  const xAOD::TruthVertexContainer *inputTruthVertices;
  if (evtStore()->retrieve(inputTruthVertices, m_truthVertexKey).isFailure()) {
    ATH_MSG_ERROR("No TruthVertexContainer with name "
                  << m_truthVertexKey << " found in StoreGate!");
    return StatusCode::FAILURE;
  }

  double smr_x;
  double smr_y;
  double smr_z;

  if (!m_do_minbias) {
    const xAOD::TruthVertex *vtx = (*inputTruthVertices)[0];

    smr_x = m_GaussGenerator->shoot(vtx->x(), m_vtxSigx);
    smr_y = m_GaussGenerator->shoot(vtx->y(), m_vtxSigy);
    smr_z = m_GaussGenerator->shoot(vtx->z(), m_vtxSigz);
  } else {
    m_vtxSigx = m_minbias_vtxSigx;
    m_vtxSigy = m_minbias_vtxSigy;
    m_vtxSigz = m_minbias_vtxSigz;
    smr_x = m_GaussGenerator->shoot(m_minbias_mux, m_vtxSigx);
    smr_y = m_GaussGenerator->shoot(m_minbias_muy, m_vtxSigy);
    smr_z = m_GaussGenerator->shoot(m_minbias_muz, m_vtxSigz);
  }

  dummyVertex->setPosition(Amg::Vector3D(smr_x, smr_y, smr_z));
  AmgSymMatrix(3) covar;
  covar.setIdentity(); /// just to make sure all elemenst are set
  covar(0, 0) = m_vtxSigx * m_vtxSigx;
  covar(1, 1) = m_vtxSigy * m_vtxSigy;
  covar(2, 2) = m_vtxSigz * m_vtxSigz;
  dummyVertex->setCovariancePosition(covar);
  dummyVertex->setVertexType(xAOD::VxType::PriVtx);

  //---- Recording section: write the results to StoreGate ---//
  if (evtStore()
          ->record(std::move(theVxContainer), m_vxCandidatesOutputName, false)
          .isFailure()) {
    if (msgLvl(MSG::ERROR))
      msg() << "Unable to record Vertex Container in StoreGate" << endreq;
    return StatusCode::FAILURE;
  }
  if (evtStore()
          ->record(std::move(aux), m_vxCandidatesOutputName + "Aux", false)
          .isFailure()) {
    if (msgLvl(MSG::ERROR))
      msg() << "Unable to record VertexAux Container in StoreGate" << endreq;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

StatusCode FakePriVtxAlg::finalize() { return StatusCode::SUCCESS; }
