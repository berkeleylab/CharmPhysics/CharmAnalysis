# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration


from MCTruthClassifier import MCTruthClassifierConfig
from AthenaCommon.AlgSequence import AthSequencer
from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.Constants import VERBOSE, DEBUG, INFO
from CharmAnalysis.Config import createFakePrimaryVertex
from CharmAnalysis.Config import createTracksFromTruth
from CharmAnalysis.Config import setupConditions
from CharmAnalysis.Config import setupEventAlgs
from CharmAnalysis.Config import setupFilterAlg
from CharmAnalysis.Config import setupNTupleOutput
from CharmAnalysis.Config import setupPileupAlgs
from CharmAnalysis.Config import setupTrackJets, setupTrackJetAlgs
from CharmAnalysis.Config import setupTriggerAlgs
from CharmAnalysis.Config import WorkingPoints
from CharmAnalysis.Config.Systematics import setupSystematics
import importlib
import os
#
# config
#

if 'config' in locals():
    config = locals()['config']
else:
    config = 'Default'

# printout configuration
log.info("Using %s configuration..." % config)

Conf = importlib.import_module(
    'CharmAnalysis.Config.JobConfigs.%s' % config).Config
for attr, value in Conf.__dict__.iteritems():
    if not attr.startswith('__'):
        if attr in locals():
            setattr(Conf, attr, locals()[attr])
            log.info("  %s: %s [default: %s]" % (attr, locals()[attr], value))
        else:
            log.info("  %s: %s" % (attr, value))

#
# input file config
#

# remove the pesky PoolFileCatalog.xml file
if os.path.isfile('PoolFileCatalog.xml'):
    os.unlink('PoolFileCatalog.xml')

# requires 'kinit <user>@CERN.CH' if not running from CERN network

# testFile = 'root://eosuser.cern.ch//eos/user/m/mmuskinj/share/data/CharmAnalysis/FTAG4/mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.deriv.DAOD_FTAG4.e3601_s3126_r9364_p3970/DAOD_FTAG4.19348350._000074.pool.root.1'

# Override next line on command line with: --filesInput=XXX
# jps.AthenaCommonFlags.FilesInput = [testFile]

jps.AthenaCommonFlags.FilesInput = []

# Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "POOLAccess"

# Check the number of input events in MC in case of AthenaMP
if Conf.athenaMP and Conf.dataType != 'data':
    import PyUtils.AthFile as af
    af.server.flush_cache()
    for input_file_name in jps.AthenaCommonFlags.FilesInput():
        input_file = af.fopen(input_file_name).infos
        nentries = input_file['nentries']
        print("number of events: %s" % nentries)
        if nentries < 18:
            print("Not enough events for a MP job. Exiting...")
            sys.exit(1)

# Stop on filter failure Override flag
#   athMasterSeq
#      |
#      +-- athFilterSeq
#             |
#             +--- athAlgSeq == TopAlg
#             |
#             +--- athOutSeq
#             |
#             +--- athRegStreams
athAlgSeq.StopOverride = False

# Set the working point
WP = WorkingPoints(Conf)

# DEBUG for everything
if Conf.debugPrintout:
    ServiceMgr.MessageSvc.OutputLevel = DEBUG
    ServiceMgr.MessageSvc.defaultLimit = 9999999

#
# Load conditions, geometry, and magnetic field
#
setupConditions(svcMgr, athAlgSeq, include)

# Override firstSimCreatedBarcode function to avoid the use of inputFilePeeker
MCTruthClassifierConfig.firstSimCreatedBarcode = lambda: 200000 + 1

# Set up a histogram/tree output file for the job:
svcMgr += CfgMgr.THistSvc()
# make job run faster by disabling file size check
svcMgr.THistSvc.MaxFileSize = -1
svcMgr.THistSvc.Output += [
    Conf.RootStreamName + " DATAFILE='CharmAnalysis." +
    Conf.dataType + "." + Conf.sampleType + ".tree.root'" + " OPT='RECREATE'"
]

# Set up the systematics loader/handler algorithm:
setupSystematics(athAlgSeq, Conf)

# Fake primary vertex
if Conf.fakePrimaryVertex:
    createFakePrimaryVertex(athAlgSeq, Conf)

# Tracks from truth
if Conf.tracksFromTruth:
    createTracksFromTruth(athAlgSeq, Conf)

# trigger matching tool
ToolSvc += CfgMgr.Trig__MatchingTool('TrigMatchingTool')

# Store info about which algorithms are active
sequences = {
    'Pileup': None,
    'Trigger': None,
    'Event': None,
    'Metadata': None,
    'Jets': None,
    'Muons': None,
    'Electrons': None,
    'MET': None,
    'AntiTightMET': None,
    'OR': None,
    'PostProcessing': None,
    'CharmEventInfo': None,
    'METInfo': None,
    'AntiTightMETInfo': None,
    'WeWmSelection': None,
    'ZSelection': None,
    'Tracks': None,
    'Truth': None,
    'DSelection': None,
}

#
# Truth config
#
if Conf.doTruth and Conf.dataType != 'data':
    from CharmAnalysis.Config import setupTruthAlgs
    sequences['Truth'] = setupTruthAlgs(athAlgSeq, Conf)
    if Conf.doTruthJets:
        from CharmAnalysis.Config import setupTruthJetAlgs
        setupTruthJetAlgs(athAlgSeq, Conf)

#
# Pileup
#
if Conf.sampleType not in ['MinBias']:
    sequences['Pileup'] = setupPileupAlgs(athAlgSeq, Conf)

#
# Metadata algs
#
if Conf.doMetaData:
    from CharmAnalysis.Config import setupMetadataAlgs
    sequences['Metadata'] = setupMetadataAlgs(athAlgSeq, Conf)

#
# Trigger
#
if Conf.sampleType not in ['MinBias', 'Truth']:
    sequences['Trigger'] = setupTriggerAlgs(athAlgSeq, Conf)

#
# Event preselection
#
if Conf.sampleType not in ['MinBias', 'Truth']:
    sequences['Event'] = setupEventAlgs(athAlgSeq, Conf)

#
# Jet config
#
if Conf.doObjectCalibration:
    from CharmAnalysis.Config import setupJetAlgs
    sequences['Jets'] = setupJetAlgs(athAlgSeq, Conf)

#
# Muon config
#
if Conf.doObjectCalibration:
    from CharmAnalysis.Config import setupMuonAlgs
    sequences['Muons'] = setupMuonAlgs(athAlgSeq, Conf)

#
# Electron config
#
if Conf.doObjectCalibration:
    from CharmAnalysis.Config import setupElectronAlgs
    sequences['Electrons'] = setupElectronAlgs(athAlgSeq, Conf)

#
# MET config
#
if Conf.doObjectCalibration:
    from CharmAnalysis.Config import setupMETAlgs, setupAntiTightMETAlgs
    sequences['MET'] = setupMETAlgs(athAlgSeq, Conf,
                                    jetSequence=sequences['Jets'],
                                    muonSequence=sequences['Muons'],
                                    electronSequence=sequences['Electrons'])
    # sequences['AntiTightMET'] = setupAntiTightMETAlgs(athAlgSeq, Conf,
    #                                                   jetSequence=sequences['Jets'])


#
# Overlap Removal
#
if Conf.doObjectCalibration:
    from CharmAnalysis.Config import setupORAlgs
    sequences['OR'] = setupORAlgs(athAlgSeq, Conf,
                                  jetSequence=sequences['Jets'],
                                  muonSequence=sequences['Muons'],
                                  electronSequence=sequences['Electrons'])

#
# Jet postprocessing:
# - calculate global jet JVT SF
# - calculate global jet FTAG SF
#
if Conf.doObjectCalibration and Conf.doJVT:
    from CharmAnalysis.Config import makeJetPostprocessingAnalysisSequence
    sequences['JetPostProcessing'] = makeJetPostprocessingAnalysisSequence(athAlgSeq, Conf,
                                                                           jetSequence=sequences['Jets'],
                                                                           muonSequence=sequences['Muons'],
                                                                           electronSequence=sequences['Electrons'])

#
# Post Processing:
# - jet JVT selection
# - event preselection
# - trigger matching decoration
#
if Conf.doObjectCalibration:
    from CharmAnalysis.Config import setupPostProcessingAlgs
    sequences['PostProcessing'] = setupPostProcessingAlgs(athAlgSeq, ToolSvc, Conf,
                                                          jetSequence=sequences['Jets'],
                                                          muonSequence=sequences['Muons'],
                                                          electronSequence=sequences['Electrons'])

#
# CharmEventInfo config
#
if Conf.doCharmEventInfo:
    from CharmAnalysis.Config import setupCharmEventInfoSeq
    sequences['CharmEventInfo'] = setupCharmEventInfoSeq(athAlgSeq, Conf)

#
# MET Reader config
#
if Conf.doObjectCalibration:
    from CharmAnalysis.Config import setupMETReaderSeq
    sequences['METInfo'] = setupMETReaderSeq(athAlgSeq, Conf,
                                             jetSequence=sequences['Jets'],
                                             muonSequence=sequences['Muons'],
                                             electronSequence=sequences['Electrons'])

#
# WeWmSelection config
#
if Conf.doDMesonReco and Conf.doWBosonReco:
    from CharmAnalysis.Config import setupWeWmSelectionAlgs
    sequences['WeWmSelection'] = setupWeWmSelectionAlgs(athAlgSeq, Conf)

#
# ZSelection config
#
if Conf.doDMesonReco and Conf.doZBosonReco:
    from CharmAnalysis.Config import setupZSelectionAlgs
    sequences['ZSelection'] = setupZSelectionAlgs(athAlgSeq, Conf)

#
# Track config
#
if Conf.doDMesonReco and not Conf.tracksFromTruth:
    from CharmAnalysis.Config import setupTrackAlgs
    sequences['Tracks'] = setupTrackAlgs(athAlgSeq, Conf)
    if Conf.doV0Finding:
        from CharmAnalysis.Config import setupV0TrackAlgs
        setupV0TrackAlgs(athAlgSeq, ToolSvc, Conf)

#
# DSelection config
#
if Conf.doDMesonReco:
    from CharmAnalysis.Config import setupDSelectionAlgs
    sequences['DSelection'] = setupDSelectionAlgs(athAlgSeq, ToolSvc, Conf)

#
# Track-jets config
# Need 'AnalysisTracks' container before running this
#
if Conf.doTrackJets:
    
    setupTrackJets(athAlgSeq, Conf)
    
    for r in WP.track_jet_radius:
        jetType ='AntiKt{}PV0Track'.format(int(r * 10))
        setupTrackJetAlgs(athAlgSeq, Conf, jetType)
    if Conf.doVRTrackJets:
        jetType ='AntiKtVR30Rmax4Rmin02Track'
        setupTrackJetAlgs(athAlgSeq, Conf, jetType)

#
# Final track selection around D-mesons
#
if Conf.doDMesonReco and not Conf.tracksFromTruth:
    from CharmAnalysis.Config import setupTrackMesonSelectionAlgs
    setupTrackMesonSelectionAlgs(athAlgSeq, Conf)

#
# Print the entire sequence
#
print(athAlgSeq)

#
# NTuple output config
#
athOutSeq = AthSequencer('AthOutSeq')

# truth-only tree
if Conf.dataType != 'data' and Conf.truthOnlyTree:
    truth_only = {k: None for k in sequences}
    truth_only['Truth'] = True
    setupNTupleOutput(athOutSeq, truth_only, Conf, "TruthTree", True)

#
# Event Filter Alg:
#     Select events that pass reco cuts
#
if Conf.treeName:
    alg = setupFilterAlg(athAlgSeq)
    athOutSeq += [alg]

    # reco + truth tree
    setupNTupleOutput(athOutSeq, sequences, Conf,
                      Conf.treeName, Conf.sampleType == 'Truth')

# Optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")
