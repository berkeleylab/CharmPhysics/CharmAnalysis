/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef CHARM_ANALYSIS__TRUTH_TO_TRACK_CONVERTER_ALG_H
#define CHARM_ANALYSIS__TRUTH_TO_TRACK_CONVERTER_ALG_H

// ROOT
#include <TFile.h>
#include <TMatrixDSym.h>
#include <TVectorD.h>

// Athena
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AthenaKernel/IAtRndmGenSvc.h>
#include <TrkExInterfaces/IExtrapolator.h>

class TruthConverterAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  TruthConverterAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

private:
  TMatrixDSym *get_cov(double pt, double eta);

private:
  TVectorD *get_mu(double pt, double eta);

private:
  ToolHandle<Trk::IExtrapolator> m_extrapolator;

private:
  std::string m_parameterizationFile{"MC12MuonParametrisationCOMB.root"};

private:
  std::string m_truthParticles{"TruthParticles"};

private:
  std::string m_trackParticles{"TruthTrackParticles"};

private:
  TFile *m_file_para;

private:
  std::vector<double> m_pt_bins{10000,  20000,  50000,  100000, 200000,
                                250000, 500000, 750000, 1000000};

private:
  std::vector<double> m_eta_bins{-2.7, -2.25, -2, -1.7, -1.5, -1.05,
                                 -0.8, -0.4,  0,  0.4,  0.8,  1.05,
                                 1.5,  1.7,   2,  2.25, 2.7};

private:
  ServiceHandle<IAtRndmGenSvc> m_rand;
};

#endif
