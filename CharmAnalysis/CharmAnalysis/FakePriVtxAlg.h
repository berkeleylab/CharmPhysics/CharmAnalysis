#ifndef CHARMANALYSIS_FAKEPRIMARYVTX_H
#define CHARMANALYSIS_FAKEPRIMARYVTX_H
/**
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
  @author Marjorie Shapiro

  * FAKE Primary Vertex Finder
  * To be used with single particle gun.
  * Smears the truth primary vertex with resoluton specified as job properties

**/

/***************************************************************************
                          FakePrimaryVtxFinder.h  -  Description
                             -------------------
 ***************************************************************************/
#include "GaudiKernel/ServiceHandle.h"
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AthenaKernel/IAtRndmGenSvc.h>
#include <CLHEP/Random/RandGauss.h>
#include <CLHEP/Random/RandomEngine.h>
#include <string>

class FakePriVtxAlg : public EL::AnaAlgorithm {
public:
  FakePriVtxAlg(const std::string &name, ISvcLocator *pSvcLocator);
  virtual ~FakePriVtxAlg();
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();

private:
  ServiceHandle<IAtRndmGenSvc> m_rndmGenSvc;
  std::string m_vxCandidatesOutputName;
  std::string m_truthVertexKey;

  double m_vtxSigx;
  double m_vtxSigy;
  double m_vtxSigz;

  bool m_do_minbias;
  double m_minbias_mux;
  double m_minbias_muy;
  double m_minbias_muz;
  double m_minbias_vtxSigx;
  double m_minbias_vtxSigy;
  double m_minbias_vtxSigz;

  int m_randomSeed;
  mutable CLHEP::HepRandomEngine *m_pRandomEngine;
  CLHEP::RandGauss *m_GaussGenerator;

public:
  // Get the Atlas random number service
  const IAtRndmGenSvc &atRndmGenSvc() const { return *m_rndmGenSvc; }
};

#endif
