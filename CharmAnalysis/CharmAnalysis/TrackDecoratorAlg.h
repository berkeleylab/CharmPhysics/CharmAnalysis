/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef TRACK_DECORATOR_ALG_H
#define TRACK_DECORATOR_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/ISelectionReadAccessor.h>
#include <SelectionHelpers/ISelectionWriteAccessor.h>
#include <SelectionHelpers/SelectionReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <xAODTracking/TrackParticleContainer.h>

#include "InDetTrackSystematicsTools/IInDetTrackTruthOriginTool.h"
#include <TrkVertexFitterInterfaces/ITrackToVertexIPEstimator.h>

class TrackDecoratorAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  TrackDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief used to turn on extra variables
private:
  bool m_extra_vars;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief the ITrackToVertexIPEstimator tool
private:
  ToolHandle<Trk::ITrackToVertexIPEstimator> m_trackToVertexIPEstimator;

  /// \brief primary vertex key
private:
  std::string m_primaryVertexKey;

  /// \brief decorators
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_ptDecorator{};
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_etaDecorator{};
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_z0sinThetaDecorator{};
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0sigDecorator{};
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0Decorator{};
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0sigPVDecorator{};
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0PVDecorator{};
  std::unique_ptr<const SG::AuxElement::Decorator<bool>> m_passLooseDecorator{};
  std::unique_ptr<const SG::AuxElement::Decorator<bool>> m_passTightDecorator{};
  std::unique_ptr<const SG::AuxElement::Decorator<int>> m_trackOrigin{};

  /// \brief the decoration for the loose track selection
private:
  std::string m_selectionDecorationLoose{"selected_track"};

  /// \brief the decoration for the tight track selection
private:
  std::string m_selectionDecorationTight{"selected_track_tight"};

  /// \brief the decorator for \ref m_selectionDecorationLoose
private:
  std::unique_ptr<CP::ISelectionReadAccessor> m_selectionAccessorLoose;

  /// \brief the accessor for \ref m_selectionDecorationTight
private:
  std::unique_ptr<CP::ISelectionReadAccessor> m_selectionAccessorTight;

  /// \brief the track collection we run on
private:
  CP::SysReadHandle<xAOD::TrackParticleContainer> m_tracksHandle{
      this, "tracks", "", "the track collection to run on"};

  /// \brief the preselection we apply to our input
private:
  CP::SelectionReadHandle m_preselection{this, "preselection", "",
                                         "the preselection to apply"};

  /// \brief track origin tool
private:
  ToolHandle<InDet::IInDetTrackTruthOriginTool> m_trackOriginTool;
};

#endif