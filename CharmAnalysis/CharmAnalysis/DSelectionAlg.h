/*
   Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
 */

/// @author Miha Muskinja

#ifndef D_SELECTION_ALG_H
#define D_SELECTION_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <DerivationFrameworkBPhys/CascadeTools.h>
#include <SelectionHelpers/ISelectionReadAccessor.h>
#include <SelectionHelpers/ISelectionWriteAccessor.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysWriteHandle.h>
#include <TrkVKalVrtFitter/ITrkVKalVrtFitter.h>
#include <TrkVKalVrtFitter/TrkVKalVrtFitter.h>
#include <TrkVertexFitterInterfaces/ITrackToVertexIPEstimator.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODParticleEvent/CompositeParticleAuxContainer.h>
#include <xAODParticleEvent/CompositeParticleContainer.h>
#include <xAODTracking/TrackParticleContainer.h>

class DSelectionAlg : public EL::AnaAlgorithm {
  /// the standard constructor
public:
  DSelectionAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// structs to help with the VertexFit input / output
  /// (https://gitlab.cern.ch/atlas/athena/blob/21.2/Tracking/TrkVertexFitter/TrkVKalVrtFitter/src/VKalVrtFitSvc.cxx)
public:
  struct FitInput {
    bool PointedFit;
    const xAOD::Vertex PrimaryVertex;
    std::vector<const xAOD::TrackParticle *> Tracks;
    std::vector<double> Masses;
  };
  struct FitOutput {
    TLorentzVector Momentum;
    std::vector<std::vector<double>> TrkAtVrt;
    std::vector<double> PerigeeCovariance;
    std::vector<double> Perigee;
    std::vector<double> ImpactError;
    std::vector<double> Impact;
    std::vector<double> ErrorMatrix;
    std::vector<double> Chi2PerTrack;
    long int Charge;
    double Lxy;
    double LxyErr;
    double Chi2;
    Amg::Vector3D VertexPosition;
    double ImpactSignificance;
  };
  struct FitSelection {
    double d0cut;
    double LxyMin;
    double massMax;
    double massMin;
    double maxChi2;
    double pTMin;
  };

  /// struct to help with the decay chain info
public:
  struct DaughterInfo {
    std::vector<const xAOD::TrackParticle *> daughterTracks;
    std::vector<float> daughterPdgIds;
    std::vector<int> daughterTruthDBarcode;
    std::vector<int> daughterTruthBarcode;
    std::vector<int> daughterTruthPdgId;
    std::vector<int> daughterParentTruthPdgId;
  };

private:
  bool m_is_mc;

  /// the ITrackToVertexIPEstimator tool
private:
  ToolHandle<Trk::ITrackToVertexIPEstimator> m_trackToVertexIPEstimator;

  /// the ITrkVKalVrtFitter tool
private:
  //    ToolHandle<Trk::ITrkVKalVrtFitter> m_VKVrtFitter;
  ToolHandle<Trk::TrkVKalVrtFitter> m_VKVrtFitter;
  bool m_doApproximateFit;

  // From DerivationFrameworkBPhys
  ToolHandle<DerivationFramework::CascadeTools> m_CascadeTools;

  /// primary vertex key
private:
  std::string m_primaryVertexKey;

  /// track container and PV in the current event
private:
  const xAOD::Vertex *m_primaryVertex;

private:
  std::string m_LambdaVertexKey;

  /// track container and LambdaVertex in the current event
private:
  const xAOD::Vertex *m_LambdaVertex;

private:
  std::string m_LambdaBarVertexKey;

  /// track container and LambdaVertex in the current event
private:
  const xAOD::Vertex *m_LambdaBarVertex;

private:
  std::string m_KSVertexKey;

  /// track container and KSVertex in the current event
private:
  const xAOD::Vertex *m_KSVertex;

  /// constraint type for the VKalVrt tool
private:
  int m_constraintType;

  /// enable filtering of events without D Mesons
private:
  bool m_enableFilter;

private:
  /// Vertex fit helper functions
  bool FindDstarCandidate(bool vertexConstraint,
                          const xAOD::Vertex *primaryVertex,
                          const xAOD::TrackParticle *Ktrack,
                          const xAOD::TrackParticle *Pitrack,
                          xAOD::CompositeParticleContainer *composites,
                          const xAOD::TrackParticleContainer *tracks);

  bool FindDplusCandidate(const xAOD::Vertex *primaryVertex,
                          const xAOD::TrackParticle *Ktrack,
                          const xAOD::TrackParticle *Pitrack1,
                          const xAOD::TrackParticle *Pitrack2,
                          xAOD::CompositeParticleContainer *composites);

  bool FindDsCandidate(const xAOD::Vertex *primaryVertex,
                       const xAOD::TrackParticle *Ktrack1,
                       const xAOD::TrackParticle *Ktrack2,
                       const xAOD::TrackParticle *Pitrack,
                       xAOD::CompositeParticleContainer *composites);

  bool FindD0K3PiCandidate(const xAOD::Vertex *primaryVertex,
                           const xAOD::TrackParticle *Ktrack,
                           const xAOD::TrackParticle *Pitrack1,
                           const xAOD::TrackParticle *Pitrack2,
                           const xAOD::TrackParticle *Pitrack3,
                           xAOD::CompositeParticleContainer *composites);

  bool FindDK3PiCandidate(const xAOD::Vertex *primaryVertex,
                          const xAOD::TrackParticle *Ktrack,
                          const xAOD::TrackParticle *Pitrack1,
                          const xAOD::TrackParticle *Pitrack2,
                          const xAOD::TrackParticle *Pitrack3,
                          xAOD::CompositeParticleContainer *composites,
                          const xAOD::TrackParticleContainer *tracks);

  bool FindLambdacCandidate(const xAOD::Vertex *primaryVertex,
                            const xAOD::TrackParticle *ptrack,
                            const xAOD::TrackParticle *Ktrack,
                            const xAOD::TrackParticle *Pitrack,
                            xAOD::CompositeParticleContainer *composites);

  bool FindV0TrackCandidate(const xAOD::Vertex *primaryVertex,
                            const xAOD::Vertex *veeVertex,
                            const xAOD::TrackParticle *theTrack,
                            xAOD::CompositeParticleContainer *composites,
                            int mode);

  int selectV0TrackCandidates(const xAOD::Vertex *primaryVertex,
                              const xAOD::VertexContainer *vertices,
                              xAOD::CompositeParticleContainer *composites,
                              const xAOD::TrackParticleContainer *tracks,
                              int decayType, int reqTrackCarge);

  bool VertexFit(FitInput &fitInput, FitOutput &fitOutput,
                 FitSelection &fitSelection);

private:
  bool BasicKinematicCuts(FitOutput &fitOutput, FitSelection &fitSelection);

  void DecorateCompositeParticle(const xAOD::CompositeParticle *cp,
                                 const xAOD::TrackParticleContainer *tracks);

  void initializeDecorators();

private:
  bool m_doDplus;
  bool m_doDsubs;
  bool m_doD0Kpi;
  bool m_doD0K2pi;
  bool m_doD0K3pi;
  bool m_doD0K3pi_only;
  bool m_doLambdaCLambdaPi;
  bool m_doLambdaCKshortP;
  bool m_doKshortK;
  bool m_doKshortpi;
  bool m_doLambdaC;
  bool m_doTruthMathing;
  double m_maxd0D0_Kpi;
  double m_maxd0D0_Kpipi0;
  double m_maxd0PiSlow;
  double m_maxd0LambdaCToLambdaPi;
  double m_maxd0LambdaCToKsP;
  double m_maxd0DToKsK;
  double m_maxd0DtoKsPi;
  double m_maxdM;
  double m_maxDPdRKPi;
  double m_maxdRD0PiSlow;
  double m_maxdRKPi;
  double m_maxfitChi2D0;
  double m_maxfitChi2LambdaC;
  double m_maxMassD0_Kpi;
  double m_maxMassD0_Kpipi0;
  double m_maxMassD0in_Kpi;
  double m_maxMassD0in_Kpipi0;
  double m_maxMassDP;
  double m_maxMassDPin;
  double m_maxMassLambdaC;
  double m_maxMassLambdaCin;
  double m_maxd0LambdaC;
  double m_mincosthetastarDP;
  double m_mincosthetastarDs;
  double m_mincosthetastarLambdaC;
  double m_maxcosthetastarDP;
  double m_maxcosthetastarDs;
  double m_maxcosthetastarLambdaC;
  double m_mindM;
  double m_mindMDP;
  double m_mindMLC;
  double m_minLxyD0;
  double m_minMassD0_Kpi;
  double m_minMassD0_Kpipi0;
  double m_minMassD0in_Kpi;
  double m_minMassD0in_Kpipi0;
  double m_minMassDP;
  double m_minMassDPin;
  double m_minMassLambdaC;
  double m_minMassLambdaCin;
  double m_minPtD0;
  double m_minTrackPtD0;
  double m_minTrackPtDP;
  double m_minTrackPtLambdaC;
  double m_minLxyLambdaC;
  double m_phiwindowDP;
  double m_phiwindowLC;
  double m_phiwindowDs;
  double m_minPtDP;
  double m_minPtLambdaC;
  double m_minLxyDP;
  double m_minLxyDs;
  double m_minLxyLambdaCToLambdaPi;
  double m_minLxyLambdaCToKsP;
  double m_minLxyDToKsK;
  double m_minLxyDToKsPi;
  double m_minTrackPtD3;
  bool m_constrV0;
  int m_V0_Index;

  /// counters
private:
  unsigned int m_NDplus;
  unsigned int m_NDsubs;
  unsigned int m_NDstar;
  unsigned int m_NDstar3pi;
  unsigned int m_ND0K3pi;
  unsigned int m_NLambdaC;

  /// decorators for composite particles
private:
  std::unique_ptr<SG::AuxElement::Decorator<int>> m_D0Index;
  std::unique_ptr<SG::AuxElement::Decorator<int>> m_V0Index;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_DeltaMass;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_SlowPionD0;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_SlowPionZ0SinTheta;

  std::unique_ptr<SG::AuxElement::Decorator<float>> m_ptcone20;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_ptcone30;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_ptcone40;

  std::unique_ptr<SG::AuxElement::Decorator<int>> m_truthBarcode;
  std::unique_ptr<SG::AuxElement::Decorator<int>> m_decayType;
  std::unique_ptr<SG::AuxElement::Decorator<int>> m_fitOutput__Charge;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_fitOutput__ImpactError;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_fitOutput__Impact;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_fitOutput__ImpactZ0;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_fitOutput__ImpactZ0Error;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_fitOutput__ImpactTheta;
  std::unique_ptr<SG::AuxElement::Decorator<float>>
      m_fitOutput__ImpactZ0SinTheta;
  std::unique_ptr<SG::AuxElement::Decorator<float>>
      m_fitOutput__ImpactSignificance;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_fitOutput__Lxy;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_fitOutput__LxyErr;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_fitOutput__Chi2;
  std::unique_ptr<SG::AuxElement::Decorator<FitOutput>> m_fitOutput;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_fitOutput__VertexPosition;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<std::vector<double>>>>
      m_fitOutput__TrkAtVrt;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<double>>>
      m_fitOutput__PerigeeCovariance;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<double>>>
      m_fitOutput__Perigee;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<double>>>
      m_fitOutput__ErrorMatrix;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<double>>>
      m_fitOutput__Chi2PerTrack;

  std::unique_ptr<SG::AuxElement::Decorator<float>> m_costhetastar;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_mKpi1;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_mKpi2;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_mPhi1;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_mPhi2;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_mKpipi;

  std::unique_ptr<SG::AuxElement::Decorator<DaughterInfo>> m_daughter_info;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_daughter_pt;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_daughter_eta;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_daughter_phi;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_daughter_chi2;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_daughter_z0SinTheta;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_daughter_z0SinThetaPV;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_daughter_d0;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_daughter_d0PV;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>>
      m_daughter_trackId;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>> m_daughter_pdgId;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>>
      m_daughter_truthBarcode;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>>
      m_daughter_truthDBarcode;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>>
      m_daughter_truthPdgId;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>>
      m_daughter_truthParentPdgId;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<bool>>>
      m_daughter_passLoose;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<bool>>>
      m_daughter_passTight;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_daughter_truthMatchProbability;

  /// the systematics list we run
protected:
  CP::SysListHandle m_systematicsList{this};

  /// the track collection we run on
protected:
  CP::SysReadHandle<xAOD::TrackParticleContainer> m_tracksHandle{
      this, "tracks", "InDetTrackParticles", "the track collection to run on"};

  /// \brief the decoration for the loose track selection
private:
  std::string m_selectionDecorationLoose{"selected_track"};

  /// \brief the decoration for the tight track selection
private:
  std::string m_selectionDecorationTight{"selected_track_tight"};

  /// \brief the accessor for \ref m_selectionDecorationLoose
private:
  std::unique_ptr<CP::ISelectionReadAccessor> m_selectionAccessorLoose;

  /// \brief the accessor for \ref m_selectionDecorationTight
private:
  std::unique_ptr<CP::ISelectionReadAccessor> m_selectionAccessorTight;

  /// the composite particle container output
protected:
  CP::SysWriteHandle<xAOD::CompositeParticleContainer,
                     xAOD::CompositeParticleAuxContainer>
      m_compositesHandle{this, "composites", "CompositeParticles",
                         "the composite particle container output"};

  CP::SysWriteHandle<xAOD::CompositeParticleContainer,
                     xAOD::CompositeParticleAuxContainer>
      m_compositesHandle_k3pi{this, "composites_k3pi", "CompositeParticles",
                              "the composite particle container output"};

  CP::SysWriteHandle<xAOD::CompositeParticleContainer,
                     xAOD::CompositeParticleAuxContainer>
      m_compositesHandle_vee{this, "composites_vee", "CompositeParticles",
                             "the composite particle container output"};
};

#endif // JET_SMEARING_ALG_H
