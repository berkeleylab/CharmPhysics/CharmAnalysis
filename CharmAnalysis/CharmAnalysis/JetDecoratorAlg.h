/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef JET_DECORATOR_ALG_H
#define JET_DECORATOR_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/SelectionReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <xAODJet/Jet.h>
#include <xAODJet/JetContainer.h>

#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"

class JetDecoratorAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  JetDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief save extra jet variables
private:
  bool m_extraVars;

  /// \brief the ITrackToVertexIPEstimator tool
private:
  ToolHandle<IBTaggingSelectionTool> m_btagSelTool;

  /// \brief flavor tagger name
private:
  std::string m_taggerName;

  /// \brief decoration for the discriminant output
private:
  std::string m_quantileDecoration;

  /// \brief decoration for the pu output
private:
  std::string m_DL1puDecoration;

  /// \brief decoration for the pb output
private:
  std::string m_DL1pbDecoration;

  /// \brief decoration for the pc output
private:
  std::string m_DL1pcDecoration;

  /// \brief decoration for the jetfitter output
private:
  std::string m_JetFitterNVTXDecoration;

  /// \brief decoration for the jetfitter output
private:
  std::string m_JetFitterNTracksAtVtxDecoration;

  /// \brief decoration for the jetfitter output
private:
  std::string m_JetFitterMassDecoration;

  /// \brief decoration for the jetfitter output
private:
  std::string m_SV1NGTinSvxDecoration;

  /// \brief decoration for the jetfitter output
private:
  std::string m_SV1massvtxDecoration;

  /// \brief decoration for the jetfitter output
private:
  std::string m_secondaryVertexnTrksDecoration;

  /// \brief decoration for the jetfitter output
private:
  std::string m_secondaryVertxmDecoration;

  /// \brief decoration for MV2c100 discriminant
private:
  std::string m_MV2c100Decoration;

  /// \brief decoration for the MV2cl100 discriminant
private:
  std::string m_MV2cl100Decoration;

  /// \brief the decorator for \ref m_quantileDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<int>>
      m_quantileDecorationDecorator{};

  /// \brief the decorator for \ref m_DL1puDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_DL1puDecorationDecorator{};

  /// \brief the decorator for \ref m_DL1pbDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_DL1pbDecorationDecorator{};

  /// \brief the decorator for \ref m_DL1pcDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_DL1pcDecorationDecorator{};

  /// \brief the decorator for \ref m_JetFitter_nVTX
private:
  std::unique_ptr<const SG::AuxElement::Decorator<int>>
      m_JetFitterNVTXDecorationDecorator{};

  /// \brief decorator for the jetfitterNtracks output
private:
  std::unique_ptr<const SG::AuxElement::Decorator<int>>
      m_JetFitterNTracksAtVtxDecorationDecorator{};

  /// \brief decorator for the jetfitter mass output
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_JetFitterMassDecorationDecorator{};

  /// \brief decorator for the SV1 output
private:
  std::unique_ptr<const SG::AuxElement::Decorator<int>>
      m_SV1NGTinSvxDecorationDecorator{};

  /// \brief decorator for the SV1 mass output
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_SV1massvtxDecorationDecorator{};

  /* /// \brief decorator for the secondary vertex n tracks output */
  /* private: */
  /*   std::unique_ptr<const SG::AuxElement::Decorator<int>>
   * m_secondaryVertexnTrksDecorationDecorator{}; */

  /*   /// \brief decorator for the secondary vertex mass output */
  /* private: */
  /*   std::unique_ptr<const SG::AuxElement::Decorator<int>>
   * m_secondaryVertxmDecorationDecorator{}; */

  /// \brief the decorator for \ref m_MV2c100Decoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_MV2c100DecorationDecorator{};

  /// \brief the decorator for \ref m_MV2cl100Decoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_MV2cl100DecorationDecorator{};

  /// \brief the muon collection we run on
private:
  CP::SysReadHandle<xAOD::JetContainer> m_jetsHandle{
      this, "jets", "jets", "the jets collection to run on"};

  /// \brief the preselection we apply to our input
private:
  CP::SelectionReadHandle m_preselection{this, "preselection", "",
                                         "the preselection to apply"};
};

#endif
