/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef TRACK_JET_DECORATOR_ALG_H
#define TRACK_JET_DECORATOR_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgServices/ServiceHandle.h>
#include <InDetTrackSelectionTool/IInDetTrackSelectionTool.h>
#include <SelectionHelpers/SelectionReadHandle.h>
#include <SystematicsHandles/ISystematicsSvc.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <TrkVertexFitterInterfaces/ITrackToVertexIPEstimator.h>
#include <xAODJet/JetContainer.h>
#include <xAODTracking/TrackParticleContainer.h>

class TrackJetDecoratorAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  TrackJetDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief flag to apply systematics
private:
  bool m_doSyst;

  /// \brief the jet type
private:
  std::string m_jetType;

  /// \brief the track-jet collection we run on
private:
  CP::SysReadHandle<xAOD::JetContainer> m_jetsHandle{
      this, "jets", "jets", "the jets collection to run on"};

  /// \brief the track collection we run on
private:
  CP::SysReadHandle<xAOD::TrackParticleContainer> m_tracksHandle{
      this, "tracks", "tracks", "the track collection to run on"};

  /// \brief the preselection we apply to our input
private:
  CP::SelectionReadHandle m_preselection{this, "preselection", "",
                                         "the preselection to apply"};

  /// \brief the ITrackToVertexIPEstimator tool
private:
  ToolHandle<Trk::ITrackToVertexIPEstimator> m_trackToVertexIPEstimator;

  /// \brief the InDetTrackSelectionTool tool
private:
  ToolHandle<InDet::IInDetTrackSelectionTool> m_InDetTrackSelectionTool;

  /// \brief the systematics service
private:
  ServiceHandle<CP::ISystematicsSvc> m_systematics{"SystematicsSvc", ""};

  /// decorators
private:
  std::unique_ptr<SG::AuxElement::Decorator<long unsigned int>>
      m_numConstituents;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_daughter_pt;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_daughter_eta;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_daughter_phi;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_daughter_z0sinTheta;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_daughter_d0;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_daughter_d0PV;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<bool>>>
      m_daughter_passLoose;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>>
      m_daughter_trackId;
};

#endif
