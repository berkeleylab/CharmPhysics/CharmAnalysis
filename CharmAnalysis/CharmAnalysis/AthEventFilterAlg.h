/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef CHARM_ANALYSIS__ATH_EVENT_FILTER_ALG_H
#define CHARM_ANALYSIS__ATH_EVENT_FILTER_ALG_H

#include "GaudiKernel/IAlgManager.h"
#include "GaudiKernel/ServiceHandle.h"

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/ISelectionReadAccessor.h>
#include <SelectionHelpers/ISelectionWriteAccessor.h>
#include <SystematicsHandles/SysListHandle.h>

namespace CP {

/// \brief an algorithm for making an event cutflow from algorithm decisions
class AthEventFilterAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  AthEventFilterAlg(const std::string &name, ISvcLocator *pSvcLocator);

  /// \brief initialize
public:
  StatusCode initialize() override;

  /// \brief run once for each event
public:
  StatusCode execute() override;

  /// \brief the algorithm manager service
private:
  ServiceHandle<IAlgManager> m_algManager;

  /// \brief flag for MC
private:
  bool m_is_mc;

  /// \brief algorithms set
private:
  std::vector<std::string> m_algs;
};

} // namespace CP

#endif
