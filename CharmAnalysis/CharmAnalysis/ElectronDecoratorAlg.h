/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef ELECTRON_DECORATOR_ALG_H
#define ELECTRON_DECORATOR_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/SelectionReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <xAODEgamma/Electron.h>
#include <xAODEgamma/ElectronContainer.h>

#include <TrkVertexFitterInterfaces/ITrackToVertexIPEstimator.h>

class ElectronDecoratorAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  ElectronDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief used to turn on extra variables
private:
  bool m_extra_vars;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief the ITrackToVertexIPEstimator tool
private:
  ToolHandle<Trk::ITrackToVertexIPEstimator> m_trackToVertexIPEstimator;

  /// \brief primary vertex key
private:
  std::string m_primaryVertexKey;

  /// \brief the electron collection we run on
private:
  CP::SysReadHandle<xAOD::ElectronContainer> m_electronsHandle{
      this, "electrons", "Electrons", "the electron collection to run on"};

  /// \brief the preselection we apply to our input
private:
  CP::SelectionReadHandle m_preselection{this, "preselection", "",
                                         "the preselection to apply"};

  /// \brief the decoration for the calo cluster eta
private:
  std::string m_caloClusterEtaDecoration{"caloCluster_eta"};

  /// \brief the decoration for the EOverP variable
private:
  std::string m_EOverPDecoration{"EOverP"};

  /// \brief the decoration for the POverP variable
private:
  std::string m_POverPDecoration{"POverP"};

  /// \brief the decoration for the deltaPhi2
private:
  std::string m_deltaPhi2Decoration{"deltaPhi2"};

  /// \brief the decoration for the z0sinTheta
private:
  std::string m_z0sinThetaDecoration{"z0sinTheta"};

  /// \brief the decoration for the d0sig
private:
  std::string m_d0sigDecoration{"d0sig"};

  /// \brief the decoration for the d0
private:
  std::string m_d0Decoration{"d0"};

  /// \brief the decoration for the d0sigPV
private:
  std::string m_d0sigPVDecoration{"d0sigPV"};

  /// \brief the decoration for the d0PV
private:
  std::string m_d0PVDecoration{"d0PV"};

  /// \brief the decorator for \ref m_caloClusterEtaDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_caloClusterEtaDecorator{};

  /// \brief the decorator for \ref m_EOverPDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_EOverPDecorator{};

  /// \brief the decorator for \ref m_POverPDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_POverPDecorator{};

  /// \brief the decorator for \ref m_deltaPhi2Decoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_deltaPhi2Decorator{};

  /// \brief the decorator for \ref m_z0sinThetaDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_z0sinThetaDecorator{};

  /// \brief the decorator for \ref m_d0sigDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0sigDecorator{};

  /// \brief the decorator for \ref m_d0Decoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0Decorator{};

  /// \brief the decorator for \ref m_d0sigPVDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0sigPVDecorator{};

  /// \brief the decorator for \ref m_d0PVDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0PVDecorator{};
};

#endif