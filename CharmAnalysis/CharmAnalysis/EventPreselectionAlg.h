/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef EVENT_PRESELECTION_ALG_H
#define EVENT_PRESELECTION_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadDecorHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTau/TauJetContainer.h>

/// \brief a simple preselection alg
class EventPreselectionAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  EventPreselectionAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  virtual StatusCode initialize() final;
  virtual StatusCode execute() final;
  virtual StatusCode finalize() final;

private:
  bool executePreselection(const CP::SystematicSet &sys,
                           const xAOD::ElectronContainer *electrons,
                           const xAOD::MuonContainer *muons,
                           const xAOD::TauJetContainer *taus,
                           const xAOD::JetContainer *jets) const;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

private:
  CP::SysReadHandle<xAOD::ElectronContainer> m_electronsHandle{
      this, "electrons", "", "the electrons container to use"};
  CP::SysReadHandle<xAOD::MuonContainer> m_muonsHandle{
      this, "muons", "", "the muons container to use"};
  CP::SysReadHandle<xAOD::JetContainer> m_jetsHandle{
      this, "jets", "", "the jets container to use"};
  CP::SysReadHandle<xAOD::TauJetContainer> m_tausHandle{
      this, "taus", "", "the taus container to use"};

private:
  CP::SysReadDecorHandle<char> m_electronSelectionDecoration{
      this, "electronSelectionDecoration", "",
      "the decoration for the output electron selection flag"};
  CP::SysReadDecorHandle<char> m_muonSelectionDecoration{
      this, "muonSelectionDecoration", "",
      "the decoration for the output muon selection flag"};
  CP::SysReadDecorHandle<char> m_jetSelectionDecoration{
      this, "jetSelectionDecoration", "",
      "the decoration for the output jet selection flag"};
  CP::SysReadDecorHandle<char> m_bjetSelectionDecoration{
      this, "bjetSelectionDecoration", "",
      "the decoration for the output b-jet selection flag"};
  CP::SysReadDecorHandle<char> m_tauSelectionDecoration{
      this, "tauSelectionDecoration", "",
      "the decoration for the output tau selection flag"};

  /// \brief counter for passed events
private:
  long long m_passed{};

  /// \brief counter for total events
private:
  long long m_total{};

  // Properties
private:
  int m_min_leptons{-1};
  int m_max_leptons{-1};
  int m_min_leptons_light{-1};
  int m_max_leptons_light{-1};

  int m_min_electrons{-1};
  int m_max_electrons{-1};
  int m_min_muons{-1};
  int m_max_muons{-1};
  int m_min_taus{-1};
  int m_max_taus{-1};

  int m_min_jets{-1};
  int m_max_jets{-1};
  int m_min_bjets{-1};
  int m_max_bjets{-1};
};

#endif
