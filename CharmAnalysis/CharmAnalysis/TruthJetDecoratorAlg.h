/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef TRUTH_JET_DECORATOR_ALG_H
#define TRUTH_JET_DECORATOR_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/SelectionReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <xAODJet/Jet.h>
#include <xAODJet/JetContainer.h>

class TruthJetDecoratorAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  TruthJetDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief decorators
private:
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_ghostB_pt;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_ghostB_eta;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_ghostB_phi;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_ghostB_m;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>> m_ghostB_pdgId;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_ghostC_pt;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_ghostC_eta;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_ghostC_phi;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_ghostC_m;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>> m_ghostC_pdgId;

private:
  CP::SysReadHandle<xAOD::JetContainer> m_truthJetsHandle{
      this, "jets", "jets", "the truth jets collection to run on"};

  /// \brief the preselection we apply to our input
private:
  CP::SelectionReadHandle m_preselection{this, "preselection", "",
                                         "the preselection to apply"};
};

#endif
