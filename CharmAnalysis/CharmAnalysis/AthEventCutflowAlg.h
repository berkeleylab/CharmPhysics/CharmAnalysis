/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef CHARM_ANALYSIS__ATH_EVENT_CUTFLOW_ALG_H
#define CHARM_ANALYSIS__ATH_EVENT_CUTFLOW_ALG_H

#include "GaudiKernel/IAlgManager.h"
#include "GaudiKernel/ServiceHandle.h"
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>

namespace CP {

/// \brief an algorithm for making an event cutflow from algorithm decisions
class AthEventCutflowAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  AthEventCutflowAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  /// \brief initialize
  StatusCode initialize() override;

public:
  /// \brief run once for each event
  StatusCode execute() override;

public:
  /// \brief finalize
  StatusCode finalize() override;

  /// \brief the systematics list we run
private:
  SysListHandle m_systematicsList{this};

  /// \brief the algorithm manager service
private:
  ServiceHandle<IAlgManager> m_algManager;

  /// \brief algorithms set
private:
  std::vector<std::string> m_algs;

  /// \brief the pattern for cutflow histogram names
private:
  std::string m_histo_name{"cutflow"};
};

} // namespace CP

#endif
