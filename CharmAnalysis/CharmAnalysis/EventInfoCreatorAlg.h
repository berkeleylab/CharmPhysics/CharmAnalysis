/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef EVENT_INFO_CREATOR_ALG_H
#define EVENT_INFO_CREATOR_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/ISelectionReadAccessor.h>
#include <SelectionHelpers/ISelectionWriteAccessor.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysWriteHandle.h>

#include <TTbarNNLOReweighter/TTbarNNLORecursiveRew.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEventInfo/EventAuxInfo.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEventInfo/EventInfoContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTruth/TruthParticleContainer.h>

/// \brief an algorithm for decorating the EventInfo object

class EventInfoCreatorAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  EventInfoCreatorAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

private:
  void initializeAccessors();
  void fillAccessors(xAOD::EventInfo *eventInfo);

private:
  // trigger match
  bool el_is_trigger_matched(const xAOD::Electron *el,
                             const xAOD::EventInfo *eventInfo,
                             unsigned int runNumber);
  bool mu_is_trigger_matched(const xAOD::Muon *mu,
                             const xAOD::EventInfo *eventInfo,
                             unsigned int runNumber);

  // single electron weight
  float get_single_tight_el_weight(const xAOD::Electron *el);
  float get_single_tight_mu_weight(const xAOD::Muon *mu,
                                   unsigned int runNumber);
  float get_single_medium_mu_weight(const xAOD::Muon *mu,
                                    unsigned int runNumber);

  // double electron weight
  float get_double_tight_el_weight(const xAOD::Electron *el1,
                                   const xAOD::Electron *el2);
  float get_double_tight_mu_weight(const xAOD::Muon *mu1, const xAOD::Muon *mu2,
                                   unsigned int runNumber);
  float get_double_medium_mu_weight(const xAOD::Muon *mu1,
                                    const xAOD::Muon *mu2,
                                    unsigned int runNumber);

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief the eventInfo collection we run on
private:
  CP::SysWriteHandle<xAOD::EventInfo, xAOD::EventAuxInfo> m_eventInfoHandle{
      this, "eventInfo", "CharmEventInfo", "the event info object to run on"};

  /// \brief the Electron collection we run on
private:
  CP::SysReadHandle<xAOD::ElectronContainer> m_electronsHandle{
      this, "electrons", "electrons", "the electrons to run on"};

  /// \brief the Muon collection we run on
private:
  CP::SysReadHandle<xAOD::MuonContainer> m_muonsHandle{this, "muons", "muons",
                                                       "the muons to run on"};

  /// \brief the truth lepton collection we run on
private:
  CP::SysReadHandle<xAOD::TruthParticleContainer> m_truthLeptons{
      this, "truthLep", "truthLep", "the truth lepton container to run on"};

  /// \brief top reweighting tool
private:
  std::map<int, std::unique_ptr<TTbarNNLORecursiveRew>> m_top_reweighter_map;

  /// \brief top reweighting tool variations
private:
  std::vector<int> m_top_rw_variations{};

  /// \brief enable event filtering
private:
  bool m_enableFilter;

  /// \brief number of leptons to cut on
private:
  unsigned int m_Nlep;

  /// \brief do truth
private:
  bool m_doTruth;

  /// \brief do pileup RW
private:
  bool m_pileupRW;

  /// \brief print out lepton information
private:
  bool m_leptonInfo;

  /// \brief ID of the ttbar sample for reweigthing
private:
  int m_sampleId;

  /// \brief primary vertex key
private:
  std::string m_primaryVertexKey{"PrimaryVertices"};
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_primaryVertex_x_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_primaryVertex_y_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_primaryVertex_z_accessor;

  /// \brief beamspot
private:
  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_BS_posx_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_BS_posy_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_BS_posz_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_BS_possigmax_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_BS_possigmay_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_BS_possigmaz_accessor;

  /// \brief the decoration for the OR selection
private:
  std::string m_OR_selectionDecoration{"passesOR"};

  /// \brief the accessor for \ref m_OR_selectionDecoration
private:
  std::unique_ptr<CP::ISelectionReadAccessor> m_OR_selectionAccessor;

  /// \brief tight electron selection accessors
private:
  float m_tight_el_pt_cut;
  std::string m_tight_el_id_decoration;
  std::string m_tight_el_iso_decoration;
  std::unique_ptr<CP::ISelectionReadAccessor> m_tight_el_id_accessor;
  std::unique_ptr<CP::ISelectionReadAccessor> m_tight_el_iso_accessor;

  /// \brief tight muon selection accessors
private:
  float m_tight_mu_pt_cut;
  std::string m_tight_mu_quality_decoration;
  std::string m_tight_mu_iso_decoration;
  std::unique_ptr<CP::ISelectionReadAccessor> m_tight_mu_quality_accessor;
  std::unique_ptr<CP::ISelectionReadAccessor> m_tight_mu_iso_accessor;

  /// \brief the decoration for tight electrons
private:
  std::string m_tight_el1_pt = "TIGHT_EL1_PT";
  std::string m_tight_el1_phi = "TIGHT_EL1_PHI";
  std::string m_tight_el1_eta = "TIGHT_EL1_ETA";
  std::string m_tight_el1_charge = "TIGHT_EL1_CHARGE";
  std::string m_tight_el1_index = "TIGHT_EL1_INDEX";
  std::string m_tight_el1_matched = "TIGHT_EL1_MATCHED";
  std::string m_tight_el2_pt = "TIGHT_EL2_PT";
  std::string m_tight_el2_phi = "TIGHT_EL2_PHI";
  std::string m_tight_el2_eta = "TIGHT_EL2_ETA";
  std::string m_tight_el2_charge = "TIGHT_EL2_CHARGE";
  std::string m_tight_el2_index = "TIGHT_EL2_INDEX";
  std::string m_tight_el2_matched = "TIGHT_EL2_MATCHED";
  std::string m_tight_el_weight = "TIGHT_EL_WEIGHT";
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_el1_pt_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_el1_phi_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_el1_eta_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_el1_charge_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<int>>
      m_tight_el1_index_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<bool>>
      m_tight_el1_matched_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_el2_pt_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_el2_phi_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_el2_eta_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_el2_charge_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<int>>
      m_tight_el2_index_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<bool>>
      m_tight_el2_matched_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_el_weight_accessor;

  /// \brief the decoration for tight muons
private:
  std::string m_tight_mu1_pt = "TIGHT_MU1_PT";
  std::string m_tight_mu1_phi = "TIGHT_MU1_PHI";
  std::string m_tight_mu1_eta = "TIGHT_MU1_ETA";
  std::string m_tight_mu1_charge = "TIGHT_MU1_CHARGE";
  std::string m_tight_mu1_index = "TIGHT_MU1_INDEX";
  std::string m_tight_mu1_matched = "TIGHT_MU1_MATCHED";
  std::string m_tight_mu2_pt = "TIGHT_MU2_PT";
  std::string m_tight_mu2_phi = "TIGHT_MU2_PHI";
  std::string m_tight_mu2_eta = "TIGHT_MU2_ETA";
  std::string m_tight_mu2_charge = "TIGHT_MU2_CHARGE";
  std::string m_tight_mu2_index = "TIGHT_MU2_INDEX";
  std::string m_tight_mu2_matched = "TIGHT_MU2_MATCHED";
  std::string m_tight_mu_weight = "TIGHT_MU_WEIGHT";
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_mu1_pt_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_mu1_phi_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_mu1_eta_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_mu1_charge_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<int>>
      m_tight_mu1_index_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<bool>>
      m_tight_mu1_matched_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_mu2_pt_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_mu2_phi_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_mu2_eta_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_mu2_charge_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<int>>
      m_tight_mu2_index_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<bool>>
      m_tight_mu2_matched_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_tight_mu_weight_accessor;

  /// \brief scale factor accessors
private:
  std::string m_tight_el_id;
  std::string m_tight_el_iso;
  std::string m_el_eff_sf = "effSF_NOSYS";
  std::string m_el_id_tight_sf;
  std::string m_el_iso_tight_sf;
  std::string m_el_tight_trig_sf;
  std::string m_el_tight_trig_eff;

  std::string m_tight_mu_quality;
  std::string m_tight_mu_iso;
  std::string m_mu_ttva_sf = "muon_effSF_TTVA_NOSYS";
  std::string m_mu_quality_tight_sf;
  std::string m_mu_iso_tight_sf;
  std::string m_mu_tight_trig_mc_2015_eff;
  std::string m_mu_tight_trig_data_2015_eff;
  std::string m_mu_tight_trig_mc_2016_eff;
  std::string m_mu_tight_trig_data_2016_eff;

  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_el_eff_sf_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_el_id_tight_sf_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_el_iso_tight_sf_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_el_tight_trig_sf_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_el_tight_trig_eff_accessor;

  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_mu_ttva_sf_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_mu_quality_tight_sf_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_mu_iso_tight_sf_accessor;

  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_mu_tight_trig_mc_2015_eff_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_mu_tight_trig_data_2015_eff_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_mu_tight_trig_mc_2016_eff_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_mu_tight_trig_data_2016_eff_accessor;

  /// \brief top weight decoration
private:
  std::string m_top_weight_decoration = "TopWeight";
  std::map<int, std::unique_ptr<const SG::AuxElement::Accessor<float>>>
      m_top_weight_accessor_map;

  /// \brief truth boson
private:
  std::string m_truth_lepton_container;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_truth_boson_pt_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_truth_boson_eta_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_truth_boson_phi_accessor;
  std::unique_ptr<const SG::AuxElement::Accessor<float>>
      m_truth_boson_m_accessor;

  /// \brief the decoration for the single electron trigger chain
private:
  std::string m_passSingleElectronTriggerChain;

  /// \brief the decoration for the single muon trigger chain
private:
  std::string m_passSingleMuonTriggerChain;

  /// \brief the decoration for event weight
private:
  std::string m_EventWeight;

  /// \brief the accessor for \ref m_passSingleElectronTriggerChain
private:
  std::unique_ptr<const SG::AuxElement::Accessor<bool>>
      m_passSingleElectronTriggerChainAccessor;

  /// \brief the accessor for \ref m_passSingleMuonTriggerChain
private:
  std::unique_ptr<const SG::AuxElement::Accessor<bool>>
      m_passSingleMuonTriggerChainAccessor;

  /// \brief the accessor for \ref m_EventWeight
private:
  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_EventWeightAccessor;
};

#endif
