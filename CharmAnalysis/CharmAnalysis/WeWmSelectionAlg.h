/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

/// @author Marjorie Shapiro, based on Run 1 code from Lea Caminada

#ifndef WEWM_SELECTION_ALG_H
#define WEWM_SELECTION_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/ISelectionReadAccessor.h>
#include <SelectionHelpers/ISelectionWriteAccessor.h>
#include <SystematicsHandles/SysCopyHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysWriteHandle.h>

#include "xAODParticleEvent/CompositeParticleAuxContainer.h"
#include "xAODParticleEvent/CompositeParticleContainer.h"
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTracking/TrackParticleContainer.h>

class WeWmSelectionAlg : public EL::AnaAlgorithm {
  /// the standard constructor
public:
  WeWmSelectionAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

public:
  StatusCode finalize() override;

  //  input containers for the current event
private:
  //    const xAOD::TrackParticleContainer *m_tracks;
  const xAOD::ElectronContainer *m_electrons;
  const xAOD::MuonContainer *m_muons;
  const xAOD::MissingETContainer *m_MET;

  /// Functions that do the actual work
private:
  bool findWmunu(xAOD::CompositeParticleContainer *composites);
  bool findWenu(xAOD::CompositeParticleContainer *composites);
  StatusCode initCutflowHistograms();
  void decorateCompositeParticle(const xAOD::CompositeParticle *cp);

  /// WeWmSelection properties
private:
  // Selection parameters

  bool m_invertCuts;
  bool m_requireTightElectron;
  double m_minPt;
  double m_minEtMiss;
  double m_minMt;
  std::string m_MetComponent;

  // Control parameters
  bool m_doWm;
  bool m_doWe;
  bool m_doLeptonVeto;
  bool m_enableFilter;

  // counters to keep statistics
  int m_eventsSeen;
  int m_eventsPass;
  int m_wmuPass;
  int m_wmuCand;
  int m_wePass;
  int m_weCand;

  // histograms to keep cutflow statistics
  TH1I *m_hcutsWe;
  TH1I *m_hcutsWmu;

  /// decorators for composite particles
private:
  std::unique_ptr<SG::AuxElement::Decorator<int>> m_decayType;

  /// the systematics list we run
protected:
  CP::SysListHandle m_systematicsList{this};

  /// the collections we run on
protected:
  /*
  CP::SysReadHandle<xAOD::TrackParticleContainer> m_tracksHandle{
      this, "tracks", "InDetTrackParticles",
      "the track collection to run on"};
  */

  CP::SysReadHandle<xAOD::ElectronContainer> m_electronsHandle{
      this, "electrons", "electrons", "the electron collection to run on"};

  CP::SysReadHandle<xAOD::MuonContainer> m_muonsHandle{
      this, "muons", "muons", "the muon collection to run on"};

  CP::SysReadHandle<xAOD::MissingETContainer> m_metHandle{
      this, "met", "met", "the MissingETContainer to run on"};

  /// the composite particle container output
  CP::SysWriteHandle<xAOD::CompositeParticleContainer,
                     xAOD::CompositeParticleAuxContainer>
      m_compositesHandle{this, "composites", "CompositeParticles_WBosons",
                         "the composite particle container output"};
};

#endif // WEWM_SELECTION_ALG_H
