/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Greg Ottino
/// @author Miha Muskinja

#ifndef CHARM_ANALYSIS__TRUTH_UTILS_H
#define CHARM_ANALYSIS__TRUTH_UTILS_H

#include <xAODTracking/TrackParticle.h>
#include <xAODTruth/TruthParticle.h>

namespace TruthUtils {

// Finds numbers of k's and Pi's in the final state
// 0 = Kpi
// 1 = Kpipi
// 2 = KKpi
// 3 = Kpipipi
int chargeDecayMode(std::vector<const xAOD::TruthParticle *> children);

bool isCharmedHadron(const xAOD::TruthParticle *particle);

bool isBottomHadron(const xAOD::TruthParticle *particle);

bool fromDStar(const xAOD::TruthParticle *particle);

bool phiInDecayChain(const xAOD::TruthParticle *particle);

bool lambdaToPipInDecayChain(const xAOD::TruthParticle *particle);

bool lambdaInDecayChain(const xAOD::TruthParticle *particle);

bool kshortToPiPiInDecayChain(const xAOD::TruthParticle *particle);

bool kshortInDecayChain(const xAOD::TruthParticle *particle);

bool fromBdecay(const xAOD::TruthParticle *particle);

float cosThetaStar(const xAOD::TruthParticle *kPart,
                   const xAOD::TruthParticle *dPart);

    float Lxy(const xAOD::TruthParticle *particle, const xAOD::TruthVertex *prodVtx);

float ImpactParam(const xAOD::TruthParticle *particle);

int findDParentBarcode(const xAOD::TruthParticle *particle, int D0code);

int GetTruthBarcode(std::vector<const xAOD::TrackParticle *> &daughterTracks,
                    std::vector<int> &daughterDCodes,
                    std::vector<int> &daughterParentTruthPdgId,
                    std::vector<int> &daughterCodes,
                    std::vector<int> &daughterTruthPdgId);

const xAOD::TruthParticle *getSecondaryD0(const xAOD::TruthParticle *particle);

const xAOD::TruthParticle *
getSecondaryDplus(const xAOD::TruthParticle *particle);

const xAOD::TruthParticle *
findFinalParticle(const xAOD::TruthParticle *particle);

const xAOD::TruthParticle *
findInitialParticle(const xAOD::TruthParticle *particle);

std::vector<const xAOD::TruthParticle *>
findStatusOneChildren(const xAOD::TruthParticle *particle);

} // namespace TruthUtils

#endif
