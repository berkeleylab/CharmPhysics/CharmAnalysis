/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef MUON_DECORATOR_ALG_H
#define MUON_DECORATOR_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/SelectionReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <xAODMuon/Muon.h>
#include <xAODMuon/MuonContainer.h>

#include <TrkVertexFitterInterfaces/ITrackToVertexIPEstimator.h>

class MuonDecoratorAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  MuonDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief used to turn on extra variables
private:
  bool m_extra_vars;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief the ITrackToVertexIPEstimator tool
private:
  ToolHandle<Trk::ITrackToVertexIPEstimator> m_trackToVertexIPEstimator;

  /// \brief primary vertex key
private:
  std::string m_primaryVertexKey;

  /// \brief the muon collection we run on
private:
  CP::SysReadHandle<xAOD::MuonContainer> m_muonsHandle{
      this, "muons", "Muons", "the muon collection to run on"};

  /// \brief the preselection we apply to our input
private:
  CP::SelectionReadHandle m_preselection{this, "preselection", "",
                                         "the preselection to apply"};

  /// \brief the decoration for the d0sig
private:
  std::string m_d0sigDecoration{"d0sig"};

  /// \brief the decoration for the d0
private:
  std::string m_d0Decoration{"d0"};

  /// \brief the decoration for the z0sinTheta
private:
  std::string m_z0sinThetaDecoration{"z0sinTheta"};

  /// \brief the decoration for the d0sigPV
private:
  std::string m_d0sigPVDecoration{"d0sigPV"};

  /// \brief the decoration for the d0PV
private:
  std::string m_d0PVDecoration{"d0PV"};

  /// \brief the decorator for \ref m_z0sinThetaDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>>
      m_z0sinThetaDecorator{};

  /// \brief the decorator for \ref m_d0sigDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0sigDecorator{};

  /// \brief the decorator for \ref m_d0sigDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0Decorator{};

  /// \brief the decorator for \ref m_d0sigPVDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0sigPVDecorator{};

  /// \brief the decorator for \ref m_d0PVDecoration
private:
  std::unique_ptr<const SG::AuxElement::Decorator<float>> m_d0PVDecorator{};
};

#endif