/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

/// @author Miha Muskinja

// particle masses
#define MASS_KAON 493.677
#define MASS_PION 139.570
#define MASS_PROTON 938.272
#define MASS_D0 1864.83
#define MASS_DPLUS 1869.60
#define MASS_PHI 1019.455
#define MASS_MUON 105.658
#define MASS_ELECTRON 0.5109989
#define MASS_LAMBDA0 1115.683
#define MASS_KSHORT 497.614

#define PDGID_PION 211
#define PDGID_KAON 321
#define PDGID_PROTON 2212
#define PDGID_LAMBDA 3122
#define PDGID_KSHORT 310
