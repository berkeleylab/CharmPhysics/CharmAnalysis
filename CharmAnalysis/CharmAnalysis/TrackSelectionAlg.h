/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef TRACK_ANALYSIS_ALGORITHMS__TRACK_SELECTION_ALG_H
#define TRACK_ANALYSIS_ALGORITHMS__TRACK_SELECTION_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/ISelectionReadAccessor.h>
#include <SelectionHelpers/ISelectionWriteAccessor.h>
#include <SelectionHelpers/SelectionReadHandle.h>
#include <SystematicsHandles/SysCopyHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>

#include <xAODParticleEvent/CompositeParticleContainer.h>
#include <xAODTracking/TrackParticleContainer.h>

#include <InDetTrackSelectionTool/IInDetTrackSelectionTool.h>
#include <InDetTrackSystematicsTools/IInDetTrackBiasingTool.h>
#include <InDetTrackSystematicsTools/IInDetTrackSmearingTool.h>
#include <InDetTrackSystematicsTools/IInDetTrackTruthFilterTool.h>

class TrackSelectionAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  TrackSelectionAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief the InDetTrackSelectionTool tool
private:
  ToolHandle<InDet::IInDetTrackSelectionTool> m_InDetTrackSelectionTool;

  /// \brief the InDetTrackSelectionTool tool
private:
  ToolHandle<InDet::IInDetTrackSmearingTool> m_InDetTrackSmearingTool;

  /// \brief the InDetTrackBiasingTool tool
private:
  ToolHandle<InDet::IInDetTrackBiasingTool> m_InDetTrackBiasingTool;

  /// \brief the InDetTrackTruthFilterTool tool
private:
  ToolHandle<InDet::IInDetTrackTruthFilterTool> m_InDetTrackTruthFilterTool;

  /// remove tracks from bososn
private:
  bool m_removeTracksFromBosons;

  /// user primary vertex for z0 calculation
private:
  bool m_usePrimaryVertex;

  /// fix d0 mis-alignment in 2015 and 2016 data
private:
  bool m_fixd0;

  /// apply the sys smearing for d0/z0
private:
  bool m_applySysSmearing;

  /// apply the sys biasing for d0/z0
private:
  bool m_applySysBiasing;

  /// apply the efficiency sys
private:
  bool m_applySysEfficiency;

  /// primary vertex key
private:
  std::string m_primaryVertexKey;

  /// \brief the decoration for the track selection
private:
  std::string m_selectionDecoration{"selected_track"};

  /// \brief the accessor for \ref m_selectionDecoration
private:
  std::unique_ptr<CP::ISelectionWriteAccessor> m_selectionAccessor;

  /// \brief the track collection we run on
protected:
  CP::SysCopyHandle<xAOD::TrackParticleContainer> m_tracksHandle{
      this, "tracks", "InDetTrackParticles", "the track collection to run on"};

  /// composite particle (Z or W) input
protected:
  CP::SysReadHandle<xAOD::CompositeParticleContainer> m_bosonHandle{
      this, "bosons", "bosons", "input boson collection"};
};

#endif
