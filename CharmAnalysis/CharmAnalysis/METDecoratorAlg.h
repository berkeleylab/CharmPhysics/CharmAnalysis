/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef MET_DECORATOR_ALG_H
#define MET_DECORATOR_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysWriteHandle.h>

#include <xAODEventInfo/EventAuxInfo.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEventInfo/EventInfoContainer.h>
#include <xAODMissingET/MissingETContainer.h>

/// \brief an algorithm for decorating the EventInfo object

class METDecoratorAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  METDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief the eventInfo collection we run on
private:
  CP::SysWriteHandle<xAOD::EventInfo, xAOD::EventAuxInfo> m_eventInfoHandle{
      this, "eventInfo", "METInfo", "the event info object to run on"};

  /// \brief the MET collection we run on
private:
  CP::SysReadHandle<xAOD::MissingETContainer> m_metHandle{
      this, "met", "met", "the MissingETContainer to run on"};

  /// \brief the MET component
private:
  std::string m_MetComponent;

  /// \brief the decoration for MET
private:
  std::string m_MET;

  /// \brief the decoration for MET phi
private:
  std::string m_METPhi;

  /// \brief the accessor for \ref m_MET
private:
  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_METAccessor;

  /// \brief the accessor for \ref m_METPhi
private:
  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_METPhiAccessor;
};

#endif
