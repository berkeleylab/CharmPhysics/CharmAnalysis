/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

/// @author Cesar Gonzalez Renteria, based on Run 1 code from Lea Caminada
/// and ZSelectionAlg.h written by Marjorie Shapiro

#ifndef Z_SELECTION_ALG_H
#define Z_SELECTION_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/ISelectionReadAccessor.h>
#include <SelectionHelpers/ISelectionWriteAccessor.h>
#include <SystematicsHandles/SysCopyHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysWriteHandle.h>

#include "xAODParticleEvent/CompositeParticleAuxContainer.h"
#include "xAODParticleEvent/CompositeParticleContainer.h"
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTracking/TrackParticleContainer.h>

class ZSelectionAlg : public EL::AnaAlgorithm {
  /// the standard constructor
public:
  ZSelectionAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

public:
  StatusCode finalize() override;

  //  input containers for the current event
private:
  const xAOD::ElectronContainer *m_electrons;
  const xAOD::MuonContainer *m_muons;

  /// Functions that do the actual work
private:
  bool findZmumu(xAOD::CompositeParticleContainer *composites);
  bool findZee(xAOD::CompositeParticleContainer *composites);
  StatusCode initCutflowHistograms();
  void decorateCompositeParticle(const xAOD::CompositeParticle *cp);

  /// ZSelection properties
private:
  // Selection parameters
  bool m_requireTightElectron;
  double m_minPt;
  double m_minZMass;
  double m_maxZMass;

  // Control parameters
  bool m_doZmumu;
  bool m_doZee;
  bool m_doLeptonVeto;
  bool m_enableFilter;

  // counters to keep statistics
  int m_eventsSeen;
  int m_eventsPass;
  int m_ZmumuPass;
  int m_ZmumuCand;
  int m_ZeePass;
  int m_ZeeCand;

  // histograms to keep cutflow statistics
  TH1I *m_hcutsZee;
  TH1I *m_hcutsZmumu;

  /// decorators for composite particles
private:
  std::unique_ptr<SG::AuxElement::Decorator<int>> m_decayType;

  /// the systematics list we run
protected:
  CP::SysListHandle m_systematicsList{this};

  /// the collections we run on
protected:
  CP::SysReadHandle<xAOD::ElectronContainer> m_electronsHandle{
      this, "electrons", "electrons", "the electron collection to run on"};

  CP::SysReadHandle<xAOD::MuonContainer> m_muonsHandle{
      this, "muons", "muons", "the muon collection to run on"};

  /// the composite particle container output
  CP::SysWriteHandle<xAOD::CompositeParticleContainer,
                     xAOD::CompositeParticleAuxContainer>
      m_compositesHandle{this, "composites", "CompositeParticles_ZBosons",
                         "the composite particle container output"};
};

#endif // Z_SELECTION_ALG_H
