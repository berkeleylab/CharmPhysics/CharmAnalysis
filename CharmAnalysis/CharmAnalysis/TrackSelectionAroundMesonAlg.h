/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

#ifndef TRACK_ANALYSIS_ALGORITHMS__TRACK_SELECTION_AROUND_MESON_ALG_H
#define TRACK_ANALYSIS_ALGORITHMS__TRACK_SELECTION_AROUND_MESON_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/ISelectionWriteAccessor.h>
#include <SystematicsHandles/SysCopyHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>

#include <xAODParticleEvent/CompositeParticleContainer.h>
#include <xAODTracking/TrackParticleContainer.h>

class TrackSelectionAroundMesonAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  TrackSelectionAroundMesonAlg(const std::string &name,
                               ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief the decoration for the track selection
private:
  std::string m_selectionDecoration{"selected_around_meson_track"};

  /// \brief the dR cut value
private:
  double m_max_dR;

  /// \brief the accessor for \ref m_selectionDecoration
private:
  std::unique_ptr<CP::ISelectionWriteAccessor> m_selectionAccessor;

  /// \brief the track collection we run on
protected:
  CP::SysCopyHandle<xAOD::TrackParticleContainer> m_tracksHandle{
      this, "tracks", "tracks", "the track collection to run on"};

  /// composite particle (D-meson) input
protected:
  CP::SysReadHandle<xAOD::CompositeParticleContainer> m_mesonHandle{
      this, "mesons", "mesons", "input mesons collection"};

  /// decorators
private:
  std::unique_ptr<SG::AuxElement::Decorator<int>> m_trackId;
};

#endif
