/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Greg Ottino
/// @author Miha Muskinja

#ifndef CHARM_ANALYSIS__TRUTH_SELECTION_ALG_H
#define CHARM_ANALYSIS__TRUTH_SELECTION_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <MCTruthClassifier/IMCTruthClassifier.h>
#include <SelectionHelpers/ISelectionReadAccessor.h>
#include <SelectionHelpers/ISelectionWriteAccessor.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysWriteHandle.h>
#include <xAODEventInfo/EventInfo.h>

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

class TruthSelectionAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  TruthSelectionAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

private:
  void decorateTruthParticle(const xAOD::TruthParticle *p,
                             std::vector<const xAOD::TruthParticle *> truthVec);

private:
  void checkDecorations(const xAOD::TruthParticleContainer *truth);

private:
  void decorateDAux(const xAOD::TruthParticle *particle,
                    std::vector<const xAOD::TruthParticle *> children);

private:
  bool GetCharmDaughters(const xAOD::TruthParticle *particle);

private:
  void initializeAuxElements();

  /// \brief Fiducial cuts for D-mesons
private:
  double m_trueD_MaxEta;
  double m_trueD_MinPt;
  int m_N_trueD_fiducial;

  /// \brief Fiducial cuts for charged leptons
private:
  double m_trueLep_MaxEta;
  double m_trueLep_MinPt;
  int m_N_trueLep_fiducial;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief MCTC tool handle
private:
  ToolHandle<IMCTruthClassifier> m_classifier;

  /// \brief name of the truth electron container
private:
  std::string m_truthElectrons{"TruthElectrons"};

  /// \brief name of the truth muon container
private:
  std::string m_truthMuons{"TruthMuons"};

  /// \brief the decoration for the truth selection
private:
  std::string m_selectionDecoration{"selected_truth"};

  /// \brief the decorator for \ref m_selectionDecoration
private:
  std::unique_ptr<CP::ISelectionWriteAccessor> m_selectionAccessor;

  /// \brief dress leptons
private:
  bool m_dressLeptons;

  /// \brief truth selection based on fiducial lep and D cuts
private:
  bool m_do_truth_selection;

  /// \brief perform truth selection on charm hadrons
private:
  bool m_do_truth_charm;

  /// \brief perform truth selection on bottom hadrons
private:
  bool m_do_truth_bottom;

  /// \brief perform truth selection on leptons
private:
    bool m_do_truth_leptons;

    /// \brief stores the primary beam interaction point
private:
    const xAOD::TruthVertex *truthPV;

  /// \brief the decorator for truth selection
private:
  std::unique_ptr<SG::AuxElement::Decorator<bool>> m_truthSelectionDecorator;

  /// \brief check if the derivation alread has these decorations
private:
  bool m_has_pt_decoration{};
  bool m_has_eta_decoration{};
  bool m_has_phi_decoration{};

  /// \brief truth particle accessors
private:
  std::unique_ptr<const SG::AuxElement::Accessor<unsigned int>> m_outcomeAcc;
  std::unique_ptr<const SG::AuxElement::Accessor<unsigned int>> m_originAcc;
  std::unique_ptr<const SG::AuxElement::Accessor<unsigned int>> m_typeAcc;
  std::unique_ptr<const SG::AuxElement::Accessor<unsigned int>>
      m_classificationAcc;
  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_e_dressed;
  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_pt_dressed;
  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_eta_dressed;
  std::unique_ptr<const SG::AuxElement::Accessor<float>> m_phi_dressed;

  /// \brief truth particle decorations
private:
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_pt;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_eta;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_phi;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_costhetastarT;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_LxyT;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_ImpactT;
  std::unique_ptr<SG::AuxElement::Decorator<float>> m_dRDLepT;
  std::unique_ptr<SG::AuxElement::Decorator<bool>> m_fromBdecay;
  std::unique_ptr<SG::AuxElement::Decorator<int>> m_decayMode;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_VertexPositionT;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>>
      m_daughterT_barcode;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>> m_daughterT_pt;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_daughterT_eta;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<float>>>
      m_daughterT_phi;
  std::unique_ptr<SG::AuxElement::Decorator<std::vector<int>>>
      m_daughterT_pdgId;

  /// \brief the truth collection we run on
protected:
  CP::SysReadHandle<xAOD::TruthParticleContainer> m_truthHandle{
      this, "truth", "truth", "the truth prticle collection to run on"};

  /// \brief the truth lepton container output
protected:
  CP::SysWriteHandle<xAOD::TruthParticleContainer,
                     xAOD::TruthParticleAuxContainer>
      m_turthLeptonHandle{this, "truthLep", "truthLep",
                          "the truth lepton container output"};
};

#endif
