/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

///
/// Kindly stolen from Tadej Novak
///

#ifndef MULTI_LEPTON_ALGS__TRIGGER_MATCHING_ALG_H
#define MULTI_LEPTON_ALGS__TRIGGER_MATCHING_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <TriggerMatchingTool/IMatchingTool.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTau/TauJetContainer.h>

/// \brief an algorithm for trigger matching
class TriggerMatchingAlg final : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  TriggerMatchingAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief trigger decision tool handle
private:
  ToolHandle<Trig::IMatchingTool> m_trigMatchingTool;

  /// \brief the systematics list we run
private:
  CP::SysListHandle m_systematicsList{this};

  /// \brief the decoration for trigger matching
private:
  std::string m_matchingDecoration;

  /// \brief the decoration for trigger matching pairs
private:
  std::string m_matchingPairDecoration;

  /// \brief the accessors for \ref m_matchingDecoration and triggers
  /// combination
private:
  std::unordered_map<std::string, SG::AuxElement::Decorator<bool>>
      m_matchingAccessors;

  /// \brief the accessors for \ref m_matchingPairDecoration and dilepton
  /// triggers combination
private:
  std::unordered_map<std::string, SG::AuxElement::Decorator<
                                      std::vector<std::pair<uint8_t, uint8_t>>>>
      m_matchingPairAccessors;

  /// \brief list of triggers
private:
  std::vector<std::string> m_trigListSingleElectron;
  std::vector<std::string> m_trigListSingleMuon;
  std::vector<std::string> m_trigListSingleTau;
  std::vector<std::string> m_trigListDiElectron;
  std::vector<std::string> m_trigListDiMuon;
  std::vector<std::string> m_trigListDiTau;
  std::vector<std::string> m_trigListElectronMuon;
  std::vector<std::string> m_trigListElectronTau;
  std::vector<std::string> m_trigListMuonTau;

private:
  CP::SysReadHandle<xAOD::ElectronContainer> m_electronsHandle{
      this, "electrons", "", "the electrons container to use"};
  CP::SysReadHandle<xAOD::MuonContainer> m_muonsHandle{
      this, "muons", "", "the muons container to use"};
  CP::SysReadHandle<xAOD::TauJetContainer> m_tausHandle{
      this, "taus", "", "the taus container to use"};
};

#endif
