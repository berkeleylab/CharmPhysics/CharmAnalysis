# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# AnaAlgorithm import(s):
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
import ROOT


def makeTrackSelectionAnalysisSequence(dataType,
                                       trackSelection,
                                       primaryVertexKey='PrimaryVertices',
                                       removeTracksFromBosons=True,
                                       fixd0=True):

    if not dataType in ['data', 'mc', 'afii']:
        raise ValueError("invalid data type: " + dataType)

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('TrackSelectionAnalysisSequence')

    # Track selection algorithm:
    alg = createAlgorithm('TrackSelectionAlg', 'TrackSelectionAlg')
    alg.primaryVertexKey = primaryVertexKey
    alg.usePrimaryVertex = True
    alg.ApplySysSmearing = True
    alg.ApplySysBiasing = True
    alg.ApplySysEfficiency = True
    alg.fixd0 = fixd0
    alg.removeTracksFromBosons = removeTracksFromBosons
    alg.selectionDecoration = 'selected_track'

    # Print systematic tools
    print(alg.TrackTruthFilterTool)
    print(alg.TrackSmearingTool)
    print(alg.TrackBiasingTool)

    # In Athena the tool is designed to also be able to work on Trk::Track objects.
    # The corresponding accept() function needs access to some other Athena tools,
    # namely Trk::TrackSummaryTool and Trk::Extrapolator. Setting these up can make
    # running a job options more difficult, so the user will need to flag their desire
    # for this functionality explicitly.
    alg.TrackSelectionTool.UseTrkTrackTools = False

    # track selection tool
    # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/InDetTrackSelectionTool
    for cut in trackSelection:
        setattr(alg.TrackSelectionTool, cut, trackSelection[cut])
    seq.append(alg, inputPropName={'tracks': 'tracks',
                                   'bosons': 'bosons'},
               outputPropName={'tracks': 'tracksOut'},
               stageName='selection')

    # Decoration for Tight tracks
    trackSelection['CutLevel'] = "TightPrimary"
    alg = createAlgorithm('TrackSelectionAlg', 'TightTrackSelectionAlg')
    alg.primaryVertexKey = primaryVertexKey
    alg.ApplySysSmearing = False
    alg.usePrimaryVertex = True
    alg.fixd0 = fixd0
    alg.removeTracksFromBosons = removeTracksFromBosons
    alg.selectionDecoration = 'selected_track_tight'
    alg.TrackSelectionTool.UseTrkTrackTools = False
    for cut in trackSelection:
        setattr(alg.TrackSelectionTool, cut, trackSelection[cut])
    seq.append(alg, inputPropName={'tracks': 'tracks',
                                   'bosons': 'bosons'},
               stageName='selection')

    # Track decoration algorithm
    alg = createAlgorithm('TrackDecoratorAlg', 'TrackDecoratorAlg')
    alg.primaryVertexKey = primaryVertexKey
    seq.append(alg, inputPropName={'tracks': 'tracks'},
               stageName='calibration')

    # Set up an algorithm used for debugging the track selection:
    alg = createAlgorithm('CP::ObjectCutFlowHistAlg',
                          'TrackCutFlowDumperAlg')
    alg.histPattern = 'track_cflow_%SYS%'
    alg.selection = ['selected_track']
    alg.selectionNCuts = [10]
    seq.append(alg, inputPropName={'tracks': 'input'},
               stageName='selection')

    # Set up an algorithm used for debugging the tight track selection
    alg = createAlgorithm('CP::ObjectCutFlowHistAlg',
                          'TightTrackCutFlowDumperAlg')
    alg.histPattern = 'tight_track_cflow_%SYS%'
    alg.selection = ['selected_track_tight']
    alg.selectionNCuts = [10]
    seq.append(alg, inputPropName={'tracks': 'input'},
               stageName='selection')

    # # Union selection alg
    # alg = createAlgorithm( 'CP::AsgUnionSelectionAlg',
    #                        'TrackUnionSelectionAlg' )
    # alg.preselection = 'selected_track'
    # alg.selectionDecoration = 'final_selection'
    # seq.append( alg, inputPropName = {'tracks': 'particles'} )

    # Set up an algorithm that makes a view container using the selections
    alg = createAlgorithm('CP::AsgViewFromSelectionAlg',
                          'TrackViewFromSelectionAlg')
    alg.selection = ['selected_track']
    seq.append(alg, inputPropName={'tracks': 'input'},
               outputPropName={'tracks': 'output'},
               stageName='selection')

    # Return the sequence:
    return seq


def makeV0TrackSelectionAnalysisSequence(dataType,
                                         trackSelection,
                                         primaryVertexKey='PrimaryVertices',
                                         removeTracksFromBosons=True,
                                         fixd0=True):

    if not dataType in ['data', 'mc', 'afii']:
        raise ValueError("invalid data type: " + dataType)

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('V0TrackSelectionAnalysisSequence')

    # Track selection algorithm:
    alg = createAlgorithm('TrackSelectionAlg', 'V0TrackSelectionAlg')
    alg.primaryVertexKey = primaryVertexKey
    alg.usePrimaryVertex = True
    alg.fixd0 = fixd0
    alg.removeTracksFromBosons = removeTracksFromBosons
    alg.selectionDecoration = 'selected_track_v0'
    alg.TrackSelectionTool.UseTrkTrackTools = False
    for cut in trackSelection:
        setattr(alg.TrackSelectionTool, cut, trackSelection[cut])

    # Add alg in sequence
    seq.append(alg, inputPropName={'tracksv0': 'tracks',
                                   'bosons': 'bosons'},
               outputPropName={'tracksv0': 'tracksOut'},
               stageName='selection')

    # Set up an algorithm that makes a view container using the selections
    alg = createAlgorithm('CP::AsgViewFromSelectionAlg',
                          'V0TrackViewFromSelectionAlg')
    alg.selection = ['selected_track_v0']
    alg.deepCopy = True
    seq.append(alg, inputPropName={'tracksv0': 'input'},
               outputPropName={'tracksv0': 'output'},
               stageName='selection')

    # Return the sequence:
    return seq


def makeTrackSelectionAroundMesonAnalysisSequence(dataType):

    if not dataType in ['data', 'mc', 'afii']:
        raise ValueError("invalid data type: " + dataType)

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('TrackSelectionAroundMesonAnalysisSequence')

    # Track selection algorithm:
    alg = createAlgorithm('TrackSelectionAroundMesonAlg',
                          'TrackSelectionAroundMesonAlg')
    alg.selectionDecoration = 'selected_around_meson_track'
    alg.maxDeltaR = 1.2

    # Add alg in sequence
    seq.append(alg, inputPropName={'tracks': 'tracks',
                                   'mesons': 'mesons'},
               outputPropName={'tracks': 'tracksOut'},
               stageName='selection')

    # Set up an algorithm that makes a view container using the selections
    alg = createAlgorithm('CP::AsgViewFromSelectionAlg',
                          'TrackAroundMesonViewFromSelectionAlg')
    alg.selection = ['selected_around_meson_track']
    seq.append(alg, inputPropName={'tracks': 'input'},
               outputPropName={'tracks': 'output'},
               stageName='selection')

    # Return the sequence:
    return seq
