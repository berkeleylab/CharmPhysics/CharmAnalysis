# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

# AnaAlgorithm import(s):
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
import ROOT


def makeDSelectionAnalysisSequence(dataType,
                                   deepCopyOutput=False,
                                   postfix = ''):
    """Create a jet analysis algorithm sequence

    Keyword arguments:
      dataType       -- The data type to run on ("data", "mc" or "afii")
      deepCopyOutput -- If set to 'True', the output containers will be
                        standalone, deep copies (slower, but needed for xAOD
                        output writing)
      postfix        -- a postfix to apply to decorations and algorithm
                        names.  this is mostly used/needed when using this
                        sequence with multiple working points to ensure all
                        names are unique.
    """

    if not dataType in ['data', 'mc', 'afii']:
        raise ValueError("invalid data type: " + dataType)

    if postfix != '':
        postfix = '_' + postfix
        pass

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('DSelectionAnalysisSequence')

    # Set up the jet calibration and smearing algorithm:
    alg = createAlgorithm('DSelectionAlg', 'DSelectionAlg')
    
    seq.append(alg, inputPropName = {'tracks': 'tracks'},
                    outputPropName = {'composites' : 'composites',
                                      'composites_k3pi' : 'composites_k3pi',
                                      'composites_vee' : 'composites_vee'},
                    stageName = 'selection')
    
    # Set up a final deep copy making algorithm if requested:
    if deepCopyOutput:
        alg = createAlgorithm('CP::AsgViewFromSelectionAlg',
                              'DSelectionDeepCopyMaker' + postfix)
        alg.deepCopy = True
        seq.append(alg, inputPropName = 'input',
                        outputPropName = 'output',
                        stageName = 'selection')

    # Return the sequence:
    return seq
