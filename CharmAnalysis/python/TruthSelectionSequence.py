# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# AnaAlgorithm import(s):
from AthenaCommon.Constants import VERBOSE, DEBUG, INFO
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
import ROOT


def makeTruthSelectionAnalysisSequence(dataType):

    if not dataType in ['mc', 'afii']:
        raise ValueError("invalid data type for truth selection algorithms: " + dataType)

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('TruthSelectionAnalysisSequence')

    # Set up the jet calibration and smearing algorithm:
    alg = createAlgorithm('TruthSelectionAlg', 'TruthSelectionAlg')
    alg.OutputLevel = INFO
    alg.selectionDecoration = 'selected_truth'

    # Get MC truth classifier
    print("Getting MCTruthClassifier")
    from MCTruthClassifier.MCTruthClassifierBase import MCTruthClassifier
    print("Retrieved MCTruthClassifier")
    alg.tool = MCTruthClassifier

    seq.append(alg, inputPropName = {'truth' : 'truth'},
                    outputPropName = {'truthLep' : 'truthLep'},
                    stageName = 'selection' )

    # Skimmed truth container
    alg = createAlgorithm( 'CP::AsgViewFromSelectionAlg', 'TruthViewFromSelectionAlg' )
    alg.selection = ['selected_truth']
    seq.append( alg, inputPropName = {'truth' : 'input'},
                     outputPropName = {'truth' : 'output'},
                     stageName = 'selection' )

    # # Truth lepton container
    # alg = createAlgorithm( 'CP::AsgViewFromSelectionAlg', 'TruthLepViewFromSelectionAlg' )
    # seq.append( alg, inputPropName = {'truthLep' : 'input'},
    #                  outputPropName = {'truthLep' : 'output'},
    #                  stageName = 'selection' )

    # Return the sequence:
    return seq


def makeTruthJetAnalysisSequence(jet):
    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('TruthJetAnalysisSequence_%s' % jet)

    # Set up an algorithm that makes a view container for truth jets
    alg = createAlgorithm('CP::AsgViewFromSelectionAlg',
                            'TruthJetViewFromSelectionAlg_%s' % jet)
    seq.append(alg, inputPropName={'jets': 'input'},
                outputPropName={'jets': 'output'},
                stageName='selection')

    # truth jet decorator
    alg = createAlgorithm('TruthJetDecoratorAlg',' TruthJetDecoratorAlg_%s' % jet)
    seq.append(alg, inputPropName = {'jets' : 'jets'},
                    stageName = 'selection' )

    # Return the sequence:
    return seq
