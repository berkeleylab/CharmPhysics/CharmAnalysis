# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.Constants import VERBOSE, DEBUG, INFO

# AnaAlgorithm import(s):
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
import ROOT


def makeZSelectionSequence(dataType,
                           enableFilter=False,
                           deepCopyOutput=False,
                           postfix = ''):
    """Create a ZSelection algorithm sequence

    Keyword arguments:
      dataType       -- The data type to run on ("data", "mc" or "afii")
      deepCopyOutput -- If set to 'True', the output containers will be
                        standalone, deep copies (slower, but needed for xAOD
                        output writing)
      postfix        -- a postfix to apply to decorations and algorithm
                        names.  this is mostly used/needed when using this
                        sequence with multiple working points to ensure all
                        names are unique.
    """

    if not dataType in ['data', 'mc', 'afii']:
        raise ValueError("invalid data type: " + dataType)

    if postfix != '':
        postfix = '_' + postfix
        pass

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('ZSelectionSequence')

    # Set up the algorithm:
    alg = createAlgorithm('ZSelectionAlg', 'ZSelectionAlg')
    alg.OutputLevel = DEBUG
    alg.enableFilter = enableFilter

    seq.append(alg, inputPropName = {'electrons' : 'electrons',
                                     'muons' : 'muons'},
                    outputPropName = 'composites',
                    stageName = 'selection')

    # Set up a final deep copy making algorithm if requested:
    if deepCopyOutput:
        alg = createAlgorithm('CP::AsgViewFromSelectionAlg',
                              'ZSelectionDeepCopyMaker' + postfix)
        alg.deepCopy = True
        seq.append(alg, inputPropName = 'input',
                        outputPropName = 'output',
                        stageName = 'selection')
        pass

    # Return the sequence:
    return seq
