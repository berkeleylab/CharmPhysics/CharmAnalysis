#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.DualUseConfig import createAlgorithm

#
# Output Algorithms config
#

def setupNTupleOutput(athAlgSeq, sequences, Conf, treeName, truthOnly):

    # All branches
    branches = []

    # EventInfo
    if sequences['Event']:
        from CharmAnalysis.Config import getEventBranches
        branches += getEventBranches(Conf)

    # EventInfo for TRUTH derivations
    if truthOnly:
        from CharmAnalysis.Config import getTruthOnlyBranches
        branches += getTruthOnlyBranches(Conf)

    # Electrons
    if sequences['Electrons']:
        from CharmAnalysis.Config import getElectronBranches
        branches += getElectronBranches(Conf, sequences['PostProcessing'])

    # Muons
    if sequences['Muons']:
        from CharmAnalysis.Config import getMuonBranches
        branches += getMuonBranches(Conf, sequences['PostProcessing'])

    # Jets
    if sequences['Jets']:
        from CharmAnalysis.Config import getJetBranches
        branches += getJetBranches(Conf)

    # MET
    if sequences['METInfo']:
        from CharmAnalysis.Config import getMETInfoBranches
        branches += getMETInfoBranches(Conf)

    # # MET
    # if sequences['MET']:
    #     from CharmAnalysis.Config import getMETBranches
    #     branches += getMETBranches()

    # # W Composites
    # if sequences['WeWmSelection']:
    #     from CharmAnalysis.Config import getWeWmSelectionBranches
    #     branches += getWeWmSelectionBranches()

    # # Z Composites
    # if sequences['ZSelection']:
    #     from CharmAnalysis.Config import getZSelectionBranches
    #     branches += getZSelectionBranches()

    # D Composites
    if sequences['DSelection']:
        from CharmAnalysis.Config import getDSelectionBranches
        branches += getDSelectionBranches(Conf)

    # D Composites
    if sequences['DSelection'] and Conf.doV0Finding:
        from CharmAnalysis.Config import getVeeBranches
        branches += getVeeBranches()

    # Truth
    if sequences['Truth']:
        from CharmAnalysis.Config import getTruthBranches, getTruthJets
        branches += getTruthBranches(Conf)
        if Conf.doTruthJets:
            branches += getTruthJets(Conf)

    # Tracks
    if sequences['Tracks'] and Conf.saveTracks:
        from CharmAnalysis.Config import getTrackBranches
        branches += getTrackBranches(Conf)

    # Truth PV
    if Conf.saveTruthVertices:
        from CharmAnalysis.Config import getTruthVerticesBranches
        branches += getTruthVerticesBranches()

    # Track-jets
    if Conf.doTrackJets and not truthOnly:
        from CharmAnalysis.Config import getTrackJetBranches
        branches += getTrackJetBranches(Conf)

    # Add an ntuple dumper algorithm
    treeMaker = createAlgorithm('CP::TreeMakerAlg', '%sTreeMaker' % treeName)
    treeMaker.RootStreamName = Conf.RootStreamName
    treeMaker.TreeName = treeName
    athAlgSeq += treeMaker

    # Ntuple maker
    ntupleMaker = createAlgorithm('CP::AsgxAODNTupleMakerAlg', '%sNTupleMaker' % treeName)
    ntupleMaker.RootStreamName = Conf.RootStreamName
    ntupleMaker.TreeName = treeName
    ntupleMaker.Branches = branches
    athAlgSeq += ntupleMaker

    # TreeFiller
    treeFiller = createAlgorithm('CP::TreeFillerAlg', '%sTreeFiller' % treeName)
    treeFiller.RootStreamName = Conf.RootStreamName
    treeFiller.TreeName = treeName
    athAlgSeq += treeFiller
