#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
from AthenaCommon.SystemOfUnits import GeV


class WorkingPoints:

    def __init__(self, Conf):
        self.Conf = Conf
        assert self.Conf, "Configuration file not provided to Working Points"

        # Jets
        self.jet_pt_min = 20 * GeV
        self.jet_eta_max = 4.5

        # Track-jets
        self.track_jet_radius = [0.6, 0.8, 1.0]

        # b-jets
        self.jet_flavour_taggers = ['DL1r']
        self.jet_flavour_wp = ['FixedCutBEff']
        self.jet_flavour_eff = ['70']

        # Electrons
        self.ele_pt_min = 20 * GeV
        self.ele_pt_max = 0 # no cut
        self.ele_d0sig_max = 5
        self.ele_base_id = 'TightLHElectron'
        self.ele_base_isol = 'NonIso'
        self.ele_id_list = ['Tight']
        self.ele_isol_list = ['Tight_VarRad']
        self.ele_trig_list = ['SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0']

        # Muons
        self.mu_pt_min = 3 * GeV
        self.mu_pt_max = 0 # no cut
        self.mu_d0sig_max = 3
        self.mu_base_reco = 'Loose'
        self.mu_base_isol = 'NonIso'
        self.mu_id_list = ['Loose', 'Medium', 'Tight']
        self.mu_isol_list = ['PflowTight_VarRad']
        self.mu_trig_list = set()
        self.mu_trig_list.add(('HLT_mu20_iloose_L1MU15_OR_HLT_mu50', 0, 290000)) # 2015 only
        self.mu_trig_list.add(('HLT_mu26_ivarmedium_OR_HLT_mu50', 290000, 999999)) # 2016-2018

        # MET
        self.met_component = 'Final'
        self.met_min = 0
        self.met_max = 0

        if self.Conf.analysisType == "zplusd":

            # Electrons
            self.ele_pt_min = 15 * GeV
            self.ele_base_id = 'MediumLHElectron'
            self.ele_base_isol = 'NonIso'
            self.ele_id_list = ['Medium', 'Tight']
            self.ele_isol_list = ['FCLoose', 'FCTight', 'Loose_VarRad', 'Tight_VarRad']

            # Muons
            self.mu_pt_min = 15 * GeV
            self.mu_base_reco = 'Medium'
            self.mu_base_isol = 'NonIso'
            self.mu_id_list = ['Medium']
            self.mu_isol_list = ['PflowLoose_VarRad','PflowTight_VarRad']
