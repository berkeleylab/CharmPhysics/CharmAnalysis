#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#


class ContainerNames:

    # Jet type
    jetContainer = 'AntiKt4EMPFlowJets_BTagging201903'

    # Fake primary vertex container
    FakePrimaryVertex = 'FakeVxPrimaryVertex'

    # Primary vertex container
    PrimaryVertices = 'PrimaryVertices'

    # Jets
    jets = {'input': jetContainer,
            'output': 'AnalysisJets'}

    # Pileup
    event = {'input': 'EventInfo',
             'output': 'EventInfo'}

    # Muons
    muons = {'input': 'Muons',
             'output': 'AnalysisMuons'}

    # Electrons
    electrons = {'input': 'Electrons',
                 'output': 'AnalysisElectrons'}

    # MET
    MissingEnergy = {'output' : 'AnalysisMET'}

    # Tracks
    tracks = {'input': 'InDetTrackParticles',
              'output': 'AnalysisTracks'}
    tracksV0 = {'input': 'InDetTrackParticles',
                'output': 'AnalysisTracksV0'}
    TruthTrackParticles = 'TruthTrackParticles'

    # Truth
    truth = {'input': 'TruthParticles',
             'output': 'TruthParticles_Selected'}
    truthLep = 'TruthLeptons'

    # WeWmSelection
    WeWmSelection = {'output': 'CompositeParticles_WBosons'}

    # ZSelection
    ZSelection = {'output': 'CompositeParticles_ZBosons'}

    # DSelection
    DSelection = {'output': 'DMesons'}
    DSelection_k3pi = {'output': 'DMesons_k3pi'}
    DSelection_vee = {'output': 'DMesons_vee'}
