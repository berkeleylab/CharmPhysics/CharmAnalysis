#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

import CharmAnalysis.Config.JobConfigs.Default as default

Config = default.Config
Config.sampleType = "ForcedDecay"
Config.triggerChain = 'NoTrigger'
Config.cutflowAlg = True
Config.d0K3pi = False
Config.doV0Finding = False
Config.doJVT = False
Config.doMetaData = False
Config.doObjectCalibration = False
Config.doPileupRW = False
Config.doWBosonReco = False
Config.eventCleaning = False
Config.fakePrimaryVertex = True
Config.saveTracks = False
Config.systematics = False
Config.truthOnlyTree = False
Config.doTruthLeptons = False
