#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

# Override default with 'Athena -c "property=value"'


class Config:

    # Analysis Type (for setting the working point)
    # Options: wplusd, zplusd, prodfrac
    analysisType = "zplusd"

    # Lepton multiplicity selection
    minLeptonsLight = 2
    maxLeptonsLight = -1
    minElectrons = 0
    maxElectrons = -1
    minMuons = 0
    maxMuons = -1

    # derivation type
    derivationType = "FTAG4"

    # running with AthenaMP
    athenaMP = False

    # make an additional truth-only tree
    truthOnlyTree = False

    # DEBUG printout for all algorithms
    debugPrintout = False

    # save tracks
    saveTracks = False

    # flag for old derivation cache
    oldDerivationCache = False

    # construct track-jets
    doTrackJets = False

    # run with systematics
    systematics = True

    # perform object calibration
    doObjectCalibration = True

    # read in Metadata
    doMetaData = True

    # perform Z boson reconstruciton
    doZBosonReco = True

    # perform W boson reconstruciton
    doWBosonReco = False

    # perform D meson reconstruction
    doDMesonReco = True

    # perform V0 finding
    doV0Finding = False

    # perform Dplus finding
    dplus = True
    
    # perform Dsubs finding
    dsubs = True
    
    # perform D0kpi finding
    d0kpi = True

    # perform K3pi finding
    d0K3pi = False

    # perform K3pi finding without D*
    d0K3pi_only = False

    # perform lambdaC finding
    lambdaC = True

    # perform truth selection/output
    doTruth = True

    # perform truth lepton selection
    doTruthLeptons = True

    # perform truth jet selection
    doTruthJets = False

    # create a 'CHARM EventInfo' object
    doCharmEventInfo = True

    # CHARM EventInfo contains info about leptons
    leptonInfo = False

    # Use a 'fake' primary vertex (for particle-gun samples)
    fakePrimaryVertex = False

    # Create tracks from truth
    tracksFromTruth = False

    # perform pielup reweighting
    doPileupRW = True

    # calculate JVT
    doJVT = True

    # (mc or data)
    dataType = 'mc'

    # (MC16a/d/e for mc and 2015/6/7/8 for data)
    sampleType = 'MC16e'

    # MC generator (for flavor tagging MC/MC SFs)
    MCGen = 'default'

    # name of the tree in the otput ntuple file
    treeName = "CharmAnalysis"

    # do an approximate fit before the actual SV fit
    doApproximateFit = True

    # use primary vertex in track z0 selection
    usePrimaryVertex = True

    # running on a DxAOD file
    onDerivation = True

    # print out extra variables
    extraVars = True

    # run event cleaning
    eventCleaning = True

    # dress leptons
    dressLeptons = True

    # save truth vertices
    saveTruthVertices = False

    # set trigger chain
    triggerChain = 'SingleLepton'

    # save FTAG discriminatn
    saveFtagDiscriminant = True

    # root stream name
    RootStreamName = 'ANALYSIS'
