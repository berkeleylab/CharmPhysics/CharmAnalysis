#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

import CharmAnalysis.Config.JobConfigs.Default as default

Config = default.Config
Config.doWBosonReco = False
Config.doDMesonReco = True
Config.doObjectCalibration = False
Config.doMetaData = False
Config.doTruth = True
Config.doCharmEventInfo = False
Config.doPileupRW = False
Config.doJVT = True
Config.dataType = 'mc'
Config.sampleType = 'MinBias'
Config.MCGen = 'default'
Config.doApproximateFit = True
Config.onDerivation = False
Config.xAODOutput = False
Config.truthOnlyTree = False
