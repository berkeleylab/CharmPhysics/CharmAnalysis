#
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#

# Override defautl with 'Athena -c "property=value"'

import CharmAnalysis.Config.JobConfigs.Default as default

Config = default.Config

Config.doTrackJets = False
Config.doTrackJetSystematics = False
Config.doVRTrackJets = False

Config.systematics = False

# Only Dplus for now
Config.dplus = True
Config.dsubs = False
Config.d0kpi = False
Config.lambdaC = False
