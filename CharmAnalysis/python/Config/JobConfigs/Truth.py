#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

import CharmAnalysis.Config.JobConfigs.Default as default

Config = default.Config
Config.sampleType = "Truth"
Config.doCharmEventInfo = False
Config.doDMesonReco = False
Config.doJVT = False
Config.doMetaData = True
Config.doObjectCalibration = False
Config.doPileupRW = False
Config.doTruth = True
Config.doWBosonReco = False
Config.systematics = False
Config.tracksFromTruth = False
Config.treeName = ""
Config.truthOnlyTree = True
