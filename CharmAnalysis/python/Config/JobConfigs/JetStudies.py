#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

# Override defautl with 'Athena -c "property=value"'


class Config:

    # Analysis Type (for setting the working point)
    # Options: wplusd, zplusd, prodfrac
    analysisType = "wplusd"

    # flag for old derivation cache
    oldDerivationCache = False

    # run with systematics
    systematics = False

    # perform object calibration
    doObjectCalibration = True

    # read in Metadata
    doMetaData = True

    # perform Z boson reconstruciton
    doZBosonReco = False

    # perform W boson reconstruciton
    doWBosonReco = False

    # perform D meson reconstruction
    doDMesonReco = False

    # perform truth selection/output
    doTruth = False

    # create a 'CHARM EventInfo' object
    doCharmEventInfo = True

    # CHARM EventInfo contains info about leptons
    leptonInfo = False

    # Use a 'fake' primary vertex (for particle-gun samples)
    fakePrimaryVertex = False

    # perform pielup reweighting
    doPileupRW = True

    # calculate JVT
    doJVT = True

    # (mc or data)
    dataType = 'mc'

    # (MC16a/d/e for mc and 2015/6/7/8 for data)
    sampleType = 'MC16e'

    # MC generator (for flavor tagging MC/MC SFs)
    MCGen = 'default'

    # name of the tree in the otput ntuple file
    treeName = "CharmAnalysis"

    # do an approximate fit before the actual SV fit
    doApproximateFit = True

    # use primary vertex in track z0 selection
    usePrimaryVertex = True

    # running on a DxAOD file
    onDerivation = True

    # print out extra variables
    extraVars = True

    # run event cleaning
    eventCleaning = True

    # set trigger chain
    triggerChain = 'SingleLepton'

    # save FTAG discriminatn
    saveFtagDiscriminant = True

    # root stream name
    RootStreamName = 'ANALYSIS'
