#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

import CharmAnalysis.Config.JobConfigs.Default as default

Config = default.Config
Config.systematics = False
