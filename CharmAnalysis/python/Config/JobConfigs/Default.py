#
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#

# Override defautl with 'Athena -c "property=value"'


class Config:

    # Analysis Type (for setting the working point)
    # Options: wplusd, zplusd, prodfrac
    analysisType = "wplusd"

    # Lepton multiplicity selection
    minLeptonsLight = 1
    maxLeptonsLight = -1
    minElectrons = 0
    maxElectrons = -1
    minMuons = 0
    maxMuons = -1

    # "SampleID" for ttbar reweighting
    sampleID = -1

    # running with AthenaMP
    athenaMP = False

    # DEBUG printout for all algorithms
    debugPrintout = False

    # construct track-jets
    doTrackJets = True

    # systematics for track-jets
    doTrackJetSystematics = True

    #construct VR track jets
    doVRTrackJets = True
    
    # perform Dplus finding
    dplus = True
    
    # perform Dsubs finding
    dsubs = True
    
    # perform D0kpi finding
    d0kpi = True

    # perform K2pi finding for D*
    d0K2pi = False

    # perform K3pi finding for D*
    d0K3pi = False

    # perform K3pi finding without D*
    d0K3pi_only = False
    
    # perform lambdaC finding
    lambdaC = True

    # perform object calibration
    doObjectCalibration = True

    # read in Metadata
    doMetaData = True

    # perform Z boson reconstruciton
    doZBosonReco = False

    # perform W boson reconstruciton
    doWBosonReco = True

    # perform D meson reconstruction
    doDMesonReco = True

    # perform V0 finding
    doV0Finding = False

    # perform truth selection/output
    doTruth = True

    # perform truth bottom selection
    doTruthBottom = True

    # perform truth lepton selection
    doTruthLeptons = True

    # perform truth jet selection
    doTruthJets = True

    # create a 'CHARM EventInfo' object
    doCharmEventInfo = True

    # PV info
    doPVinfo = False

    # perform pielup reweighting
    doPileupRW = True

    # calculate JVT
    doJVT = True

    # do an approximate fit before the actual SV fit
    doApproximateFit = True

    # dress leptons
    dressLeptons = True

    # run event cleaning
    eventCleaning = True

    # print out extra variables
    extraVars = False

    # Use a 'fake' primary vertex (for particle-gun samples)
    fakePrimaryVertex = False

    # only variables needed for the final analysis
    fewerVars = True

    # CHARM EventInfo contains info about leptons
    leptonInfo = False

    # new STDM13 with truth charged jets
    newSTDM13 = True

    # running on a DxAOD file
    onDerivation = True

    # flag for old derivation cache
    oldDerivationCache = False

    # produce HF production fraction weights
    prodFractionWeight = True

    # run with systematics
    systematics = True

    # Create tracks from truth
    tracksFromTruth = False

    # use primary vertex in track z0 selection
    usePrimaryVertex = True

    # save FTAG discriminatn
    saveFtagDiscriminant = True

    # save tracks
    saveTracks = True

    # save truth vertices
    saveTruthVertices = False

    # make an additional truth-only tree
    truthOnlyTree = False

    # MC generator (for flavor tagging MC/MC SFs)
    MCGen = 'default'

    # root stream name
    RootStreamName = 'ANALYSIS'

    # derivation type
    derivationType = "FTAG4"

    # (mc or data)
    dataType = 'mc'

    # (MC16a/d/e for mc and 2015/6/7/8 for data)
    sampleType = 'MC16e'

    # name of the tree in the otput ntuple file
    treeName = "CharmAnalysis"

    # set trigger chain
    triggerChain = 'SingleLepton'
