#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm

from CharmAnalysis.Config.Trigger import TriggerList


def sampleTypeRunNumber(sampleType):
    if sampleType == 'MC16a':
        return 284500
    if sampleType == 'MC16d':
        return 300000
    if sampleType == 'MC16e':
        return 310000
    return 999999


def generateTriggerChain(triggerMode):

    # Single lepton triggers
    if triggerMode == 'SingleLepton':
        return TriggerList.el_single + TriggerList.mu_single
    elif triggerMode == 'ElectronTrigger':
        return TriggerList.el_single
    elif triggerMode == 'MuonTrigger':
        return TriggerList.mu_single
    elif triggerMode == 'NoTrigger':
        return []

    # Unknown mode
    else:
        raise ValueError("Unknown trigger mode")


def formatBranchName(outputContainer, branches):
    """ Creates the correct branch name format
        for the AsgxAODNTupleMakerAlg algorithm.

        Examples:
            'AnalysisMuons_%SYS%.pt -> AnalysisMuons_%SYS%_pt'
            'CompositeParticles_NOSYS.pdgId -> CompositeParticles_pdgId'

        Expected input is:
            'NOSYS.var' or
            '%SYS%.var'
    """

    for branch in branches:
        assert branch.startswith('NOSYS.') or branch.startswith('%SYS%.')

    formattedBranches = []
    for branch in branches:
        out = outputContainer + '_' + branch + ' -> '
        out += outputContainer + '_' + branch.replace('NOSYS.', '').replace('%SYS%.', '').replace('_NOSYS', '').replace('_%SYS%', '').replace('.', '_')
        if "%SYS%" in branch:
            out += "_%SYS%"
        formattedBranches += [out]

    print('Branches for output:')
    for branch in formattedBranches:
        print('    ' + branch)

    return formattedBranches


def formatBranchNameNoSYS(outputContainer, branches):

    formattedBranches = [outputContainer + '.' + branch + ' -> ' +
                         outputContainer + '_' + branch for branch in branches]

    print('Branches for output:')
    for branch in formattedBranches:
        print('    ' + branch)

    return formattedBranches


def getAlgorithmNames(seq, veto=['CondInputLoader', 'SysLoaderAlg']):
    algs = []
    for obj in seq:
        if type(obj) == AnaAlgSequence:
            algs += getAlgorithmNames(obj)
        else:
            add = True
            for a in veto:
                if a in obj.name():
                    add = False
            if add:
                algs += [obj.name()]
    return algs


def setupCutFlow(athAlgSeq):
    i = 0
    for seq in athAlgSeq:
        cutflow_algs = []
        if type(seq) == AnaAlgSequence:
            for x in seq:
                alg = createAlgorithm(
                    'CP::AthEventCutflowAlg', 'AthEventCutflowAlg' + x.name())
                alg.algorithmList = [x.name()]
                alg.histPattern = x.name() + "_" + str(i)
                cutflow_algs += [alg]
                i += 1

        for j, alg in enumerate(cutflow_algs):
            seq.insert(2*j+1, alg, None)


def setupFilterAlg(athAlgSeq):
    alg = createAlgorithm('CP::AthEventFilterAlg', 'AthEventFilterAlg')
    algs_names = []
    for seq in athAlgSeq:
        if type(seq) == AnaAlgSequence:
            for x in seq:
                algs_names += [x.name()]
    alg.algorithmList = algs_names
    return alg
