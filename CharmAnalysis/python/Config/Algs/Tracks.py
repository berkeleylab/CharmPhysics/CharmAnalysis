#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from AthenaCommon.SystemOfUnits import MeV, mm

from AnaAlgorithm.DualUseConfig import createAlgorithm

from CharmAnalysis.Config import ContainerNames
from CharmAnalysis.Config import formatBranchName

#
# Track Algorithms config
#


def setupTrackAlgs(athAlgSeq, Conf):
    # TrackSelection bits
    # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/InDetTrackSelectionTool
    trackSelection = {}
    trackSelection['CutLevel'] = "Loose"
    trackSelection['minPt'] = 500 * MeV
    trackSelection['maxZ0SinTheta'] = 5 * mm

    # PV container name
    PVContainer = ContainerNames.PrimaryVertices if not Conf.fakePrimaryVertex else ContainerNames.FakePrimaryVertex

    # TrackSelection sequence:
    from CharmAnalysis.TrackSelectionSequence import makeTrackSelectionAnalysisSequence
    seq = makeTrackSelectionAnalysisSequence(dataType=Conf.dataType,
                                             trackSelection=trackSelection,
                                             primaryVertexKey=PVContainer,
                                             removeTracksFromBosons=Conf.doZBosonReco or Conf.doWBosonReco,
                                             fixd0=True)

    # Configure the sequence
    if (Conf.analysisType == "zplusd"):
        seq.configure(inputName={'tracks': ContainerNames.tracks['input'],
                                 'bosons': ContainerNames.ZSelection['output']},
                      outputName={'tracks': ContainerNames.tracks['output'] + "_%SYS%"})
    else:
        seq.configure(inputName={'tracks': ContainerNames.tracks['input'],
                                 'bosons': ContainerNames.WeWmSelection['output']},
                      outputName={'tracks': ContainerNames.tracks['output'] + "_%SYS%"})
    # Add all algorithms to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def setupV0TrackAlgs(athAlgSeq, ToolSvc, Conf):
    # TrackSelection sequence:
    trackSelection = {}
    trackSelection['CutLevel'] = "Loose"
    trackSelection['minPt'] = 500 * MeV
    # trackSelection['maxZ0SinTheta'] = 5 * mm

    # PV container name
    PVContainer = ContainerNames.PrimaryVertices if not Conf.fakePrimaryVertex else ContainerNames.FakePrimaryVertex

    from CharmAnalysis.TrackSelectionSequence import makeV0TrackSelectionAnalysisSequence
    seq = makeV0TrackSelectionAnalysisSequence(dataType=Conf.dataType,
                                               trackSelection=trackSelection,
                                               primaryVertexKey=PVContainer,
                                               removeTracksFromBosons=Conf.doZBosonReco or Conf.doWBosonReco,
                                               fixd0=True)

    # Configure the sequence
    seq.configure(inputName={'tracksv0': ContainerNames.tracksV0['input'],
                             'bosons': ContainerNames.WeWmSelection['output']},
                  outputName={'tracksv0': ContainerNames.tracksV0['output']})
    # Add all algorithms to the job:
    athAlgSeq += seq


def setupTrackMesonSelectionAlgs(athAlgSeq, Conf):
    # TrackSelection sequence:
    from CharmAnalysis.TrackSelectionSequence import makeTrackSelectionAroundMesonAnalysisSequence
    seq = makeTrackSelectionAroundMesonAnalysisSequence(dataType=Conf.dataType)

    # Configure the sequence
    seq.configure(inputName={'tracks': ContainerNames.tracks['output'] + "_%SYS%",
                             'mesons': ContainerNames.DSelection['output'] + "_%SYS%"},
                  outputName={'tracks': "MesonTracks_%SYS%"})

    # Add all algorithms to the job:
    athAlgSeq += seq


def getTrackBranches(Conf):
    branches = [
        '%SYS%.d0sig',
        '%SYS%.d0sigPV',
        '%SYS%.eta',
        '%SYS%.passTight',
        '%SYS%.phi',
        '%SYS%.pt',
        '%SYS%.trackId',
        '%SYS%.z0sinTheta',
    ]
    if not Conf.fewerVars:
        branches += [
            '%SYS%.d0',
            '%SYS%.d0PV',
            '%SYS%.numberOfInnermostPixelLayerHits',
            '%SYS%.numberOfNextToInnermostPixelLayerHits',
            '%SYS%.numberOfPixelDeadSensors',
            '%SYS%.numberOfPixelHits',
            '%SYS%.numberOfPixelHoles',
            '%SYS%.numberOfPixelSharedHits',
            '%SYS%.numberOfSCTDeadSensors',
            '%SYS%.numberOfSCTHits',
            '%SYS%.numberOfSCTHoles',
            '%SYS%.numberOfSCTSharedHits',
            '%SYS%.passLoose',
            '%SYS%.trackOrigin',
            '%SYS%.z0',
        ]


    if Conf.dataType == "mc":
        branches += ['%SYS%.truthMatchProbability']

    return formatBranchName("MesonTracks", branches)
