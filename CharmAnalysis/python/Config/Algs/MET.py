#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm

from CharmAnalysis.Config import ContainerNames
from CharmAnalysis.Config import WorkingPoints
from CharmAnalysis.Config import formatBranchName

#
# MET Algorithms config
#


def setupMETAlgs(athAlgSeq, Conf, jetSequence, muonSequence, electronSequence):

    # Set the working point
    WP = WorkingPoints(Conf)

    # Create the MET preselection sequence.
    # Only include objects that pass baselineSelection
    # into the MET calculation.
    seq = AnaAlgSequence("MetPreselectionSequence")

    commonCuts = ['baselineSelection,as_char']

    # muon preselection
    alg = createAlgorithm('CP::AsgViewFromSelectionAlg',
                          'MuonMetPreselectionViewFromSelectionAlg')
    alg.selection = commonCuts[:]
    seq.append(alg, inputPropName={'muons': 'input'},
               outputPropName={'muons': 'output'})

    # electron preselection
    alg = createAlgorithm('CP::AsgViewFromSelectionAlg',
                          'ElectronMetPreselectionViewFromSelectionAlg')
    alg.selection = commonCuts[:]
    seq.append(alg, inputPropName={'electrons': 'input'},
               outputPropName={'electrons': 'output'})

    # configure MET preselection sequence
    seq.configure(inputName={'muons': ContainerNames.muons['output'] + 'Base_%SYS%',
                             'electrons': ContainerNames.electrons['output'] + 'Base_%SYS%',
                             'eventInfo': ContainerNames.event['output'] + '_%SYS%'},
                  outputName={'muons': 'AnalysisMuonsMET_%SYS%',
                              'electrons': 'AnalysisElectronsMET_%SYS%'})

    # Add the sequence to the job:
    athAlgSeq += seq

    # Remove b-tagging calibration from the container name
    btIndex = ContainerNames.jetContainer.find('_BTagging')
    if btIndex != -1:
        jetCollection = ContainerNames.jetContainer[:btIndex-4]
    else:
        jetCollection = ContainerNames.jetContainer[:-4]

    print ("MET Suffix: %s" % jetCollection)

    # Include, and then set up the met analysis algorithm sequence:
    from MetAnalysisAlgorithms.MetAnalysisSequence import makeMetAnalysisSequence
    seq = makeMetAnalysisSequence(Conf.dataType, metSuffix=jetCollection,
                                  useFJVT=False,
                                  treatPUJets=False)

    # Configure the main MET sequence
    seq.configure(inputName={'jets': ContainerNames.jets['output'] + 'Base_%SYS%',
                             'muons': 'AnalysisMuonsMET_%SYS%',
                             'electrons': 'AnalysisElectronsMET_%SYS%'},
                  outputName=ContainerNames.MissingEnergy['output'] + "_%SYS%")

    # Add the sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def setupAntiTightMETAlgs(athAlgSeq, Conf, jetSequence):

    # Create the MET preselection sequence.
    # Only include objects that pass baselineSelection
    # into the MET calculation.
    seq = AnaAlgSequence("AntiTightMetPreselectionSequence")

    # Add the sequence to the job:
    athAlgSeq += seq

    # Remove b-tagging calibration from the container name
    btIndex = ContainerNames.jetContainer.find('_BTagging')
    if btIndex != -1:
        jetCollection = ContainerNames.jetContainer[:btIndex-4]
    else:
        jetCollection = ContainerNames.jetContainer[:-4]

    print ("MET Suffix: %s" % jetCollection)

    # Include, and then set up the met analysis algorithm sequence:
    from MetAnalysisAlgorithms.MetAnalysisSequence import makeMetAnalysisSequence
    seq = makeMetAnalysisSequence(Conf.dataType, metSuffix=jetCollection,
                                  postfix="AntiTight",
                                  useFJVT=False,
                                  treatPUJets=False)

    # seq.MetMakerAlgAntiTight.electronsKey = ""
    # seq.MetMakerAlgAntiTight.muonsKey = ""
    seq.MetMakerAlgAntiTight.makerTool.DoMuonEloss = False
    seq.MetMakerAlgAntiTight.makerTool.DoSetMuonJetEMScale = False

    # Configure the main MET sequence
    seq.configure(inputName={'jets': ContainerNames.jets['output'] + 'Base_%SYS%'},
                  outputName=ContainerNames.MissingEnergy['output'] + "AntiTight_%SYS%")

    # Add the sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def getMETBranches():
    branches = [
        'NOSYS.name',
        '%SYS%.mpx',
        '%SYS%.mpy',
        '%SYS%.sumet',
        '%SYS%.significance',
    ]

    return formatBranchName(ContainerNames.MissingEnergy['output'], branches)
