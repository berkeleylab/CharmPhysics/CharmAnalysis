#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.DualUseConfig import createAlgorithm

from CharmAnalysis.Config import ContainerNames
from CharmAnalysis.Config import sampleTypeRunNumber

#
# Metadata config
#

def setupMetadataAlgs(athAlgSeq, Conf):
    if Conf.dataType == 'data':
        return

    # Include, and then set up the generator analysis sequence:
    from AsgAnalysisAlgorithms.GeneratorAnalysisSequence import \
        makeGeneratorAnalysisSequence
    seq = makeGeneratorAnalysisSequence(Conf.dataType, saveCutBookkeepers=True,
                                        runNumber=sampleTypeRunNumber(Conf.sampleType),
                                        cutBookkeepersSystematics=Conf.systematics,
                                        generator=Conf.MCGen,
                                        prodFractionWeight=Conf.prodFractionWeight,
                                        athenaMP=Conf.athenaMP)

    # Add sequence
    athAlgSeq += seq
