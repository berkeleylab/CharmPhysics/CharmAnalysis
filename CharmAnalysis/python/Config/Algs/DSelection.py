#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.DualUseConfig import createAlgorithm
from AthenaCommon.Constants import DEBUG, INFO, VERBOSE
from AthenaCommon.SystemOfUnits import GeV, MeV, mm
from CharmAnalysis.Config import ContainerNames, formatBranchNameNoSYS, formatBranchName

#
# DSelection Algorithms config
#


def setupDSelectionAlgs(athAlgSeq, ToolSvc, Conf):
    # Track container name
    tracks = ContainerNames.tracks['output'] + '_%SYS%'
    if Conf.tracksFromTruth:
        tracks = ContainerNames.TruthTrackParticles

    # DSelection analysis algorithm sequence
    from CharmAnalysis.DSelectionSequence import makeDSelectionAnalysisSequence
    seq = makeDSelectionAnalysisSequence(dataType=Conf.dataType)
    seq.configure(inputName={'tracks' : tracks},
                  outputName={'composites': ContainerNames.DSelection['output'] + '_%SYS%',
                              'composites_k3pi': ContainerNames.DSelection_k3pi['output'] + '_%SYS%',
                              'composites_vee': ContainerNames.DSelection_vee['output'] + '_%SYS%'})

    # PV container name
    PVContainer = ContainerNames.PrimaryVertices if not Conf.fakePrimaryVertex else ContainerNames.FakePrimaryVertex
    seq.DSelectionAlg.primaryVertexKey = PVContainer

    if Conf.doV0Finding:
        # V0 Finder tools
        from CharmAnalysis.Config import V0FinderTools
        V0FinderTool = V0FinderTools.setupV0FinderTools(
            ToolSvc, TrackParticleCollection=ContainerNames.tracksV0['output'])

        # V0 Finder Algorithm
        from InDetV0Finder.InDetV0FinderConf import InDet__InDetV0Finder
        InDetV0Finder = InDet__InDetV0Finder(name='InDetV0Finder',
                                             decorateV0=True,
                                             InDetV0FinderToolName=ToolSvc.InDetV0FinderTool,
                                             V0ContainerName='V0Candidates',
                                             KshortContainerName='V0KshortVertices',
                                             LambdaContainerName='V0LambdaVertices',
                                             LambdabarContainerName='V0LambdabarVertices',
                                             VxPrimaryCandidateName=PVContainer)
        athAlgSeq += InDetV0Finder

    # Do Approximate Fit first
    seq.DSelectionAlg.doApproximateFit = Conf.doApproximateFit

    # Print level
    seq.DSelectionAlg.OutputLevel = INFO

    # Enable filtering
    seq.DSelectionAlg.enableFilter = False

    # Channels
    seq.DSelectionAlg.doDplus = Conf.dplus
    seq.DSelectionAlg.doDsubs = Conf.dsubs
    seq.DSelectionAlg.doD0Kpi = Conf.d0kpi
    seq.DSelectionAlg.doD0K2pi = Conf.d0K2pi
    seq.DSelectionAlg.doD0K3pi = Conf.d0K3pi
    seq.DSelectionAlg.doD0K3pi_only = Conf.d0K3pi_only
    seq.DSelectionAlg.doLambdaC = Conf.lambdaC
    if Conf.doV0Finding:
        seq.DSelectionAlg.doLambdaCLambdaPi = True
        seq.DSelectionAlg.doLambdaCKshortPi = True
        seq.DSelectionAlg.doKshortK = True
        seq.DSelectionAlg.doKshortPi = True

    # Truth Matcing
    seq.DSelectionAlg.doTruthMatching = True if not Conf.tracksFromTruth else False

    # Loose selection
    # Dplus
    seq.DSelectionAlg.MinMassDP = 1700 * MeV
    seq.DSelectionAlg.MaxMassDP = 2200 * MeV
    seq.DSelectionAlg.MaxfitChi2D0 = 20
    seq.DSelectionAlg.MinTrackPtDP = 500 * MeV
    seq.DSelectionAlg.MaxDPdRKPi = 0.8
    seq.DSelectionAlg.MinLxyDP = 1.0 * mm
    seq.DSelectionAlg.Maxd0D0_Kpi = 100 * mm
    seq.DSelectionAlg.MincosthetastarDP = -1
    seq.DSelectionAlg.MindMDP = 0 * MeV
    seq.DSelectionAlg.PhiwindowDP = 12 * MeV
    # Ds -> phi pi
    seq.DSelectionAlg.MinLxyDs = 0 * mm
    seq.DSelectionAlg.MincosthetastarDs = -1
    seq.DSelectionAlg.MaxcosthetastarDs = 1
    seq.DSelectionAlg.PhiwindowDs = 12 * MeV
    # D* -> D0pi
    seq.DSelectionAlg.MaxfitChi2D0 = 20
    seq.DSelectionAlg.MinTrackPtD0 = 500 * MeV
    seq.DSelectionAlg.MaxDRKPi = 1.0
    seq.DSelectionAlg.MinLxyD0 = 0 * mm
    seq.DSelectionAlg.Maxd0D0_Kpi = 100 * mm
    seq.DSelectionAlg.Maxd0D0_Kpipi0 = 100 * mm
    seq.DSelectionAlg.Maxd0PiSlow = 100 * mm
    seq.DSelectionAlg.MaxdRD0PiSlow = 0.8
    seq.DSelectionAlg.MaxMassD0_Kpi = 2000 * MeV
    seq.DSelectionAlg.MinMassD0_Kpi = 1800 * MeV
    seq.DSelectionAlg.MaxMassD0_Kpipi0 = 1800 * MeV
    seq.DSelectionAlg.MinMassD0_Kpipi0 = 1400 * MeV
    # Vee modes
    seq.DSelectionAlg.Maxd0LambdaCtoLambdaPi = 1.0 * mm
    seq.DSelectionAlg.Maxd0LambaCtoKsP = 1.0 * mm
    seq.DSelectionAlg.Maxd0DToKsK = 1.0 * mm
    seq.DSelectionAlg.Maxd0DToKsPi = 1.0 * mm
    seq.DSelectionAlg.MaxfitChi2LambdaC = 50.0
    seq.DSelectionAlg.MaxMassLambdaC = 2.5 * GeV
    seq.DSelectionAlg.MaxMassLambdaCin = 2.5 * GeV
    seq.DSelectionAlg.MincosthetastarLambdaC = -1.0
    seq.DSelectionAlg.MaxcosthetastarLambdaC = 1.0
    seq.DSelectionAlg.MinMassLambdaC = 2.00 * GeV
    seq.DSelectionAlg.MinMassLambdaCin = 1.95 * GeV
    seq.DSelectionAlg.MinTrackPtLambdaC = 600 * MeV
    seq.DSelectionAlg.MinPtLambaC = 5 * GeV
    seq.DSelectionAlg.MinLxyLambaCToLambdaPi = 0.0
    seq.DSelectionAlg.MinLxyLambdaCToKsP = -10.0
    seq.DSelectionAlg.MinLxyDToKsK = 0.0
    seq.DSelectionAlg.MinLxyDToKsPi = 0.0
    seq.DSelectionAlg.constrainV0 = True

    # TrkVKalVrtFitter
    from TrkVKalVrtFitter.TrkVKalVrtFitterConf import Trk__TrkVKalVrtFitter
    DSelectionTrkVKalVrtFitter = Trk__TrkVKalVrtFitter(name="DSelectionTrkVKalVrtFitter",
                                                       Extrapolator="Trk::Extrapolator/InDetExtrapolator",
                                                       MakeExtendedVertex=True,
                                                       FirstMeasuredPoint=False)
    ToolSvc += DSelectionTrkVKalVrtFitter
    DSelectionTrkVKalVrtFitter.OutputLevel = INFO
    seq.DSelectionAlg.VKVrtFitter = DSelectionTrkVKalVrtFitter
    # constraint type: https://gitlab.cern.ch/atlas/athena/blob/21.2/Tracking/TrkVertexFitter/TrkVKalVrtFitter/src/SetFitOptions.cxx
    # -1) do not set it to any value (function call is omitted in c++)
    #  0) default (probably same as 1?)
    #  1) only mass constraint (no vertex constraint)
    #  2) exact pointing of summary track to vertex v0
    #  4) 2 + 1
    #  6) reconstructed vertex is close to v0 (Chi2 term)
    #  7) (summary_track-vertex_v0) Chi2 term. Both summary track and vertex covariances are used
    #  8) 7 + 1
    #  9) (summary_track-vertex_v0) Chi2 term. Only vertex covariance is used
    # 10) 9 + 1
    seq.DSelectionAlg.constraintType = -1

    # Add all algorithms to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def getDSelectionBranches(Conf):
    branches = [
        '%SYS%.costhetastar',
        '%SYS%.D0Index',
        '%SYS%.daughterInfo__eta',
        '%SYS%.daughterInfo__passTight',
        '%SYS%.daughterInfo__pdgId',
        '%SYS%.daughterInfo__phi',
        '%SYS%.daughterInfo__pt',
        '%SYS%.daughterInfo__trackId',
        '%SYS%.daughterInfo__truthBarcode',
        '%SYS%.daughterInfo__truthDBarcode',
        '%SYS%.daughterInfo__z0SinTheta',
        '%SYS%.daughterInfo__z0SinThetaPV',
        '%SYS%.decayType',
        '%SYS%.DeltaMass',
        '%SYS%.eta',
        '%SYS%.fitOutput__Charge',
        '%SYS%.fitOutput__Chi2',
        '%SYS%.fitOutput__Impact',
        '%SYS%.fitOutput__ImpactSignificance',
        '%SYS%.fitOutput__ImpactZ0SinTheta',
        '%SYS%.fitOutput__Lxy',
        '%SYS%.fitOutput__VertexPosition',
        '%SYS%.m',
        '%SYS%.mKpi1',
        '%SYS%.mKpi2',
        '%SYS%.mPhi1',
        '%SYS%.mPhi2',
        '%SYS%.pdgId',
        '%SYS%.phi',
        '%SYS%.pt',
        '%SYS%.ptcone40',
        '%SYS%.SlowPionD0',
        '%SYS%.SlowPionZ0SinTheta',
        '%SYS%.truthBarcode',
    ]
    if not Conf.fewerVars:
        branches += [
            '%SYS%.charge',
            '%SYS%.daughterInfo__chi2',
            '%SYS%.daughterInfo__d0',
            '%SYS%.daughterInfo__d0PV',
            '%SYS%.daughterInfo__passLoose',
            '%SYS%.daughterInfo__truthMatchProbability',
            '%SYS%.daughterInfo__truthParentPdgId',
            '%SYS%.daughterInfo__truthPdgId',
            '%SYS%.fitOutput__Chi2PerTrack',
            '%SYS%.fitOutput__ErrorMatrix',
            '%SYS%.fitOutput__ImpactError',
            '%SYS%.fitOutput__ImpactTheta',
            '%SYS%.fitOutput__ImpactZ0',
            '%SYS%.fitOutput__ImpactZ0Error',
            '%SYS%.fitOutput__LxyErr',
            '%SYS%.fitOutput__Perigee',
            '%SYS%.fitOutput__PerigeeCovariance',
            '%SYS%.ptcone20',
            '%SYS%.ptcone30',
        ]

    output = formatBranchName(ContainerNames.DSelection['output'], branches)
    if Conf.d0K3pi:
        output += formatBranchName(ContainerNames.DSelection_k3pi['output'], branches)
    if Conf.doV0Finding:
        branches += ['V0Index']
        output += formatBranchName(ContainerNames.DSelection_vee['output'], branches)
    return output


def getVeeBranches():

    generic_container_names = [
        "V0Candidates",
    ]
    generic_branches = [
        'Kshort_mass',
        'Kshort_massError',
        'Lambda_mass',
        'Lambda_massError',
        'Lambdabar_mass',
        'Lambdabar_massError',
        'pT',
        'pTError',
        'Rxy',
        'RxyError',
        'px',
        'py',
        'pz',
    ]

    container_names = [
        "V0LambdaVertices",
        "V0LambdabarVertices",
        "V0KshortVertices",
    ]
    branches = [
        'mass',
        'pT',
        'pTError',
        'Rxy',
        'RxyError',
        'px',
        'py',
        'pz',
    ]

    output = []

    for container in generic_container_names:
        output += formatBranchNameNoSYS(container, generic_branches)

    for container in container_names:
        output += formatBranchNameNoSYS(container, branches)

    return output
