#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool

from CharmAnalysis.Config import ContainerNames
from CharmAnalysis.Config import WorkingPoints
from CharmAnalysis.Config import formatBranchName

#
# Overlap Removal Algorithms config
#


def setupORAlgs(athAlgSeq, Conf, jetSequence, muonSequence, electronSequence):

    # Set the working point
    WP = WorkingPoints(Conf)

    # Preselection for OR
    seq = AnaAlgSequence("ORPreselectionSequence")

    commonCuts = ['baselineSelection,as_char']

    alg = createAlgorithm('CP::AsgSelectionAlg', 'JetORSelectionSummary')
    addPrivateTool(alg, 'selectionTool', 'CP::AsgFlagSelectionTool')
    alg.selectionTool.selectionFlags = commonCuts[:]
    alg.selectionDecoration = 'baselineOR,as_char'
    seq.append(alg, inputPropName={'jets': 'particles'})

    alg = createAlgorithm('CP::AsgSelectionAlg', 'MuonORSelectionSummary')
    addPrivateTool(alg, 'selectionTool', 'CP::AsgFlagSelectionTool')
    alg.selectionTool.selectionFlags = commonCuts[:]
    alg.selectionDecoration = 'baselineOR,as_char'
    seq.append(alg, inputPropName={'muons': 'particles'})

    alg = createAlgorithm('CP::AsgSelectionAlg', 'ElectronORSelectionSummary')
    addPrivateTool(alg, 'selectionTool', 'CP::AsgFlagSelectionTool')
    alg.selectionTool.selectionFlags = commonCuts[:]
    alg.selectionDecoration = 'baselineOR,as_char'
    seq.append(alg, inputPropName={'electrons': 'particles'})

    seq.configure(inputName={'jets': ContainerNames.jets['output'] + 'Base_%SYS%',
                             'muons': ContainerNames.muons['output'] + 'Base_%SYS%',
                             'electrons': ContainerNames.electrons['output'] + 'Base_%SYS%'},
                  outputName={})
    # Add the sequence to the job
    athAlgSeq += seq

    # Include, and then set up the overlap analysis algorithm sequence:
    from AsgAnalysisAlgorithms.OverlapAnalysisSequence import \
        makeOverlapAnalysisSequence

    seq = makeOverlapAnalysisSequence(Conf.dataType, inputLabel='baselineOR',
                                      shallowViewOutput=False,
                                      doPhotons=False,
                                      doTaus=False,
                                      doMuPFJetOR=True)

    seq.configure(
        inputName={'jets': ContainerNames.jets['output'] + 'Base_%SYS%',
                   'muons': ContainerNames.muons['output'] + 'Base_%SYS%',
                   'electrons': ContainerNames.electrons['output'] + 'Base_%SYS%'},
        outputName={})

    # Add the sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq
