#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
from AthenaCommon.SystemOfUnits import GeV

from CharmAnalysis.Config import ContainerNames, TriggerList
from CharmAnalysis.Config import WorkingPoints
from CharmAnalysis.Config import formatBranchName, generateTriggerChain


#
# Post Processing Algorithms config
#


def setupPostProcessingAlgs(athAlgSeq, ToolSvc, Conf,
                            jetSequence, muonSequence, electronSequence,
                            **kwargs):

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('PostProcessingAnalysisSequence')

    # helperDecoration = 'finalSelection,as_char'
    commonPreselection = ['baselineSelection,as_char', 'passesOR_%SYS%,as_char']
    unionSelectionDecoration = 'finalSelection,as_char'

    # Final jets selection
    alg = createAlgorithm( 'CP::AsgSelectionAlg',
                           'JetFinalSelectionSummary')
    alg.preselection = '&&'.join(commonPreselection + ['jvt_selection'])
    alg.selectionDecoration = 'jet_selected_%SYS%,as_char'
    seq.append( alg, inputPropName = {'jets': 'particles'} )

    alg = createAlgorithm( 'CP::AsgUnionSelectionAlg',
                           'JetUnionSelectionAlg' )
    alg.preselection = 'jet_selected_%SYS%,as_char'
    alg.selectionDecoration = unionSelectionDecoration
    seq.append( alg, inputPropName = {'jets': 'particles'} )

    alg = createAlgorithm( 'CP::AsgViewFromSelectionAlg',
                           'JetFinalViewFromSelectionAlg' )
    alg.selection = [unionSelectionDecoration]
    seq.append( alg, inputPropName = {'jets': 'input'}, outputPropName = {'jets': 'output'} )

    # Final muons selection
    alg = createAlgorithm( 'CP::AsgSelectionAlg',
                           'MuonFinalSelectionSummary')
    alg.preselection = '&&'.join(commonPreselection)
    alg.selectionDecoration = 'mu_selected_%SYS%,as_char'
    seq.append( alg, inputPropName = {'muons': 'particles'} )

    alg = createAlgorithm( 'CP::AsgUnionSelectionAlg',
                           'MuonUnionSelectionAlg' )
    alg.preselection = 'mu_selected_%SYS%,as_char'
    alg.selectionDecoration = unionSelectionDecoration
    seq.append( alg, inputPropName = {'muons': 'particles'} )

    alg = createAlgorithm( 'CP::AsgViewFromSelectionAlg',
                           'MuonFinalViewFromSelectionAlg' )
    alg.selection = [unionSelectionDecoration]
    seq.append( alg, inputPropName = {'muons': 'input'}, outputPropName = {'muons': 'output'} )

    # Final electrons selection
    alg = createAlgorithm( 'CP::AsgSelectionAlg',
                           'ElectronFinalSelectionSummary')
    alg.preselection = '&&'.join(commonPreselection)
    alg.selectionDecoration = 'el_selected_%SYS%,as_char'
    seq.append( alg, inputPropName = {'electrons': 'particles'} )

    alg = createAlgorithm( 'CP::AsgUnionSelectionAlg',
                           'ElectronUnionSelectionAlg' )
    alg.preselection = 'el_selected_%SYS%,as_char'
    alg.selectionDecoration = unionSelectionDecoration
    seq.append( alg, inputPropName = {'electrons': 'particles'} )

    alg = createAlgorithm( 'CP::AsgViewFromSelectionAlg',
                           'ElectronFinalViewFromSelectionAlg' )
    alg.selection = [unionSelectionDecoration]
    seq.append( alg, inputPropName = {'electrons': 'input'}, outputPropName = {'electrons': 'output'} )

    # Trigger matching
    if Conf.triggerChain == 'SingleLepton':
        alg = createAlgorithm('TriggerMatchingAlg', 'TriggerMatchingAlg')
        addPrivateTool(alg, 'tool', 'Trig::MatchingTool')
        alg.tool.TrigDecisionTool = 'TrigDecisionTool'
        # next line is needed because of the explanation here:
        # https://roups.cern.ch/group/hn-atlas-TriggerHelp/Lists/Archive/Flat.aspx?RootFolder=%2fgroup%2fhn%2datlas%2dTriggerHelp%2fLists%2fArchive%2fTrigger%20navigation%20error%20in%20AthDerivation&FolderCTID=0x012002001FA16C589111D24CA634A8138BBC0A5C
        from TrigEDMConfig.TriggerEDM import EDMLibraries
        ToolSvc.TrigDecisionTool.Navigation.Dlls = EDMLibraries
        alg.matchingDecoration = 'matched'
        alg.matchingPairDecoration = 'matchedPairs'
        alg.triggersSingleElectron = TriggerList.el_single
        alg.triggersDiElectron = list()
        alg.triggersSingleMuon = TriggerList.mu_single
        alg.triggersDiMuon = list()
        alg.triggersElectronMuon = list()

        seq.append(alg, inputPropName={'electrons': 'electrons',
                                       'muons': 'muons'})

    # Preselection
    alg = createAlgorithm('EventPreselectionAlg', 'EventPreselectionAlg')
    alg.jetSelectionDecoration = 'jet_selected_%SYS%'
    alg.muonSelectionDecoration = 'mu_selected_%SYS%'
    alg.electronSelectionDecoration = 'el_selected_%SYS%'

    # Lepton multiplicity selection
    alg.minLeptonsLight = Conf.minLeptonsLight
    alg.maxLeptonsLight = Conf.maxLeptonsLight
    alg.minElectrons = Conf.minElectrons
    alg.maxElectrons = Conf.maxElectrons
    alg.minMuons = Conf.minMuons
    alg.maxMuons = Conf.maxMuons

    seq.append(alg, inputPropName={'electrons': 'electrons',
                                   'muons': 'muons',
                                   'jets': 'jets'})

    # configure the sequence
    sysPostfix = 'Base_%SYS%'

    seq.configure(inputName={'jets': ContainerNames.jets['output'] + sysPostfix,
                             'muons': ContainerNames.muons['output'] + sysPostfix,
                             'electrons': ContainerNames.electrons['output'] + sysPostfix},
                  outputName={'electrons': 'AnalysisElectrons_%SYS%',
                              'muons': 'AnalysisMuons_%SYS%',
                              'jets': 'AnalysisJets_%SYS%'})

    # Add the sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def setupCharmEventInfoSeq(athAlgSeq, Conf):

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('setupCharmEventInfoSequence')

    # PV container name
    PVContainer = ContainerNames.PrimaryVertices if not Conf.fakePrimaryVertex else ContainerNames.FakePrimaryVertex

    # Event Info Decorator alg
    alg = createAlgorithm('EventInfoCreatorAlg', 'EventInfoCreatorAlg')
    alg.EventWeightDecoration = 'EventWeight'
    alg.primaryVertexKey = PVContainer
    alg.enableFilter = True
    alg.doTruth = Conf.doTruth
    alg.pileupRW = Conf.doPileupRW
    alg.leptonInfo = Conf.leptonInfo
    if not Conf.leptonInfo:
        alg.Nlep = 0
    alg.Nlep = 0
    alg.SampleID = int(Conf.sampleID)

    # Trigger
    alg.passSingleElectronTriggerChainDecoration = 'passedElectronChain'
    alg.passSingleMuonTriggerChainDecoration = 'passedMuonChain'

    # Tight electrons
    alg.TightElPtCut = 30 * GeV
    alg.TightElId = 'Tight'
    alg.TightElIso = 'FCTight'

    # Tight muons
    alg.TightMuPtCut = 30 * GeV
    alg.TightMuQuality = 'Tight'
    alg.TightMuIso = 'FCTight'

    seq.append(alg, inputPropName={'muons': 'muons',
                                   'electrons': 'electrons',
                                   'truthLep': 'truthLep'},
               outputPropName={'eventInfo': 'eventInfo'})

    sysPostfix = '_%SYS%'
    seq.configure(inputName={'muons': ContainerNames.muons['output'] + sysPostfix,
                             'electrons': ContainerNames.electrons['output'] + sysPostfix,
                             'truthLep': ContainerNames.truthLep},
                  outputName={'eventInfo': 'CharmEventInfo'})

    # Add the sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def setupMETReaderSeq(athAlgSeq, Conf,
                      jetSequence, muonSequence, electronSequence):

    # Set the working point
    WP = WorkingPoints(Conf)

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('METReaderSequence')

    # Event Info Decorator alg
    alg = createAlgorithm('METDecoratorAlg', 'METDecoratorAlg')

    # MET
    alg.METComponent = WP.met_component
    alg.METDecoration = 'MET'
    alg.METPhiDecoration = 'METPhi'

    seq.append(alg, inputPropName={'met': 'met'},
               outputPropName={'eventInfo': 'eventInfo'})

    seq.configure(inputName={'met': ContainerNames.MissingEnergy['output'] + "_%SYS%"},
                  outputName={'eventInfo': 'METInfo_%SYS%'})

    # Add the sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def setupAntiTightMETReaderSeq(athAlgSeq, Conf, jetSequence):

    # Set the working point
    WP = WorkingPoints(Conf)

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence('AntiTightMETReaderSequence')

    # Event Info Decorator alg
    alg = createAlgorithm('METDecoratorAlg', 'AntiTightMETDecoratorAlg')

    # MET
    alg.METComponent = WP.met_component
    alg.METDecoration = 'MET'
    alg.METPhiDecoration = 'METPhi'

    seq.append(alg, inputPropName={'met': 'met'},
               outputPropName={'eventInfo': 'eventInfo'})

    seq.configure(inputName={'met': ContainerNames.MissingEnergy['output'] + "AntiTight_%SYS%"},
                  outputName={'eventInfo': 'METInfoAntiTight_%SYS%'})

    # Add the sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def getMETInfoBranches(Conf):

    # Custom Charm branches
    METInfoBranches = [
        '%SYS%.MET',
        '%SYS%.METPhi',
    ]

    # output = formatBranchName("METInfo", METInfoBranches) + formatBranchName("METInfoAntiTight", METInfoBranches)
    output = formatBranchName("METInfo", METInfoBranches)

    return output
