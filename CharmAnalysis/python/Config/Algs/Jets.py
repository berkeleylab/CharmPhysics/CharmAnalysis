#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool

from CharmAnalysis.Config import ContainerNames
from CharmAnalysis.Config import WorkingPoints
from CharmAnalysis.Config import formatBranchName, formatBranchNameNoSYS

#
# Jets Algorithms config
#


def setupJetAlgs(athAlgSeq, Conf):

    # Set the working point
    WP = WorkingPoints(Conf)

    # Include, and then set up the jet analysis algorithm sequence:
    from JetAnalysisAlgorithms.JetAnalysisSequence import makeJetAnalysisSequence
    seq = makeJetAnalysisSequence(Conf.dataType, ContainerNames.jetContainer,
                                  runFJvtUpdate=False,
                                  runFJvtSelection=False,
                                  runFJvtEfficiency=False,
                                  runJvtUpdate=Conf.doJVT,
                                  runJvtSelection=Conf.doJVT,
                                  runJvtEfficiency=Conf.doJVT)

    # Jet kinematic selection
    makeJetsKinematicCutsAlgs(seq, Conf)

    # Hack to pickup a local root file
    # seq.JetUncertaintiesTool.uncertaintiesTool.ConfigFile = 'CharmAnalysis/R4_GlobalReduction_SimpleJER.config'

    # Add the sequence to the job:
    athAlgSeq += seq

    # Include, and then set up the jet flavour tagging analysis algorithm sequence:
    from FTagAnalysisAlgorithms.FTagAnalysisSequence import makeFTagAnalysisSequence

    # b-jets
    for tagger in WP.jet_flavour_taggers:
        for wp in WP.jet_flavour_wp:
            for eff in WP.jet_flavour_eff:
                makeFTagAnalysisSequence(seq, Conf.dataType, ContainerNames.jetContainer,
                                         btagWP=wp + '_' + eff,
                                         legacyRecommendations=Conf.oldDerivationCache,
                                         btagger=tagger,
                                         generator=Conf.MCGen)
                # Modify the BTagging efficiency tool for b-jet stuff
                # https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BTagging_Efficiency_Tool
                suffix = tagger + wp + '_' + eff
                if hasattr(seq, 'FTagEfficiencyScaleFactorAlg' + suffix):
                    getattr(seq, 'FTagEfficiencyScaleFactorAlg' +
                            suffix).efficiencyTool.SystematicsStrategy = "SFEigen"

                # Add FTAG discriminant
                if Conf.saveFtagDiscriminant:
                    makeFTagDecorationAlg(seq, tagger,
                                          getattr(
                                              seq, 'FTagSelectionAlg' + suffix).selectionTool.FlvTagCutDefinitionsFileName,
                                          getattr(seq, 'FTagSelectionAlg' + suffix).selectionTool.MinPt, False)

    # Jet input output
    seq.configure(inputName=ContainerNames.jets['input'],
                  outputName=ContainerNames.jets['output'] + 'Base_%SYS%')

    # Return to let the JO know this is active
    return seq


def makeJetsKinematicCutsAlgs(seq, Conf):

    # Set the working point
    WP = WorkingPoints(Conf)

    # Set up the jet pt selection algorithm:
    alg = createAlgorithm('CP::AsgSelectionAlg', 'JetPtCutAlg')
    addPrivateTool(alg, 'selectionTool', 'CP::AsgPtEtaSelectionTool')
    alg.selectionTool.minPt = WP.jet_pt_min
    alg.selectionTool.maxEta = WP.jet_eta_max
    alg.selectionDecoration = 'baselineSelection,as_char'
    seq.append(alg, inputPropName='particles',
               stageName='selection')


def makeFTagEventScaleFactorAlg(seq, btagger, btagWP, jetSys, otherSys, extraDecor, extraDecorSys):
    alg = createAlgorithm('CP::AsgEventScaleFactorAlg',
                          'FTagEventScaleFactorAlg' + btagger + btagWP)
    alg.particleSystematicsRegex = jetSys
    alg.scaleFactorInputDecoration = 'ftag_effSF_' + btagger + '_' + btagWP + '_%SYS%'
    alg.scaleFactorInputDecorationRegex = '(^FT_EFF_.*)'
    alg.scaleFactorOutputDecoration = 'ftag_effSF_' + \
        btagger + '_' + btagWP + '_%SYS%'
    alg.preselection = '&&'.join(
        ['baselineSelection,as_char', 'jvt_selection', 'no_ftag_' + btagger + '_' + btagWP])
    if extraDecor:
        alg.inputSelectionDecoration = extraDecor
        alg.inputSelectionDecorationRegex = extraDecorSys
    seq.append(alg,
               affectingSystematics=jetSys + '|' +
               otherSys + '|' + '(^FT_EFF_.*)',
               inputPropName={'jets': 'particles',
                              'eventInfo': 'eventInfo'})


def makeFTagDecorationAlg(seq, btagger, FlvTagCutDefinitionsFileName, MinPt, extraVars=False):
    alg = createAlgorithm('JetDecoratorAlg', 'JetDecoratorAlg' + btagger)
    alg.preselection = '&&'.join(['baselineSelection,as_char'])
    alg.extraVars = extraVars
    alg.quantileDecoration = 'ftag_quantile_' + btagger
    alg.DL1puDecoration = 'ftag_DL1pu_' + btagger
    alg.DL1pbDecoration = 'ftag_DL1pb_' + btagger
    alg.DL1pcDecoration = 'ftag_DL1pc_' + btagger
    alg.JetFitter_nVTX = 'JetFitter_nVTX'
    alg.JetFitter_mass = 'JetFitter_mass'
    alg.JetFitter_nTracksAtVtx = 'JetFitter_nTracksAtVtx'
    alg.SV1_NGTinSvx = 'SV1_NGTinSvx'
    alg.SV1_mass = 'SV1_mass'
    alg.TaggerName = btagger
    addPrivateTool(alg, 'btagSelTool', 'BTaggingSelectionTool')

    alg.btagSelTool.TaggerName = btagger
    alg.btagSelTool.OperatingPoint = "Continuous"
    alg.btagSelTool.JetAuthor = ContainerNames.jetContainer
    alg.btagSelTool.FlvTagCutDefinitionsFileName = FlvTagCutDefinitionsFileName
    alg.btagSelTool.MinPt = MinPt
    seq.append(alg, inputPropName='jets')


def makeJetPostprocessingAnalysisSequence(athAlgSeq, Conf,
                                          jetSequence, electronSequence, muonSequence):

    # Set the working point
    WP = WorkingPoints(Conf)

    ORSelection = 'passesOR_%SYS%'

    # Include, and then set up the jet JVT analysis algorithm sequence:
    from JetAnalysisAlgorithms.JetJvtAnalysisSequence import makeJetJvtAnalysisSequence
    seq = makeJetJvtAnalysisSequence(Conf.dataType, ContainerNames.jetContainer,
                                     preselection='baselineSelection,as_char',
                                     runSelection=False,
                                     disableFJvt=True)

    # FTag
    # if Conf.dataType != 'data':
    #     # b-jets
    #     for tagger in WP.jet_flavour_taggers:
    #         for wp in WP.jet_flavour_wp:
    #             for eff in WP.jet_flavour_eff:
    #                 makeFTagEventScaleFactorAlg(seq,
    #                                             btagWP = wp + '_' + eff,
    #                                             btagger = tagger,
    #                                             jetSys = jetSys,
    #                                             otherSys = otherSys,
    #                                             extraDecor = ORSelection,
    #                                             extraDecorSys = allSys)

    # configure the sequence
    sysPostfix = 'Base_%SYS%'

    seq.configure(inputName={'jets': ContainerNames.jets['output'] + sysPostfix},
                  outputName={})

    # Add all algorithms to the job:
    athAlgSeq += seq

    return seq


def getJetBranches(Conf):

    # Set the working point
    WP = WorkingPoints(Conf)

    branches = [
        '%SYS%.pt',
        '%SYS%.jet_selected_%SYS%',
    ]
    branches += [
        'NOSYS.m',
        'NOSYS.eta',
        'NOSYS.phi',
    ]

    # b-jets
    for tagger in WP.jet_flavour_taggers:
        for wp in WP.jet_flavour_wp:
            for eff in WP.jet_flavour_eff:
                out = tagger + '_' + wp + '_' + eff
                branches.append('%s.ftag_select_%s' % ('NOSYS', out))
                if Conf.dataType != 'data':
                    branches.append('%s.ftag_effSF_%s_%s' %
                                    ('%SYS%', out, '%SYS%'))
                if Conf.saveFtagDiscriminant:
                    branches.append('%s.ftag_quantile_%s' % ('NOSYS', tagger))
                    branches.append('%s.ftag_DL1pu_%s' % ('NOSYS', tagger))
                    branches.append('%s.ftag_DL1pb_%s' % ('NOSYS', tagger))
                    branches.append('%s.ftag_DL1pc_%s' % ('NOSYS', tagger))

    # # FTAG variables
    # if Conf.saveFtagDiscriminant:
    #     branches += [
    #         'NOSYS.MV2c100',
    #         'NOSYS.MV2cl100',
    #         'NOSYS.JetFitter_nVTX',
    #         'NOSYS.JetFitter_nTracksAtVtx',
    #         'NOSYS.SV1_NGTinSvx',
    #         'NOSYS.SV1_mass',
    #         'NOSYS.JetFitter_mass',
    #     ]

    # Truth labels
    if Conf.dataType != 'data':
        branches += [
            'NOSYS.HadronConeExclExtendedTruthLabelID',
            'NOSYS.HadronConeExclTruthLabelID',
        ]

    return formatBranchName(ContainerNames.jets['output'], branches)


def getTrackJetBranches(Conf):

    branches = [
        '%SYS%.pt',
        '%SYS%.eta',
        '%SYS%.phi',
        '%SYS%.m',
        '%SYS%.daughter__pt',
        '%SYS%.daughter__eta',
        '%SYS%.daughter__phi',
        '%SYS%.daughter__trackId',
    ]
    if not Conf.fewerVars:
        branches += [
            '%SYS%.numConstituents',
            '%SYS%.daughter__z0sinTheta',
            '%SYS%.daughter__d0',
            '%SYS%.daughter__d0PV',
            '%SYS%.daughter__passLoose',
        ]

    radius = ["6", "8", "10"]
    rules = []
    for r in radius:
        rules += formatBranchName("AntiKt{}PV0TrackJets".format(r), branches)

    if Conf.doVRTrackJets:
        rules += formatBranchName("AntiKtVR30Rmax4Rmin02TrackJets", branches)

    return rules
