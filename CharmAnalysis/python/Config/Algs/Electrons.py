#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from ROOT import PATCore
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool

from CharmAnalysis.Config import ContainerNames, TriggerList
from CharmAnalysis.Config import WorkingPoints
from CharmAnalysis.Config import formatBranchName, generateTriggerChain

#
# Electron Algorithms config
#


def setupElectronAlgs(athAlgSeq, Conf):

    # Set the working point
    WP = WorkingPoints(Conf)

    # Include, and then set up the electron analysis algorithm sequence:
    from EgammaAnalysisAlgorithms.ElectronAnalysisSequence import makeElectronAnalysisSequence
    seq = makeElectronAnalysisSequence(dataType=Conf.dataType,
                                       workingPoint='%s.%s' % (
                                           WP.ele_base_id, WP.ele_base_isol),
                                       recomputeLikelihood=(
                                           not Conf.onDerivation),
                                       crackVeto=True,
                                       ptSelectionOutput=False)
    # Adapt the pt cut
    seq.ElectronPtCutAlg.selectionTool.minPt = WP.ele_pt_min
    seq.ElectronPtCutAlg.selectionTool.maxPt = WP.ele_pt_max

    # Adapt the d0 cut
    seq.ElectronTrackSelectionAlg.maxD0Significance = WP.ele_d0sig_max

    # PV container name
    PVContainer = ContainerNames.PrimaryVertices if not Conf.fakePrimaryVertex else ContainerNames.FakePrimaryVertex

    # Additional electron decoration
    alg = createAlgorithm('ElectronDecoratorAlg', 'ElectronDecoratorAlg')
    alg.primaryVertexKey = PVContainer
    alg.preselection = 'baselineSelection,as_char'
    alg.extraVars = Conf.extraVars
    seq.append(alg, inputPropName='electrons',
               stageName='calibration')

    # ID decoration and eff
    for id in WP.ele_id_list:
        makeElectronIDSelectionAlg(seq, id)
        if Conf.dataType != 'data' and Conf.doPileupRW:
            makeElectronEfficiencyAlg(seq, Conf.dataType, id, "ID")

    # isolation decoration and eff
    for isol in WP.ele_isol_list:
        makeElectronIsolationAlg(seq, isol)
        if Conf.dataType != 'data' and Conf.doPileupRW:
            for id in WP.ele_id_list:
                makeElectronEfficiencyAlg(seq, Conf.dataType, '%s.%s' % (id, isol), "Isol")

    # charge-flip eff
    if Conf.dataType != 'data' and Conf.doPileupRW:
        for isol in WP.ele_isol_list + ["noIso"]:
                for id in WP.ele_id_list:
                    makeElectronChflipEfficiencyAlg(seq, Conf.dataType, id + '+' + isol)

    # trigger eff
    if Conf.dataType != 'data' and Conf.doPileupRW:
        for trig in WP.ele_trig_list:
            for isol in WP.ele_isol_list + ["noIso"]:
                for id in WP.ele_id_list:
                    if 'SINGLE' in trig:
                        if 'Medium' in id:
                            if 'VarRad' in isol:
                                continue
                    elif 'DI' in trig:
                        if 'VarRad' in isol:
                            continue
                    makeElectronEfficiencyAlg(
                        seq, Conf.dataType, '%s.%s.%s' % (id, isol, trig), "Trig")
                    makeElectronEfficiencyAlg(
                        seq, Conf.dataType, '%s.%s.Eff_%s' % (id, isol, trig), "Trig")

    # configure input/output
    seq.configure(inputName=ContainerNames.electrons['input'],
                  outputName=ContainerNames.electrons['output'] + 'Base_%SYS%')

    # Add the sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def makeElectronIDSelectionAlg(seq, likelihoodWP):

    # inconcistenciy in electron CP algs
    wp = likelihoodWP
    if "LooseBLayer" in wp:
        wp = likelihoodWP.replace("LooseBLayer", "LooseBL")

    alg = createAlgorithm('CP::AsgSelectionAlg',
                          'ElectronLikelihoodAlg' + wp)
    alg.preselection = 'baselineSelection,as_char'
    addPrivateTool(alg, 'selectionTool', 'CP::AsgFlagSelectionTool')
    alg.selectionDecoration = 'likelihood_' + wp + ',as_char'
    alg.selectionTool.selectionFlags = ['DFCommonElectronsLH' + wp]
    seq.append(alg, inputPropName='particles',
               stageName='efficiency')


def makeElectronIsolationAlg(seq, isolation):
    alg = createAlgorithm('CP::EgammaIsolationSelectionAlg',
                          'ElectronIsolationSelectionAlg' + isolation)
    alg.preselection = 'baselineSelection,as_char'
    addPrivateTool(alg, 'selectionTool', 'CP::IsolationSelectionTool')
    alg.selectionDecoration = 'isIsolated_' + isolation + ',as_char'
    alg.selectionTool.ElectronWP = isolation
    seq.append(alg, inputPropName='egammas',
               stageName='efficiency')


def makeElectronEfficiencyAlg(seq, dataType, wp, effType):
    wps = wp.split('.')
    suffix = wp.replace('.', '_')

    alg = createAlgorithm('CP::ElectronEfficiencyCorrectionAlg',
                          'ElectronEfficiencyCorrectionAlg' + suffix)
    alg.preselection = 'baselineSelection,as_char'
    addPrivateTool(alg, 'efficiencyCorrectionTool',
                   'AsgElectronEfficiencyCorrectionTool')
    alg.scaleFactorDecoration = 'effSF_' + effType + "_" + suffix + '_%SYS%'
    alg.efficiencyCorrectionTool.CorrelationModel = 'TOTAL'
    if dataType == 'afii':
        alg.efficiencyCorrectionTool.ForceDataType = \
            PATCore.ParticleDataType.Fast
    elif dataType == 'mc':
        alg.efficiencyCorrectionTool.ForceDataType = \
            PATCore.ParticleDataType.Full
        pass
    alg.outOfValidity = 2  # silent
    alg.outOfValidityDeco = 'bad_eff'

    # # old recommendations for isolation, corresponding to:
    # # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RecommendedIsolationWPs?rev=10
    # # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/LatestRecommendationsElectronIDRun2?rev=73
    # alg.efficiencyCorrectionTool.MapFilePath = "ElectronEfficiencyCorrection/2015_2018/rel21.2/Precision_Summer2020_v1/map3.txt"

    # Set the correct correction
    alg.efficiencyCorrectionTool.IdKey = wps[0]
    if len(wps) >= 2 and wps[1] != "noIso":
        alg.efficiencyCorrectionTool.IsoKey = wps[1]
    if len(wps) == 3:
        alg.efficiencyCorrectionTool.TriggerKey = wps[2]

    seq.append(alg, inputPropName='electrons',
               stageName='efficiency')


def makeElectronChflipEfficiencyAlg(seq, dataType, wp):
    wps = wp.split('+')
    if len(wps) > 1 and wps[1] == "noIso":
        wps = [wps[0]]
    suffix = wp.replace('+', '_')

    filePath = 'ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/charge_misID'
    fileName = filePath + '/chargeEfficiencySF.'
    fileName += wps[0] + 'LLH'
    fileName += '_d0z0_v13'
    if len(wps) == 2:
        if "Loose" in wps[1]:
            fileName += '_' + "FCLoose"
        elif "Tight" in wps[1]:
            fileName += '_' + "FCTight"
        else:
            fileName += '_' + wps[1]
    fileName += '.root'

    alg = createAlgorithm('CP::ElectronEfficiencyCorrectionAlg',
                          'ElectronEfficiencyCorrectionAlgChflip' + suffix)
    alg.preselection = 'baselineSelection,as_char'
    addPrivateTool(alg, 'efficiencyCorrectionTool',
                   'CP::ElectronChargeEfficiencyCorrectionTool')
    alg.scaleFactorDecoration = 'effSF_Chflip_' + suffix + '_%SYS%'
    alg.efficiencyCorrectionTool.CorrectionFileName = fileName
    if dataType == 'afii':
        alg.efficiencyCorrectionTool.ForceDataType = \
            PATCore.ParticleDataType.Fast
    elif dataType == 'mc':
        alg.efficiencyCorrectionTool.ForceDataType = \
            PATCore.ParticleDataType.Full
        pass
    alg.outOfValidity = 2  # silent
    alg.outOfValidityDeco = 'bad_eff'

    seq.append(alg, inputPropName='electrons',
               stageName='efficiency')


def getElectronBranches(Conf, PostProcessing):

    # Set the working point
    WP = WorkingPoints(Conf)

    branches = [
        '%SYS%.pt',
        '%SYS%.el_selected_%SYS%'
    ]
    branches += [
        'NOSYS.eta',
        'NOSYS.caloCluster_eta',
        'NOSYS.phi',
        'NOSYS.charge',
        'NOSYS.d0sig',
        'NOSYS.d0',
        'NOSYS.z0sinTheta',
        'NOSYS.topoetcone20',
        'NOSYS.ptvarcone20_TightTTVA_pt1000',
        'NOSYS.ptvarcone30_TightTTVA_pt1000',
        # 'NOSYS.POverP',     # not available in derivation
        # 'NOSYS.deltaPhi2',  # not available in derivation
    ]

    # selection
    for id in WP.ele_id_list:
        # inconcistenciy in electron CP algs
        wp = id
        if "LooseBLayer" in wp:
            wp = wp.replace("LooseBLayer", "LooseBL")
        branches.append('NOSYS.likelihood_%s' % wp)
    for isol in WP.ele_isol_list:
        branches.append('%s.isIsolated_%s' % ('%SYS%', isol))

    # efficiencies
    if Conf.dataType != 'data' and Conf.doPileupRW:
        # reconstruction efficiency
        branches.append('%SYS%.effSF_%SYS%')
        # identification efficiency
        for id in WP.ele_id_list:
            branches.append('%s.effSF_ID_%s_%s' % ('%SYS%', id, '%SYS%'))
            # isolation efficiency
            for isol in WP.ele_isol_list + ["noIso"]:
                isolwp = id + '_' + isol
                branches.append('%s.effSF_Chflip_%s_%s' % ('%SYS%', isolwp, '%SYS%'))
                if isol != "noIso":
                    branches.append('%s.effSF_Isol_%s_%s' % ('%SYS%', isolwp, '%SYS%'))
                # trigger efficiency
                for trig in WP.ele_trig_list:
                    if 'SINGLE' in trig:
                        if 'Medium' in id:
                            if 'VarRad' in isol:
                                continue
                    elif 'DI' in trig:
                        if 'VarRad' in isol:
                            continue
                    trigwp = id + '_' + isol + '_' + trig
                    trigwpmc = id + '_' + isol + '_Eff_' + trig
                    branches.append('%s.effSF_Trig_%s_%s' % ('%SYS%', trigwp, '%SYS%'))
                    branches.append('%s.effSF_Trig_%s_%s' % ('%SYS%', trigwpmc, '%SYS%'))

    # trigger matching
    if PostProcessing and Conf.triggerChain == 'SingleLepton':
        triggers = TriggerList.el_single
        for trig in triggers:
            branches.append('NOSYS.matched_%s' % trig)

    # truth
    if Conf.dataType != 'data' and Conf.doPileupRW:
        branches += [
            'NOSYS.truthType',
            'NOSYS.truthOrigin',
            'NOSYS.firstEgMotherPdgId',
            'NOSYS.firstEgMotherTruthType',
            'NOSYS.firstEgMotherTruthOrigin',
        ]

    # extra vars
    if Conf.extraVars:
        branches += [
            'NOSYS.EOverP',
            'NOSYS.d0sigPV',
            'NOSYS.d0PV',
        ]

    return formatBranchName(ContainerNames.electrons['output'], branches)
