#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from CharmAnalysis.Config import ContainerNames
from CharmAnalysis.Config import formatBranchName, formatBranchNameNoSYS, generateTriggerChain
from CharmAnalysis.Config import PRWConfigFiles, lumiCalcFiles, GRLFiles

#
# Event algorithms
#


def setupPileupAlgs(athAlgSeq, Conf):
    # Include, and then set up the pileup analysis sequence:
    from AsgAnalysisAlgorithms.PileupAnalysisSequence import \
        makePileupAnalysisSequence
    seq = makePileupAnalysisSequence(Conf.dataType,
                                     userLumicalcFiles=lumiCalcFiles(Conf.sampleType),
                                     userPileupConfigs=PRWConfigFiles(Conf.dataType, Conf.sampleType, Conf.derivationType))

    # configure the pileup RW tool
    if not Conf.doPileupRW:
        PURWTool = seq.PileupReweightingAlg.pileupReweightingTool
        PURWTool.ConfigFiles = []
        PURWTool.LumiCalcFiles = []
        PURWTool.UsePeriodConfig = 'Run2'
    else:
        seq.PileupReweightingAlg.correctedScaledAverageMuDecoration = 'correctedScaled_averageInteractionsPerCrossing'
        seq.PileupReweightingAlg.correctedActualMuDecoration = 'corrected_actualInteractionsPerCrossing'
        seq.PileupReweightingAlg.correctedScaledActualMuDecoration = 'correctedScaled_actualInteractionsPerCrossing'

    # Add the pileup sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def setupTriggerAlgs(athAlgSeq, Conf):
    # Include, and then set up the trigger analysis sequence:
    if generateTriggerChain(Conf.triggerChain):
        from TriggerAnalysisAlgorithms.TriggerAnalysisSequence import \
            makeTriggerAnalysisSequence
        seq = makeTriggerAnalysisSequence(Conf.dataType,
                                          triggerChains=generateTriggerChain(Conf.triggerChain))

        # Add the trigger sequence to the job:
        athAlgSeq += seq

        # Return to let the JO know this is active
        return seq


def setupEventAlgs(athAlgSeq, Conf):
    # Include, and then set up the event selection analysis sequence:
    from AsgAnalysisAlgorithms.EventSelectionAnalysisSequence import \
        makeEventSelectionAnalysisSequence
    seq = makeEventSelectionAnalysisSequence(Conf.dataType,
                                             userGRLFiles=GRLFiles(Conf.sampleType),
                                             runPrimaryVertexSelection=Conf.sampleType not in ['ForcedDecay'],
                                             runEventCleaning=Conf.eventCleaning)
    if Conf.sampleType not in ['ForcedDecay']:
        seq.PrimaryVertexSelectorAlg.MinTracks = 2

    # Add the event selection sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def getTruthOnlyBranches(Conf):
    # Event number
    branches = ['eventNumber']
    branches += ['runNumber']

    # Pileup info
    if Conf.doPileupRW:
        branches += [
            'PileupWeight_%SYS%',
            'generatorWeight_%SYS%',
            'prodFracWeight_%SYS%',
        ]

    # MCEventWeight
    if not Conf.systematics:
        branches += ['mcEventWeights']

    output = formatBranchNameNoSYS(ContainerNames.event['output'], branches)
    return output


def getEventBranches(Conf):
    branches = []
    # Event number
    branches += ['eventNumber']
    if Conf.dataType != 'data':
        branches += ['RandomRunNumber']
    else:
        branches += ['runNumber']

    # Pileup info
    if Conf.doPileupRW:
        if Conf.dataType != 'data':
            branches += ['PileupWeight_%SYS%']
        branches += [
            'correctedScaled_averageInteractionsPerCrossing',
            'correctedScaled_actualInteractionsPerCrossing',
        ]

    # MCEventWeight
    if Conf.dataType != 'data' and Conf.sampleType not in ['ForcedDecay']:
        if Conf.doPileupRW:
            branches += [
                'generatorWeight_%SYS%',
                'prodFracWeight_%SYS%',
            ]
        if not Conf.systematics:
            branches += ['mcEventWeights']

    # Trigger
    triggers = generateTriggerChain(Conf.triggerChain)
    for trig in triggers:
        branches.append('trigPassed_%s' % trig)

    # Event scale factors
    if Conf.dataType != 'data' and Conf.doJVT:
        # JVT/fJVT
        branches += [
            'jvt_effSF_%SYS%',
            # 'fjvt_effSF_%SYS%',
        ]

    # ttbar weights
    charmBranches = [
        'TopWeight',
    ]
    if int(Conf.sampleID) == 0:
        for i in [1, -1, 2, -2, 4, -4, 10, -10, 20, -20, 11, -11, 21, -21, 12, -12, 22, -22, 200]:
            varType = "1up"
            if i < 0:
                varType = "1down"
            charmBranches += ['TopWeight_var%s__%s' % (abs(i), varType)]

    # Lepton selection branches
    if Conf.leptonInfo:
        charmBranches += [
            'TIGHT_EL1_PT',
            'TIGHT_EL2_PT',
            'TIGHT_MU1_PT',
            'TIGHT_MU2_PT',
            'TIGHT_EL1_MATCHED',
            'TIGHT_EL2_MATCHED',
            'TIGHT_MU1_MATCHED',
            'TIGHT_MU2_MATCHED',

            'TIGHT_EL1_PHI',
            'TIGHT_EL1_ETA',
            'TIGHT_EL1_CHARGE',
            'TIGHT_EL2_PHI',
            'TIGHT_EL2_ETA',
            'TIGHT_EL2_CHARGE',

            'TIGHT_MU1_PHI',
            'TIGHT_MU1_ETA',
            'TIGHT_MU1_CHARGE',
            'TIGHT_MU2_PHI',
            'TIGHT_MU2_ETA',
            'TIGHT_MU2_CHARGE',

            'TIGHT_EL_WEIGHT',
            'TIGHT_MU_WEIGHT',
            'TIGHT_EL1_INDEX',
            'TIGHT_EL2_INDEX',
            'TIGHT_MU1_INDEX',
            'TIGHT_MU2_INDEX',
        ]

    # PV and beam position
    if Conf.doPVinfo:
        charmBranches += [
            'PV_X',
            'PV_Y',
            'PV_Z',
            'beamPosX',
            'beamPosY',
            'beamPosZ',
            'beamPosSigmaX',
            'beamPosSigmaY',
            'beamPosSigmaZ',
        ]

    output = formatBranchNameNoSYS(ContainerNames.event['output'], branches)
    if Conf.doCharmEventInfo:
        output += formatBranchNameNoSYS("CharmEventInfo", charmBranches)

    return output


def getTruthVerticesBranches():
    branches = [
        'TruthVertices.x -> TruthVertices_x',
        'TruthVertices.y -> TruthVertices_y',
        'TruthVertices.z -> TruthVertices_z',
    ]

    return branches
