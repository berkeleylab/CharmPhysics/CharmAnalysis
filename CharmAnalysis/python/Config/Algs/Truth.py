#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from AthenaCommon.SystemOfUnits import GeV

from CharmAnalysis.Config import ContainerNames
from CharmAnalysis.Config import formatBranchNameNoSYS

#
# Truth Algorithms config
#


def setupTruthAlgs(athAlgSeq, Conf):

    # TruthSelection sequence:
    from CharmAnalysis.TruthSelectionSequence import makeTruthSelectionAnalysisSequence
    seq = makeTruthSelectionAnalysisSequence(dataType=Conf.dataType)

    # Configure the sequence
    seq.configure(inputName={'truth': ContainerNames.truth['input']},
                  outputName={'truth': ContainerNames.truth['output'],
                              'truthLep': ContainerNames.truthLep})

    # Configuration
    seq.TruthSelectionAlg.MaxDEta = 2.6
    seq.TruthSelectionAlg.MinDPt = 5 * GeV
    seq.TruthSelectionAlg.MaxLepEta = 2.6
    seq.TruthSelectionAlg.MinLepPt = 20 * GeV

    # TRUTH derivation
    seq.TruthSelectionAlg.DressLeptons = Conf.dressLeptons
    seq.TruthSelectionAlg.DoTruthCharm = True
    seq.TruthSelectionAlg.DoTruthBottom = Conf.doTruthBottom
    seq.TruthSelectionAlg.DoTruthLeptons = Conf.doTruthLeptons
    seq.TruthSelectionAlg.TruthElectrons = ""
    seq.TruthSelectionAlg.TruthMuons = ""
    if Conf.sampleType == 'Truth':
        seq.TruthSelectionAlg.DressLeptons = True

    # Add all algorithms to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def setupTruthJetAlgs(athAlgSeq, Conf):

    # TruthJet sequence:
    from CharmAnalysis.TruthSelectionSequence import makeTruthJetAnalysisSequence
    seq = makeTruthJetAnalysisSequence("AntiKt4TruthJet")
    # Configure the sequence
    seq.configure(inputName={'jets': "AntiKt4TruthJets"},
                  outputName={'jets': "AnalysisAntiKt4TruthJets"})
    athAlgSeq += seq

    if Conf.derivationType == "STDM13" and Conf.newSTDM13:
        for jet in ["AntiKt%sTruthChargedJets" % r for r in ["4", "6", "8", "10"]]:
            seq = makeTruthJetAnalysisSequence(jet)
            # Configure the sequence
            seq.configure(inputName={'jets': jet},
                        outputName={'jets': "Analysis" + jet})
            athAlgSeq += seq


def getTruthBranches(Conf):
    branches = [
        'barcode',
        'cosThetaStarT',
        'daughterInfoT__barcode',
        'daughterInfoT__eta',
        'daughterInfoT__pdgId',
        'daughterInfoT__phi',
        'daughterInfoT__pt',
        'decayMode',
        'eta',
        'fromBdecay',
        'ImpactT',
        'LxyT',
        'm',
        'pdgId',
        'phi',
        'pt',
        'status',
        'vertexPosition',
    ]

    branches_lep = [
        'barcode',
        'classifierParticleOrigin',
        'classifierParticleType',
        'e',
        'eta',
        'm',
        'pdgId',
        'phi',
        'pt',
        'status',
    ]
    if Conf.dressLeptons:
        branches_lep += [
            'e_dressed',
            'eta_dressed',
            'phi_dressed',
            'pt_dressed',
        ]

    out = formatBranchNameNoSYS(ContainerNames.truth['output'], branches) + \
        formatBranchNameNoSYS(ContainerNames.truthLep, branches_lep)
    return out


def getTruthJets(Conf):
    branches = [
        'ConeTruthLabelID',
        'eta',
        'HadronConeExclTruthLabelID',
        'm',
        'phi',
        'pt',
    ]
    # Variables below don't work for track-jets (and are probably not needed anyway)
    # 'HadronConeExclExtendedTruthLabelID',
    # 'ghostB_pt',
    # 'ghostB_eta',
    # 'ghostB_phi',
    # 'ghostB_m',
    # 'ghostB_pdgId',
    # 'ghostC_pt',
    # 'ghostC_eta',
    # 'ghostC_phi',
    # 'ghostC_m',
    # 'ghostC_pdgId',

    out = formatBranchNameNoSYS("AntiKt4TruthJets", branches)
    if Conf.derivationType == "STDM13" and Conf.newSTDM13:
        for jet in ["AntiKt%sTruthChargedJets" % r for r in ["4", "6", "8", "10"]]:
            out += formatBranchNameNoSYS(jet, branches)
    return out
