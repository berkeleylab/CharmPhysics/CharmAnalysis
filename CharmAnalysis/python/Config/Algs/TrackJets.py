#
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AthenaCommon import Logging
from CharmAnalysis.Config import ContainerNames, WorkingPoints

#
# Track-jet clustering config
#
# copied from JetCommon.py to avoid nested imports that
# don't work outside the DerivationFramework
#

dfjetlog = Logging.logging.getLogger('JetCommon')
DFJetAlgs = {}


def addStandardJets(jetalg, rsize, inputtype, ptmin=0., ptminFilter=0.,
                    mods="default", calibOpt="none", ghostArea=0.01,
                    algseq=None, namesuffix="",
                    outputGroup="CustomJets", customGetters=None, pretools=[], constmods=[],
                    overwrite=False, syssuffix=""):
    if namesuffix:
        jetnamebase = "{0}{1}{2}{3}".format(jetalg, int(rsize*10), inputtype, namesuffix)
        jetname = jetnamebase + "Jets"
        algname = "jetalg"+jetnamebase
    elif syssuffix:
        jetnamebase = "{0}{1}{2}Jets_{3}".format(jetalg, int(rsize*10), inputtype, syssuffix)
        algname = "jetalg"+jetnamebase
        jetname = jetnamebase

    # return if the alg is already scheduled here :
    # from RecExConfig.AutoConfiguration import IsInInputFile
    if algseq is None:
        dfjetlog.warning("No algsequence passed! Will not schedule "+algname)
        return
    elif algname in DFJetAlgs:
        if hasattr(algseq, algname):
            dfjetlog.warning("Algsequence "+algseq.name() +
                             " already has an instance of "+algname)
        else:
            dfjetlog.info("Added "+algname+" to sequence "+algseq.name())
            algseq += DFJetAlgs[algname]
        return DFJetAlgs[algname]

    from JetRec.JetRecStandard import jtm

    if not jetname in jtm.tools:
        # no container exist. simply build a new one.
        # Set default for the arguments to be passed to addJetFinder
        defaultmods = {"EMTopo": "emtopo_ungroomed",
                       "LCTopo": "lctopo_ungroomed",
                       "EMPFlow": "pflow_ungroomed",
                       "EMCPFlow": "pflow_ungroomed",
                       "PFlowCustomVtx": "pflow_ungroomed",
                       "Truth": "truth_ungroomed",
                       "TruthWZ": "truth_ungroomed",
                       "PV0Track": "track_ungroomed",
                       "TrackCaloCluster": "tcc_ungroomed",
                       "UFOCSSK": "tcc_ungroomed",
                       "UFOCHS": "tcc_ungroomed",
                       }
        if mods == "default":
            mods = defaultmods[inputtype] if inputtype in defaultmods else []
        finderArgs = dict(modifiersin=mods, consumers=[])
        finderArgs['ptmin'] = ptmin
        finderArgs['ptminFilter'] = ptminFilter
        finderArgs['ghostArea'] = ghostArea
        finderArgs['modifiersin'] = mods
        finderArgs['calibOpt'] = calibOpt
        dfjetlog.info("mods in:" + str(finderArgs['modifiersin']))
        if overwrite:
            dfjetlog.info("Will overwrite AOD version of "+jetname)
            finderArgs['overwrite'] = True

        # map the input to the jtm code for PseudoJetGetter
        getterMap = dict(LCTopo='lctopo', EMTopo='emtopo', EMPFlow='empflow', EMCPFlow='emcpflow',
                         Truth='truth',  TruthWZ='truthwz', TruthDressedWZ='truthdressedwz', TruthCharged='truthcharged',
                         PV0Track='pv0track', TrackCaloCluster='tcc', UFOCSSK='ufocssk', UFOCHS='ufochs')

        # set input pseudojet getter -- allows for custom getters
        if customGetters is None:
            inGetter = getterMap[inputtype]
        else:
            inGetter = customGetters

        # add the jet finder and set the input
        finderTool = jtm.addJetFinder(jetname, jetalg, rsize, inGetter, constmods=constmods, **finderArgs)
        finderTool.PseudoJetGetters[0] = jtm.tools["trackget_" + syssuffix]

        from JetRec.JetRecConf import JetAlgorithm
        alg = JetAlgorithm(algname, Tools=pretools+[finderTool])
        dfjetlog.info("Added "+algname+" to sequence "+algseq.name())
        algseq += alg
        DFJetAlgs[algname] = alg

# build all track-jets
def setupTrackJets(athAlgSeq, Conf):

    # TJ
    from JetRec.JetRecFlags import jetFlags
    jetFlags.useBTagging.set_Value_and_Lock(False)
    jetFlags.useCaloQualityTool.set_Value_and_Lock(False)
    jetFlags.usePFlow.set_Value_and_Lock(False)
    jetFlags.useTopo.set_Value_and_Lock(False)
    from JetRec.JetAlgorithm import addJetRecoToAlgSequence

    # Systematics
    doTrackSyst = Conf.dataType == 'mc' and Conf.systematics and Conf.doTrackJetSystematics

    # Disable truh for data
    if Conf.dataType is 'data':
        jetFlags.useTruth.set_Value_and_Lock(False)

    # Modified locally to disable cluster origin calculation
    addJetRecoToAlgSequence(athAlgSeq, clusterOrigin=False, doTrackSyst=doTrackSyst)

    # Set the working point
    WP = WorkingPoints(Conf)

    trackSysts = ["NOSYS"]
    if doTrackSyst:
        trackSysts += ["TRK_RES_D0_DEAD",
                       "TRK_RES_D0_MEAS",
                       "TRK_RES_Z0_DEAD",
                       "TRK_RES_Z0_MEAS",
                       "TRK_BIAS_D0_WM",
                       "TRK_BIAS_Z0_WM",
                       "TRK_BIAS_QOVERP_SAGITTA_WM",
                       "TRK_FAKE_RATE_LOOSE",
                       "TRK_EFF_LOOSE_GLOBAL",
                       "TRK_EFF_LOOSE_IBL",
                       "TRK_EFF_LOOSE_PP0",
                       "TRK_EFF_LOOSE_PHYSMODEL"]

    # Create track-jet collections
    for suffix in trackSysts:
        for r in WP.track_jet_radius:
            addStandardJets("AntiKt", r, "PV0Track", ptmin=5000, algseq=athAlgSeq, ghostArea=0.0, syssuffix=suffix)

    # track systematics are not considered for now.
    if Conf.doVRTrackJets:
        for suffix in trackSysts:
            buildVRJets(athAlgSeq, Conf, do_ghost=False, syssuffix=suffix)

def setupTrackJetAlgs(athAlgSeq, Conf, jetType):

    # Track-jet sequence:

    seq = makeTrackJetAnalysisSequence(Conf, jetType=jetType)

    athAlgSeq += seq

    # Track container name
    tracks = ContainerNames.tracks['output'] + '_%SYS%'
    if Conf.tracksFromTruth:
        tracks = ContainerNames.TruthTrackParticles

    # TrackJet container name
    jets = jetType + 'Jets_%SYS%'
    seq.configure(inputName={'jets': jets,
                             'tracks': tracks},
                        outputName={})

    return seq

def makeTrackJetAnalysisSequence(Conf, jetType):

    if Conf.dataType not in ['data', 'mc', 'afii']:
        raise ValueError("invalid data type: " + Conf.dataType)

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence( jetType + 'AnalysisSequence')

    # Track-jet decorator algorithm:
    alg = createAlgorithm('TrackJetDecoratorAlg',
                          jetType + 'DecoratorAlg')

    alg.TrackSelectionTool.CutLevel = "Loose"
    alg.JetType = jetType
    alg.DoSyst = Conf.systematics and Conf.doTrackJetSystematics

    seq.append(alg, inputPropName={'jets': 'jets',
                                   'tracks': 'tracks'})


    return seq

# modified from DerivationFrameworkFlavourTag/HbbCommon.py
def buildVRJets(sequence, Conf, do_ghost, logger = None, doFlipTagger=False, training='201903',syssuffix=''):

    from AthenaCommon import Logging

    if logger is None:
        logger = Logging.logging.getLogger('VRLogger')

    supported_trainings = ['201810', '201903']
    # Check allowed trainings
    # Is there a better way to do this with a central DB?
    if training not in ['201810', '201903']:
      logger.warning("WARNING: Using an unsupported training tag! This is UNDEFINED and will probably break. Please choose a training tag from")
      logger.warning(supported_trainings)

    from JetRec.JetRecStandard import jtm

    # Making Chris Happy: all VR track-jet b-tagging should have the training campaign label
    trainingTag = '_BTagging%s' % (training)
    VRJetName="AntiKtVR30Rmax4Rmin02Track%s" % (trainingTag)

    VRGhostLabel="GhostVR30Rmax4Rmin02TrackJet%s" % (trainingTag)
    VRJetAlg="AntiKt"
    VRJetRadius=0.4
    VRJetInputs='pv0track'
    VRJetOptions = dict(
        ghostArea = 0 , ptmin = 4000,
        variableRMinRadius = 0.02, variableRMassScale = 30000,
        calibOpt = "none")

    # Change some options if we have do_ghost set to true. Hopefully
    # this will be the only VR collection in the future.
    if do_ghost:
        ghost_suffix = "GhostTag"
        VRJetName += ghost_suffix
        VRGhostLabel += ghost_suffix

    #==========================================================
    # Build VR jets
    #==========================================================

    # from DerivationFrameworkJetEtMiss.ExtendedJetCommon import nameJetsFromAlg
    # VRJetRecToolName = nameJetsFromAlg(VRJetName)
    VRJetRecToolName = VRJetName

    VRJetAlgName = "jfind_%s" % (VRJetRecToolName)
    VRJetBTagName = "BTagging_%s" % (VRJetName.replace('BTagging',''))

    VRJetRecToolName = "AntiKtVR30Rmax4Rmin02TrackJets"
    if syssuffix:
        VRJetRecToolName += "_{}".format(syssuffix)
        VRJetAlgName += "_{}".format(syssuffix)

    logger.info("VR Btag name: %s" % VRJetBTagName)
    logger.info("VR jet name: %s" % VRJetRecToolName)
    from AthenaCommon.AppMgr import ToolSvc

    # add delta-R to nearest jet
    from FlavorTagDiscriminants.FlavorTagDiscriminantsLibConf import (
        FlavorTagDiscriminants__VRJetOverlapDecoratorTool as VRORTool )
    vrdr_label = VRORTool(name=VRJetRecToolName + "_VRLabeling")
    ToolSvc += vrdr_label

    # add Ghost label id
    from ParticleJetTools.ParticleJetToolsConf import (
        ParticleJetGhostLabelTool as GhostLabelTool)
    gl_tool = GhostLabelTool(
        name=VRJetRecToolName + "_GhostLabeling")
    ToolSvc += gl_tool


    # Slice the array - this forces a copy so that if we modify it we don't also
    # change the array in jtm.
    pseudoJetGetters = jtm.gettersMap[VRJetInputs][:]
    # We want to include ghost associated tracks in the pv0 tracks so that
    # we can use the looser ghost association criteria for b-tagging.
    if VRJetInputs == "pv0track":
        pseudoJetGetters.append(jtm["gtrackget"])

    if VRJetAlgName in DFJetAlgs:
        logger.info("Algorithm %s already built before" % VRJetAlgName)

        if hasattr(sequence, VRJetAlgName):
            logger.info("Sequence %s already has an instance of algorithm %s" % (sequence, VRJetAlgName))
        else:
            logger.info("Add algorithm %s to sequence %s" % (VRJetAlgName, sequence))
            sequence += DFJetAlgs[VRJetAlgName]
    else:
        logger.info("Create algorithm %s" % VRJetAlgName)

        if hasattr(jtm, VRJetRecToolName):
            logger.info("JetRecTool %s is alredy in jtm.tools in sequence %s" % (VRJetRecToolName, sequence))
        else:
            logger.info("Create JetRecTool %s" % VRJetRecToolName)

            mods = []

            if not Conf.dataType is 'data':
                mods += [jtm.trackjetdrlabeler, gl_tool]

            finderTool = jtm.addJetFinder(VRJetRecToolName,
                                          VRJetAlg,
                                          VRJetRadius,
                                          pseudoJetGetters,
                                          modifiersin=mods,
                                          ivtxin=0,
                                          **VRJetOptions)
            finderTool.PseudoJetGetters[0] = jtm.tools["trackget_" + syssuffix]

        from JetRec.JetRecConf import JetAlgorithm
        jetalg_smallvr30_track = JetAlgorithm(VRJetAlgName, Tools = [ jtm[VRJetRecToolName] ])
        sequence += jetalg_smallvr30_track
        DFJetAlgs[VRJetAlgName] = jetalg_smallvr30_track

