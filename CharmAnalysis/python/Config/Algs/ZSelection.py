#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from CharmAnalysis.Config import ContainerNames
from CharmAnalysis.Config import formatBranchNameNoSYS

#
# ZSelection Algorithms config
#


def setupZSelectionAlgs(athAlgSeq, Conf):
    # ZSelection analysis algorithm sequence
    from CharmAnalysis.ZSelectionSequence import makeZSelectionSequence
    seq = makeZSelectionSequence(dataType=Conf.dataType)
    seq.configure(
        inputName={'electrons': ContainerNames.electrons['output'] + "_NOSYS",
                   'muons': ContainerNames.muons['output'] + "_NOSYS"},
        outputName=ContainerNames.ZSelection['output'])

    # Add all algorithms to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def getZSelectionBranches():
    branches = [
        'pt',
        'charge',
        'phi',
        'm',
        'pdgId',
        'decayType',
    ]

    return formatBranchNameNoSYS(ContainerNames.ZSelection['output'], branches)
