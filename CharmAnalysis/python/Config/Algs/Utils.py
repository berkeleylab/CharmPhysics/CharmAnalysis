#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.DualUseConfig import createAlgorithm
from CharmAnalysis.Config import ContainerNames


def createFakePrimaryVertex(athAlgSeq, Conf):
    # Create a fake primary vertex container
    if Conf.fakePrimaryVertex:
        alg = createAlgorithm('FakePriVtxAlg', 'FakePriVtxAlg')
        alg.VxCandidatesOutputName = ContainerNames.FakePrimaryVertex
        alg.vtxSigmax = 0.0076
        alg.vtxSigmay = 0.0076
        alg.vtxSigmaz = 0.059
        athAlgSeq += alg


def createTracksFromTruth(athAlgSeq, Conf):
    if Conf.tracksFromTruth:
        alg = createAlgorithm('TruthConverterAlg', 'TruthConverterAlg')
        alg.TruthParticles = ContainerNames.truth['input']
        alg.TrackParticles = ContainerNames.TruthTrackParticles
        athAlgSeq += alg
