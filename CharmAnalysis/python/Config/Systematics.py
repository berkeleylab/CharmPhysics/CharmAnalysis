#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, createService


def setupSystematics(athAlgSeq, Conf):

    # Set up the systematics loader/handler service
    algSequence = AlgSequence()
    sysService = createService('CP::SystematicsSvc', 'SystematicsSvc', sequence=algSequence)
    sysService.systematicsRegex = '(^$)'
    if Conf.systematics:
        sysService.sigmaRecommended = 1
    else:
        sysService.sigmaRecommended = 0

    # add to main sequence
    athAlgSeq += algSequence

    if Conf.systematics and Conf.dataType == 'mc':

        #
        # Weight based systematics
        #

        # PRW systematics
        sysService.systematicsRegex += '|(^PRW_.*)'
        alg = createAlgorithm('CP::SysListDumperAlg', 'SysListDumperAlgPRW')
        alg.systematicsRegex = '(^PRW_.*)'
        alg.histogramName = 'systematics_PRW'
        athAlgSeq += alg

        # Theory systematics
        sysService.systematicsRegex += '|(^GEN_.*)'
        alg = createAlgorithm('CP::SysListDumperAlg', 'SysListDumperAlgGEN')
        alg.systematicsRegex = '(^GEN_.*)'
        alg.histogramName = 'systematics_GEN'
        athAlgSeq += alg

        # JVT systematics
        sysService.systematicsRegex += '|(^JET_Jvt.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgJET_Jvt')
        alg.systematicsRegex = '(^JET_Jvt.*)'
        alg.histogramName = 'systematics_JET_Jvt'
        athAlgSeq += alg

        # fJVT systematics
        sysService.systematicsRegex += '|(^JET_fJvt.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgJET_fJvt')
        alg.systematicsRegex = '(^JET_fJvt.*)'
        alg.histogramName = 'systematics_JET_fJvt'
        athAlgSeq += alg

        # FTAG systematics
        sysService.systematicsRegex += '|(^FT_EFF_.*)'
        alg = createAlgorithm('CP::SysListDumperAlg', 'SysListDumperAlgFT_EFF')
        alg.systematicsRegex = '(^FT_EFF_.*)'
        alg.histogramName = 'systematics_FT_EFF'
        athAlgSeq += alg

        # Electron Reco systematics
        sysService.systematicsRegex += '|(^EL_EFF_Reco.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgEL_EFF_Reco')
        alg.systematicsRegex = '(^EL_EFF_Reco.*)'
        alg.histogramName = 'systematics_EL_EFF_Reco'
        athAlgSeq += alg

        # Electron ID systematics
        sysService.systematicsRegex += '|(^EL_EFF_ID.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgEL_EFF_ID')
        alg.systematicsRegex = '(^EL_EFF_ID.*)'
        alg.histogramName = 'systematics_EL_EFF_ID'
        athAlgSeq += alg

        # Electron Iso systematics
        sysService.systematicsRegex += '|(^EL_EFF_Iso.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgEL_EFF_Iso')
        alg.systematicsRegex = '(^EL_EFF_Iso.*)'
        alg.histogramName = 'systematics_EL_EFF_Iso'
        athAlgSeq += alg

        # Electron Trigger systematics
        sysService.systematicsRegex += '|(^EL_EFF_Trigger_.*)|(^EL_EFF_TriggerEff_.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgEL_EFF_Trigger')
        alg.systematicsRegex = '(^EL_EFF_Trigger_.*)|(^EL_EFF_TriggerEff_.*)'
        alg.histogramName = 'systematics_EL_EFF_Trigger'
        athAlgSeq += alg

        # Electron Charge-Flip systematics
        sysService.systematicsRegex += '|(^EL_CHARGE.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgEL_CHARGE')
        alg.systematicsRegex = '(^EL_CHARGE.*)'
        alg.histogramName = 'systematics_EL_CHARGE'
        athAlgSeq += alg

        # Muon Reco systematics
        sysService.systematicsRegex += '|(^MUON_EFF_RECO.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgMUON_EFF_RECO')
        alg.systematicsRegex = '(^MUON_EFF_RECO.*)'
        alg.histogramName = 'systematics_MUON_EFF_RECO'
        athAlgSeq += alg

        # Muon TTVA systematics
        sysService.systematicsRegex += '|(^MUON_EFF_TTVA.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgMUON_EFF_TTVA')
        alg.systematicsRegex = '(^MUON_EFF_TTVA.*)'
        alg.histogramName = 'systematics_MUON_EFF_TTVA'
        athAlgSeq += alg

        # Muon Iso systematics
        sysService.systematicsRegex += '|(^MUON_EFF_ISO.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgMUON_EFF_ISO')
        alg.systematicsRegex = '(^MUON_EFF_ISO.*)'
        alg.histogramName = 'systematics_MUON_EFF_ISO'
        athAlgSeq += alg

        # Muon Trigger systematics
        sysService.systematicsRegex += '|(^MUON_EFF_Trig.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgMUON_EFF_Trig')
        alg.systematicsRegex = '(^MUON_EFF_Trig.*)'
        alg.histogramName = 'systematics_MUON_EFF_Trig'
        athAlgSeq += alg

        # Production Fraction systematics
        sysService.systematicsRegex += '|(^PROD_FRAC_CHARM.*)|(^PROD_FRAC_BOTTOM.*)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgPROD_FRAC')
        alg.systematicsRegex = '(^PROD_FRAC_CHARM_.*)|(^PROD_FRAC_BOTTOM.*)'
        alg.histogramName = 'systematics_PROD_FRAC'
        athAlgSeq += alg

        #
        # Calibration based systematics
        #

        # Jet Calibration systematics
        jet_systematics = "|".join([
            "(^JET_BJES_Response$)",
            "(^JET_EtaIntercalibration_.*)",
            "(^JET_Flavor_.*)",
            "(^JET_Gjet_.*)",
            "(^JET_JER_.*)",
            "(^JET_MJB_.*)",
            "(^JET_Pileup_.*)",
            "(^JET_PunchThrough_.*)",
            "(^JET_RelativeNonClosure_.*)",
            "(^JET_SingleParticle_HighPt$)",
            "(^JET_Zjet_.*)",
            "(^JET_EffectiveNP_.*)",
            "(^JET_GroupedNP_.*)"])
        sysService.systematicsRegex += '|%s' % jet_systematics
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgJET_Calib')
        alg.systematicsRegex = jet_systematics
        alg.histogramName = 'systematics_JET_Calib'
        athAlgSeq += alg

        # MET Calibration systematics
        sysService.systematicsRegex += '|(^MET_.*$)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgMET_Calib')
        alg.systematicsRegex = '(^MET_.*$)'
        alg.histogramName = 'systematics_MET_Calib'
        athAlgSeq += alg

        # Electron Calibration systematics
        sysService.systematicsRegex += '|(^EG_RESOLUTION.*$)|(^EG_SCALE.*$)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgEL_Calib')
        alg.systematicsRegex = '(^EG_RESOLUTION.*$)|(^EG_SCALE.*$)'
        alg.histogramName = 'systematics_EL_Calib'
        athAlgSeq += alg

        # Muon Calibration systematics
        sysService.systematicsRegex += '|(^MUON_ID.*$)|(^MUON_MS.*$)|(^MUON_CB.*$)|(^MUON_SAGITTA.*)|(^MUON_SCALE.*$)'
        alg = createAlgorithm('CP::SysListDumperAlg',
                              'SysListDumperAlgMUON_Calib')
        alg.systematicsRegex = '(^MUON_ID.*$)|(^MUON_MS.*$)|(^MUON_CB.*$)|(^MUON_SAGITTA.*)|(^MUON_SCALE.*$)'
        alg.histogramName = 'systematics_MUON_Calib'
        athAlgSeq += alg

        # Track systematics
        if Conf.doTrackJetSystematics:
            sysService.systematicsRegex += '|(^TRK_RES.*$)|(^TRK_BIAS.*$)|(^TRK_EFF_LOOSE.*$)|(^TRK_FAKE_RATE_LOOSE$)'
            alg = createAlgorithm('CP::SysListDumperAlg',
                                'SysListDumperAlgTRACK_Calib')
            alg.systematicsRegex = '(TRK_RES.*$)|(^TRK_BIAS.*$)|(^TRK_EFF_LOOSE.*$)|(^TRK_FAKE_RATE_LOOSE$)'
            alg.histogramName = 'systematics_TRACK_Calib'
            athAlgSeq += alg
