# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration


def setupV0FinderTools(ToolSvc, TrackParticleCollection='InDetTrackParticles'):
    InDetV0Fitter = None

    # AtlasTrackSummaryTool
    from TrkTrackSummaryTool.AtlasTrackSummaryTool import AtlasTrackSummaryTool
    AtlasTrackSummaryTool = AtlasTrackSummaryTool()
    ToolSvc += AtlasTrackSummaryTool

    from TrkVKalVrtFitter.TrkVKalVrtFitterConf import Trk__TrkVKalVrtFitter
    InDetVKVertexFitter = Trk__TrkVKalVrtFitter(name="InDetVKVFitter",
                                                Extrapolator="Trk::Extrapolator/InDetExtrapolator",
                                                IterationNumber=30,
                                                MakeExtendedVertex=True,
                                                FirstMeasuredPoint=False)  # True)
    ToolSvc += InDetVKVertexFitter

    from TrkVKalVrtFitter.TrkVKalVrtFitterConf import Trk__TrkVKalVrtFitter
    InDetKshortFitter = Trk__TrkVKalVrtFitter(name="InDetVKKVFitter",
                                              Extrapolator="Trk::Extrapolator/InDetExtrapolator",
                                              IterationNumber=30,
                                              MakeExtendedVertex=True,
                                              FirstMeasuredPoint=False,  # True,
                                              InputParticleMasses=[139.57, 139.57],
                                              MassForConstraint=497.672)
    ToolSvc += InDetKshortFitter

    from TrkVKalVrtFitter.TrkVKalVrtFitterConf import Trk__TrkVKalVrtFitter
    InDetLambdaFitter = Trk__TrkVKalVrtFitter(name="InDetVKLFitter",
                                              Extrapolator="Trk::Extrapolator/InDetExtrapolator",
                                              IterationNumber=30,
                                              MakeExtendedVertex=True,
                                              FirstMeasuredPoint=False,  # True,
                                              InputParticleMasses=[938.272, 139.57],
                                              MassForConstraint=1115.68)
    ToolSvc += InDetLambdaFitter

    from TrkVKalVrtFitter.TrkVKalVrtFitterConf import Trk__TrkVKalVrtFitter
    InDetLambdabarFitter = Trk__TrkVKalVrtFitter(name="InDetVKLbFitter",
                                                 Extrapolator="Trk::Extrapolator/InDetExtrapolator",
                                                 IterationNumber=30,
                                                 MakeExtendedVertex=True,
                                                 FirstMeasuredPoint=False,  # True,
                                                 InputParticleMasses=[139.57, 938.272],
                                                 MassForConstraint=1115.68)
    ToolSvc += InDetLambdabarFitter

    from TrkVKalVrtFitter.TrkVKalVrtFitterConf import Trk__TrkVKalVrtFitter
    InDetGammaFitter = Trk__TrkVKalVrtFitter(name="InDetVKGFitter",
                                             Extrapolator="Trk::Extrapolator/InDetExtrapolator",
                                             IterationNumber=30,
                                             Robustness=6,
                                             MakeExtendedVertex=True,
                                             FirstMeasuredPoint=False,  # True,
                                             usePhiCnst=True,
                                             useThetaCnst=True,
                                             InputParticleMasses=[0.511, 0.511])
    ToolSvc += InDetGammaFitter

    # Track selector tool
    #
    from InDetTrackSelectorTool.InDetTrackSelectorToolConf import InDet__InDetConversionTrackSelectorTool
    InDetV0VxTrackSelector = InDet__InDetConversionTrackSelectorTool(name="InDetV0VxTrackSelector",
                                                                     #TrackSummaryTool = InDetTrackSummaryTool,
                                                                     TrackSummaryTool=AtlasTrackSummaryTool,
                                                                     Extrapolator="Trk::Extrapolator/InDetExtrapolator",
                                                                     maxTrtD0=50.,
                                                                     maxSiZ0=250.,
                                                                     significanceD0_Si=1.,
                                                                     significanceD0_Trt=1.,
                                                                     significanceZ0_Trt=3.,
                                                                     minPt=400.0,
                                                                     IsConversion=False)
    ToolSvc += InDetV0VxTrackSelector

    # Vertex point estimator
    #
    from InDetConversionFinderTools.InDetConversionFinderToolsConf import InDet__VertexPointEstimator
    InDetV0VtxPointEstimator = InDet__VertexPointEstimator(name="InDetV0VtxPointEstimator",
                                                           MaxTrkXYDiffAtVtx=[20., 20., 20.],
                                                           MaxTrkZDiffAtVtx=[100., 100., 100.],
                                                           MaxTrkXYValue=[400., 400., 400.],
                                                           MinArcLength=[-800., -800., -800.],
                                                           MaxArcLength=[800., 800., 800.],
                                                           MinDeltaR=[-10000., -10000., -10000.],
                                                           MaxDeltaR=[10000., 10000., 10000.],
                                                           MaxPhi=[10000., 10000., 10000.],
                                                           MaxChi2OfVtxEstimation=2000.)
    ToolSvc += InDetV0VtxPointEstimator

    #
    # InDetV0FinderTool
    #
    from InDetV0Finder.InDetV0FinderConf import InDet__InDetV0FinderTool
    V0FinderTool = InDet__InDetV0FinderTool(name='InDetV0FinderTool',
                                            TrackParticleCollection=TrackParticleCollection,
                                            useV0Fitter=False,
                                            #useV0Fitter = InDetFlags.useV0Fitter(),
                                            VertexFitterTool=InDetV0Fitter,
                                            VKVertexFitterTool=InDetVKVertexFitter,
                                            KshortFitterTool=InDetKshortFitter,
                                            LambdaFitterTool=InDetLambdaFitter,
                                            LambdabarFitterTool=InDetLambdabarFitter,
                                            GammaFitterTool=InDetGammaFitter,
                                            TrackSelectorTool=InDetV0VxTrackSelector,
                                            VertexPointEstimator=InDetV0VtxPointEstimator,
                                            doSimpleV0=False,
                                            useorigin=False,
                                            #useTRTplusTRT = True,
                                            #useTRTplusSi = True,
                                            useVertexCollection=False,
                                            trkSelPV=True,
##                                            d0_cut=0,
                                            Extrapolator="Trk::Extrapolator/InDetExtrapolator")
    ToolSvc += V0FinderTool
