# CharmAnalysis

[![pipeline status](https://gitlab.cern.ch/berkeleylab/CharmPhysics/CharmAnalysis/badges/master/pipeline.svg)](https://gitlab.cern.ch/berkeleylab/CharmPhysics/CharmAnalysis/commits/master)


Analysis code for LBNL Charm project

## Contact Details

| Contact Person | Contact E-mail        |
|----------------|-----------------------|
| Miha Muskinja  | miha.muskinja@cern.ch |


### CMake configuration

The repository comes with its own project configuration file
([CMakeLists.txt](CMakeLists.txt)). It sets up the build of all of the
code of the repository against AthDerivation 21.2.163.0.

To build the release project please follow the instructions below.

```
mkdir Charm; cd Charm
lsetup git
git clone ssh://git@gitlab.cern.ch:7999/berkeleylab/CharmPhysics/CharmAnalysis.git --recursive
mkdir build run
cd build/
asetup 21.2,AthDerivation,21.2.163.0
cmake ../CharmAnalysis/
make -j[number of cores] (something like 'make -j8' is usually much faster than just 'make')
source x86_64[...]/setup.sh (folder name depends on the platform)
cd ../
```

### Copy data files

Binary files are not included in the repository and have to be copied to the data directory manually.
Run the following script to copy data files:

```
fetch_data_files
```

The script requires login to CERN network with 'kinit <user>@CERN.CH'.

### Example STDM13 derivation

An example FTAG4 DAOD is available on perlmutter at:

```
/global/cfs/cdirs/atlas/wcharm/DAOD/FTAG4/mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.deriv.DAOD_FTAG4.e3601_s3126_r9364_p3970/DAOD_FTAG4.19348350._000074.pool.root.1
```
or on lxplus at:
```
/eos/user/m/mmuskinj/share/data/CharmAnalysis/FTAG4/mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.deriv.DAOD_FTAG4.e3601_s3126_r9364_p3970/DAOD_FTAG4.19348350._000074.pool.root.1
```

### Running a job

To run a test job, execute the following from the run folder:

```
athena CharmAnalysis/AthTestRun_jobOptions.py \
       --evtMax 100 \
       -c "sampleType='MC16a'" \
       --filesInput ${inputFile}
```

### CI Configuration

Nothing here yet.

### Docker Configuration

Nothing here yet.

